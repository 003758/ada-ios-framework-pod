//
//  AppData+CoreDataProperties.swift
//  
//
//  Created by Choldarong-r on 24/10/2561 BE.
//
//  This file was automatically generated and should not be edited.
//

import Foundation
import CoreData


extension AppData {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<AppData> {
        return NSFetchRequest<AppData>(entityName: "AppData")
    }

    @NSManaged public var data: NSData?
    @NSManaged public var key: String?

}
