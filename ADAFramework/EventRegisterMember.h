//
//  EventRegisterMember.h
//  ADAFramework
//
//  Created by Choldarong-r on 29/3/2561 BE.
//  Copyright © 2561 G-ABLE Company Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EventReviewTopic.h"

@interface EventRegisterMember : NSObject

@property (nonatomic, assign) NSInteger memberID;
@property (nonatomic, retain) NSString *seat;
@property (nonatomic, assign) BOOL doReview;
@property (nonatomic, retain) NSArray<EventReviewTopic*> *reviewTopic;

@end
