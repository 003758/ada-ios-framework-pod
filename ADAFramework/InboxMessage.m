//
//  InboxMessage.m
//  ADAFramework
//
//  Created by Choldarong-r on 18/7/2560 BE.
//  Copyright © 2560 G-ABLE ITS. All rights reserved.
//

#import "InboxMessage.h"

@implementation InboxMessage

- (id)copyWithZone:(NSZone *)zone {
    InboxMessage *obj = [InboxMessage new];
    obj.messageID = self.messageID;
    obj.title = self.title;
    obj.message = self.message;
    obj.author = self.author;
    obj.date = self.date;
    obj.image = self.image;
    obj.pdf = self.pdf;
    return obj;
}

@end
