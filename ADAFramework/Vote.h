//
//  Vote.h
//  ADAFramework
//
//  Created by Choldarong-r on 9/8/2560 BE.
//  Copyright © 2560 G-ABLE ITS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VoteChoice.h"
#import "VoteAnswerMember.h"
#import "ADAMember.h"

@interface Vote : NSObject

typedef NS_OPTIONS(NSInteger, VoteState) {
    VoteStateSubmited = 1 << 3,
    VoteStateNotAnswered = 1 << 2,
    VoteStateExpired = 1 << 1,
    VoteStateActive = 1 << 0
};


typedef NS_ENUM(NSInteger, VoteType) {
//typedef enum VoteType : NSUInteger {
    VoteNormal      = 1,
    VoteSequence     = 2,
    VotePrivate      = 3
    
};


@property (nonatomic, assign) long voteID;
@property (nonatomic, assign) int seq;
@property (nonatomic, assign) VoteType voteType;
@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *detail;
@property (nonatomic, retain) NSString *imageID;
@property (nonatomic, retain) NSDate *createDate;
@property (nonatomic, retain) NSDate *startDate;
@property (nonatomic, retain) NSDate *endDate;
@property (nonatomic, assign) BOOL expired;
@property (nonatomic, assign) BOOL answered;
@property (nonatomic, retain) ADAMember *creator;
@property (nonatomic, retain) NSArray<VoteChoice*> *choices;

@property (nonatomic, retain) NSArray<VoteAnswerMember*> *answeredMembers;
@property (nonatomic, retain) NSArray<ADAMember*> *members;
@end
