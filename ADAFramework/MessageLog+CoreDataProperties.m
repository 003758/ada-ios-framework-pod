//
//  MessageLog+CoreDataProperties.m
//  ADAFramework
//
//  Created by Choldarong-r on 14/12/2560 BE.
//  Copyright © 2560 G-ABLE Company Limited. All rights reserved.
//
//

#import "MessageLog+CoreDataProperties.h"

@implementation MessageLog (CoreDataProperties)

+ (NSFetchRequest<MessageLog *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"MessageLog"];
}

@dynamic message;
@dynamic msgDate;
@dynamic msgID;
@dynamic readStatus;
@dynamic roomID;
@dynamic seq;
@dynamic user;

@end
