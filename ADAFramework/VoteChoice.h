//
//  VoteChoice.h
//  ADAFramework
//
//  Created by Choldarong-r on 23/8/2560 BE.
//  Copyright © 2560 G-ABLE ITS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VoteChoice : NSObject


@property (nonatomic, assign) long choiceID;
@property (nonatomic, assign) long seq;
@property (nonatomic, assign) long ansSeq;
@property (nonatomic, retain) NSString *detail;
@end
