//
//  ADAAppDelegate.h
//  ADAFramework
//
//  Created by Choldarong-r on 17/7/2560 BE.
//  Copyright © 2560 G-ABLE ITS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ADA.h"

@import UserNotifications;

@interface ADAAppDelegate : UIResponder <UIApplicationDelegate, UNUserNotificationCenterDelegate>

@property (strong, nonatomic)   UIWindow *window;
@property (strong, nonatomic)   ADA *ada;


@end
