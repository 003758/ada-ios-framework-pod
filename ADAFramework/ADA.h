//
//  ADA.h
//  ADAFramework
//
//  Created by Choldarong-r on 2/12/2559 BE.
//  Copyright © 2559 G-ABLE ITS. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "ContactPerson.h"
#import "ContactColumn.h"
#import "AppHelper.h"
#import "ChatMessage.h"
#import "FileMessage.h"
#import "ImageMessage.h"
#import "VideoMessage.h"
#import "StickerMessage.h"
#import "NewsFeedDetail.h"
#import "NewsFeed.h"
#import "NewsFeedCategory.h"
#import "Login.h"
#import "RSSFeed.h"
#import "RSSFeedChannel.h"
#import "ChatRoom.h"
#import "O365User.h"
#import "O365Calendar.h"
#import "Personal.h"
#import "ShuttleBusRoute.h"
#import "ShuttleBusTimeTable.h"
#import "ShuttleBusLocation.h"
#import "ShuttleBusStation.h"
#import "QuizSet.h"
#import "Quiz.h"
#import "QuizChoice.h"
#import "Vote.h"
#import "VoteChoice.h"
#import "FlashNews.h"
#import "ShareYourThought.h"
#import "ShareYourThoughtCategory.h"
#import "ADANotification.h"
#import "InboxMessage.h"
#import "CompanyProfile.h"
#import "Media.h"
#import "ADAClient.h"
#import "FeatureGroup.h"
#import "WeatherData.h"
#import "LocationMessage.h"
#import "Version.h"
#import "EventRegister.h"
#import "EventRegisterMember.h"
#import "LinkPreview.h"
#import "Phonebook.h"
#import "MeetingRoom.h"
#import "ReserveRoom.h"

#import <CoreLocation/CoreLocation.h>

typedef NS_ENUM(NSInteger, ADALanguage) {
//typedef enum ADALanguage : NSInteger {
    ADALanguageTH                 = 1,
    ADALanguageEN                 = 2
};

#define HAVE_MPROTECT 1

#define kADARecivedDeviceToken      @"ADARecivedDeviceToken"
#define kADARecivedRemoteMessage    @"ADARecivedRemoteMessage"
#define kADARecivedRemoteBGMessage   @"ADARecivedRemoteBGMessage"
#define kADANoficationUpdate        @"ADANoficationUpdate"


#define kSender                     @"senderID"
#define kSenderName                 @"sender"
#define kMessage                    @"message"
#define kMessageImage               @"messageImage"
#define kMessageImageHeight         @"messageImageHeight"
#define kLocationName               @"locationName"
#define kLocationDescription        @"locationDesc"
#define kTime                       @"time"
#define kDate                       @"date"
#define kRoomID                     @"roomID"
#define kImageID                    @"imageID"
#define kClientID                   @"clientID"
#define kGroupID                    @"groupID"
#define kBroadCast                  @"broadcast"
#define kRoomAdded                  @"roomAdded"
#define kMessageImageOrientation    @"messageImageOr"
#define kMessageImageID             @"messageImageID"
#define kWidth                      @"width"
#define kHeight                     @"height"
#define kSearchMessageImageID       @"\"messageImageID\":"
#define kSearchMessageSticker       @"$sticker:"
#define kSearchMessageReward        @"$reward:"
#define kSearchLocationMessage      @"\"locationDesc\":"
#define kSearchMessageVideo         @"\"videoID\":"
#define kSearchMessageFile          @"\"fileID\":"
#define kReadStatus                 @"readStatus"
#define kReadUser                   @"readUser"
#define kMessageID                  @"msgID"
#define kAction                     @"action"


typedef void (^ JSONResponse)(NSJSONSerialization* _Nullable json);
typedef void (^ DataResponse)(NSData* _Nullable data);
typedef void (^ JSONResponseWithTarget)(id _Nullable target, NSJSONSerialization* _Nullable json);
typedef void (^ DataResponseWithTarget)(id _Nullable target, NSData* _Nullable data);
typedef void (^ SuccessBlock)(BOOL success);
typedef void (^ StringBlock)(NSString* _Nullable  value);

typedef void (^ ADANotificationBlock)(ADANotification* _Nullable  nofication, NSError* _Nullable error);
typedef void (^ ADANotificationsBlock)(NSArray<ADANotification*>* _Nullable  nofication, NSError* _Nullable error);

typedef void (^ ADAClientBlock)(NSArray<ADAClient*>* _Nullable arrays, NSError * _Nullable error);
//typedef void (^ ArrayBlock)(NSArray<NSObject*>* _Nullable arrays, NSError * _Nullable error);
typedef void (^ LoginBlock)(Login* _Nullable login, NSError * _Nullable error);
typedef void (^ ContactBlock)(NSArray<ContactColumn*>* _Nullable columns, NSArray<ContactPerson*>* _Nullable searchResult, NSError * _Nullable error);

typedef void (^ AppOverviewBlock)(NSArray<Version*>* _Nullable notes, NSArray<NSString*>* _Nullable images, NSError * _Nullable error);
typedef void (^ ChatMessageBlock)(NSArray<ChatMessage*>* _Nullable arrays, NSError * _Nullable error);
typedef void (^ FeatureGroupBlock)(NSArray<FeatureGroup*>* _Nullable arrays, NSError * _Nullable error);
typedef void (^ FeatureBlock)(NSArray<Feature*>* _Nullable arrays, NSError * _Nullable error);
typedef void (^ FlashNewsBlock)(NSArray<FlashNews*>* _Nullable arrays, NSError * _Nullable error);
typedef void (^ ContactColumnBlock)(NSArray<ContactColumn*>* _Nullable arrays, NSError * _Nullable error);
typedef void (^ ContactPersonBlock)(NSArray<ContactPerson*>* _Nullable arrays, NSError * _Nullable error);
typedef void (^ ADAMemberBlock)(ADAMember* _Nullable arrays, NSError * _Nullable error);
typedef void (^ ADAMembersBlock)(NSArray<ADAMember*>* _Nullable arrays, NSError * _Nullable error);
typedef void (^ NewsFeedCategoryBlock)(NSArray<NewsFeedCategory*>* _Nullable arrays, NSError * _Nullable error);
typedef void (^ NewsFeedBlock)(NSArray<NewsFeed*>* _Nullable arrays, NSError * _Nullable error);
typedef void (^ RSSFeedBlock)(NSArray<RSSFeed*>* _Nullable arrays, NSError * _Nullable error);
typedef void (^ RSSFeedChannelBlock)(NSArray<RSSFeedChannel*>* _Nullable arrays, NSError * _Nullable error);
typedef void (^ O365CalendarBlock)(NSArray<O365Calendar*>* _Nullable arrays, NSError * _Nullable error);
typedef void (^ ShuttleBusRouteBlock)(NSArray<ShuttleBusRoute*>* _Nullable arrays, NSError * _Nullable error);
typedef void (^ QuizSetBlock)(NSArray<QuizSet*>* _Nullable arrays, NSError * _Nullable error);
typedef void (^ QuizPollBlock)(NSArray<Quiz*>* _Nullable arrays, NSError * _Nullable error);
typedef void (^ QuizResultBlock)(NSInteger score, NSArray<ADAMember*>* _Nullable arrays, NSError * _Nullable error);
typedef void (^ VoteBlock)(NSArray<Vote*>* _Nullable arrays, NSError * _Nullable error);
typedef void (^ VoteResultBlock)(Vote* _Nullable vote, NSError * _Nullable error);
typedef void (^ ShareYourThoughtCategoryBlock)(NSArray<ShareYourThoughtCategory*>* _Nullable arrays, NSError * _Nullable error);
typedef void (^ ShareYourThoughtBlock)(NSArray<ShareYourThought*>* _Nullable arrays, NSError * _Nullable error);
typedef void (^ InboxMessageBlock)(NSArray<InboxMessage*>* _Nullable arrays, NSError * _Nullable error);
typedef void (^ WeatherForecastBlock)(WeatherData* _Nullable data, NSError * _Nullable error);
typedef void (^ EventRegisterBlock)(EventRegister* _Nullable data, NSError * _Nullable error);
typedef void (^ EventRegistersBlock)(NSArray<EventRegister*>* _Nullable data, NSError * _Nullable error);
typedef void (^ RegisterResultBlock)(EventRegisterMember * _Nullable member, NSError * _Nullable error);
typedef void (^ LinkPreviewBlock)(NSArray<LinkPreview*> * _Nullable previews, id _Nullable refObject , NSError * _Nullable error);
typedef void (^ PhonebookBlock)(NSArray<Phonebook*> * _Nullable phonebooks, NSError * _Nullable error);
typedef void (^ MeetingRoomBlock)(NSArray<MeetingRoom*> * _Nullable rooms, NSError * _Nullable error);
typedef void (^ ReserveRoomBlock)(NSArray<ReserveRoom*> * _Nullable rooms, NSError * _Nullable error);
typedef void (^ BuildingBlock)(NSArray<Building*> * _Nullable rooms, NSError * _Nullable error);


typedef void (^ UploadBlock)(NSString* _Nullable fileID, NSError * _Nullable error);
typedef void (^ ProgressBlock)(long progress, long total);
typedef void (^ DownloadBlock)(NSData* _Nullable data, NSString* _Nullable filePath, NSError * _Nullable error);
typedef void (^ DownloadRefBlock)(id _Nonnull ref, NSData* _Nullable data, NSString* _Nullable filePath, NSError * _Nullable error);
typedef void (^ ImageBlock)(UIImage* _Nullable image);
typedef void (^ LoadedImageViewBlock)(UIImageView* _Nullable imageView, UIImage* _Nullable image);
typedef void (^ ChatVideoBlock)(VideoMessage* _Nullable message);
typedef void (^ ChatImageBlock)(ImageMessage* _Nullable message);
typedef void (^ ChatFileBlock)(FileMessage* _Nullable message);
typedef void (^ ChatStickerBlock)(StickerMessage* _Nullable message);
typedef void (^ ChatRoomBlock)(ChatRoom* _Nullable room);
typedef void (^ ChatRoomsBlock)(NSArray<ChatRoom*>* _Nullable room, NSError * _Nullable error);
typedef void (^ ChatRoomStateBlock)(ChatRoom* _Nullable room);
typedef void (^ ChatRoomIDBlock)(NSString* _Nullable roomID);
typedef void (^ NewsFeedDetailBlock)(NewsFeedDetail* _Nullable detail, NSError * _Nullable error);
typedef void (^ PersonalBlock)(Personal* _Nullable personal, NSError * _Nullable error);
typedef void (^ ShuttleBusBlock)(NSArray<ShuttleBusRoute*>* _Nullable route, NSError * _Nullable error);
typedef void (^ ShuttleBusStationBlock)(NSArray<ShuttleBusStation*>* _Nullable stations, NSError * _Nullable error);
typedef void (^ ShuttleBusLocationBlock)(ShuttleBusLocation* _Nullable location, NSError * _Nullable error);
typedef void (^ CompanyProfileBlock)(CompanyProfile* _Nullable companyProfile, NSError * _Nullable error);
typedef void (^ NearbyLocationBlock)(NSArray<LocationMessage*>* _Nullable route, NSError * _Nullable error);

@interface ADA : AppHelper
@property (nonatomic, strong) Login* _Nullable login;
@property (nonatomic, strong) NSString* _Nullable  pushNotificationToken;
- (instancetype _Nonnull)initWithLanguage:(enum ADALanguage) lang;
- (void)changeLanguage:(enum ADALanguage) lang handler:(SuccessBlock _Nullable) handler;
+ (ADALanguage)getCurrentLanguage;
+ (void) getConfigurationWithCode:(NSString* _Nonnull) code handler:(JSONResponse _Nullable) config;
+ (void) getClients:(BOOL) bomsClient handler:(ADAClientBlock _Nullable) handler;
+ (void) getClientImageLogo:(NSString* _Nonnull) code handler:(ImageBlock _Nullable) handler;

#pragma mark login
- (void) loginWithUser:(NSString* _Nonnull) userid
              password:(NSString* _Nonnull) password
            clientCode:(NSString* _Nonnull) clientCode
               handler:(LoginBlock _Nullable) handler;
- (void) reConnect:(SuccessBlock _Nullable) handler ;
- (BOOL) isLoggedOn;
- (void) setAppThemeColor;
- (void) logout:(SuccessBlock _Nullable) handler;
- (NSArray<Login*>* _Nullable) getExistsUsers;
- (NSArray<Login*>* _Nullable) removeExistsUsers:(Login* _Nonnull) user;
- (void) getUserSetting:(JSONResponse _Nullable) handler;
- (void) setUserSetting:(NSDictionary* _Nonnull) data :(SuccessBlock _Nullable) handler;
- (void) translate:(NSString* _Nonnull) content handler:(StringBlock _Nonnull) handler;

#pragma mark Notification
- (void) registerPushNotification:(NSDictionary* _Nullable) launchOptions :(SuccessBlock _Nullable) handler;
- (void) registerDeviceToken:(NSData* _Nonnull) deviceToken handler:(SuccessBlock _Nullable) handler;
- (void) didNotificationReceived:(ADANotificationBlock _Nullable) handler;
- (void) didDeviceTokenReceived:(DataResponse _Nullable) handler;
- (void) removeNotifications:(ADANotification* _Nonnull) removeNotification handler:(SuccessBlock _Nullable) handler;
- (void) saveNotification:(ADANotification* _Nonnull) notification handler:(SuccessBlock _Nullable) handler;
- (void) removeLocalNotification:(ADANotification* _Nonnull) notification handler:(SuccessBlock _Nullable) handler;
- (void) addLocalNotification:(ADANotification* _Nonnull) notification handler:(SuccessBlock _Nullable) handler;
- (void) getLocalNotification:(NSString* _Nonnull) messageType handler:(ADANotificationsBlock _Nullable) handler;
- (NSArray<ADANotification*>* _Nonnull) getNotifications;

#pragma mark Util


+ (NSDateFormatter* _Nonnull) getDateFormatter;
- (void) trackScreenName:(NSString* _Nonnull) screenName trackerID:(NSString* _Nonnull) trackID;
- (void) saveImage:(UIImage* _Nonnull) image progress:(ProgressBlock _Nullable) progress handler:(UploadBlock _Nullable) handler;
- (void) saveData:(NSData* _Nonnull) image progress:(ProgressBlock _Nullable) progress handler:(UploadBlock _Nullable) handler;
- (void) saveImage:(UIImage* _Nonnull) image type:(NSString* _Nullable) type progress:(ProgressBlock _Nullable) progress handler:(UploadBlock _Nullable) handler;
- (void) saveData:(NSData* _Nonnull) image type:(NSString* _Nullable) type progress:(ProgressBlock _Nullable) progress handler:(UploadBlock _Nullable) handler;

- (NSString* _Nonnull) stringURL:(NSString* _Nonnull) imageID;
- (NSString* _Nonnull) stringURL:(NSString* _Nonnull) imageID type:(NSString* _Nullable) type;
- (NSURLRequest* _Nonnull) requestFromStringURLSafe:(NSString* _Nonnull) url;

- (void) loadImage:(UIImageView* _Nonnull) imageView imageID:(NSString* _Nonnull) imageID placeholder:(UIImage* _Nullable) placeholder fitToImageView:(BOOL) fitToImageView cache:(BOOL) doCache;
- (void) loadImage:(UIImageView* _Nonnull) imageView imageURL:(NSURL* _Nonnull) imageURL placeholder:(UIImage* _Nullable) placeholder cache:(BOOL) doCache;
- (void) loadImage:(UIImageView* _Nonnull) imageView imageURL:(NSURL* _Nonnull) imageURL placeholder:(UIImage* _Nullable) placeholder cache:(BOOL) doCache handler:(LoadedImageViewBlock _Nullable) handler;
- (void) loadImage:(UIImageView* _Nonnull) imageView imageID:(NSString* _Nonnull) imageID placeholder:(UIImage* _Nullable) placeholder cache:(BOOL) doCache;
- (void) loadImage:(UIImageView* _Nonnull) imageView imageID:(NSString* _Nonnull) imageID placeholder:(UIImage* _Nullable) placeholder cache:(BOOL) doCache handler:(LoadedImageViewBlock _Nullable) handler;
- (void) loadImage:(UIImageView* _Nonnull) imageView imageID:(NSString* _Nonnull) imageID placeholder:(UIImage* _Nullable) placeholder type:(NSString* _Nullable) type cache:(BOOL) doCache;
- (void) loadImage:(UIImageView* _Nonnull) imageView imageID:(NSString* _Nonnull) imageID placeholder:(UIImage* _Nullable) placeholder type:(NSString* _Nullable) type cache:(BOOL) doCache handler:(LoadedImageViewBlock _Nullable) handler;

- (void) loadImage:(UIImageView* _Nonnull) imageView imageID:(NSString* _Nonnull) imageID placeholder:(UIImage* _Nullable) placeholder type:(NSString* _Nullable) type fitToImageView:(BOOL) fitToImageView cache:(BOOL) doCache handler:(LoadedImageViewBlock _Nullable) handler;

- (void) loadImage:(UIImageView* _Nonnull) imageView imageURL:(NSURL* _Nonnull) imageURL placeholder:(UIImage* _Nullable) placeholder fitToImageView:(BOOL) fitToImageView cache:(BOOL) doCache handler:(LoadedImageViewBlock _Nullable) handler;
- (void) saveFile: (NSData* _Nonnull) data withFileName:(NSString* _Nonnull) fileName handler:(SuccessBlock _Nullable) handler;
- (void) getFileWithName:(NSString* _Nonnull) fileNane handler:(DataResponse _Nullable) handler;
- (void) getImage:(NSString* _Nonnull) imageID cache:(BOOL) doCache handler:(ImageBlock _Nullable) handler;
- (void) getImage:(NSString* _Nonnull) imageID type:(NSString* _Nullable) type cache:(BOOL) doCache handler:(ImageBlock _Nullable) handler;
- (void) getImageURL:(NSString* _Nonnull) imageURL cache:(BOOL) doCache handler:(ImageBlock _Nullable) handler;

- (void) upload:(NSURL* _Nonnull) url progress:(ProgressBlock _Nullable) progress handler:(UploadBlock _Nullable) handler;
- (void) upload:(NSURL* _Nonnull) url type:(NSString* _Nullable) type progress:(ProgressBlock _Nullable) progress handler:(UploadBlock _Nullable) handler;
- (void) download:(NSString* _Nonnull) fileID  saveName:(NSString* _Nullable) saveName cache:(BOOL) doCache progress:(ProgressBlock _Nullable) progress handler:(DownloadBlock _Nullable) handler;
- (void) download:(NSString* _Nonnull) fileID type:(NSString* _Nullable) type  saveName:(NSString* _Nullable) saveName cache:(BOOL) doCache progress:(ProgressBlock _Nullable) progress handler:(DownloadBlock _Nullable) handler;
- (void) downloadFromURL:(NSURL* _Nonnull) fileURL cache:(BOOL) doCache progress:(ProgressBlock _Nullable) progress handler:(DownloadBlock _Nullable) handler;


- (void) download:(NSString* _Nonnull) fileID  saveName:(NSString* _Nullable) saveName cache:(BOOL) doCache progress:(ProgressBlock _Nullable) progress handler:(DownloadRefBlock _Nullable) handler ref:(id _Nonnull) ref;
- (void) download:(NSString* _Nonnull) fileID type:(NSString* _Nullable) type  saveName:(NSString* _Nullable) saveName cache:(BOOL) doCache progress:(ProgressBlock _Nullable) progress handler:(DownloadRefBlock _Nullable) handler ref:(id _Nonnull) ref;
- (void) downloadFromURL:(NSURL* _Nonnull) fileURL cache:(BOOL) doCache progress:(ProgressBlock _Nullable) progress handler:(DownloadRefBlock _Nullable) handler ref:(id _Nonnull) ref;

- (void) clearCache:(SuccessBlock _Nullable) handler;



#pragma mark All Features
- (void) getAppOverview:(AppOverviewBlock _Nullable) handler;
- (void) getGroupFeatures:(FeatureGroupBlock _Nullable) handler;
- (void) getMainFeatures:(FeatureBlock _Nullable) handler;
- (void) getFeatures:(FeatureBlock _Nullable) handler;
- (void) getAllFeatures:(FeatureBlock _Nullable)handler;
#pragma mark FlashNews
- (void) getFlashNews:(FlashNewsBlock _Nullable) handler;
#pragma mark Contact
//- (void) getUserInfo:(NSString*) userid group:(NSString*) group handler:(ContactBlock _Nullable) handler;
- (void) getContactColumns:(ContactColumnBlock _Nullable) handler;
- (NSString* _Nullable) getLastestSearchContactKeyword;
- (void) searchContactWithKeyword:(NSString* _Nonnull) keyword lookInCache:(BOOL) doCache handler:(ContactBlock _Nullable) handler;
- (void) getAllPhonebook:(SuccessBlock _Nullable) handler;
- (void) setContactNickname:(NSString *_Nonnull) nickname with:(id _Nonnull) user;
- (NSString* _Nullable) getContactNickname:(id _Nonnull) user;
#pragma mark Chat
- (void) getEvent:(ChatRoomsBlock _Nullable) handler;
- (void) chatWithUser:(NSString* _Nonnull) usid group:(NSString* _Nonnull) group handler:(ChatRoomIDBlock _Nullable) handler;
- (void) getChatRoomWithUser:(NSString* _Nonnull) usid group:(NSString* _Nonnull) group handler:(ChatRoomBlock _Nullable) handler;
- (void) getChatRoomWithID:(NSString* _Nonnull) roomID handler:(ChatRoomBlock _Nullable) handler;
- (void) canSendRewardSticker:(SuccessBlock _Nullable) handler;
- (void) canChatWithUser:(NSString* _Nonnull) usid group:(NSString* _Nonnull) group handler:(SuccessBlock _Nullable) handler;
- (void) getADAMember:(NSString* _Nonnull) userID group:(NSString* _Nonnull) group handler:(ADAMemberBlock _Nullable) handler;
- (void) searchADAMember:(NSString* _Nonnull) keyword handler:(ContactPersonBlock _Nullable) handler;
- (void) createGroupChatRoom:(ChatRoom* _Nonnull) room handler:(SuccessBlock _Nullable) handler;
- (void) createChatRoomWithUser:(NSString* _Nonnull) userID group:(NSString* _Nonnull) group handler:(SuccessBlock _Nullable) handler;
- (void) removeChatRoom:(ChatRoom* _Nonnull) chatRoom handler:(SuccessBlock _Nullable) handler;
- (void) inviteMember:(ChatRoom* _Nonnull) room member:(NSArray<ADAMember*>* _Nonnull) members handler:(SuccessBlock _Nullable) handler;
- (void) declineChatInvitation:(ChatRoom* _Nonnull) room handler:(SuccessBlock _Nullable) handler;
- (void) acceptChatInvitation:(ChatRoom* _Nonnull) room handler:(SuccessBlock _Nullable) handler;
- (void) leaveChatGroup:(ChatRoom* _Nonnull) room handler:(SuccessBlock _Nullable) handler;
- (void) updateChatGroup:(ChatRoom* _Nonnull) room handler:(SuccessBlock _Nullable) handler;
- (void) turnChatNotification:(ChatRoom* _Nonnull) room turn:(BOOL) on handler:(SuccessBlock _Nullable) handler;
- (void) createEventRoom:(ChatRoom* _Nonnull) room handler:(SuccessBlock _Nullable) handler;
- (void) updateEventRoom:(ChatRoom* _Nonnull) room handler:(SuccessBlock _Nullable) handler;
- (void) cancelEventRoom:(ChatRoom* _Nonnull) room handler:(SuccessBlock _Nullable) handler;
- (void) joinEvent:(ChatRoom * _Nonnull)room handler:(ChatRoomIDBlock _Nullable) handler;
- (void) getEventRoom:(ChatRoom * _Nonnull)room handler:(ChatRoomBlock _Nullable) handler;
- (void) fetchEventInfo:(ChatRoom * _Nonnull)room handler:(SuccessBlock _Nullable) handler;
- (void) getEventMember:(ChatRoom* _Nonnull) room handler:(ADAMembersBlock _Nullable) handler;

- (void) sendMessageTextToAllMembers:(ChatRoom* _Nonnull) room message:(ChatMessage* _Nonnull) message handler:(SuccessBlock _Nullable) handler;
- (void) sendMessageImageToAllMembers:(ChatRoom* _Nonnull) room message:(ImageMessage* _Nonnull) message handler:(SuccessBlock _Nullable) handler;
- (void) sendMessageVideoToAllMembers:(ChatRoom* _Nonnull) room message:(VideoMessage* _Nonnull) message handler:(SuccessBlock _Nullable) handler;
- (void) sendMessageFileToAllMembers:(ChatRoom* _Nonnull) room message:(FileMessage* _Nonnull) message handler:(SuccessBlock _Nullable) handler;
- (void) sendMessageLocationToAllMembers:(ChatRoom* _Nonnull) room message:(LocationMessage* _Nonnull) message handler:(SuccessBlock _Nullable) handler;
- (void) sendMessageStickerToAllMembers:(ChatRoom* _Nonnull) room message:(StickerMessage* _Nonnull) message handler:(SuccessBlock _Nullable) handler;

- (void) searchLocation:(NSString* _Nullable) keyword handler:(NearbyLocationBlock _Nullable) handler;
- (void) getNearbyLocation:(CLLocationCoordinate2D) location handler:(NearbyLocationBlock _Nullable) handler;
- (void) getLinkPreview:(NSString* _Nonnull) content refObject:(id _Nullable) refObject handler:(LinkPreviewBlock _Nonnull) handler;

#pragma mark News Feed
- (void) getNewsFeedCategory:(NewsFeedCategoryBlock _Nullable) handler;
- (void) getNewsFeedList:(NewsFeedCategory* _Nonnull) category inPeroid:(NSString* _Nullable) peroid handler:(NewsFeedBlock _Nullable) handler;
- (void) getNewsFeedDetail:(NewsFeed* _Nonnull) newsFeed handler:(NewsFeedDetailBlock _Nullable) handler;
- (void) getRSSFeed:(RSSFeedBlock _Nullable) handler;
- (void) getRSSFeedChannel:(RSSFeed* _Nonnull) rssFeed handler:(RSSFeedChannelBlock _Nullable) handler;
#pragma mark O365
- (void) getO365CalendarWithUser:(O365User* _Nonnull) user dateFrom: (NSDate* _Nonnull) dateFrom to:(NSDate* _Nonnull) dateTo handler:(O365CalendarBlock _Nullable) handler;
#pragma mark Personal
- (void) getPersonalInfoWithUser:(NSString* _Nonnull) userid group:(NSString* _Nonnull)group handler:(PersonalBlock _Nullable) handler;
- (void) getPersonalInfo:(PersonalBlock _Nullable) handler;
- (void) submitPersonalData:(Personal* _Nonnull) personal handler:(SuccessBlock _Nullable) handler;
#pragma mark Shuttle Bus
- (void) getShuttleBusStation:(ShuttleBusStationBlock _Nullable) handler;
- (void) getShuttleBusRoute:(ShuttleBusRouteBlock _Nullable) handler;
- (void) shuttleBusTracking:(ShuttleBusRoute* _Nonnull) route handler:(ShuttleBusLocationBlock _Nullable) handler;
#pragma mark Quiz
- (void) getQuiz:(QuizState) state handler:(QuizSetBlock _Nullable) handler;
- (void) getQuizResult:(QuizSet* _Nonnull) quizSet handler:(QuizResultBlock _Nullable) handler;
//- (void) getQuizPoll:(QuizSet* _Nonnull) quizSet handler:(QuizPollBlock _Nullable) handler;
//- (void) getActiveQuiz:(QuizSetBlock _Nullable) handler;
//- (void) getArchiveQuiz:(QuizSetBlock _Nullable) handler;
- (void) submitQuiz:(QuizSet* _Nonnull) quizSet :(SuccessBlock _Nullable) handler;
#pragma mark Vote
- (void) submitVote:(Vote* _Nonnull) vote :(SuccessBlock _Nullable) handler;
- (void) getVote:(VoteState) state handler:(VoteBlock _Nullable) handler;
- (void) getVoteWithID:(long) voteID handler:(VoteResultBlock _Nullable) handler;
- (void) addVote:(Vote* _Nonnull) vote :(VoteResultBlock _Nullable) handler;
- (void) updateVote:(Vote* _Nonnull) vote :(VoteResultBlock _Nullable) handler;
- (void) deleteVote:(Vote* _Nonnull) vote :(SuccessBlock _Nullable) handler;

#pragma mark Thought
- (void) getThoughtCategory:(ShareYourThoughtCategoryBlock _Nullable) handler;
- (void) submitThought:(ShareYourThought* _Nonnull) thought handler:(ProgressBlock _Nullable) handler;
- (void) getSummitedThought:(ShareYourThoughtBlock _Nullable) handler;
#pragma mark Inbox
- (void) getInbox:(InboxMessageBlock _Nullable) handler;
- (void) readInboxMessage:(NSArray<InboxMessage*>* _Nonnull) message handler:(SuccessBlock _Nullable) handler;
- (void) removeInboxMessage:(NSArray<InboxMessage*>* _Nonnull) message handler:(SuccessBlock _Nullable) handler;
#pragma mark Company Profile
- (void) getCompanyProfile:(CompanyProfileBlock _Nullable) handler;
#pragma mark weather forecast
- (void) getWeatherForecast:(CLLocationCoordinate2D) coordinate handler:(WeatherForecastBlock _Nullable) handler;
#pragma mark event register
- (void) getEventRegister:(NSInteger) eventID handler:(EventRegisterBlock _Nullable) handler;
- (void) getMyEventRegister:(EventRegistersBlock _Nullable) handler;
- (void) reviewEvent:(EventRegisterMember* _Nonnull) eventMember handler:(SuccessBlock _Nullable) handler;
- (void) registerEvent:(EventRegister* _Nonnull) event handler:(RegisterResultBlock _Nullable) handler;
#pragma mark meeting room reservation
- (void) verifyEWSWith:(NSString* _Nonnull) user password:(NSString*_Nonnull) password handler:(SuccessBlock _Nullable) handler;
- (void) getBuilding:(BuildingBlock _Nullable) handler;
- (void) searchAvailableRoom:(ReserveRoom* _Nonnull) room handler:(MeetingRoomBlock _Nullable) handler;
- (void) getReservedRoom:(ReserveRoom* _Nonnull) room handler:(ReserveRoomBlock _Nullable) handler;
- (void) reserveRoom:(ReserveRoom* _Nonnull) room handler:(SuccessBlock _Nullable) handler;
- (void) editRoom:(ReserveRoom* _Nonnull) room handler:(SuccessBlock _Nullable) handler;
- (void) cancelRoom:(ReserveRoom* _Nonnull) room handler:(SuccessBlock _Nullable) handler;
- (void) confirmUsingRoom:(ReserveRoom* _Nonnull) room handler:(SuccessBlock _Nullable) handler;

#pragma mark VOIP
- (void) sendVOIPCall:(id _Nonnull ) targetMember handler:(SuccessBlock _Nullable) handler;
- (void) registerVOIPToken:(NSData* _Nonnull) deviceToken handler:(SuccessBlock _Nullable) handler;
@end
