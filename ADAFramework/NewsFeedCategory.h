//
//  NewsFeedCategory.h
//  ADAFramework
//
//  Created by Choldarong-r on 6/12/2559 BE.
//  Copyright © 2559 G-ABLE ITS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NewsFeedCategory : NSObject

typedef NS_ENUM(NSInteger, CategoryType) {
//typedef enum CategoryType : NSUInteger {
    CategoryTypeEventCalendar       = 0,
    CategoryTypeNewsWithImage       = 1,
    CategoryTypePDF                 = 2
};

@property (nonatomic, retain) NSString      *title;
@property (nonatomic, retain) NSString      *icon;
@property (nonatomic, retain) NSString      *detail;
@property (nonatomic, retain) NSString      *categoryID;
@property (nonatomic, assign) CategoryType  categoryType;

@end
