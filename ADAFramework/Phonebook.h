//
//  Phonebook.h
//  ADAFramework
//
//  Created by Choldarong-r on 31/5/2561 BE.
//  Copyright © 2561 G-ABLE Company Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Phonebook : NSObject

@property (nonatomic, retain) NSString *name;
@property (nonatomic, assign) long mobileNo;
@end
