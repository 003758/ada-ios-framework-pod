//
//  UserSession+CoreDataClass.h
//  ADAFramework
//
//  Created by Choldarong-r on 2/12/2560 BE.
//  Copyright © 2560 G-ABLE ITS. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserSession : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "UserSession+CoreDataProperties.h"
