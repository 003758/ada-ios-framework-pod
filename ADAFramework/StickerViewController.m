//
//  StickerViewController.m
//  ADAFramework
//
//  Created by Choldarong-r on 17/12/2559 BE.
//  Copyright © 2559 G-ABLE ITS. All rights reserved.
//

#import "StickerViewController.h"
#import "URLConnection.h"
#import "AppHelper.h"
@import AFNetworking;

@interface StickerViewController () <UICollectionViewDataSource, UICollectionViewDelegate>

@end

@implementation StickerViewController{
    UIScrollView *stickerScroll1;
    UIScrollView *stickerScroll2;
    UIScrollView *stickerCateScroll;
    UIView *parentView;
    UIButton *switchKeyBtn;
    UIPageControl *pageControl;
    NSMutableDictionary *stickersDict;
    NSMutableDictionary *imageViewDict;
    NSMutableDictionary *stickerImageDict;
    NSMutableArray *collectionViews;
    NSFileManager *fileMgr;
    NSURL *tmpDirURL;
    CGRect currentFrame;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"View Did Load");
    fileMgr = [NSFileManager defaultManager];
    tmpDirURL = [NSURL fileURLWithPath:NSTemporaryDirectory() isDirectory:YES];
    self.view.backgroundColor = [UIColor whiteColor];
    CGRect stickerFrame = self.view.frame;
    CGRect scrollRect = stickerFrame;
    scrollRect.size.width = stickerFrame.size.width;
    stickerImageDict = [[NSMutableDictionary alloc] init];
    stickersDict = [[NSMutableDictionary alloc] init];
    collectionViews = [[NSMutableArray alloc] init];
    imageViewDict = [[NSMutableDictionary alloc] init];
    
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    UICollectionView *collectionView = [[UICollectionView alloc] initWithFrame:self.view.frame collectionViewLayout:layout];
    collectionView.tag = 1111;
    collectionView.delegate = self;
    collectionView.dataSource = self;
    //collectionView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    collectionView.backgroundColor = [UIColor clearColor];
    [collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"stickerCell"];
    [collectionViews addObject:collectionView];
    collectionView.autoresizingMask = (UIViewAutoresizingFlexibleWidth |
                                       UIViewAutoresizingFlexibleHeight);
    [self.view addSubview:collectionView];
    

    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self loadStickerSet];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) dismissViewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void) loadStickerSet {
    if(!_stickerSet){
        return;
    }
    //NSLog(@"_stickerSet: %@", _stickerSet);
    int stickerNumber = 0;
    //CGFloat xPos = 0;
    
    
    [stickersDict removeAllObjects];
    for (NSJSONSerialization *json in _stickerSet) {
        
        
        
        //collectionView.tag = stickerNumber++;
        
        [stickersDict setObject:json forKey:@(stickerNumber++)];
        //xPos += collectionView.frame.size.width;
        
        
    }
    UICollectionView *collectionView = (UICollectionView*)[self.view viewWithTag:1111];
    [collectionView  reloadData];
    //pageControl.numberOfPages = stickerNumber-1;
    stickerCateScroll.contentSize = CGSizeMake(currentFrame.size.width, currentFrame.size.height);
    
    
    
    
}

- (void)didSelectSticker:(id) sender {
    if (self.delegate) {
        [self.delegate didSelectSticker:sender withCode:((UIButton*) sender).titleLabel.text];
    }
    [self dismissViewController];
    
}

#pragma mark - UICollectionView

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return stickersDict.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    NSJSONSerialization *json = [stickersDict objectForKey:@(section)];
    return [[json valueForKey:@"detail"] count];
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    //NSArray *images = [stickersDict objectForKey:@(indexPath.section)];
    
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"stickerCell" forIndexPath:indexPath];
    
    if (![cell viewWithTag:1122]) {
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
        imgView.tag = 1122;
        [cell addSubview:imgView];
    }
    
    NSJSONSerialization *json = [stickersDict objectForKey:@(indexPath.section)];
    NSJSONSerialization *item = [[json valueForKey:@"detail"] objectAtIndex:indexPath.row];
    NSString *imageID = [item valueForKey:@"code"];
    UIImageView *imgView = (UIImageView*)[cell viewWithTag:1122];
    if ([stickerImageDict valueForKey:imageID]) {
        imgView.image = [stickerImageDict valueForKey:imageID];
        return cell;
    }
    
    
    
    NSString *imgFolder = [NSString stringWithFormat:@"%@chat/stickerSet/%@", tmpDirURL, [json valueForKey:@"id"]];
    imgFolder = [imgFolder stringByReplacingOccurrencesOfString:@"file://" withString:@""];
    if (![fileMgr fileExistsAtPath:imgFolder]) {
        if([fileMgr createDirectoryAtPath:imgFolder withIntermediateDirectories:YES attributes:nil error:nil]){
            //NSLog(@"Dir created");
        }
    }
    
    NSString *strPath = [NSString stringWithFormat:@"%@/%@", imgFolder, imageID];
    
    if ([fileMgr fileExistsAtPath:strPath]){
        NSData *imgData = [fileMgr contentsAtPath:strPath];
        UIImage *image = [UIImage imageWithData:imgData];
        imgView.image = image;
        [stickerImageDict setObject:image forKey:imageID];
        
    }else{
        
        
        NSMutableURLRequest *req = [URLConnection requestFromURL:[NSString stringWithFormat:@"%@file", kSERVER_HOST]
                                    
                                                          method:@"GET"
                                                       parameter:@{
                                                                   @"id": [item valueForKey:@"image_id"],
                                                                   @"mediaType": @"chatSticker",
                                                                   @"raw": @"true"
                                                                   }];
        [req setValue:[AppHelper getUserDataForKey:kADA_CLIENT] forHTTPHeaderField:kADA_CLIENT];
        /*AFImageResponseSerializer *serializer = [[AFImageResponseSerializer alloc] init];
         serializer.acceptableContentTypes = [serializer.acceptableContentTypes setByAddingObject:@"image/jpg"];
         imgIcon.imageResponseSerializer = serializer;*/
        [imageViewDict setObject:imgView forKey:req.URL];
        [imgView setImageWithURLRequest:req
                       placeholderImage:[UIImage imageNamed:@"emo-76"]
                                success:^(NSURLRequest * req, NSHTTPURLResponse * res, UIImage * image) {
                                    if (image) {
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            UIImageView *imgView = [imageViewDict objectForKey:req.URL];
                                            imgView.image = image;
                                            [stickerImageDict setObject:image forKey:imageID];
                                            NSData *imgData = UIImagePNGRepresentation(image);
                                            if([imgData writeToFile:strPath atomically:YES]){
                                                //NSLog(@"write success");
                                            }
                                        });
                                    }
                                } failure:^(NSURLRequest *req, NSHTTPURLResponse * res, NSError * error) {
                                }];
    }
    
    
    
    
    //imgView.image = [UIImage imageNamed:[images objectAtIndex:indexPath.row]];
    
    
    return cell;
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    //NSArray *images = [stickersDict objectForKey:@(indexPath.section)];
    NSJSONSerialization *json = [stickersDict objectForKey:@(indexPath.section)];
    NSJSONSerialization *item = [[json valueForKey:@"detail"] objectAtIndex:indexPath.row];
    
    
    if (self.delegate) {
        [self.delegate didSelectSticker:collectionView withCode:[item valueForKey:@"code"]];
    }
    [self dismissViewController];
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(100, 100);
}

@end
