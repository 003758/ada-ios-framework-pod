//
//  FileMessage.h
//  ADAFramework
//
//  Created by Choldarong-r on 13/12/2559 BE.
//  Copyright © 2559 G-ABLE ITS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FileMessage : NSObject
+ (FileMessage* _Nonnull) instanceWithURL:(NSURL* _Nonnull) url;
+ (NSArray<NSString*>* _Nonnull) allowDocumentTypes;

@property (nonatomic, retain) NSString * _Nullable fileID;
@property (nonatomic, retain) NSString * _Nullable contentType;
@property (nonatomic, retain) NSString * _Nullable name;
@property (nonatomic, retain) NSString * _Nullable fileSize;
@property (nonatomic, retain) NSString * _Nullable filePath;
@end
