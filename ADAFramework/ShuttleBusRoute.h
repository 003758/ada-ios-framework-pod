//
//  ShuttleBusRoute.h
//  ADAFramework
//
//  Created by Choldarong-r on 22/6/2560 BE.
//  Copyright © 2560 G-ABLE ITS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ShuttleBusTimeTable.h"
#import "ShuttleBusStation.h"
@import CoreLocation;

@interface ShuttleBusRoute : NSObject

@property (nonatomic, assign) int routeID;
@property (nonatomic, retain) NSString *routeName;
@property (nonatomic, retain) NSArray<ShuttleBusTimeTable*> *timetable;
@property (nonatomic, retain) ShuttleBusStation *startPoint;
@property (nonatomic, retain) ShuttleBusStation *endPoint;
@property (nonatomic, assign) BOOL trackingAvailable;
@end
