//
//  StickerMessage.h
//  ADAFramework
//
//  Created by Choldarong-r on 13/12/2559 BE.
//  Copyright © 2559 G-ABLE ITS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StickerMessage : NSObject

@property (nonatomic, retain) NSString *stickerID;
@property (nonatomic, retain) NSString *point;
@property (nonatomic, retain) NSString *message;
@end
