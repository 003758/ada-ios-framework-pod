//
//  ReserveRoom.h
//  ADAFramework
//
//  Created by Choldarong-r on 19/7/2561 BE.
//  Copyright © 2561 G-ABLE Company Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MeetingRoom.h"
#import "ADAMember.h"

@interface ReserveRoom : NSObject

//
@property (nonatomic, assign) BOOL showReserved;
@property (nonatomic, retain) NSString *usedTitle;
@property (nonatomic, retain) NSString *usedDetail;
@property (nonatomic, retain) NSString *changeDetail;
@property (nonatomic, retain) NSDate *beginTime;
@property (nonatomic, retain) NSDate *endTime;
@property (nonatomic, retain) NSArray<Building*> *buildings;
@property (nonatomic, retain) NSArray<ADAMember*> *attendees;
@property (nonatomic, retain) NSDictionary<NSString*, id> *timeTableData;
@property (nonatomic, retain) NSString *mailUser;
@property (nonatomic, retain) NSString *mailPassword;
@property (nonatomic, retain) ADAMember *reserveTo;
@property (nonatomic, retain) MeetingRoom *room;
@end
