//
//  PushNofication.h
//  ADAFramework
//
//  Created by Choldarong-r on 17/7/2560 BE.
//  Copyright © 2560 G-ABLE ITS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Feature.h"

@interface ADANotification : NSObject

- (ADANotification* _Nonnull) initWith:(NSDictionary* _Nullable) userInfo;

@property (nonatomic, assign) long messageID;
@property (nonatomic, retain) NSString * _Nullable message;
@property (nonatomic, assign) NSInteger badge;
@property (nonatomic, retain) NSString * _Nullable sound;
@property (nonatomic, retain) NSString *  _Nullable messageType;
@property (nonatomic, assign) FeatureType featureType;
@property (nonatomic, retain) NSJSONSerialization * _Nullable json;
@property (nonatomic, retain) NSDictionary * _Nullable userInfo;
@property (nonatomic, assign) BOOL foregroundMessage;
@property (nonatomic, retain) NSDate * _Nullable date;

@end
