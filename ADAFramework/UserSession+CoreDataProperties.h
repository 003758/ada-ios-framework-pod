//
//  UserSession+CoreDataProperties.h
//  ADAFramework
//
//  Created by Choldarong-r on 2/12/2560 BE.
//  Copyright © 2560 G-ABLE ITS. All rights reserved.
//
//

#import "UserSession+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface UserSession (CoreDataProperties)

+ (NSFetchRequest<UserSession *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *key;
@property (nullable, nonatomic, retain) NSData *data;
@property (nullable, nonatomic, copy) NSString *stringData;
@property (nonatomic) BOOL boolData;
@property (nonatomic) int16_t intData;

@end

NS_ASSUME_NONNULL_END
