//
//  ImageMessage.h
//  ADAFramework
//
//  Created by Choldarong-r on 13/12/2559 BE.
//  Copyright © 2559 G-ABLE ITS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ImageMessage : NSObject

+ (NSArray<NSString*>* _Nonnull) mediaTypes;
+ (ImageMessage* _Nonnull) instanceWithImage:(UIImage* _Nonnull) image;

@property (nonatomic, retain) NSString * _Nullable imageID;
@property (nonatomic, retain) NSString * _Nullable contentType;
@property (nonatomic, assign) CGFloat width;
@property (nonatomic, assign) CGFloat height;
@property (nonatomic, assign) CGFloat sizeRatio;
@property (nonatomic, retain) UIImage * _Nullable image;
@property (nonatomic, assign) BOOL sourceCamera;
@end
