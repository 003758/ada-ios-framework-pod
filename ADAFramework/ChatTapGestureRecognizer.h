//
//  ChatTapGestureRecognizer.h
//  ADAFramework
//
//  Created by Choldarong-r on 21/12/2559 BE.
//  Copyright © 2559 G-ABLE ITS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatTapGestureRecognizer : UITapGestureRecognizer
@property (nonatomic , retain) id refObject; 
@end
