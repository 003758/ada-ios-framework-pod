//
//  Version.h
//  ADAFramework
//
//  Created by Choldarong-r on 1/12/2560 BE.
//  Copyright © 2560 G-ABLE ITS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Version : NSObject
@property (nonatomic, retain) NSString *version;
@property (nonatomic, retain) NSString *note;
@end
