//
//  PreviewItem.h
//  ADAFramework
//
//  Created by Choldarong-r on 14/3/2562 BE.
//  Copyright © 2562 G-ABLE Company Limited. All rights reserved.
//
@import QuickLook;
#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PreviewItem : NSObject <QLPreviewItem>
@property(readonly, nullable, nonatomic) NSURL    *previewItemURL;
@property(readonly, nullable, nonatomic) NSString *previewItemTitle;
@end
@implementation PreviewItem
- (instancetype)initPreviewURL:(NSURL *)docURL
                     WithTitle:(NSString *)title {
    self = [super init];
    if (self) {
        _previewItemURL = [docURL copy];
        _previewItemTitle = [title copy];
    }
    return self;
}
@end

NS_ASSUME_NONNULL_END
