//
//  Building.h
//  ADAFramework
//
//  Created by Choldarong-r on 31/7/2561 BE.
//  Copyright © 2561 G-ABLE Company Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Building : NSObject

@property (nonatomic, assign) NSInteger buildingID;
@property (nonatomic, retain) NSString *name;
@end
