//
//  CacheService+CoreDataProperties.h
//  ADAFramework
//
//  Created by Choldarong-r on 23/12/2559 BE.
//  Copyright © 2559 G-ABLE ITS. All rights reserved.
//

#import "CacheService+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface CacheService (CoreDataProperties)

+ (NSFetchRequest<CacheService *> *)fetchRequest;

@property (nullable, nonatomic, retain) NSData *serviceData;
@property (nullable, nonatomic, copy) NSString *serviceKey;
@property (nullable, nonatomic, retain) ADAUser *user;

@end

NS_ASSUME_NONNULL_END
