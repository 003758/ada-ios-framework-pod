//
//  Contact.h
//  ADAFramework
//
//  Created by Choldarong-r on 8/12/2559 BE.
//  Copyright © 2559 G-ABLE ITS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ContactPerson : NSObject

@property (nonatomic, retain) NSString *groupTitle;
@property (nonatomic, retain) NSArray<NSDictionary<NSString*, id>*> *contactList;
@end
