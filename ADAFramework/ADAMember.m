//
//  ADAMember.m
//  ADAFramework
//
//  Created by Choldarong-r on 19/12/2559 BE.
//  Copyright © 2559 G-ABLE ITS. All rights reserved.
//

#import "ADAMember.h"
#import "AppHelper.h"

@implementation ADAMember


- (void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    
    if(self.roomID) {
        [encoder encodeObject:self.roomID forKey:@"roomID"];
    }
    if(self.roomName) {
        [encoder encodeObject:self.roomName forKey:@"roomName"];
    }
    if(self.imageID) {
        [encoder encodeObject:self.imageID forKey:@"imageID"];
    }
    if(self.memberType == ADAMemberTypeChatGroup) {
        [encoder encodeInteger:2 forKey:@"memberType"];
    }else{
        [encoder encodeInteger:1 forKey:@"memberType"];
    }
    
    if(self.userID) {
        [encoder encodeObject:self.userID forKey:@"userID"];
    }
    if(self.group) {
        [encoder encodeObject:self.group forKey:@"group"];
    }
    if(self.name) {
        [encoder encodeObject:self.name forKey:@"name"];
    }
    if(self.contactInfo) {
        [encoder encodeObject:self.contactInfo forKey:@"contactInfo"];
    }
    if(self.imageURL) {
        [encoder encodeObject:self.imageURL forKey:@"imageURL"];
    }
    if(self.userData) {
        [encoder encodeObject:self.userData forKey:@"userData"];
    }

}

- (void)setCurrentLanguage {
    if(self.userData) {
        
        self.name = [[[AppHelper getLabelForThai: [NSString stringWithFormat:@"%@ %@",
                                                   [self.userData valueForKey:@"firstname_th"],
                                                   [self.userData valueForKey:@"lastname_th"]]
                                             eng: [NSString stringWithFormat:@"%@ %@",
                                                   [self.userData valueForKey:@"firstname_en"],
                                                   [self.userData valueForKey:@"lastname_en"]]] stringByReplacingOccurrencesOfString:@"null" withString:@"" ]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        self.contactInfo = [AppHelper getLabelForThai:[self.userData valueForKey:@"contact_display"]
                                                  eng:[self.userData valueForKey:@"contact_display_en"]];
    }
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        self.roomID = [decoder decodeObjectForKey:@"roomID"];
        self.roomName = [decoder decodeObjectForKey:@"roomName"];
        self.imageID = [decoder decodeObjectForKey:@"imageID"];
        NSInteger memberType = [decoder decodeIntegerForKey:@"memberType"];
        if(memberType == 2) {
            self.memberType = ADAMemberTypeChatGroup;
        }else{
            self.memberType = ADAMemberTypePeople;
        }
        
        
        
        self.userID = [decoder decodeObjectForKey:@"userID"];
        self.group = [decoder decodeObjectForKey:@"group"];
        self.name = [decoder decodeObjectForKey:@"name"];
        self.contactInfo = [decoder decodeObjectForKey:@"contactInfo"];
        self.imageURL = [decoder decodeObjectForKey:@"imageURL"];
        self.userData = [decoder decodeObjectForKey:@"userData"];
        
        //[self setCurrentLanguage];
    }
    return self;
}


+ (ADAMember*) withDictionary:(NSDictionary<NSString*, id>* _Nonnull) member {
    ADAMember *m = [ADAMember new];
    m.memberType = ADAMemberTypePeople;
    m.name = [[[AppHelper getLabelForThai: [NSString stringWithFormat:@"%@ %@",
                                            [member valueForKey:@"firstname_th"],
                                            [member valueForKey:@"lastname_th"]]
                                      eng: [NSString stringWithFormat:@"%@ %@",
                                            [member valueForKey:@"firstname_en"],
                                            [member valueForKey:@"lastname_en"]]] stringByReplacingOccurrencesOfString:@"null" withString:@"" ]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    m.userID = [member valueForKey:@"user_id"];
    m.group = [member valueForKey:@"user_group"];
    m.imageURL = [member valueForKey:@"user_image"];
    if([member valueForKey:@"contact_cover_image"] && [[member valueForKey:@"contact_cover_image"] rangeOfString:@":contact-cover"].location == NSNotFound) {
        [member setValue:[NSString stringWithFormat:@"%@:contact-cover", [member valueForKey:@"contact_cover_image"]] forKey:@"contact_cover_image"];
    }
    m.contactInfo = [AppHelper getLabelForThai:[member valueForKey:@"contact_display"] eng:[member valueForKey:@"contact_display_en"]];
    m.userData = member;
    return  m;
}

@end
