//
//  PushNofication.m
//  ADAFramework
//
//  Created by Choldarong-r on 17/7/2560 BE.
//  Copyright © 2560 G-ABLE ITS. All rights reserved.
//

#import "ADANotification.h"
#import "AppHelper.h"

@implementation ADANotification

- (instancetype)init {
    if (self = [super init]) {
        self.messageID = [[NSDate date] timeIntervalSince1970];
        self.date = [NSDate date];
        self.sound = @"default";
    }
    return self;
}

- (ADANotification* _Nonnull) initWith:(NSDictionary *)userInfo {
    if (self = [self init]) {
        
        NSDictionary *ui = [userInfo valueForKey:@"userInfo"];
        NSDictionary *aps = [ui valueForKey:@"aps"];
        self.userInfo = ui;
        if([[aps valueForKey:@"alert"] isKindOfClass:[NSDictionary class]]) {
            self.message = [[aps valueForKey:@"alert"] valueForKey:@"body"];
        }else{
            self.message = [NSString stringWithFormat:@"%@", [aps valueForKey:@"alert"]];
        }
        self.badge = [[aps valueForKey:@"badge"] integerValue];
        self.sound = [aps valueForKey:@"sound"];
        if([ui valueForKey:@"messageType"]){
            self.messageType = [ui valueForKey:@"messageType"];
            self.featureType = [Feature typeFromString:self.messageType];
        }else if([ui valueForKey:@"shuttlebus@ada"]){
            self.messageType = @"bus";
            self.featureType = [Feature typeFromString:self.messageType];
        }
        if([ui valueForKey:@"json"]){
            self.json = [AppHelper jsonWithData:[[ui valueForKey:@"json"] dataUsingEncoding:NSUTF8StringEncoding]];
        }

    }
    return self;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"id: %ld\ndate: %@\nuserInfo: %@\nmessage: %@\nbadge %ld\nsound: %@\nmessageType: %@\njson: %@\n", self.messageID, self.date, self.userInfo, self.message, (long)self.badge, self.sound, self.messageType, self.json];
}

- (void)encodeWithCoder:(NSCoder *)coder
{
    [coder encodeInteger:_messageID forKey:@"messageID"];
    [coder encodeObject:_message forKey:@"message"];
    [coder encodeInteger:_badge forKey:@"badge"];
    [coder encodeObject:_sound forKey:@"sound"];
    [coder encodeObject:_messageType forKey:@"messageType"];
    [coder encodeObject:_json forKey:@"json"];
    [coder encodeObject:_userInfo forKey:@"userInfo"];
    [coder encodeBool:_foregroundMessage forKey:@"foregroundMessage"];
    [coder encodeInteger:_date.timeIntervalSince1970 forKey:@"date"];
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    if (self = [super init]) {
        self.messageID = [coder decodeIntegerForKey:@"messageID"];
        if([[coder decodeObjectForKey:@"message"] isKindOfClass:[NSDictionary class]]) {
            self.message = [[coder decodeObjectForKey:@"message"] valueForKey:@"body"];
        }else{
            self.message = [NSString stringWithFormat:@"%@", [coder decodeObjectForKey:@"message"]];
        }
        self.badge = [coder decodeIntegerForKey:@"badge"];
        self.sound = [coder decodeObjectForKey:@"sound"];
        self.messageType = [coder decodeObjectForKey:@"messageType"];
        self.featureType = [Feature typeFromString:self.messageType];
        self.json = [coder decodeObjectForKey:@"json"];
        self.userInfo = [coder decodeObjectForKey:@"userInfo"];
        self.foregroundMessage = [coder decodeBoolForKey:@"foregroundMessage"];
        if([coder decodeIntegerForKey:@"date"]){
            self.date = [NSDate dateWithTimeIntervalSince1970:[coder decodeIntegerForKey:@"date"]];
        }
    }
    return self;
}

@end
