//
//  ChatMessage.m
//  ADAFramework
//
//  Created by Choldarong-r on 4/12/2559 BE.
//  Copyright © 2559 G-ABLE ITS. All rights reserved.
//
#import "ADA.h"
#import "ChatMessage.h"
#import "NSDateFormatter+Extend.h"

@implementation ChatMessage





- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (id)copyWithZone:(NSZone *)zone {
    ChatMessage *obj = [ChatMessage new];
    
    obj.jsonString = self.jsonString;
    obj.roomID = self.roomID;
    obj.message = self.message;
    obj.json = self.json;
    obj.messageID = self.messageID;
    obj.ownMessage = self.ownMessage;
    obj.messageType = self.messageType;
    obj.messageDate = self.messageDate;
    obj.messageTime = self.messageTime;
    obj.receivedDate = self.receivedDate;
    obj.senderID = self.senderID;
    obj.senderName = self.senderName;
    obj.senderImageID = self.senderImageID;
    obj.imageURL = self.imageURL;
    obj.senderGroup = self.senderGroup;
    obj.readUsers = self.readUsers;
    obj.readUsersCount = self.readUsersCount;
    
    obj.imageMessage = self.imageMessage;
    obj.fileMessage = self.fileMessage;
    obj.videoMessage = self.videoMessage;
    obj.locationMessage = self.locationMessage;
    obj.stickerMessage = self.stickerMessage;
    obj.voteMessage = self.voteMessage;
    obj.seq = self.seq;
    
    return obj;
}

- (void)encodeWithCoder:(NSCoder *)coder {
    [coder encodeObject:_jsonString forKey:@"jsonString"];
}

- (instancetype)initWithCoder:(NSCoder *)coder {
    if (self = [super init]) {
        self.jsonString = [coder decodeObjectForKey:@"jsonString"];
        NSJSONSerialization *json = [AppHelper jsonWithData:[self.jsonString dataUsingEncoding:NSUTF8StringEncoding]];
        [ChatMessage init:self FromJSON:json];
    }
    return self;
}


+ (void)init:(ChatMessage*) message FromJSON:(NSJSONSerialization *)json {
    NSString *usid = [json valueForKey:@"user_id"];
    NSString *group = [json valueForKey:@"user_group"];
    
    NSDateFormatter *df, *df2, *df3, *dateParser;
    df = [NSDateFormatter instanceWithUSLocale];
    [df setDateFormat:@"dd-MM-yyyy"];
    df2 = [[NSDateFormatter alloc] init];
    [df2 setDateFormat:@"d MMMM yyyy  "];
    
    
    df3 = [NSDateFormatter instanceWithUSLocale];
    [df3 setDateFormat:@"HH:mm"];
    
    dateParser = [NSDateFormatter instanceWithUSLocale];
    [dateParser setDateFormat:@"dd-MM-yyyy HH:mm"];
    
    NSString *msgID = [NSString stringWithFormat:@"%@",[json valueForKey:kMessageID]];
    NSString *roomID = [json valueForKey:kRoomID];
    ChatMessage *m = message;
    m.jsonString = [AppHelper stringWithJSON:json];
    m.seq = [[json valueForKey:@"stanzaID"] longValue];
    m.imageURL = [json valueForKey:@"image_url"];
    if ([[json valueForKey:kReadStatus] boolValue]) {
        
        NSString *msgFrom = ([[json valueForKey:@"senderID"] isEqualToString:usid] &&
                             [[json valueForKey:@"groupID"] isEqualToString:group]) ? @"You" :
        [json valueForKey:@"sender"];
   
        m.senderName = msgFrom;
        m.senderID = [json valueForKey:kSender];
        m.senderGroup = [json valueForKey:kGroupID];
        m.messageID = [json valueForKey:kMessageID];
        m.messageDate = [json valueForKey:kDate];
        m.messageTime = [json valueForKey:kTime];
        m.ownMessage = [@"You" isEqualToString:msgFrom];
        m.messageType = ChatMessageTypeReadMessage;
        m.messageID = msgID;
        m.roomID = roomID;
     
        return;
    } else if([[json valueForKey:kBroadCast] boolValue]) {
        
        
        NSString *yourMessage = [AppHelper getLabelForThai:@"คุณ" eng:@"You"];
        NSString *msgFrom = ([[json valueForKey:@"senderID"] isEqualToString:usid] &&
                             [[json valueForKey:@"groupID"] isEqualToString:group]) ? yourMessage :
        [json valueForKey:@"sender"];
        
        NSString *lastMsg = [json valueForKey:kMessage];
        if([[json valueForKey:kMessage] isKindOfClass:[NSDictionary class]]) {
            lastMsg = [AppHelper stringWithJSON:[json valueForKey:kMessage]];
        }
        ChatMessageType msgType = ChatMessageTypeText;
        
        if ([lastMsg rangeOfString:@"voteID"].location != NSNotFound) {
            msgType = ChatMessageTypeVote;

            NSJSONSerialization *voteJSON = [AppHelper jsonWithData:[lastMsg dataUsingEncoding:NSUTF8StringEncoding]];
            VoteMessage *voteMessage = [VoteMessage new];
            voteMessage.voteID = [[voteJSON valueForKey:@"voteID"] longValue];
            voteMessage.title = [voteJSON valueForKey:@"title"];
            NSDateFormatter *formatter = [NSDateFormatter instanceWithUSLocale];
            formatter.dateFormat =  @"dd/MM/yyyy HH:mm:ss";
            voteMessage.endDate = [formatter dateFromString:[voteJSON valueForKey:@"endDate"]];//[[voteJSON valueForKey:@"voteID"] longValue];
            m.voteMessage = voteMessage;
        }else if ([lastMsg rangeOfString:@"messageImageID"].location != NSNotFound) {
            msgType = ChatMessageTypeImage;
            NSJSONSerialization *imgJSON = [AppHelper jsonWithData:[lastMsg dataUsingEncoding:NSUTF8StringEncoding]];
            ImageMessage *imgMessage = [ImageMessage new];
            imgMessage.width = [[imgJSON valueForKey:@"width"] floatValue];
            imgMessage.height = [[imgJSON valueForKey:@"height"] floatValue];
            
            imgMessage.contentType = [imgJSON valueForKey:@"type"];
            
            if([imgJSON valueForKey:@"src"]){
                imgMessage.sourceCamera = [@"camera" isEqualToString:[imgJSON valueForKey:@"src"]];
            }
            
            NSInteger imageOrientation = [[imgJSON valueForKey:@"messageImageOr"] integerValue];
            if(imgMessage.sourceCamera){
                
                /*
                 imgMessage.sizeRatio = imgMessage.width/imgMessage.height;
                 imgMessage.imageID = [[[[imgJSON valueForKey:@"messageImageID"] stringByAppendingString:@"@"] stringByAppendingString:[NSString stringWithFormat:@"%@", [imgJSON valueForKey:@"messageImageOr"]]] stringByAppendingString:[NSString stringWithFormat:@"#%f,%f", imgMessage.height, imgMessage.width]];
                 */
                if (imageOrientation != UIImageOrientationUp && imageOrientation != UIImageOrientationDown) {
                    
                    imgMessage.sizeRatio = imgMessage.height/imgMessage.width;
                    imgMessage.imageID = [[[[imgJSON valueForKey:@"messageImageID"] stringByAppendingString:@"@"] stringByAppendingString:[NSString stringWithFormat:@"%@", [imgJSON valueForKey:@"messageImageOr"]]] stringByAppendingString:[NSString stringWithFormat:@"#%f,%f", imgMessage.height, imgMessage.width]];
                    imgMessage.width = [[imgJSON valueForKey:@"height"] floatValue];
                    imgMessage.height = [[imgJSON valueForKey:@"width"] floatValue];
                }else{
                    imgMessage.sizeRatio = imgMessage.width/imgMessage.height;
                    imgMessage.imageID = [[[[imgJSON valueForKey:@"messageImageID"] stringByAppendingString:@"@"] stringByAppendingString:[NSString stringWithFormat:@"%@", [imgJSON valueForKey:@"messageImageOr"]]] stringByAppendingString:[NSString stringWithFormat:@"#%f,%f", imgMessage.height, imgMessage.width]];
                }
            }else{
                
                if (imageOrientation != UIImageOrientationUp && imageOrientation != UIImageOrientationDown) {
                    
                    imgMessage.sizeRatio = imgMessage.height/imgMessage.width;
                    imgMessage.imageID = [[[[imgJSON valueForKey:@"messageImageID"] stringByAppendingString:@"@"] stringByAppendingString:[NSString stringWithFormat:@"%@", [imgJSON valueForKey:@"messageImageOr"]]] stringByAppendingString:[NSString stringWithFormat:@"#%f,%f", imgMessage.height, imgMessage.width]];
                    imgMessage.width = [[imgJSON valueForKey:@"height"] floatValue];
                    imgMessage.height = [[imgJSON valueForKey:@"width"] floatValue];
                }else{
                    imgMessage.sizeRatio = imgMessage.width/imgMessage.height;
                    imgMessage.imageID = [[[[imgJSON valueForKey:@"messageImageID"] stringByAppendingString:@"@"] stringByAppendingString:[NSString stringWithFormat:@"%@", [imgJSON valueForKey:@"messageImageOr"]]] stringByAppendingString:[NSString stringWithFormat:@"#%f,%f", imgMessage.height, imgMessage.width]];
                }
            }
            
            m.imageMessage = imgMessage;
            
        }else if ([lastMsg rangeOfString:@"locationDesc"].location != NSNotFound) {
            msgType = ChatMessageTypeLocation;
            NSJSONSerialization *locationJSON = [AppHelper jsonWithData:[lastMsg dataUsingEncoding:NSUTF8StringEncoding]];
            //NSLog(@"%@", locationJSON);
            LocationMessage *locationMessage = [LocationMessage new];
            locationMessage.placeName = [locationJSON valueForKey:@"locationName"];
            locationMessage.address = [locationJSON valueForKey:@"locationDesc"];
            locationMessage.latitude = [[locationJSON valueForKey:@"latitude"] floatValue];
            locationMessage.longitude = [[locationJSON valueForKey:@"longitude"] floatValue];
            m.locationMessage = locationMessage;
        }else if ([lastMsg rangeOfString:@"videoID"].location != NSNotFound) {
            msgType = ChatMessageTypeVideo;
            NSJSONSerialization *imgJSON = [AppHelper jsonWithData:[lastMsg dataUsingEncoding:NSUTF8StringEncoding]];
            VideoMessage *videoMessage = [VideoMessage new];
            videoMessage.videoID = [[imgJSON valueForKey:@"videoID"] stringByAppendingString:@":video"];
            videoMessage.contentType = [imgJSON valueForKey:@"type"];
            m.videoMessage = videoMessage;
        }else if ([lastMsg rangeOfString:@"$sticker:"].location != NSNotFound) {
            NSString *code = [lastMsg stringByReplacingOccurrencesOfString:kSearchMessageSticker withString:@""];
            msgType = ChatMessageTypeSticker;
            StickerMessage *stickerMessage = [StickerMessage new];
            stickerMessage.stickerID = code;
            m.stickerMessage = stickerMessage;
            
        }else if ([lastMsg rangeOfString:@"$reward:"].location != NSNotFound) {
            msgType = ChatMessageTypeRewardSticker;
            
            NSString *code = [lastMsg stringByReplacingOccurrencesOfString:kSearchMessageReward withString:@""];
            
            NSArray *reward = [code componentsSeparatedByString:@";"];
            
            if (!reward || reward.count == 1) {
                reward = [code componentsSeparatedByString:@":"];
            }
            NSString *type = reward.firstObject;
            NSString *point = [reward objectAtIndex:1];
            NSString *message = nil;
            if (reward.count > 2) {
                message = reward.lastObject;
            }
            
            StickerMessage *stickerMessage = [StickerMessage new];
            stickerMessage.stickerID = [type stringByAppendingString:@":sticker"];
            stickerMessage.point = point;
            stickerMessage.message = message;
            m.stickerMessage = stickerMessage;
        }else if ([lastMsg rangeOfString:@"fileID"].location != NSNotFound) {
            msgType = ChatMessageTypeFile;
            NSJSONSerialization *imgJSON = [AppHelper jsonWithData:[lastMsg dataUsingEncoding:NSUTF8StringEncoding]];
            FileMessage *fileMessage = [FileMessage new];
            fileMessage.fileID = [[imgJSON valueForKey:@"fileID"] stringByAppendingString:@":document"];
            fileMessage.contentType = [imgJSON valueForKey:@"type"];
            fileMessage.name = [imgJSON valueForKey:@"name"];
            fileMessage.fileSize = [imgJSON valueForKey:@"size"];
            m.fileMessage = fileMessage;
        }else if([self isJson:lastMsg]) {
            msgType = ChatMessageTypeJSON;
            m.json = [AppHelper jsonWithData:[lastMsg dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        m.messageID = msgID;
        m.senderName = msgFrom;
        m.senderID = [json valueForKey:kSender];
        m.senderGroup = [json valueForKey:kGroupID];
        if([json valueForKey:kImageID]) {
            if([[json valueForKey:kImageID] rangeOfString:@":"].location != NSNotFound) {
                m.senderImageID = [json valueForKey:kImageID];
            }else{
                m.senderImageID = [NSString stringWithFormat:@"%@:profilePicture", [json valueForKey:kImageID]];
            }
        }else{
            m.senderImageID = [NSString stringWithFormat:@"%@:profilePicture", [json valueForKey:kImageID]];
        }
        m.messageDate = [json valueForKey:kDate];
        m.messageTime = [json valueForKey:kTime];
        m.ownMessage = [yourMessage isEqualToString:msgFrom];
        m.messageType = msgType;
        m.message = [json valueForKey:kMessage];
        m.roomID = roomID;
        if([json valueForKey:kReadUser]){
            m.readUsers = [NSMutableSet setWithArray:[json valueForKey:kReadUser]];
            m.readUsersCount = (unsigned long)m.readUsers.count;
        }
        m.receivedDate = [dateParser dateFromString:[NSString stringWithFormat:@"%@ %@", m.messageDate, m.messageTime]];
        
        return;
    }
}

+ (ChatMessage*) instanceFromJSON:(NSJSONSerialization*) json with:(Login*) login {
    
    if(!login){
        return nil;
    }
    [json setValue:login.usid forKey:@"user_id"];
    [json setValue:login.group forKey:@"user_group"];
    ChatMessage *m = [ChatMessage new];
    [self init:m FromJSON:json];
    return m;
    
}

+ (BOOL) isJson: (NSString*) responseString {
    NSData *responseData=[responseString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSError *error;
    
    if([NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error] == nil){
        return NO;
    }
    
    return YES;
    
}

@end
