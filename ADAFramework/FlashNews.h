//
//  FlashNews.h
//  ADAFramework
//
//  Created by Choldarong-r on 9/12/2559 BE.
//  Copyright © 2559 G-ABLE ITS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FlashNews : NSObject

@property (nonatomic, assign) long newsID;
@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *detail;
@property (nonatomic, retain) NSString *author;
@property (nonatomic, retain) NSString *imageID;
@property (nonatomic, retain) NSString *startDate;
@property (nonatomic, assign) BOOL important;

@end
