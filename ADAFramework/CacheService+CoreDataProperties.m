//
//  CacheService+CoreDataProperties.m
//  ADAFramework
//
//  Created by Choldarong-r on 23/12/2559 BE.
//  Copyright © 2559 G-ABLE ITS. All rights reserved.
//

#import "CacheService+CoreDataProperties.h"

@implementation CacheService (CoreDataProperties)

+ (NSFetchRequest<CacheService *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"CacheService"];
}

@dynamic serviceData;
@dynamic serviceKey;
@dynamic user;

@end
