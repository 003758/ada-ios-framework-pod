//
//  VideoMessage.m
//  ADAFramework
//
//  Created by Choldarong-r on 13/12/2559 BE.
//  Copyright © 2559 G-ABLE ITS. All rights reserved.
//

#import "VideoMessage.h"
@import CoreData;
@import AVFoundation;
@import AVKit;
#import "NSDateFormatter+Extend.h"
@import MobileCoreServices;


@implementation VideoMessage

+ (NSArray<NSString*>*) mediaTypes {
    return @[(NSString *)kUTTypeMovie];
}

+ (void) instanceWithURL:(NSURL *)videoURL handler:(ChatVideoBlock) handler {
    //NSLog(@"%@", videoURL);
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:videoURL options:nil];
    
    // Create the composition and tracks
    AVMutableComposition *composition = [AVMutableComposition composition];
    AVMutableCompositionTrack *videoTrack = [composition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
    AVMutableCompositionTrack *audioTrack = [composition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
    NSArray *assetVideoTracks = [asset tracksWithMediaType:AVMediaTypeVideo];
    if (assetVideoTracks.count <= 0)
    {
        NSLog(@"Error reading the transformed video track");
        if(handler) {
            handler(nil);
        }
    }
    
    // Insert the tracks in the composition's tracks
    AVAssetTrack *assetVideoTrack = [assetVideoTracks firstObject];
    [videoTrack insertTimeRange:assetVideoTrack.timeRange ofTrack:assetVideoTrack atTime:CMTimeMake(0, 1) error:nil];
    [videoTrack setPreferredTransform:assetVideoTrack.preferredTransform];
    
    AVAssetTrack *assetAudioTrack = [[asset tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0];
    [audioTrack insertTimeRange:assetAudioTrack.timeRange ofTrack:assetAudioTrack atTime:CMTimeMake(0, 1) error:nil];
    
    
    //CGSize size = [assetVideoTrack naturalSize];
    //NSLog(@"size.width = %f size.height = %f", size.width, size.height);
    CGAffineTransform txf = [videoTrack preferredTransform];
    //NSLog(@"txf.a = %f txf.b = %f txf.c = %f txf.d = %f txf.tx = %f txf.ty = %f", txf.a, txf.b, txf.c, txf.d, txf.tx, txf.ty);
    
    int angle = txf.c == -1 ? 90 : txf.c == 1 ? -90 : txf.d == -1 ? 180 : 1 ;
    
    //NSLog(@"angle = %d", angle);
    // Export to mp4
    NSDateFormatter *videoDF = [NSDateFormatter instanceWithUSLocale];
    [videoDF setDateFormat:@"yyyyMMddHHmmss"];
    NSString *fileName = [NSString stringWithFormat:@"%@.mp4", [videoDF stringFromDate:[NSDate date]]];
    NSString *mp4Quality = AVAssetExportPresetMediumQuality;
    NSString *exportPath = [NSString stringWithFormat:@"%@/%@",
                            [NSHomeDirectory() stringByAppendingString:@"/tmp"],
                            fileName];
    
    
    NSURL *exportUrl = [NSURL fileURLWithPath:exportPath];
    AVAssetExportSession *exportSession = [[AVAssetExportSession alloc] initWithAsset:composition presetName:mp4Quality];
    exportSession.outputURL = exportUrl;
    CMTime start = CMTimeMakeWithSeconds(0.0, 0);
    CMTimeRange range = CMTimeRangeMake(start, [asset duration]);
    exportSession.timeRange = range;
    exportSession.outputFileType = AVFileTypeMPEG4;
    
    VideoMessage *message = [VideoMessage new];
    
    [exportSession exportAsynchronouslyWithCompletionHandler:^{
        switch ([exportSession status])
        {
            case AVAssetExportSessionStatusCompleted:
                NSLog(@"MP4 Successful!");
            {
                message.filePath = exportPath;
                message.fileName = fileName;
                message.angle = angle;
                message.contentType = @"video/mp4";
                if(handler) {
                    handler(message);
                }
            }
                break;
            case AVAssetExportSessionStatusFailed:
                NSLog(@"Export failed: %@", [[exportSession error] localizedDescription]);
                if(handler) {
                    handler(nil);
                }
                break;
            case AVAssetExportSessionStatusCancelled:
                NSLog(@"Export canceled");
                if(handler) {
                    handler(nil);
                }
                break;
            default:
                if(handler) {
                    handler(nil);
                }
                break;
        }

    }];
    

}

@end
