//
//  ChatMessage.h
//  ADAFramework
//
//  Created by Choldarong-r on 4/12/2559 BE.
//  Copyright © 2559 G-ABLE ITS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Login.h"
#import "ImageMessage.h"
#import "FileMessage.h"
#import "VideoMessage.h"
#import "StickerMessage.h"
#import "LocationMessage.h"
#import "VoteMessage.h"


@interface ChatMessage : NSObject<NSCopying>
    
    typedef NS_ENUM(NSInteger, ChatMessageType) {
        //typedef enum ChatMessageType : NSUInteger {
        ChatMessageTypeText            = 1,
        ChatMessageTypeSticker         = 2,
        ChatMessageTypeRewardSticker   = 3,
        ChatMessageTypeLocation        = 4,
        ChatMessageTypeImage           = 5,
        ChatMessageTypeFile            = 6,
        ChatMessageTypeVideo           = 7,
        ChatMessageTypeReadMessage     = 8,
        ChatMessageTypeJSON            = 9,
        ChatMessageTypeVote     = 10
    };

    @property (nonatomic, retain) NSString *jsonString;
    @property (nonatomic, retain) NSString *roomID;
    @property (nonatomic, retain) NSString *message;
    @property (nonatomic, retain) NSDictionary<NSString *, id> *json;
    @property (nonatomic, retain) NSString *messageID;
    @property (nonatomic, assign) BOOL ownMessage;
    @property (nonatomic, assign) ChatMessageType messageType;
    @property (nonatomic, retain) NSString *messageDate;
    @property (nonatomic, retain) NSString *messageTime;
    @property (nonatomic, retain) NSDate *receivedDate;
    @property (nonatomic, retain) NSString *senderID;
    @property (nonatomic, retain) NSString *senderName;
    @property (nonatomic, retain) NSString *senderImageID;
    @property (nonatomic, retain) NSString *imageURL;
    @property (nonatomic, retain) NSString *senderGroup;
    @property (nonatomic, strong) NSSet<NSString*> *readUsers;
    @property (nonatomic, assign) unsigned long readUsersCount;
    
    @property (nonatomic, retain) ImageMessage *imageMessage;
    @property (nonatomic, retain) FileMessage *fileMessage;
    @property (nonatomic, retain) VideoMessage *videoMessage;
    @property (nonatomic, retain) LocationMessage *locationMessage;
    @property (nonatomic, retain) StickerMessage *stickerMessage;
    @property (nonatomic, retain) VoteMessage *voteMessage;
    @property (nonatomic, assign) long seq;
    
+ (ChatMessage*) instanceFromJSON:(NSJSONSerialization*) json with:(Login*) login;
    
    
    @end
