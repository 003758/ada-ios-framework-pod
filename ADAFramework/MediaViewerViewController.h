//
//  MediaViewerViewController.h
//  ADAFramework
//
//  Created by Choldarong-r on 26/7/2560 BE.
//  Copyright © 2560 G-ABLE ITS. All rights reserved.
//

#import <UIKit/UIKit.h>
@import QuickLook;
#import "ADA.h"
//#import "ADAFramework.h"

@interface MediaViewerViewController : QLPreviewController


- (instancetype)initWith:(Media*) media;
- (instancetype)initWithArray:(NSArray<Media*>*) media;


@property (nonatomic , retain) Media* media;
@property (nonatomic , retain) NSArray<Media*>* mediaList;
@property (nonatomic , assign) NSInteger previewItemIndex;

- (void) setProgressHandler:(ProgressBlock) handler;

@end
