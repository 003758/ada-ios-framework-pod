//
//  ShareYourThoughtCategory.h
//  ADAFramework
//
//  Created by Choldarong-r on 13/7/2560 BE.
//  Copyright © 2560 G-ABLE ITS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ShareYourThoughtCategory : NSObject
@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *icon;
@property (nonatomic, retain) NSString *titleTH;
@property (nonatomic, retain) NSString *titleEN;

@end
