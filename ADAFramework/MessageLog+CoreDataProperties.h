//
//  MessageLog+CoreDataProperties.h
//  ADAFramework
//
//  Created by Choldarong-r on 14/12/2560 BE.
//  Copyright © 2560 G-ABLE Company Limited. All rights reserved.
//
//

#import "MessageLog+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface MessageLog (CoreDataProperties)

+ (NSFetchRequest<MessageLog *> *)fetchRequest;

@property (nullable, nonatomic, retain) NSData *message;
@property (nullable, nonatomic, copy) NSDate *msgDate;
@property (nullable, nonatomic, copy) NSString *msgID;
@property (nonatomic) BOOL readStatus;
@property (nullable, nonatomic, copy) NSString *roomID;
@property (nonatomic) int64_t seq;
@property (nullable, nonatomic, retain) ADAUser *user;

@end

NS_ASSUME_NONNULL_END
