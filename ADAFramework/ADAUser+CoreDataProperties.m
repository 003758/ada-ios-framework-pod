//
//  ADAUser+CoreDataProperties.m
//  ADAFramework
//
//  Created by Choldarong-r on 23/12/2559 BE.
//  Copyright © 2559 G-ABLE ITS. All rights reserved.
//

#import "ADAUser+CoreDataProperties.h"

@implementation ADAUser (CoreDataProperties)

+ (NSFetchRequest<ADAUser *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"ADAUser"];
}

@dynamic userGroup;
@dynamic usid;
@dynamic messages;
@dynamic cache;

@end
