//
//  MeetingRoom.h
//  ADAFramework
//
//  Created by Choldarong-r on 19/7/2561 BE.
//  Copyright © 2561 G-ABLE Company Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Building.h"
//#import "ReserveRoom.h"

@interface MeetingRoom : NSObject

@property (nonatomic, assign) NSInteger roomID;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *detail;
@property (nonatomic, assign) NSInteger numberOfSeat;
@property (nonatomic, retain) NSString *assetCode;
@property (nonatomic, retain) NSString *floorCode;
@property (nonatomic, retain) NSString *floorName;
@property (nonatomic, retain) NSString *floorPlanURL;
@property (nonatomic, retain) Building *building;
//@property (nonatomic, retain) NSArray<ReserveRoom*> *reservedRooms;

@end
