//
//  FeatureGroup.h
//  ADAFramework
//
//  Created by Choldarong-r on 9/6/2560 BE.
//  Copyright © 2560 G-ABLE ITS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Feature.h"

@interface FeatureGroup : NSObject

@property (nonatomic, retain) NSString      *groupName;
@property (nonatomic, retain) NSString      *groupDescription;
@property (nonatomic, retain) NSString      *coverImage;
@property (nonatomic, retain) NSArray<Feature*>       *features;
@end
