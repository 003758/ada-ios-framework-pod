//
//  ChatViewController.m
//  ADAFramework
//
//  Created by Choldarong-r on 5/12/2559 BE.
//  Copyright © 2559 G-ABLE ITS. All rights reserved.
//
@import XMPPFramework;
@import CoreData;
@import AFNetworking;
@import AVFoundation;
@import AVKit;
@import QuickLook;
#import <CoreMedia/CoreMedia.h>
#import <CoreVideo/CoreVideo.h>
#import "UIImage+Extend.h"
#import "NSData+Base64.h"
#import "URLConnection.h"
#import "ServiceCaller.h"
#import "ADAChatViewController.h"
#import "ServiceCaller.h"
#import "ChatRoom.h"
#import "ADAMember.h"
#import "AppHelper.h"
#import "NSDateFormatter+Extend.h"
#import "ManagedObjectContextFactory.h"
#import "ADAUser+CoreDataClass.h"
#import "MessageLog+CoreDataClass.h"
#import "StickerViewController.h"
#import "RewardStickerViewController.h"
#import "ChatTapGestureRecognizer.h"
#import "ADAAppDelegate.h"

@interface ADAChatViewController () <XMPPStreamDelegate, XMPPRoomDelegate,
    AVPlayerViewControllerDelegate,
    QLPreviewControllerDelegate, QLPreviewControllerDataSource,
    UINavigationControllerDelegate, UIImagePickerControllerDelegate,
    UIDocumentMenuDelegate, UIDocumentPickerDelegate,
    StickerViewDelegate, RewardStickerViewDelegate>

@property (nonatomic, retain) NSMutableArray *stickerSet;
@property (nonatomic, retain) AVPlayer *player;
@property (nonatomic, retain) NSMutableDictionary<NSString*, NSString*> *senderNameMap;
@property (nonatomic, retain) NSMutableDictionary<NSString*, NSString*> *senderImageMap;
//@property (nonatomic, retain) ChatRoom *room;
@end

@implementation ADAChatViewController {
    XMPPStream* chatStream;
    XMPPRoom *xmppRoom;
    NSMutableArray *usersOnline;
    NSMutableArray *allMessages;
    NSMutableArray<MessageLog*> *userMessages;
    NSMutableSet<NSString*> *receivedMsgSet;
    NSString *xmppRoomID;
    BOOL skipLeaveConnection;
    BOOL roomEntered;
    id unsendData;
    int maxMessage;
    NSString *currentDate;
    NSDateFormatter *df, *df2, *df3, *dateParser;
    NSMutableSet *chkSet;
    dispatch_queue_t chatQueue;
    ADA *ada;
    ADAUser *user;
    NSString *msgSent;
    ChatMessage *currentChatMessage;
    ChatImageBlock imageHandler;
    ChatVideoBlock videoHandler;
    ChatFileBlock fileHandler;
    ChatStickerBlock stickerHandler;
    ProgressBlock progressHandler;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    self->maxMessage = 50;
    self->ada = ((ADAAppDelegate*)[UIApplication sharedApplication].delegate).ada;
    // Do any additional setup after loading the view.
    self.senderNameMap = [[NSMutableDictionary<NSString*, NSString*> alloc] init];
    self.senderImageMap = [[NSMutableDictionary<NSString*, NSString*> alloc] init];
    self->userMessages = [[NSMutableArray alloc] init];
    self->allMessages = [[NSMutableArray alloc] init];
    self->usersOnline = [[NSMutableArray alloc] init];
    self->receivedMsgSet = [[NSMutableSet<NSString*> alloc] init];
    self->chkSet = [[NSMutableSet alloc] init];
    
    df = [NSDateFormatter instanceWithUSLocale];
    [df setDateFormat:@"dd-MM-yyyy"];
    df2 = [[NSDateFormatter alloc] init];
    [df2 setDateFormat:@"d MMMM yyyy  "];
    currentDate  = [df2 stringFromDate:[NSDate date]];
    
    df3 = [NSDateFormatter instanceWithUSLocale];
    [df3 setDateFormat:@"HH:mm"];
    
    dateParser = [NSDateFormatter instanceWithUSLocale];
    [dateParser setDateFormat:@"dd-MM-yyyy HH:mm"];
    
    if(self.room) {
        self->xmppRoomID = self.room.roomID;
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear: animated];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear: animated];
    
    
    
    self->skipLeaveConnection = NO;
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidBecomeActiveNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(initXMPP)
                                                name:UIApplicationDidBecomeActiveNotification
                                              object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationWillResignActiveNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(closeXMPPConnection)
                                                name:UIApplicationWillResignActiveNotification
                                              object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationWillTerminateNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(closeXMPPConnection)
                                                name:UIApplicationWillTerminateNotification
                                              object:nil];
    [self initXMPP];
}

- (void) initXMPP {
    
    //dispatch_queue_t queue = dispatch_get_main_queue(); //dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0ul);
    
    //dispatch_async(queue, ^{
        if(self->ada && self->xmppRoomID){
            if(!self->roomEntered){
                if(!self->user){
                    self->user = (ADAUser*)[ManagedObjectContextFactory getObjectOrNewEntityForName:@"ADAUser"
                                                                                    withValue:self->ada.login.usid
                                                                                 forFieldName:@"usid"];
                    self->user.usid = self->ada.login.usid;
                    self->user.userGroup = self->ada.login.group;
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [ManagedObjectContextFactory saveContext];
                    });
                }
                [self->userMessages removeAllObjects];
                NSArray *msgLogs = [self->user.messages.allObjects
                                    filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"roomID == %@", self->xmppRoomID]];
                [self->userMessages addObjectsFromArray:msgLogs];
                
                [self enterRoom:self.room queue:queue];
                self->roomEntered = YES;
                [self getLogFromLastMessage:^(BOOL success) {
                }];
            }else{
                [self connectXMPPConnection];
            }
            
            
        }
    //});
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear: animated];
    
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidBecomeActiveNotification
                                                  object:nil];

    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationWillResignActiveNotification
                                                  object:nil];

    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationWillTerminateNotification
                                                  object:nil];
    
    
    if(!self->skipLeaveConnection){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self->chatStream disconnect];
            [self->chatStream removeDelegate:self];
        });
        
    }
}



- (void) enterRoom:(ChatRoom*) chatRoom queue:(dispatch_queue_t) queue{
    if (!chatRoom || !self->ada) {
        return;
    }
    
    //self->chatQueue = dispatch_get_main_queue();//dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0);//queue ? queue : dispatch_get_main_queue();
    self->chatQueue = dispatch_queue_create("XMPP Handling", NULL);
    //self->chatQueue = queue;//dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0ul);
    self->xmppRoomID = chatRoom.roomID;
    [self->usersOnline removeAllObjects];
    [self->allMessages removeAllObjects];
    
    [self->chkSet removeAllObjects];
    
    //NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    //NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *kNotificationKey = [NSString stringWithFormat:@"%@-%@@notifications", self->ada.login.usid, self->ada.login.group];
    
    NSArray *notifications = (NSArray*)[AppHelper getUserDataForKey:kNotificationKey];
    if(notifications){
        NSMutableArray<NSData*> *arr = [NSMutableArray arrayWithArray:notifications];
        
        for(NSData *data in arr) {
            ADANotification *notification = [NSKeyedUnarchiver unarchiveObjectWithData:data];
            if(notification.json && (notification.featureType == FeatureTypeChat || notification.featureType == FeatureTypeChatAction)) {
                
                NSString *roomID = [NSString stringWithFormat:@"%@", [notification.json valueForKey:kRoomID]];
                if([roomID isEqualToString:self.room.roomID]) {
                    [self->ada removeNotifications:notification handler:nil];
                }
            }
        }
    }

    SuccessBlock handler = ^(BOOL success) {
        NSArray *msgLogs = self->userMessages;
        if(!msgLogs || msgLogs.count == 0){
            [self loadPreviousHistory:nil handler:^(BOOL success) {
                if (success) {
                    //NSLog(@"initial load history done");
                    [self loadLogDataWithScroll:NO];
                }
            }];
        }else{
            [self loadLogDataWithScroll:NO];
        }
    };
    
    NSMutableArray<NSString*> *names = [[NSMutableArray alloc] init];
    for (NSString *member in self.room.members) {
        NSString* m = [[member componentsSeparatedByString:@"@"].firstObject uppercaseString];
        [names addObject:m];
    }
    
    
    NSString *param = [names componentsJoinedByString:@","];
    [ServiceCaller getJSONCache:[NSString stringWithFormat:@"%@user/profile-list",
                             kSERVER]
                     method:@"GET"
                     header:nil
                  parameter:@{@"users": param}
                    handler:^(NSJSONSerialization *json) {
                        BOOL foundCache = NO;
                        
                        if([[json valueForKey:@"result"] boolValue]){
                            for(NSJSONSerialization *user in [json valueForKey:@"data"]) {
                                
                                NSString *name = [AppHelper getLabelForThai:[NSString stringWithFormat:@"%@", [user valueForKey:@"firstname_th"]]
                                                                        eng:[NSString stringWithFormat:@"%@", [user valueForKey:@"firstname_en"]]];
                                name = [[name stringByReplacingOccurrencesOfString:@"(null)" withString:@""] stringByTrimming];
                                NSString *kSenderKEY = [[NSString stringWithFormat:@"%@-%@",
                                                         [user valueForKey:@"user_id"],
                                                         [user valueForKey:@"user_group"]] uppercaseString];
                                
                                if([user valueForKey:@"user_image"]) {
                                    NSString *urlString = [user valueForKey:@"user_image"];
                                    [self.senderImageMap setValue:urlString
                                                           forKey:kSenderKEY];
                                }
                                NSString *nickname = [self->ada getContactNickname:user];
                                if(nickname){
                                    [self.senderNameMap setValue:nickname forKey:kSenderKEY];
                                }else{
                                    [self.senderNameMap setValue:name forKey:kSenderKEY];
                                }
                            }
                            
                            for (ChatMessage *m in self->allMessages) {
                                NSString *kSenderKEY = [[NSString stringWithFormat:@"%@-%@", m.senderID, m.senderGroup] uppercaseString];
                                
                                m.imageURL = [self.senderImageMap valueForKey:kSenderKEY];
                            }
                            handler(YES);
                            foundCache = YES;
                        }
                        
    
                        [ServiceCaller callJSON:[NSString stringWithFormat:@"%@user/profile-list",
                                                 kSERVER]
                                         method:@"GET"
                                         header:nil
                                      parameter:@{@"users": param}
                                        handler:^(NSJSONSerialization *json) {
                                            
                                            if([[json valueForKey:@"result"] boolValue]){
                                                for(NSJSONSerialization *user in [json valueForKey:@"data"]) {
                                                    NSString *name = [AppHelper getLabelForThai:[NSString stringWithFormat:@"%@", [user valueForKey:@"firstname_th"]]
                                                                      
                                                                                            eng:[NSString stringWithFormat:@"%@", [user valueForKey:@"firstname_en"]]];
                                                    
                                                    name = [[name stringByReplacingOccurrencesOfString:@"(null)" withString:@""] stringByTrimming];
 
                                                    NSString *kSenderKEY = [[NSString stringWithFormat:@"%@-%@",
                                                                             [user valueForKey:@"user_id"],
                                                                             [user valueForKey:@"user_group"]] uppercaseString];
                                                    if([user valueForKey:@"user_image"]) {
                                                        NSString *urlString = [user valueForKey:@"user_image"];
                                                        [self.senderImageMap setValue:urlString
                                                                               forKey:kSenderKEY];
                                                    }
                                                    NSString *nickname = [self->ada getContactNickname:user];
                                                    if(nickname){
                                                        [self.senderNameMap setValue:nickname forKey:kSenderKEY];
                                                    }else{
                                                        [self.senderNameMap setValue:name forKey:kSenderKEY];
                                                    }
                                                    
                                                }
                                                
                                                for (ChatMessage *m in self->allMessages) {
                                                    NSString *kSenderKEY = [[NSString stringWithFormat:@"%@-%@", m.senderID, m.senderGroup] uppercaseString];
                                                    m.imageURL = [self.senderImageMap valueForKey:kSenderKEY];
                                                }
                                                if(!foundCache) {
                                                    handler(YES);
                                                }
                                                
                                            }
                                        }];
                    }];

    
    [self loadStickerSet:nil];
    [self connectXMPPConnection];
}

- (void) reloadHistory:(SuccessBlock) handler {
    [self loadHistoryFromWS:^(BOOL success) {
        if (success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self updateMessageState];
                [self loadLogDataWithScroll:NO];
            });
        }
        handler(success);
    }];
}


# pragma  mark - Send message

- (void) sendImage:(ImageMessage*) message progress:(ProgressBlock) progress {
    self->progressHandler = progress;
    NSData *imageData = UIImageJPEGRepresentation(message.image, 1.0);//UIImagePNGRepresentation(image);
    
    
    // 1. Create `AFHTTPRequestSerializer` which will create your request.
    AFHTTPRequestSerializer *serializer = [AFHTTPRequestSerializer serializer];
    
    // 2. Create an `NSMutableURLRequest`.
    NSString *strURL = [kSERVER_HOST stringByAppendingString:@"upload-file?uploadType=form"];
    
    
//    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.requestSerializer = serializer;
    manager.responseSerializer.acceptableContentTypes = nil;
    //[manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    manager.requestSerializer = serializer;
    [manager.requestSerializer setValue:[AppHelper getUserDataForKey:kADA_CLIENT] forHTTPHeaderField:kADA_CLIENT];
    [manager.requestSerializer setValue:@"multipart/form-data" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    
    [manager POST:strURL
       parameters:@{}
constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
    
    
    
    [formData appendPartWithFileData:imageData name:@"file" fileName:[@"image" stringByAppendingString:message.image.extension] mimeType:message.image.contentType];
}
         progress:^(NSProgress * _Nonnull uploadProgress) {
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 long writen = (long)((double)imageData.length * uploadProgress.fractionCompleted);
                 long total = (long)imageData.length;
                 if (progress) {
                     progress(writen, total);
                 }
                 
             });
         } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
             //NSLog(@"Success: %@", responseObject);
             [AppHelper logData:responseObject];
             NSJSONSerialization *json = [AppHelper jsonWithData:responseObject];
             
             NSString *jsonSend = [NSString stringWithFormat:@"{\"messageImageID\":\"%@\", \"messageImageOr\":%li, \"width\":%f, \"height\":%f, \"type\":\"%@\", \"src\":\"%@\"}",
                                   [json valueForKey:@"id"],
                                   (long)message.image.imageOrientation,
                                   message.image.size.width,
                                   message.image.size.height,
                                   [json valueForKey:@"type"],
                                   message.sourceCamera ? @"camera" : @""
                                   ];
             
             NSMutableDictionary *msg = [NSMutableDictionary dictionaryWithDictionary: @{
                                                                                         kMessage: jsonSend,
                                                                                         kSender: self->ada.login.usid,
                                                                                         kRoomID: self->xmppRoomID
                                                                                         }];
             
             
             NSData *jsonData = [NSJSONSerialization dataWithJSONObject:msg
                                                                options:NSJSONWritingPrettyPrinted
                                                                  error: nil];
             NSString *jsonString = [[NSString alloc] initWithData:jsonData
                                                          encoding:NSUTF8StringEncoding];
             [self sendStreamMessage: jsonString];
             if(progress){
                 progress(imageData.length, imageData.length);
             }
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
             NSLog(@"Failure %@", error.description);
             if(progress){
                 progress(0,0);
             }
             
             
         }];
    

}

- (void) sendVideo:(VideoMessage*) message progress:(ProgressBlock) progress {
    self->progressHandler = progress;
    //unsigned long long fileSize = [[[NSFileManager defaultManager] attributesOfItemAtPath:message.filePath error:nil] fileSize];
    //NSLog(@"export path %@", message.filePath);
    //NSLog(@"export size %llu KB", fileSize / 1024);
    
    NSString *mp4Path = [message.filePath stringByReplacingOccurrencesOfString:@"file://" withString:@""];
    NSData *imageData = [[NSFileManager defaultManager] contentsAtPath:mp4Path];//UIImagePNGRepresentation(image);
    
    
    
    
    // 1. Create `AFHTTPRequestSerializer` which will create your request.
    AFHTTPRequestSerializer *serializer = [AFHTTPRequestSerializer serializer];
    
    // 2. Create an `NSMutableURLRequest`.
    NSString *strURL = [kSERVER_HOST stringByAppendingString:@"upload-file?uploadType=form&mediaType=video"];
    strURL = [NSString stringWithFormat:@"%@&angle=%ld", strURL, (long)message.angle];
    
    
    //NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = nil;
    //[manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    manager.requestSerializer = serializer;
    [manager.requestSerializer setValue:[AppHelper getUserDataForKey:kADA_CLIENT] forHTTPHeaderField:kADA_CLIENT];
    
    [manager POST:strURL
       parameters:@{}
constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
    [formData appendPartWithFileData:imageData name:@"file" fileName:message.fileName mimeType:@"video/mp4"];
}
         progress:^(NSProgress * _Nonnull uploadProgress) {
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 long writen = (long)((double)imageData.length * uploadProgress.fractionCompleted);
                 long total = (long)imageData.length;
                 
                 if(progress){
                     progress(writen, total);
                 }
                 
             });
         } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
             //NSLog(@"Success: %@", responseObject);
             
             NSJSONSerialization *json = [AppHelper jsonWithData:responseObject];
             
             NSString *jsonSend = [NSString stringWithFormat:@"{\"videoID\":\"%@\", \"type\":\"%@\"}",
                                   [json valueForKey:@"id"],
                                   [json valueForKey:@"type"]
                                   ];
             
             NSMutableDictionary *msg = [NSMutableDictionary dictionaryWithDictionary: @{
                                                                                         kMessage: jsonSend,
                                                                                         kSender: self->ada.login.usid,
                                                                                         kRoomID: self->xmppRoomID
                                                                                         }];
             
             //NSLog(@"json %@", jsonSend);
             NSData *jsonData = [NSJSONSerialization dataWithJSONObject:msg
                                                                options:NSJSONWritingPrettyPrinted
                                                                  error: nil];
             NSString *jsonString = [[NSString alloc] initWithData:jsonData
                                                          encoding:NSUTF8StringEncoding];
             [self sendStreamMessage: jsonString];
             if(progress){
                 progress(imageData.length, imageData.length);
             }
             [[NSFileManager defaultManager] removeItemAtPath:mp4Path error:nil];
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
             NSLog(@"Failure %@", error.description);
             if(progress){
                 progress(0,0);
             }
             
             
         }];

}

- (void) sendFile:(FileMessage*) message progress:(ProgressBlock) progress {
    self->progressHandler = progress;
    
    unsigned long long fileSize = [[[NSFileManager defaultManager] attributesOfItemAtPath:message.filePath error:nil] fileSize];
    
    NSString *filePath = message.filePath;
    NSData *fileData = [[NSFileManager defaultManager] contentsAtPath:filePath];
    fileSize = fileData.length;
    if (fileSize == 0) {
        [AppHelper showErrorMessage:@"Cannot send this file" completion:nil];
        return;
    }
    
    NSArray *fileNames = [filePath componentsSeparatedByString:@"/"];
    NSString *fileName = fileNames.lastObject;
    NSString *fileType = [self mimeTypeForFileAtPath:filePath];
    
    
    // 1. Create `AFHTTPRequestSerializer` which will create your request.
    AFHTTPRequestSerializer *serializer = [AFHTTPRequestSerializer serializer];
    
    // 2. Create an `NSMutableURLRequest`.
    NSString *strURL = [kSERVER_HOST stringByAppendingString:@"upload-file?uploadType=form&mediaType=document"];
    strURL = [NSString stringWithFormat:@"%@", strURL];
    
    
    //NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = nil;
    //[manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    manager.requestSerializer = serializer;
    [manager.requestSerializer setValue:[AppHelper getUserDataForKey:kADA_CLIENT] forHTTPHeaderField:kADA_CLIENT];
    
    [manager POST:strURL
       parameters:@{}
constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
    [formData appendPartWithFileData:fileData name:@"file" fileName:fileName mimeType: fileType];
}
         progress:^(NSProgress * _Nonnull uploadProgress) {
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 long writen = (long)((double)fileData.length * uploadProgress.fractionCompleted);
                 long total = (long)fileData.length;
                 
                 if(progress){
                     progress(writen, total);
                 }
                 
             });
         } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
             //NSLog(@"Success: %@", responseObject);
             
             NSJSONSerialization *json = [AppHelper jsonWithData:responseObject];
             
             NSString *jsonSend = [NSString stringWithFormat:@"{\"fileID\":\"%@\", \"name\": \"%@\", \"type\":\"%@\", \"size\": \"%@\"}",
                                   [json valueForKey:@"id"],
                                   fileName,
                                   [json valueForKey:@"type"],
                                   [json valueForKey:@"size"]
                                   ];
             
             NSMutableDictionary *msg = [NSMutableDictionary dictionaryWithDictionary: @{
                                                                                         kMessage: jsonSend,
                                                                                         kSender: self->ada.login.usid,
                                                                                         kRoomID: self->xmppRoomID
                                                                                         }];
             
             
             NSData *jsonData = [NSJSONSerialization dataWithJSONObject:msg
                                                                options:NSJSONWritingPrettyPrinted
                                                                  error: nil];
             NSString *jsonString = [[NSString alloc] initWithData:jsonData
                                                          encoding:NSUTF8StringEncoding];
             [self sendStreamMessage: jsonString];
             if(progress){
                 progress(fileData.length, fileData.length);
             }
             [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
             NSLog(@"Failure %@", error.description);
             if(progress){
                 progress(0,0);
             }
             
             
         }];
    
}

- (void) sendSticker:(StickerMessage*) message {
    
    
    NSMutableDictionary *msg = nil;
    if (message.message || message.point) {
        msg = [NSMutableDictionary dictionaryWithDictionary: @{
                                                               kMessage: [NSString stringWithFormat:@"$reward:%@;%@;%@", message.stickerID, message.point, message.message],
                                                               kSender: self->ada.login.usid,
                                                               kRoomID: self->xmppRoomID
                                                               }];
    }else{
        
        
        msg = [NSMutableDictionary dictionaryWithDictionary: @{
                                                               kMessage: message.stickerID,
                                                               kSender: self->ada.login.usid,
                                                               kRoomID: self->xmppRoomID
                                                               }];
    }
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:msg
                                                       options:NSJSONWritingPrettyPrinted
                                                         error: nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData
                                                 encoding:NSUTF8StringEncoding];
    
    
    [self sendStreamMessage: jsonString];
}

- (void) sendVote:(VoteMessage*) message {
    NSDateFormatter *formatter = [NSDateFormatter instanceWithUSLocale];
    formatter.dateFormat =  @"dd/MM/yyyy HH:mm:ss";
    
    NSDictionary *dict = nil;
  
    dict = @{
             @"voteID" : [NSNumber numberWithLong:message.voteID],
             @"title" : message.title,
             @"endDate" : [formatter stringFromDate:message.endDate]
             };
    NSJSONSerialization *json = [AppHelper jsonWithdictionary:dict];
    NSString *jsonSend = [AppHelper stringWithJSON:json];
    NSMutableDictionary *msg = [NSMutableDictionary dictionaryWithDictionary: @{
                                                               kMessage: jsonSend,
                                                               kSender: self->ada.login.usid,
                                                               kRoomID: self->xmppRoomID
                                                               }];
    
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:msg
                                                       options:NSJSONWritingPrettyPrinted
                                                         error: nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData
                                                 encoding:NSUTF8StringEncoding];
    
    //NSLog(@"JSON %@", jsonString);
    [self sendStreamMessage: jsonString];
}


- (void) sendLocation:(LocationMessage*) message {

    
    NSString *jsonSend = [NSString stringWithFormat:@"{\"locationDesc\":\"%@\",\"locationName\":\"%@\",\"latitude\":%f,\"longitude\":%f}",
                          [message.address stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""],
                          [message.placeName stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""],
                          message.latitude,
                          message.longitude
                          ];
    
    NSMutableDictionary *msg = [NSMutableDictionary dictionaryWithDictionary: @{
                                                                                kMessage: jsonSend,
                                                                                kSender: self->ada.login.usid,
                                                                                kRoomID: self->xmppRoomID
                                                                                }];
    
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:msg
                                                       options:kNilOptions
                                                         error: nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData
                                                 encoding:NSUTF8StringEncoding];
    
    
    self->msgSent = jsonString;
    //NSLog(@"%@", jsonString);
    [self sendStreamMessage: jsonString];
}



- (void) sendPlainChatMessage:(ChatMessage*) message {
    
    //NSLog(@"sendPlainChatMessage");
    if (!chatStream) {
//        NSLog(@"No Connection");
        self->unsendData = message;
        [self connectXMPPConnection];
        return;
        
    }
    
    NSString *usid = self->ada.login.usid;
    NSString *userGroup = self->ada.login.group;
    NSString *userName = self->ada.login.userName;
    NSString *imageID = self->ada.login.imageID;
    NSDateFormatter *dfMsgID = [NSDateFormatter instanceWithUSLocale];
    [dfMsgID setDateFormat:@"yyyyMMddHHmmssSSS"];
    NSString *msgID = [NSString stringWithFormat:@"%@-%@%@",
                       usid,
                       userGroup,
                       [dfMsgID stringFromDate:[NSDate date]]];
    
    NSDictionary *msg = nil;
    //BOOL sendReadStatus = NO;
  
    if(imageID){
        
        msg = @{
                kMessageID: msgID,
                kMessage: message.message,
                kSender: usid,
                kSenderName: userName,
                kGroupID: userGroup,
                kImageID: imageID,
                kRoomID: self->xmppRoomID,
                kTime: [df3 stringFromDate:[NSDate date]],
                kDate: [df stringFromDate:[NSDate date]],
                kBroadCast: [NSNumber numberWithBool:YES]
                };
    }else{
        msg = @{
                kMessageID: msgID,
                kMessage: message.message,
                kSender: usid,
                kSenderName: userName,
                kGroupID: userGroup,
                kRoomID: self->xmppRoomID,
                kTime: [df3 stringFromDate:[NSDate date]],
                kDate: [df stringFromDate:[NSDate date]],
                kBroadCast: [NSNumber numberWithBool:YES]
                };
    }
    
    
    

    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:msg
                                                       options:NSJSONWritingPrettyPrinted
                                                         error: nil];
    
    NSString *jsonString = [[NSString alloc] initWithData:jsonData
                                                 encoding:NSUTF8StringEncoding];
    
    if([msg valueForKey:kBroadCast]){
        //self->msgSent = jsonString;
        NSJSONSerialization *json = [AppHelper jsonWithData:jsonData];
        self->msgSent = toJSONString(json);
    }
    NSXMLElement *body = [NSXMLElement elementWithName:@"body" stringValue:jsonString];
    XMPPMessage *messageX = [XMPPMessage message];
    [messageX addChild:body];
    [xmppRoom sendMessage:messageX];
    
    //[socket send:data];
}


- (void) getAllVideo:(ChatMessageBlock) handler {
    NSArray *messages = [self->allMessages filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"messageType == %d", ChatMessageTypeVideo]];
    NSMutableArray *result = [[NSMutableArray alloc] init];
    NSMutableSet<NSString*> *chkMsg = [[NSMutableSet alloc] init];
    for (ChatMessage *message in messages) {
        if(![chkMsg containsObject:message.messageID]) {
            [result addObject:message];
            [chkMsg addObject:message.messageID];
        }
    }
    
    [result sortUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"receivedDate" ascending:NO]]];
    
    if(handler){
        handler(result, nil);
    }
}
- (void) getAllImages:(ChatMessageBlock) handler {
    NSArray *messages = [self->allMessages filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"messageType == %d", ChatMessageTypeImage]];
    NSMutableArray *result = [[NSMutableArray alloc] init];
    NSMutableSet<NSString*> *chkMsg = [[NSMutableSet alloc] init];
    for (ChatMessage *message in messages) {
        if(![chkMsg containsObject:message.messageID]) {
            [result addObject:message];
            [chkMsg addObject:message.messageID];
        }
    }
    
    [result sortUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"receivedDate" ascending:NO]]];
    if(handler){
        handler(result, nil);
    }
    
}
- (void) getAllFiles:(ChatMessageBlock) handler {
    NSArray *messages = [self->allMessages filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"messageType == %d", ChatMessageTypeFile]];
    NSMutableArray *result = [[NSMutableArray alloc] init];
    NSMutableSet<NSString*> *chkMsg = [[NSMutableSet alloc] init];
    for (ChatMessage *message in messages) {
        if(![chkMsg containsObject:message.messageID]) {
            [result addObject:message];
            [chkMsg addObject:message.messageID];
        }
    }
    
    [result sortUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"receivedDate" ascending:NO]]];
    if(handler){
        handler(result, nil);
    }
}

- (void)searchMessageWithKeyword:(NSString*) keyword handler:(ChatMessageBlock) handler {
    
}

- (NSString*) getImageURL:(ChatMessage* _Nullable) message {
    if(self.room.roomType == ChatRoomTypeEvent) {
        if(message.ownMessage) {
            if(self.room.eventOwner){
                return [self->ada stringURL:message.senderImageID type:nil];
            }
        }else{
            if(!self.room.eventOwner){
                return [self->ada stringURL:message.senderImageID type:nil];
            }
        }
    }
    
    NSString *kSenderKEY = [[NSString stringWithFormat:@"%@-%@", message.senderID, message.senderGroup] uppercaseString];
    return [self.senderImageMap valueForKey:kSenderKEY];
    
}


- (void)receivedMessage:(ChatMessage *)message scrollToBottom:(BOOL)scrollToBottom {
    
}

#pragma mark - XMPPStream

- (void)dealloc
{
    [chatStream removeDelegate:self];
}

- (void)connectXMPPConnection {
    
    
    if(chatStream){
        return;
    }
    
    
    
    /*dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
     [self showConectionInfo:@"" message:@"Connecting..." withReconnectButton:NO];
     });*/
    
    //self.XMPP_HOST = @"localhost";
    //NSLog(@"Connecting...%@:%ld", self->ada.login.chatServerHost, (long)self->ada.login.chatServerPort);
    chatStream = [[XMPPStream alloc] init];
    [chatStream setHostName:self->ada.login.chatServerHost];
    [chatStream setHostPort:self->ada.login.chatServerPort];
    [chatStream addDelegate:self delegateQueue:self->chatQueue];
    
    NSString *userID = [[NSString stringWithFormat:@"%@-%@", self->ada.login.usid, self->ada.login.group] lowercaseString];
    XMPPJID *myJID = [XMPPJID jidWithString:[[userID stringByAppendingString:@"@"] stringByAppendingString:self->ada.login.chatServerDomain] resource:[UIDevice currentDevice].identifierForVendor.UUIDString];
    
    [chatStream setMyJID:myJID];
    //NSLog(@"userID %@", chatStream.myJID.full);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        NSError *error = nil;
        if (![self->chatStream connectWithTimeout:5 error:&error]){
            NSLog(@"Cannot connect %@", error);
            
            return;
        }
    });
}

- (void) closeXMPPConnection {
    if(xmppRoom){
        [xmppRoom leaveRoom];
        xmppRoom = nil;
    }
    
    //NSLog(@"close socket");
    if(chatStream){
        [chatStream disconnect];
        chatStream = nil;
        
    }
    
}

- (void)xmppStreamDidDisconnect:(XMPPStream *)sender withError:(NSError *)error {
    NSLog(@"connot connect %@", error);
    
    self.online = NO;
    chatStream = nil;
    [self chatServerConnectionState:NO];
}



- (void)xmppStreamWillConnect:(XMPPStream *)sender {
    //NSLog(@"will connect");
}

- (void)xmppStream:(XMPPStream *)sender didReceiveError:(DDXMLElement *)error {
    if (error) {
        NSLog(@"error %@", error);
    }
}



- (void)xmppStream:(XMPPStream *)sender socketDidConnect:(GCDAsyncSocket *)socket {
    
    //NSLog(@"connect socket");
}

- (void) xmppStream:(XMPPStream *)sender didNotAuthenticate:(DDXMLElement *)error {
    NSLog(@"didNotAuthenticate : %@", error);
}

- (void)xmppStreamDidStartNegotiation:(XMPPStream *)sender {
    
}

- (void) xmppStreamDidConnect:(XMPPStream *)sender {
    
    //NSLog(@"Connected");
    NSString *password = [[NSString stringWithFormat:@"%@-%@", self->ada.login.usid, self->ada.login.group] lowercaseString];
    //dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    NSError *error = nil;
    [chatStream authenticateWithPassword:password error:&error];
    //xmppAutoTime = [[XMPPAutoTime alloc] initWithDispatchQueue:dispatch_get_main_queue()];
    //[xmppAutoTime activate:chatStream];
    if (error) {
        NSLog(@"error %@", error);
    }
    
    
    //});
}



- (void)joinXMPPRoom {
    
    if (xmppRoom) {
        return;
    }
    //NSLog(@"domain :  %@", self->ada.login.chatServerDomain);
    
    XMPPRoomMemoryStorage * roomMemory = [[XMPPRoomMemoryStorage alloc]init];
    NSString *roomID = [NSString stringWithFormat:@"%@@im.%@",
                        self->xmppRoomID,
                        self->ada.login.chatServerDomain];
    //NSLog(@"join room %@", roomID);
    XMPPJID * roomJID = [XMPPJID jidWithString:roomID resource:[UIDevice currentDevice].identifierForVendor.UUIDString];
    xmppRoom = [[XMPPRoom alloc] initWithRoomStorage:roomMemory
                                                 jid:roomJID
                                       dispatchQueue:chatQueue];
    
    
    
    
    [xmppRoom activate:chatStream];
    NSString *UDID = [UIDevice currentDevice].identifierForVendor.UUIDString;
    
    
    //NSSet *historyMsg = [self->user.messages filteredSetUsingPredicate:[NSPredicate predicateWithFormat:@"roomID = %@", self->xmppRoomID]];
    NSXMLElement *history = [NSXMLElement elementWithName:@"history"];
    /*NSString *userID = [[NSString stringWithFormat:@"%@-%@", self->ada.login.usid, self->ada.login.group] lowercaseString];
    if(historyMsg.count == 0){
        //[xmppRoom fetchConfigurationForm];
        [history addAttributeWithName:@"maxstanzas" stringValue:@"100"];
        XMPPJID *userJID =[XMPPJID jidWithString:[[userID stringByAppendingString:@"@"] stringByAppendingString:self->ada.login.chatServerDomain]
                                        resource:[UIDevice currentDevice].identifierForVendor.UUIDString];
        [xmppRoom editRoomPrivileges:@[[XMPPRoom itemWithAffiliation:@"member" jid:userJID]]];
    }
    */
    [xmppRoom joinRoomUsingNickname:UDID history:history];
    
    dispatch_async(self->chatQueue, ^{
        [self getLogFromLastMessage:nil];
    });
    //[self chatServerConnectionState:YES];
    
    
    
    
    [xmppRoom addDelegate:self delegateQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0)];
    
    //NSLog(@"joined");
}

- (void) xmppStreamDidAuthenticate:(XMPPStream *)sender {
    //NSLog(@"Authen.");
    // Initialize XMPPPresence variable
    XMPPPresence *presence = [XMPPPresence presenceWithType:@"available"];
    [sender sendElement:presence];
    //dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self joinXMPPRoom];
    //});
    
}

- (void)xmppStream:(XMPPStream *)sender didReceiveMessage:(XMPPMessage *)xmppMessage {
    
    
}


- (void)xmppStream:(XMPPStream *)sender didReceivePresence:(XMPPPresence *)presence {
    NSString *userFull = presence.from.full;
    NSString *type = presence.type;
    userFull = [userFull substringToIndex:[userFull rangeOfString:@"/"].location];
    //NSLog(@"self->user %@, type: %@", self->user, type);
    NSString *currentUser = [[NSString stringWithFormat:@"%@-%@", self->ada.login.usid, self->ada.login.group] lowercaseString];
    if ([userFull rangeOfString:currentUser].location == NSNotFound) {
        if ([@"unavailable" isEqualToString:type]) {
            [self->usersOnline removeObject:self->user];
        }else{
            [self->usersOnline addObject:self->user];
        }
        
        [self onlineChatUsers:self->usersOnline];
    }
    
}

- (BOOL)xmppStream:(XMPPStream *)sender didReceiveIQ:(XMPPIQ *)iq{
    
    return NO;
}



- (void)xmppRoomDidCreate:(XMPPRoom *)sender {
    
    //NSLog(@"room created");
}


- (void)xmppRoomDidJoin:(XMPPRoom *)sender {
    //NSLog(@"xmppRoomDidJoin");
    
    
    
    //dispatch_async(dispatch_get_main_queue(), ^{
        self.online = YES;
        [self chatServerConnectionState:YES];
        if(self->unsendData) {
            if([self->unsendData isKindOfClass:[ChatMessage class]]){
                [self sendPlainChatMessage:self->unsendData];
            }else if([self->unsendData isKindOfClass:[VideoMessage class]]){
                [self sendVideo:self->unsendData progress:self->progressHandler];
            }else if([self->unsendData isKindOfClass:[ImageMessage class]]){
                [self sendImage:self->unsendData progress:self->progressHandler];
            }else if([self->unsendData isKindOfClass:[NSString class]]){
                [self sendStreamMessage:self->unsendData];
            }
        }
    //});
    
    
    //[sender fetchConfigurationForm];
    /*dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
        });
    });
    */
    
    
    
    
}
- (void)xmppRoomDidLeave:(XMPPRoom *)sender {
    //NSLog(@"Room did leave");
    xmppRoom = nil;
}


- (void)xmppRoomDidDestroy:(XMPPRoom *)sender {
    
}
- (void)xmppRoom:(XMPPRoom *)sender occupantDidJoin:(XMPPJID *)occupantJID withPresence:(XMPPPresence *)presence {
    
}
- (void)xmppRoom:(XMPPRoom *)sender occupantDidLeave:(XMPPJID *)occupantJID withPresence:(XMPPPresence *)presence {
    
}
- (void)xmppRoom:(XMPPRoom *)sender occupantDidUpdate:(XMPPJID *)occupantJID withPresence:(XMPPPresence *)presence {
    
}



- (void)xmppRoom:(XMPPRoom *)sender didFetchConfigurationForm:(NSXMLElement *)configForm{
    //NSLog(@"didFetchConfigurationForm");
    NSXMLElement *newConfig = [configForm copy];
    //NSLog(@"BEFORE Config for the room %@",newConfig);
    NSArray *fields = [newConfig elementsForName:@"field"];
    
    for (NSXMLElement *field in fields)
    {
        NSString *var = [field attributeStringValueForName:@"var"];
        // Make Room Persistent
        if ([var isEqualToString:@"muc#roomconfig_persistentroom"]) {
            [field removeChildAtIndex:0];
            [field addChild:[NSXMLElement elementWithName:@"value" stringValue:@"1"]];
        }
        
        if ([var isEqualToString:@"muc#roomconfig_mam"]) {
            [field removeChildAtIndex:0];
            [field addChild:[NSXMLElement elementWithName:@"value" stringValue:@"1"]];
        }
 
        
        
    }
    //NSLog(@"AFTER Config for the room %@",newConfig);
    [sender configureRoomUsingOptions:newConfig];
    //[self loadLogData:chatStream];
}

- (void) sendStreamMessage:(id) data {
    
    
    if (!chatStream || chatStream.isDisconnected) {
        NSLog(@"disconnect retry again");
        self->unsendData = data;
        [self connectXMPPConnection];
        return;
        
    }
    
    NSString *strJSON = data;
    NSJSONSerialization *json = [AppHelper jsonWithData:[strJSON dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *usid = self->ada.login.usid;
    NSString *userGroup = self->ada.login.group;
    NSString *userName = self->ada.login.userName;
    NSString *imageID = self->ada.login.imageID;
    NSDateFormatter *dfMsgID = [NSDateFormatter instanceWithUSLocale];
    [dfMsgID setDateFormat:@"yyyyMMddHHmmssSSS"];
    NSString *msgID = [NSString stringWithFormat:@"%@-%@%@",
                       usid,
                       userGroup,
                       [dfMsgID stringFromDate:[NSDate date]]];
    
    NSDictionary *msg = nil;
    BOOL sendReadStatus = NO;
    if ([json valueForKey:kAction] && [[json valueForKey:kAction] isEqualToString:@"readMsg"]) {
        sendReadStatus = YES;
        msg = @{
                kReadStatus: [NSNumber numberWithBool:YES],
                kSender: usid,
                kGroupID: userGroup,
                kRoomID: [json valueForKey:kRoomID],
                kMessageID: [NSString stringWithFormat:@"%@",[json valueForKey:kMessageID]]
                };
    }else{
        if(imageID){
            
            msg = @{
                    kMessageID: msgID,
                    kMessage: [json valueForKey:kMessage],
                    kSender: usid,
                    kSenderName: userName,
                    kGroupID: userGroup,
                    kImageID: imageID,
                    kRoomID: self->xmppRoomID,
                    kTime: [df3 stringFromDate:[NSDate date]],
                    kDate: [df stringFromDate:[NSDate date]],
                    kBroadCast: [NSNumber numberWithBool:YES]
                    };
        }else{
            msg = @{
                    kMessageID: msgID,
                    kMessage: [json valueForKey:kMessage],
                    kSender: usid,
                    kSenderName: userName,
                    kGroupID: userGroup,
                    kRoomID: self->xmppRoomID,
                    kTime: [df3 stringFromDate:[NSDate date]],
                    kDate: [df stringFromDate:[NSDate date]],
                    kBroadCast: [NSNumber numberWithBool:YES]
                    };
        }
        
        
    }
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:msg
                                                       options:NSJSONWritingPrettyPrinted
                                                         error: nil];
    
    NSString *jsonString = [[NSString alloc] initWithData:jsonData
                                                 encoding:NSUTF8StringEncoding];
    
    
    
    //NSLog(@"send %@", jsonString);
    if([msg valueForKey:kBroadCast]){
        NSJSONSerialization *json = [AppHelper jsonWithData:jsonData];
        self->msgSent = toJSONString(json);
        //self->msgSent = jsonString;
    }
    NSXMLElement *body = [NSXMLElement elementWithName:@"body" stringValue:jsonString];
    
    XMPPMessage *message = [XMPPMessage message];
    [message addChild:body];
    //[message addChild:[XMPPTime timeElement]];
    [xmppRoom sendMessage:message];
    
    self->unsendData = nil;
    progressHandler = nil;
    imageHandler = nil;
    videoHandler = nil;
    
    //[socket send:data];
}

- (void)doProcessIncomingMessage:(BOOL)fromLog json:(NSJSONSerialization *)json messageID:(NSInteger) stanzaID {
    [self doProcessIncomingMessage:fromLog json:json scrollToBottom:YES messageID:stanzaID];
}




- (void)changeEventMemberName:(ChatMessage *)m {
    if(self.room.roomType == ChatRoomTypeEvent) {
        if(self.room.eventOwner && m.ownMessage) {
            m.senderImageID = self.room.imageID;
            m.senderName = self.room.eventOwnerName;
        }else if(!self.room.eventOwner && !m.ownMessage) {
            m.senderImageID = self.room.imageID;
            m.senderName = self.room.eventOwnerName;
        }
    }
}

- (void)doProcessIncomingMessage:(BOOL)fromLog json:(NSJSONSerialization *)json scrollToBottom:(BOOL) scroll messageID:(NSInteger) stanzaID {
    dispatch_async(self->chatQueue, ^{
        NSString *kSenderKEY = [NSString stringWithFormat:@"%@-%@", [json valueForKey:kSender], [json valueForKey:kGroupID]];
        [json setValue:[NSNumber numberWithInteger:stanzaID] forKey:@"stanzaID"];
        [json setValue:[self.senderImageMap valueForKey:[kSenderKEY uppercaseString]] forKey:@"image_url"];
        ChatMessage *m = [ChatMessage instanceFromJSON:json with:self->ada.login];
            //
        m.seq = stanzaID;
        NSString *sender =  m.senderID;
        NSString *groupID = m.senderGroup;
        NSString *keyReadUser = [NSString stringWithFormat:@"%@-%@", sender, groupID];
        
        NSString *msgID = m.messageID;
        NSString *roomID = m.roomID;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"msgID == %@ and roomID == %@",
                                  msgID,
                                  roomID];
        if (m.messageType == ChatMessageTypeReadMessage) {
            //NSLog(@"read %@ by %@", m.messageID, m.senderID);
            __block NSMutableSet<NSString*> *readUserArr = [[NSMutableSet alloc] init];
            if(m.readUsers){
                readUserArr = [NSMutableSet setWithSet:m.readUsers] ;
            }
            if(!([m.senderID isEqualToString: self->user.usid] && [m.senderGroup isEqualToString:self->user.userGroup])){
                [readUserArr addObject:[NSString stringWithFormat:@"%@-%@", m.senderID, m.senderGroup]];
                
                
                NSArray *messages = [self->allMessages filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"messageID == %@ and messageType != 8", m.messageID]];
                
                if(messages && messages.count > 0){
                    ChatMessage *message =  messages.firstObject;
                    message.readUsers  = readUserArr;
                }
                
            }
            NSArray *msgLogs = self->userMessages;
            
            NSArray *msgArr = [msgLogs filteredArrayUsingPredicate:predicate];
            if (msgArr.count > 0) {
                dispatch_async(self->chatQueue, ^{
                    MessageLog *msgLog = msgArr.firstObject;
                    NSJSONSerialization *jsonLog = [NSJSONSerialization JSONObjectWithData:msgLog.message
                                                                                   options:NSJSONReadingMutableContainers
                                                                                     error:nil];
                    if(![jsonLog valueForKey:kReadUser]){
                        readUserArr = [NSMutableSet setWithArray:@[keyReadUser]];
                    }else{
                        readUserArr = [NSMutableSet setWithArray:[jsonLog valueForKey:kReadUser]];
                        if(![readUserArr containsObject:keyReadUser]){
                            [readUserArr addObject:keyReadUser];
                        }
                    }
                    [jsonLog setValue:[readUserArr allObjects] forKey:kReadUser];
                    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonLog
                                                                       options:NSJSONWritingPrettyPrinted
                                                                         error:nil];
                    msgLog.message = jsonData;
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [ManagedObjectContextFactory saveContext];
                    });
                    NSArray *messages = [self->allMessages filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"messageID == %@ and messageType != 8", m.messageID]];
                    
                    if(messages && messages.count > 0){
                        ChatMessage *message =  messages.firstObject;
                        message.readUsers  = readUserArr;
                        //NSLog(@"read users %@ %@", message.message, message.readUsers);
                    }
                    
                });
            }
            m.readUsers = readUserArr;
            [self receivedMessage:m scrollToBottom:scroll];
            //dispatch_async(dispatch_get_main_queue(), ^{
                [self updateMessageState];
            //});
        } else {
            
            
            //NSArray *msgArr = self->userMessages;
            
            NSArray *msgArr = [self->userMessages filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"seq == %ld", stanzaID]];
            if (msgArr.count == 0) {
            
                NSDateFormatter *parser = [NSDateFormatter instanceWithUSLocale];
                [parser setDateFormat:@"dd-MM-yyyy HH:mm"];
                dispatch_async(dispatch_get_main_queue(), ^{
                    MessageLog *messageLog = (MessageLog*)[ManagedObjectContextFactory newEntityForName:@"MessageLog"];
                    messageLog.seq = stanzaID;
                    messageLog.message = [NSJSONSerialization dataWithJSONObject:json
                                                                         options:NSJSONWritingPrettyPrinted
                                                                           error:nil];
                    messageLog.msgID = [NSString stringWithFormat:@"%@", [json valueForKey:kMessageID]];
                    messageLog.msgDate = [parser dateFromString:[NSString stringWithFormat:@"%@ %@",[json valueForKey:kDate], [json valueForKey:kTime]]];
                    messageLog.roomID = self->xmppRoomID;
                    messageLog.readStatus = NO;
                    messageLog.user = self->user;
                    [self->userMessages addObject:messageLog];
                    [self->user addMessagesObject:messageLog];
                    
                    [ManagedObjectContextFactory saveContext];
                    
                });
            }
            
            
            
            
            //NSString *message = toJSONString(json);//[json valueForKey:kMessage];
     
            
            //NSLog(@"self->msgSent : %@", self->msgSent);
            //NSLog(@"message : %@", message);
            if (self->msgSent) {
                NSJSONSerialization *jsonSent = [AppHelper jsonWithData:[self->msgSent dataUsingEncoding:NSUTF8StringEncoding]];
                if([[json valueForKey:@"msgID"] isEqualToString:[jsonSent valueForKey:@"msgID"]] && [[json valueForKey:@"message"] isEqualToString:[jsonSent valueForKey:@"message"]]){
                //NSLog(@"sending message to offline.");
                [ServiceCaller callJSON:[NSString stringWithFormat:@"%@msg-groups/send-offline/%@",
                                         kSERVER,
                                         self->xmppRoomID
                                         ] method:@"POST"
                                 header:@{
                                          @"showMsg":@"N"
                                          }
                              parameter:@{
                                          @"memberID": [NSNumber numberWithInteger:self->ada.login.memberID],
                                          @"message": toJSONString(json),
                                          @"udid": [UIDevice currentDevice].identifierForVendor.UUIDString
                                          
                                          }
                                handler:^(NSJSONSerialization *json) {
                                }];
                
                                                 }
                                                 self->msgSent = nil;
            }
            
            NSString *selfKEY = [NSString stringWithFormat:@"%@-%@", self->ada.login.usid, self->ada.login.group];;
            
            BOOL selfSender = [selfKEY isEqualToString:kSenderKEY];
            
            
            if(selfSender){
                NSString *yourMessage = [AppHelper getLabelForThai:@"คุณ" eng:@"You"];
                m.senderName = yourMessage;
                [self changeEventMemberName:m];
                [json setValue:m.senderName forKey:kSenderName];
                m.jsonString = toJSONString(json);
                
                [self->allMessages addObject:m];
                if(![self->receivedMsgSet containsObject:m.messageID]){
                    [self->receivedMsgSet addObject:m.messageID];
                    [self receivedMessage:m scrollToBottom:scroll];
                }
                
            }else{
                if([self.senderNameMap valueForKey:kSenderKEY]){
                    m.senderName = [self.senderNameMap valueForKey:kSenderKEY];
                    [self changeEventMemberName:m];
                    [json setValue:m.senderName forKey:kSenderName];
                    m.jsonString = toJSONString(json);
                    
                    [self->allMessages addObject:m];
                    if(![self->receivedMsgSet containsObject:m.messageID]){
                        [self->receivedMsgSet addObject:m.messageID];
                        [self receivedMessage:m scrollToBottom:scroll];
                    }
                }else{
                    
                    if(self.room.roomType == ChatRoomTypePrivate && !selfSender){
                        
                        [self.senderNameMap setValue:self.room.roomName forKey:kSenderKEY];
                        m.senderName = [self.senderNameMap valueForKey:kSenderKEY];
                        
                        [self changeEventMemberName:m];
                        [json setValue:m.senderName forKey:kSenderName];
                        m.jsonString = toJSONString(json);
                        
                        
                        [self->allMessages addObject:m];
                        if(![self->receivedMsgSet containsObject:m.messageID]){
                            [self->receivedMsgSet addObject:m.messageID];
                            [self receivedMessage:m scrollToBottom:scroll];
                        }
                    }else{
                        [self->allMessages addObject:m];
                        if(![self->receivedMsgSet containsObject:m.messageID]){
                            [self->receivedMsgSet addObject:m.messageID];
                            [self receivedMessage:m scrollToBottom:scroll];
                        }
                        
                    }
                }
            }

            
            
        }
    });
}

/**
 * Invoked when a message is received.
 * The occupant parameter may be nil if the message came directly from the room, or from a non-occupant.
 **/
- (void)xmppRoom:(XMPPRoom *)sender didReceiveMessage:(XMPPMessage *)xmppMessage fromOccupant:(XMPPJID *)occupantJID {
 
    //NSLog(@"%@", xmppMessage);
    //NSLog(@"didReceiveMessage");
    
    
    
    //NSLog(@"stanzaID %ld", (long)stanzaID);
    
    id message = [[xmppMessage elementForName:@"body"] stringValue];
    if(!message) {
        return;
    }
    
    NSJSONSerialization *json = nil;
    BOOL fromLog = NO;
    
    if ([message isKindOfClass:[NSString class]]) {
        json = [AppHelper jsonWithData:[message dataUsingEncoding:NSUTF8StringEncoding]];
    }else{
        fromLog = YES;
        json = message;
    }
    if (json){
 
        BOOL readMsg = [[json valueForKey:kReadStatus] boolValue];
        BOOL found = NO;
        
        if(!readMsg){
            
            NSString *senderID = [json valueForKey:kSender];
            NSString *messageID = [json valueForKey:kMessageID];
            NSString *message = [json valueForKey:kMessage];
            NSString *date = [json valueForKey:kDate];
            NSString *time = [json valueForKey:kTime];
            
            NSString *KEY = [NSString stringWithFormat:@"%@-%@-%@-%@-%@",
                             messageID, senderID, message, date, time];
            if (![self->chkSet containsObject:KEY] && !found) {
                [self->chkSet addObject:KEY];
                //NSLog(@"Add new");
                if([xmppMessage elementForName:@"stanza-id"]) {
                    NSInteger stanzaID = [[xmppMessage elementForName:@"stanza-id"]  attributeIntegerValueForName:@"id" withDefaultValue:0];
                    [self doProcessIncomingMessage:NO json:json messageID:stanzaID];
                }
                
            }
        }else{
            NSString *senderID = [json valueForKey:kSender];
            NSString *messageID = [json valueForKey:kMessageID];
            
            
            NSString *KEY = [NSString stringWithFormat:@"%@-%@",
                             messageID, senderID];
            if (![self->chkSet containsObject:KEY] && !found) {
                [self->chkSet addObject:KEY];
                //NSLog(@"Add new");
 
                if([xmppMessage elementForName:@"stanza-id"]) {
                    NSInteger stanzaID = [[xmppMessage elementForName:@"stanza-id"]  attributeIntegerValueForName:@"id" withDefaultValue:0];
                    [self doProcessIncomingMessage:NO json:json messageID:stanzaID];
                }
                
            }
 
        }
 
    }
 
    
}


- (void) getLogFromLastMessage:(SuccessBlock) handler {
    //NSLog(@"getLogFromLastMessage");
    //dispatch_group_t group = dispatch_group_create();
    //dispatch_group_enter(group);
    
    //NSArray *msgLogs = [ManagedObjectContextFactory getAllObjectsFromEntity:@"MessageLog" sortedBy:@{@"msgDate":@"NO"}];
    NSArray *msgLogs = self->userMessages;
    
    
    dispatch_async(self->chatQueue, ^(void){
        MessageLog *lastMsg = msgLogs.firstObject;
        if(lastMsg){
            //NSLog(@"getLogFromLastMessage");
            [ServiceCaller callJSON:[NSString stringWithFormat:@"%@msg-groups/prev-logs-by-msgid/%@",
                                     kSERVER, self->xmppRoomID]
                             method:@"GET"
                             header:nil
                          parameter:@{
                                      @"hostName": self->ada.login.chatServerDomain,
                                      @"msgID": lastMsg.msgID
                                      }
                            handler:^(NSJSONSerialization *json) {
                                
                                //NSLog(@"GET Data");
                                if ([[json valueForKey:@"result"] boolValue]) {
                                    NSArray *data = [json valueForKey:@"data"];
                                    NSArray *history = msgLogs;
                                    NSDateFormatter *parser = [NSDateFormatter instanceWithUSLocale];
                                    [parser setDateFormat:@"dd-MM-yyyy HH:mm"];
                                    NSMutableArray *messages = [[NSMutableArray alloc] init];
                                    for (NSJSONSerialization *log in data) {
                                        NSInteger stanzaID = [[log valueForKey:@"id"] integerValue];
                                        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"msgID == %@ and message != nil", [NSString stringWithFormat:@"%@", [log valueForKey:kMessageID]]];
                                        if ([log valueForKey:kBroadCast] && [[log valueForKey:kBroadCast] boolValue]) {
                                            NSArray *filter = [history filteredArrayUsingPredicate:predicate];
                                            //NSLog(@"filter : %@", filter);
                                            BOOL found = YES;
                                            if (filter.count > 0) {
                                                for (MessageLog *msgLog in filter) {
                                                    NSString *strMsgLog = [[NSString alloc] initWithData:msgLog.message encoding:NSUTF8StringEncoding];
                                                    NSString *strCompare = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:log
                                                                                                                                          options:NSJSONWritingPrettyPrinted
                                                                                                                                            error:nil] encoding:NSUTF8StringEncoding];
                                                    strMsgLog = [strMsgLog stringByReplacingOccurrencesOfString:@" " withString:@""];
                                                    strCompare = [strMsgLog stringByReplacingOccurrencesOfString:@" " withString:@""];
                                                    if ([strMsgLog isEqualToString:strCompare]) {
                                                        found = NO;
                                                    }
                                                }
                                            }
                                            
                                            if(found){
                                                //NSLog(@"Add history");
                                                MessageLog *messageLog = (MessageLog*)[ManagedObjectContextFactory newEntityForName:@"MessageLog"];
                                                messageLog.seq = (long)stanzaID;
                                                messageLog.message = [NSJSONSerialization dataWithJSONObject:log
                                                                                                     options:NSJSONWritingPrettyPrinted
                                                                                                       error:nil];
                                                messageLog.msgID = [NSString stringWithFormat:@"%@", [log valueForKey:kMessageID]];
                                                messageLog.msgDate = [parser dateFromString:[NSString stringWithFormat:@"%@ %@",[log valueForKey:kDate], [log valueForKey:kTime]]];
                                                messageLog.roomID = self->xmppRoomID;
                                                messageLog.readStatus = NO;
                                                messageLog.user = self->user;
                                                [self->user addMessagesObject:messageLog];
                                                [messages addObject:messageLog];
                                                //[del.self->user addMessageLogsObject:messageLog];
                                            }
                                        }else{
                                            NSString *sender =  [log valueForKey:kSender];
                                            NSString *groupID = [log valueForKey:kGroupID];
                                            
                                            NSString *keyReadUser = [NSString stringWithFormat:@"%@-%@", sender, groupID];
                                            NSMutableArray *readUserArr = nil;
                                            NSArray *msgArr = [messages filteredArrayUsingPredicate:predicate];
                                            if (msgArr.count > 0) {
                                                
                                                MessageLog *msgLog = msgArr.firstObject;
                                                NSJSONSerialization *jsonLog = [NSJSONSerialization JSONObjectWithData:msgLog.message
                                                                                                               options:NSJSONReadingMutableContainers
                                                                                                                 error:nil];
                                                if(![jsonLog valueForKey:kReadUser]){
                                                    readUserArr = [NSMutableArray arrayWithArray:@[keyReadUser]];
                                                }else{
                                                    readUserArr = [NSMutableArray arrayWithArray:[jsonLog valueForKey:kReadUser]];
                                                    if(![readUserArr containsObject:keyReadUser]){
                                                        [readUserArr addObject:keyReadUser];
                                                    }
                                                }
                                                [jsonLog setValue:readUserArr forKey:kReadUser];
                                                //NSLog(@"update read %@", jsonLog);
                                                NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonLog
                                                                                                   options:NSJSONWritingPrettyPrinted
                                                                                                     error:nil];
                                                msgLog.message = jsonData;
                                            }
                                            
                                        }
                                        
                                    }
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        [ManagedObjectContextFactory saveContext];
                                    });
                                    
                                    if (handler) {
                                        handler(YES);
                                    }
                                    
                                    //dispatch_group_leave(group);
                                }
                            }];
        }else{
            if(handler){
                handler(NO);
            }
        }
    });
    //dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        //success(usersWithImage);
    //});
    
}

- (void)loadLogDataWithScroll:(BOOL) scroll {
    
    NSSortDescriptor *descriptor0 = [NSSortDescriptor sortDescriptorWithKey:@"seq" ascending:NO];
 
    [self->userMessages removeAllObjects];
    NSArray *msgLogs = [self->user.messages.allObjects filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"roomID == %@", self->xmppRoomID]];
    [self->userMessages addObjectsFromArray:msgLogs];
    NSArray *allMessage = [msgLogs sortedArrayUsingDescriptors:@[descriptor0]];//, descriptor1, descriptor2]];
     
    [self->chkSet removeAllObjects];
    int cnt = 0;
    
    NSMutableArray<NSDictionary<NSString*, id>*> *rows = [[NSMutableArray alloc] init];
    
    for (MessageLog *msgLog in allMessage) {
        if(cnt > self->maxMessage) {
            break;
        }
        NSJSONSerialization *jsonMsg = [NSJSONSerialization JSONObjectWithData:msgLog.message
                                                                       options:NSJSONReadingMutableContainers
                                                                         error:nil];
        if ([jsonMsg valueForKey:kMessage]) {
            NSString *senderID = [jsonMsg valueForKey:kSender];
            NSString *messageID = [jsonMsg valueForKey:kMessageID];
            NSString *message = [jsonMsg valueForKey:kMessage];
            NSString *date = [jsonMsg valueForKey:kDate];
            NSString *time = [jsonMsg valueForKey:kTime];
            NSString *KEY = [NSString stringWithFormat:@"%lld", msgLog.seq];
            
            if (![self->chkSet containsObject:KEY]) {
                [self->chkSet addObject:KEY];
                [rows insertObject:@{@"json": jsonMsg, @"seq": [NSNumber numberWithUnsignedLongLong:msgLog.seq]} atIndex:0];
                cnt++;
                //[self doProcessIncomingMessage:YES json:jsonMsg scrollToBottom:scroll messageID:(long)msgLog.seq];
            }
            KEY = nil;
            senderID = nil;
            messageID = nil;
            message = nil;
            date = nil;
            time = nil;
            //if([jsonMsg valueForKey:kBroadCast]) {
            
            //}
        }else{
            [rows insertObject:@{@"json": jsonMsg, @"seq": [NSNumber numberWithUnsignedLongLong:msgLog.seq]} atIndex:0];
            //[self doProcessIncomingMessage:YES json:jsonMsg scrollToBottom:scroll messageID:(long)msgLog.seq];
        }
    }
    //NSLog(@"cnt = %ld", (long)rows.count);
    for(NSDictionary<NSString*, id> *row in rows) {
        [self doProcessIncomingMessage:NO
                                  json:[row valueForKey:@"json"]
                        scrollToBottom:scroll
                             messageID:[[row valueForKey:@"seq"] longValue]];
    }
    
    
    
    //NSLog(@"end load log");
 
}


- (void) loadHistoryFromWS:(SuccessBlock) handler  {
    
    [ServiceCaller callJSON:[NSString stringWithFormat:@"%@msg-groups/room-logs/%@",
                             kSERVER, self->xmppRoomID]
                     method:@"GET"
                     header:nil
                  parameter:@{
                              @"hostName": self->ada.login.chatServerDomain
                              }
                    handler:^(NSJSONSerialization *json) {
                        //NSLog(@"%@", json);
                        
                        NSArray *msgLogs = self->userMessages;
                        
                        //NSArray *msgLogs = [ManagedObjectContextFactory getAllObjectsFromEntity:@"MessageLog" sortedBy:@{@"msgDate":@"NO"}];
                        NSMutableArray *messages = [[NSMutableArray alloc] init];
                        if ([[json valueForKey:@"result"] boolValue]) {
                            [self->allMessages removeAllObjects];
                            
                            NSArray *data = [json valueForKey:@"data"];
                            
                            NSArray *history = [msgLogs filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"roomID = %@", self->xmppRoomID]];
                            //NSLog(@"history count %ld", (long)history.count);
                            
                            
                            //NSLog(@"cnt %d",  self->user.messages.count);
                            
                            for (MessageLog *log in history) {
                                [self->user addMessagesObject:log];
                                [ManagedObjectContextFactory deleteObject:log];
                            }
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [ManagedObjectContextFactory saveContext];
                            });
                            
                            NSDateFormatter *parser = [NSDateFormatter instanceWithUSLocale];
                            [parser setDateFormat:@"dd-MM-yyyy HH:mm"];
                            for (NSJSONSerialization *log in data) {
                                if ([log valueForKey:kBroadCast] && [[log valueForKey:kBroadCast] boolValue]) {
                                    MessageLog *messageLog = (MessageLog*)[ManagedObjectContextFactory newEntityForName:@"MessageLog"];
                                    
                                    messageLog.message = [NSJSONSerialization dataWithJSONObject:log
                                                                                         options:NSJSONWritingPrettyPrinted
                                                                                           error:nil];
                                    messageLog.msgID = [NSString stringWithFormat:@"%@", [log valueForKey:kMessageID]];
                                    messageLog.msgDate = [parser dateFromString:[NSString stringWithFormat:@"%@ %@",[log valueForKey:kDate], [log valueForKey:kTime]]];
                                    messageLog.roomID = self->xmppRoomID;
                                    messageLog.readStatus = NO;
                                    messageLog.user = self->user;
                                    [self->user addMessagesObject:messageLog];
                                    //[self->allMessages addObject:messageLog];
                                    [messages addObject:messageLog];
                                    
                                }else{
                                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"msgID == %@ and message != nil and readStatus == NO",
                                                              [NSString stringWithFormat:@"%@", [log valueForKey:kMessageID]]];
                                    NSString *sender =  [log valueForKey:kSender];
                                    NSString *groupID = [log valueForKey:kGroupID];
                                    if(sender && groupID){
                                        
                                        NSString *keyReadUser = [NSString stringWithFormat:@"%@-%@", sender, groupID];
                                        NSMutableArray *readUserArr = nil;
                                        NSArray *msgArr = [messages filteredArrayUsingPredicate:predicate];
                                        if (msgArr.count > 0) {
                                            MessageLog *msgLog = msgArr.firstObject;
                                            NSJSONSerialization *jsonLog = [NSJSONSerialization JSONObjectWithData:msgLog.message
                                                                                                           options:NSJSONReadingMutableContainers
                                                                                                             error:nil];
                                            
                                            //NSLog(@"Add read self->user");
                                            //NSLog(@"%@", jsonLog);
                                            if(![jsonLog valueForKey:kReadUser]){
                                                readUserArr = [NSMutableArray arrayWithArray:@[keyReadUser]];
                                            }else{
                                                readUserArr = [NSMutableArray arrayWithArray:[jsonLog valueForKey:kReadUser]];
                                                if(![readUserArr containsObject:keyReadUser]){
                                                    [readUserArr addObject:keyReadUser];
                                                }
                                            }
                                            [jsonLog setValue:readUserArr forKey:kReadUser];
                                            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonLog
                                                                                               options:NSJSONWritingPrettyPrinted
                                                                                                 error:nil];
                                            msgLog.message = jsonData;
                                        }
                                    }
                                    
                                }
                                
                            }
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [ManagedObjectContextFactory saveContext];
                            });
                            [self loadLogDataWithScroll:NO];
                            if(handler){
                                handler(YES);
                            }
                            
                        }else{
                            if(handler){
                                handler(NO);
                            }
                        }
                    }];
}


- (void) loadPreviousHistory:(ChatMessage*) message handler:(SuccessBlock) handler  {
 
    NSSortDescriptor *descriptor0 = [NSSortDescriptor sortDescriptorWithKey:@"seq" ascending:NO];
    
    
    NSArray *msgLogs = [self->user.messages.allObjects filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"roomID == %@ and seq < %ld", self->xmppRoomID, message.seq]];
    /*
    NSSet *msgLogs = [self->user.messages filteredSetUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id  _Nullable evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
        MessageLog *log = evaluatedObject;
        return [log.roomID isEqualToString:self->xmppRoomID] && log.seq < message.seq;
    }]];
    */
    
    NSArray *allMessage = [msgLogs sortedArrayUsingDescriptors:@[descriptor0]];
    if(allMessage.count > 0) {
        dispatch_async(self->chatQueue, ^{
            int cnt = 0;
            NSMutableArray<NSDictionary<NSString*, id>*> *rows = [[NSMutableArray alloc] init];
            
            for (MessageLog *msgLog in allMessage) {
                
                if(cnt >= self->maxMessage) {
                    break;
                }
                
                @try {
                    NSJSONSerialization *jsonMsg = [NSJSONSerialization JSONObjectWithData:msgLog.message
                                                                                   options:NSJSONReadingMutableContainers
                                                                                     error:nil];
                    if(jsonMsg) {
                        if ([jsonMsg valueForKey:kBroadCast]) {
                            NSString *senderID = [jsonMsg valueForKey:kSender];
                            NSString *messageID = [jsonMsg valueForKey:kMessageID];
                            NSString *message = [jsonMsg valueForKey:kMessage];
                            NSString *date = [jsonMsg valueForKey:kDate];
                            NSString *time = [jsonMsg valueForKey:kTime];
                            NSString *KEY = [NSString stringWithFormat:@"%lld", msgLog.seq];
                            if (![self->chkSet containsObject:KEY]) {
                                [self->chkSet addObject:KEY];
                                [rows insertObject:@{@"json": jsonMsg, @"seq": [NSNumber numberWithUnsignedLongLong:msgLog.seq]} atIndex:0];
                                cnt++;
                                //[self doProcessIncomingMessage:YES json:jsonMsg scrollToBottom:scroll messageID:(long)msgLog.seq];
                            }
                            KEY = nil;
                            senderID = nil;
                            messageID = nil;
                            message = nil;
                            date = nil;
                            time = nil;
                            
                        }else{
                            [rows insertObject:@{@"json": jsonMsg, @"seq": [NSNumber numberWithUnsignedLongLong:msgLog.seq]} atIndex:0];
                            //[self doProcessIncomingMessage:YES json:jsonMsg scrollToBottom:scroll messageID:(long)msgLog.seq];
                        }
                    }
                } @catch(NSError *error) {
                    
                }
                
                
            }
            //NSLog(@"cnt = %ld", (long)rows.count);
            for(NSDictionary<NSString*, id> *row in rows) {
                [self doProcessIncomingMessage:NO
                                          json:[row valueForKey:@"json"]
                                scrollToBottom:NO
                                     messageID:[[row valueForKey:@"seq"] longValue]];
            }
            
            if(handler){
                handler(YES);
            }
        
        });
        return;
        
    }
    
    
    NSString *msgID = message == nil ? nil : message.messageID;
    
    [ServiceCaller callJSON:[NSString stringWithFormat:@"%@msg-groups/prev-logs-by-msgid/%@",
                             kSERVER, self->xmppRoomID]
                     method:@"GET"
                     header:nil
                  parameter:(msgID ?@{
                                      @"hostName": self->ada.login.chatServerDomain,
                                      @"msgID": msgID
                                    } :
                                    @{
                                      @"hostName": self->ada.login.chatServerDomain
                                      }
                             )
                    handler:^(NSJSONSerialization *json) {
                        //NSLog(@"%@", json);
                        
                        NSArray *msgLogs = self->userMessages;
 
                        NSMutableArray *messages = [[NSMutableArray alloc] init];
                        if ([[json valueForKey:@"result"] boolValue]) {
                            //[self->allMessages removeAllObjects];
                            
                            NSArray *data = [json valueForKey:@"data"];
                            
                            NSArray *history = msgLogs;
   
                            NSDateFormatter *parser = [NSDateFormatter instanceWithUSLocale];
                            [parser setDateFormat:@"dd-MM-yyyy HH:mm"];
                            //NSLog(@"log cnt %lu", (unsigned long)data.count);
                            for (NSJSONSerialization *log in data) {
                                
                                NSInteger stanzaID = [[log valueForKey:@"id"] integerValue];
                                if ([log valueForKey:kBroadCast] && [[log valueForKey:kBroadCast] boolValue]) {
                                    
                                    NSArray *msg = [history filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"seq == %ld", stanzaID]];
                                    
                                    if(msg && msg.count > 0) {
                                        continue;
                                    }
                                    
                              
                                    MessageLog *messageLog = (MessageLog*)[ManagedObjectContextFactory newEntityForName:@"MessageLog"];
                                    messageLog.seq = stanzaID;
                                    NSError *err = nil;
                                    messageLog.message = [NSJSONSerialization dataWithJSONObject:log
                                                                                         options:NSJSONWritingPrettyPrinted
                                                                                           error:&err];
                                    if(err){
                                        NSLog(@"error %@", err);
                                    }
                                    
                                    messageLog.msgID = [NSString stringWithFormat:@"%@", [log valueForKey:kMessageID]];
                                    messageLog.msgDate = [parser dateFromString:[NSString stringWithFormat:@"%@ %@",[log valueForKey:kDate], [log valueForKey:kTime]]];
                                    messageLog.roomID = self->xmppRoomID;
                                    messageLog.readStatus = NO;
                                    messageLog.user = self->user;
                                    [self->user addMessagesObject:messageLog];
                                    //[self->allMessages addObject:messageLog];
                                    [messages addObject:messageLog];
                                    
                                }else{
                                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"msgID == %@ and message != nil and readStatus == NO",
                                                              [NSString stringWithFormat:@"%@", [log valueForKey:kMessageID]]];
                                    NSString *sender =  [log valueForKey:kSender];
                                    NSString *groupID = [log valueForKey:kGroupID];
                                    if(sender && groupID){
                                        
                                        NSString *keyReadUser = [NSString stringWithFormat:@"%@-%@", sender, groupID];
                                        NSMutableArray *readUserArr = nil;
                                        NSArray *msgArr = [messages filteredArrayUsingPredicate:predicate];
                                        if (msgArr.count > 0) {
                                            MessageLog *msgLog = msgArr.firstObject;
                                            NSJSONSerialization *jsonLog = [NSJSONSerialization JSONObjectWithData:msgLog.message
                                                                                                           options:NSJSONReadingMutableContainers
                                                                                                             error:nil];
                                            if(![jsonLog valueForKey:kReadUser]){
                                                readUserArr = [NSMutableArray arrayWithArray:@[keyReadUser]];
                                            }else{
                                                readUserArr = [NSMutableArray arrayWithArray:[jsonLog valueForKey:kReadUser]];
                                                if(![readUserArr containsObject:keyReadUser]){
                                                    [readUserArr addObject:keyReadUser];
                                                }
                                            }
                                            [jsonLog setValue:readUserArr forKey:kReadUser];
                                            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonLog
                                                                                               options:NSJSONWritingPrettyPrinted
                                                                                                 error:nil];
                                            msgLog.message = jsonData;
                                        }
                                    }
                                    
                                }
                                
                            }
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [ManagedObjectContextFactory saveContext];
                            });
                            //NSLog(@"cnt %ld", messages.count);
                            if(messages.count > 0) {
                                if(msgID) {
                                    [self loadPreviousHistory:message handler:handler];
                                }else{
                                    [self loadLogDataWithScroll:NO];
                                }
                            }else{
                                if(handler){
                                    handler(NO);
                                }
                            }

                        }else{
                            if(handler){
                                handler(NO);
                            }
                        }
                    }];
}



- (void)loadVideoPreview:(UIImageView *)imageView withVideoID:(NSString *)videoID handler:(SuccessBlock)handler {
    static NSString *placeholderB64 = @"iVBORw0KGgoAAAANSUhEUgAAASwAAAEsCAMAAABOo35HAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBNYWNpbnRvc2giIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6NzlGMkNGQTAyMEZBMTFFMzlDRjFCMUIzOEZBODdGQjkiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6NzlGMkNGQTEyMEZBMTFFMzlDRjFCMUIzOEZBODdGQjkiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo1MjJBODZGOTIwRjcxMUUzOUNGMUIxQjM4RkE4N0ZCOSIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo1MjJBODZGQTIwRjcxMUUzOUNGMUIxQjM4RkE4N0ZCOSIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PteaQZ0AAAAzUExURbi9wOLk5vHy88/T1fr6+8XKzNjb3ezu7sDFyL3CxPX29t3g4crO0dTY2efp6rO4vP///wV/cGoAAAARdFJOU/////////////////////8AJa2ZYgAAC5FJREFUeNrs3duamyAQAOARUDzj+z9tTXY3NQezOgdgyHLVrzc1f2EcgQFYMmi9n5vG1nUN4bGtf2mbZvYmh+eEpP+68atRFw41WNVm85lYfrBtQLR2bLz7ICxT4Zz+t84O5gOwzDBCYGkwRgeLieUq2wXWBmPVl4jVD3UQaW1jysLqhzYItm4ypWAJS/149QVgVXWI1NrKqcYyFkLMZr1arHidajMcJbuXGJZrICRpIBe9hLB6GxI2axRhmaRU18kKrwTL1yGDJsHFjtVnQSXDBSXFqicukzGWm0Jmzfa5YqVKFt62xuWI5buQZYMqO6x+DNk2ttAFBY/A/21y+WCZNmTeOp8JVn7vwFdtdDlg5RrYnwL9nB6rCWoaOXJB6dHqLnKZlFgVBF2tSYblxqCu1S4NlumCwgY+BZa6IUgfimgsG9Q2dMqFxHJtUNxaExNLZ7giBy4U1gxBe6tiYVWhgGbjYNlQRLMuAlYhVmuYd9JYul+DRC34XKvzKQR8sNWaQhg5rNKszmrBR1ud1ILPtjqnBR9udUoLPt3qjBZ8vFUInePFKtnqeHYKH/aNQ9KCP6vr5Ckf1hCKb5YLqwof0AYeLAOfgBVmDiwX06qu67H5bq/L8VOmW5BH0nCprPcvN8s6P69seaRbkPxFWE9HDiDo50a8m9VULNkXYd2cWpPiq0Z/3SYalhHs9RNqd5mfumRB/j2W68SkCFuljJgX9AQsmT1FQC84NZPMeGzxWCIBi6uUWaZOdsJiSQQszipmkaqqGYfFn2GBZa7IFagtBofC4t7eDo1ArTc/14jB8tzRQKgsnp1rOI/FnDVYuQM9uMPFfv4AUQZhK3g4BX9orc9icQ5CGBZNVvsDEeQH4eiUWe2+EXew+CpyGOqL4k8gjWewjI5uJTfZ5k9gcX1JMNYnx52Y7I5jca1QtEap1U4ZBghGd+vUWr1OtkAuussOQenFAXsMq4+1WJKz1csYD0JLFG2v3OpVHg8yuXsrfFZhjOKh+QBWLTTgWa1iLL52v2P5P6vdNxQIdKxCrJ67FvB3rFKsnrsWsHescqyeuhZwd6yCrJ66FjB3rKKsHrsW8HYs8fwq8sa66g2W/bN6+EH7WNSvQunvwQQbNv0uFrVjlWd1P8G8xaLuHq3Kswqh38EibpqxJVrdbavZYnU5B/dUG8y3y2LAlTcIB/d0m/Grl1i08D4UarXNHv5jOYllSf1W23c88IR3cMVabUL8fyzSpPZcrlWAZyyT7SBMXmg1P2FR9mNBX7LV/64AHEnWULRVCO4BizIK28KtbqkWMIxCX7jVbRwCfRSOpVvdxiHQR2Gvw4pyIV51h0UYhVaJFWVz0HiH1WXYsZitKNN1sMXqM+xY3FakfWfzBmvIr2PxW1G61rTBGrPrWAJWlK7VbrCy61giVpSu1d+wPPU1ocOKMr9Z3bDw3dOrsiK8yOwNC73DodNlRf2lQApZgzKrZSYFLSCFLKfMav29lKAFlJBl1Vnhv+umbyx0ljWrs8LPGLTfWNjnA31WhHWZL6w+n1Eob4X/svNXrDmbURjBCt81hitWk8sojGGFH4f2ijVmMgrjWKHfh+0VCzvxV6m0wmeVV6w8MtJYVviXv1mxPL5bqrRCh515xcJWj09KrdDJQ7NiNWhonVboJH5cscb0ISuqFTpo1SsWcoqnU2uFntRasZJnWbGt0IEHjzWotUJ/33nAZg5erRX68xCP5dRaodPwAZBpFii2wkb4BpDRrlZshV0+nLBYk2Ir7OuwBmRO2ii2wr4Oa0CO31mxFXbuoMViecVW6DIlQM6y9oqt0LkD9pFVWynDSmuFTbSQDw2qrSJj1aqtNGElt1KEld4Ku3YYHysDK+z3TnSsHKy0YGVhpQQrDysdWJlYqcDKxUoDVjZWCvKsfKwiY3WqrSJjBdVWuWNVOVlht4YAcpPkyZlS1kvO6dsssJN/UebgM7NyOWNlZoVd3emwWI1iKyxWHWFFOjsr/Iq0+F6H/Kywc38WkHt3uyRWTDvo0LtosPuzUlhx1XRAbCyv1wqbOXjZPaVZWqFrSrzobuU8rdC7lR1g6+9atVboMgl80UBQa4WtGawJtTteqxV2Z/eldgdfJKXTCv1kDaHesFZqhT7qqFqx0EebO51W6DJnfyn7xc5hzjqt0GeGXGuksa9Dq9IKHaM70rkOnUor9LkO4xUL/duMRit0iG6uWOhBPCm0wj+ep51y1Cm0wh9k576wasZxmLsV+tyeS89gPpktdyv8iVB2IZ75B+qs8CduD99Yjuv35G+FPwvSfGPhj3arlVnhj8C9OgFhaeghxCuwwo+h8YaFP+rUqrIinIs83LAId6o5TVb4I12/hhDQMrXbFKAKK/xDfuXfQOye39fPqbAi3NRhN1iEm2QaPVaEp9zeYUEwv3QtHVaUK2DcFstSupYSK8Jj3t+7g08eAgw6rCgda7jDWjK5lEvQinDrzs92Y1jI41CHFeX69Z+tCj9Yc+FWpDsJhwesHMahpBXpytn+EcsWbUW6y/g2twL0mR4NVgPLkwHHmzV7q54UZNwz1lSuFWGi4G4aCujrHvlb0dJm/wKLxp+zFa3UcbM6CkJTnRlZkd6Ed5v2gGUeMWsrYjB2r7GaIq2IA2a7kAwcS9s5W1Frs/0OVpIsXtjKEdPHu+3+wLNem6sV+RVf7WLFzx6krahj5X5X1T3WXJgV+ZU1vMGK/IEobUXOHMG9w6r+rHYS0hdYMbtW/lYPHesJqyrGygTujvWEFa1rifcrYO9Yi+xOR81j8EXdGyxJupYGq6eO9QLLF2DFskj+XDIP/F8I+vP2F8n7HpZXbuV4/rfnQ1jSkw/SczIty1O+KtQF9oWjxFYz08Obg1iiU6Z5zyG/nCB9jyWYPsha9S3TY4I7juV1Wg1s8eP1STs7N3eMCq0c30PvHMMA/Fu/ElkNjI9sTmEtgzKrnjOV3jsMBaRm+qNaOdb39+4BTiCzSyeuVcX7rOY0FvdABDEsz5zo7J9IBHJLbk9fpjK1qdyP+eYUtTdY/G9Efq6KP7QaFJbEKmLXOMawXgl8abw7+BFifGjdxS5rmJKFSSIXfHsq2Fss1waJ1g6O3qlkpijBobF471LYtrGieM1W6rneH2QI8hP/u149rk9Zuem2X84x/O0KTNEN3+00n+tgvmkln2dcaFiL6NNdwapDEb+fG+mVlNZRsVyMbbm1bfwumfHDVEd4Cvj1P+33m2hNvE3M9Yq2ttlf2uVPtq7j/eu/X7p+4NredPvjo7YDx5AfueO4+QSrI6eQH7oQ2pZv1S5cWCnLeiJZOT4s15ZtBcfSvYP3spetBQe/7Y9eYm/gz+owVsFah62OY5WrdXz69jhWqVonprpPYJWpdWZZ4AxWiVqnllBOYRWndXIx8xxWYVpwcu3kJBbXjk2VVqexCsrlT1udx+LaOa3k25mIVciMTY1Yi8NglTAbiLrcFYXFvSEqyzlkNizlKQTMS0ws1S/FFrs1BYuV+nAyQhvR2yzwWFoDF+EuagKWymy+o+wOo2AtbvqcIUjG4itYi/QWJF4HT8RS9fHTUjdoUrF4a2ZEW0P+qXQs3rKZfLsVD5aKztVw/E4WLM5SP5kphn7JB4u/gCbhTLs81rI0uY7Fia2ogw9r6W3BI5AbS6BCi/514zl/HyvWmtFnFbq4q9CYsZalyoYLGu7fxo6VCxdwFuvJYeXA1QlQCWGJVJgmjFXCWOubMVlSX89Sv0kMS6rU9LdQxVUoGxnrMhojTzx3g5P8ObJYUbuXaKeKgnVJVKN8Bo2V/C+JgHUp1R2lpVyM3xEFaxEtbY4kFRHrmk1M7PG+m+aIPyAm1iXeV5Ytuwdk/b4arC8weg9rbWyoRFhfQ3KwSLF2bLxL89CpsK7NzM1Yn2KqfMrnTYr108v8uzp7qOvxUpHv0j9oDljbvubvW15P90+AAQAz/egmuzAEAwAAAABJRU5ErkJggg==";
    
    UIImage *placeholder = [UIImage imageWithData:[NSData dataWithBase64EncodedString:placeholderB64]];
    [self loadVideoPreview:imageView withVideoID:videoID placeholder:placeholder handler:handler];

}

- (void)loadVideoPreview:(UIImageView *)imageView withVideoID:(NSString *)videoID placeholder:(UIImage*) placeholder handler:(SuccessBlock)handler {
    //NSFileManager *fileMgr = [NSFileManager defaultManager];
    //NSURL *tmpDirURL = [NSURL fileURLWithPath:NSTemporaryDirectory() isDirectory:YES];
    NSString *imageID = [videoID stringByReplacingOccurrencesOfString:@":video" withString:@""];
    
        
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@file?id=%@&mediaType=video&outputType=img-preview", kSERVER_HOST, imageID]];
    
    //NSLog(@"preview view: %@", url);
    //NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSMutableURLRequest  *req =[NSMutableURLRequest requestWithURL:url];
    [req setCachePolicy:NSURLRequestReturnCacheDataElseLoad];
    [req setTimeoutInterval:20];
    [req setValue:[AppHelper getUserDataForKey:kADA_CLIENT] forHTTPHeaderField:kADA_CLIENT];
    
    imageView.image = placeholder;

    
    AFImageResponseSerializer* serializer = (AFImageResponseSerializer*)[UIImageView sharedImageDownloader].sessionManager.responseSerializer;
    serializer.acceptableContentTypes = nil;//
        //serializer.acceptableContentTypes = [serializer.acceptableContentTypes setByAddingObject:@"image/jpg"];
    __block UIImageView *currentImageView = imageView;
    [imageView setImageWithURLRequest:req
                        placeholderImage:placeholder
                                 success:^(NSURLRequest * req, NSHTTPURLResponse * res, UIImage * image) {
                                     
                                     if(image){
                                         CGFloat x = MIN(image.size.width, image.size.height);
                                         UIImage *vImage = [UIImage mergeImage:image withImage:[UIImage imageWithText:@"" inSize: CGSizeMake(x, x) color:[UIColor whiteColor] withFont:[UIFont fontWithName:@"FontAwesome" size:80]]];
                                         if(image.size.width > image.size.height) {
                                             currentImageView.contentMode = UIViewContentModeScaleAspectFit;
                                             //since the width > height we may fit it and we'll have bands on top/bottom
                                         } else {
                                             currentImageView.contentMode = UIViewContentModeScaleAspectFill;
                                             //width < height we fill it until width is taken up and clipped on top/bottom
                                         }
                                         currentImageView.image = vImage;
 
                                         if (handler) {
                                             handler(YES);
                                         }
                                     }
                                     
                                 } failure:^(NSURLRequest *req, NSHTTPURLResponse * res, NSError * error) {
 
                                     NSLog(@"error %@", error);
                                     if (handler) {
                                         handler(NO);
                                     }
                                     
                                 }];
        
    

}

- (void) loadSticker:(UIImageView*) imageView withCode:(NSString*) stickerID handler:(SuccessBlock) handler {
    
    static NSString *placeholder = @"iVBORw0KGgoAAAANSUhEUgAAASwAAAEsCAMAAABOo35HAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBNYWNpbnRvc2giIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6NzlGMkNGQTAyMEZBMTFFMzlDRjFCMUIzOEZBODdGQjkiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6NzlGMkNGQTEyMEZBMTFFMzlDRjFCMUIzOEZBODdGQjkiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo1MjJBODZGOTIwRjcxMUUzOUNGMUIxQjM4RkE4N0ZCOSIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo1MjJBODZGQTIwRjcxMUUzOUNGMUIxQjM4RkE4N0ZCOSIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PteaQZ0AAAAzUExURbi9wOLk5vHy88/T1fr6+8XKzNjb3ezu7sDFyL3CxPX29t3g4crO0dTY2efp6rO4vP///wV/cGoAAAARdFJOU/////////////////////8AJa2ZYgAAC5FJREFUeNrs3duamyAQAOARUDzj+z9tTXY3NQezOgdgyHLVrzc1f2EcgQFYMmi9n5vG1nUN4bGtf2mbZvYmh+eEpP+68atRFw41WNVm85lYfrBtQLR2bLz7ICxT4Zz+t84O5gOwzDBCYGkwRgeLieUq2wXWBmPVl4jVD3UQaW1jysLqhzYItm4ypWAJS/149QVgVXWI1NrKqcYyFkLMZr1arHidajMcJbuXGJZrICRpIBe9hLB6GxI2axRhmaRU18kKrwTL1yGDJsHFjtVnQSXDBSXFqicukzGWm0Jmzfa5YqVKFt62xuWI5buQZYMqO6x+DNk2ttAFBY/A/21y+WCZNmTeOp8JVn7vwFdtdDlg5RrYnwL9nB6rCWoaOXJB6dHqLnKZlFgVBF2tSYblxqCu1S4NlumCwgY+BZa6IUgfimgsG9Q2dMqFxHJtUNxaExNLZ7giBy4U1gxBe6tiYVWhgGbjYNlQRLMuAlYhVmuYd9JYul+DRC34XKvzKQR8sNWaQhg5rNKszmrBR1ud1ILPtjqnBR9udUoLPt3qjBZ8vFUInePFKtnqeHYKH/aNQ9KCP6vr5Ckf1hCKb5YLqwof0AYeLAOfgBVmDiwX06qu67H5bq/L8VOmW5BH0nCprPcvN8s6P69seaRbkPxFWE9HDiDo50a8m9VULNkXYd2cWpPiq0Z/3SYalhHs9RNqd5mfumRB/j2W68SkCFuljJgX9AQsmT1FQC84NZPMeGzxWCIBi6uUWaZOdsJiSQQszipmkaqqGYfFn2GBZa7IFagtBofC4t7eDo1ArTc/14jB8tzRQKgsnp1rOI/FnDVYuQM9uMPFfv4AUQZhK3g4BX9orc9icQ5CGBZNVvsDEeQH4eiUWe2+EXew+CpyGOqL4k8gjWewjI5uJTfZ5k9gcX1JMNYnx52Y7I5jca1QtEap1U4ZBghGd+vUWr1OtkAuussOQenFAXsMq4+1WJKz1csYD0JLFG2v3OpVHg8yuXsrfFZhjOKh+QBWLTTgWa1iLL52v2P5P6vdNxQIdKxCrJ67FvB3rFKsnrsWsHescqyeuhZwd6yCrJ66FjB3rKKsHrsW8HYs8fwq8sa66g2W/bN6+EH7WNSvQunvwQQbNv0uFrVjlWd1P8G8xaLuHq3Kswqh38EibpqxJVrdbavZYnU5B/dUG8y3y2LAlTcIB/d0m/Grl1i08D4UarXNHv5jOYllSf1W23c88IR3cMVabUL8fyzSpPZcrlWAZyyT7SBMXmg1P2FR9mNBX7LV/64AHEnWULRVCO4BizIK28KtbqkWMIxCX7jVbRwCfRSOpVvdxiHQR2Gvw4pyIV51h0UYhVaJFWVz0HiH1WXYsZitKNN1sMXqM+xY3FakfWfzBmvIr2PxW1G61rTBGrPrWAJWlK7VbrCy61giVpSu1d+wPPU1ocOKMr9Z3bDw3dOrsiK8yOwNC73DodNlRf2lQApZgzKrZSYFLSCFLKfMav29lKAFlJBl1Vnhv+umbyx0ljWrs8LPGLTfWNjnA31WhHWZL6w+n1Eob4X/svNXrDmbURjBCt81hitWk8sojGGFH4f2ijVmMgrjWKHfh+0VCzvxV6m0wmeVV6w8MtJYVviXv1mxPL5bqrRCh515xcJWj09KrdDJQ7NiNWhonVboJH5cscb0ISuqFTpo1SsWcoqnU2uFntRasZJnWbGt0IEHjzWotUJ/33nAZg5erRX68xCP5dRaodPwAZBpFii2wkb4BpDRrlZshV0+nLBYk2Ir7OuwBmRO2ii2wr4Oa0CO31mxFXbuoMViecVW6DIlQM6y9oqt0LkD9pFVWynDSmuFTbSQDw2qrSJj1aqtNGElt1KEld4Ku3YYHysDK+z3TnSsHKy0YGVhpQQrDysdWJlYqcDKxUoDVjZWCvKsfKwiY3WqrSJjBdVWuWNVOVlht4YAcpPkyZlS1kvO6dsssJN/UebgM7NyOWNlZoVd3emwWI1iKyxWHWFFOjsr/Iq0+F6H/Kywc38WkHt3uyRWTDvo0LtosPuzUlhx1XRAbCyv1wqbOXjZPaVZWqFrSrzobuU8rdC7lR1g6+9atVboMgl80UBQa4WtGawJtTteqxV2Z/eldgdfJKXTCv1kDaHesFZqhT7qqFqx0EebO51W6DJnfyn7xc5hzjqt0GeGXGuksa9Dq9IKHaM70rkOnUor9LkO4xUL/duMRit0iG6uWOhBPCm0wj+ep51y1Cm0wh9k576wasZxmLsV+tyeS89gPpktdyv8iVB2IZ75B+qs8CduD99Yjuv35G+FPwvSfGPhj3arlVnhj8C9OgFhaeghxCuwwo+h8YaFP+rUqrIinIs83LAId6o5TVb4I12/hhDQMrXbFKAKK/xDfuXfQOye39fPqbAi3NRhN1iEm2QaPVaEp9zeYUEwv3QtHVaUK2DcFstSupYSK8Jj3t+7g08eAgw6rCgda7jDWjK5lEvQinDrzs92Y1jI41CHFeX69Z+tCj9Yc+FWpDsJhwesHMahpBXpytn+EcsWbUW6y/g2twL0mR4NVgPLkwHHmzV7q54UZNwz1lSuFWGi4G4aCujrHvlb0dJm/wKLxp+zFa3UcbM6CkJTnRlZkd6Ed5v2gGUeMWsrYjB2r7GaIq2IA2a7kAwcS9s5W1Frs/0OVpIsXtjKEdPHu+3+wLNem6sV+RVf7WLFzx6krahj5X5X1T3WXJgV+ZU1vMGK/IEobUXOHMG9w6r+rHYS0hdYMbtW/lYPHesJqyrGygTujvWEFa1rifcrYO9Yi+xOR81j8EXdGyxJupYGq6eO9QLLF2DFskj+XDIP/F8I+vP2F8n7HpZXbuV4/rfnQ1jSkw/SczIty1O+KtQF9oWjxFYz08Obg1iiU6Z5zyG/nCB9jyWYPsha9S3TY4I7juV1Wg1s8eP1STs7N3eMCq0c30PvHMMA/Fu/ElkNjI9sTmEtgzKrnjOV3jsMBaRm+qNaOdb39+4BTiCzSyeuVcX7rOY0FvdABDEsz5zo7J9IBHJLbk9fpjK1qdyP+eYUtTdY/G9Efq6KP7QaFJbEKmLXOMawXgl8abw7+BFifGjdxS5rmJKFSSIXfHsq2Fss1waJ1g6O3qlkpijBobF471LYtrGieM1W6rneH2QI8hP/u149rk9Zuem2X84x/O0KTNEN3+00n+tgvmkln2dcaFiL6NNdwapDEb+fG+mVlNZRsVyMbbm1bfwumfHDVEd4Cvj1P+33m2hNvE3M9Yq2ttlf2uVPtq7j/eu/X7p+4NredPvjo7YDx5AfueO4+QSrI6eQH7oQ2pZv1S5cWCnLeiJZOT4s15ZtBcfSvYP3spetBQe/7Y9eYm/gz+owVsFah62OY5WrdXz69jhWqVonprpPYJWpdWZZ4AxWiVqnllBOYRWndXIx8xxWYVpwcu3kJBbXjk2VVqexCsrlT1udx+LaOa3k25mIVciMTY1Yi8NglTAbiLrcFYXFvSEqyzlkNizlKQTMS0ws1S/FFrs1BYuV+nAyQhvR2yzwWFoDF+EuagKWymy+o+wOo2AtbvqcIUjG4itYi/QWJF4HT8RS9fHTUjdoUrF4a2ZEW0P+qXQs3rKZfLsVD5aKztVw/E4WLM5SP5kphn7JB4u/gCbhTLs81rI0uY7Fia2ogw9r6W3BI5AbS6BCi/514zl/HyvWmtFnFbq4q9CYsZalyoYLGu7fxo6VCxdwFuvJYeXA1QlQCWGJVJgmjFXCWOubMVlSX89Sv0kMS6rU9LdQxVUoGxnrMhojTzx3g5P8ObJYUbuXaKeKgnVJVKN8Bo2V/C+JgHUp1R2lpVyM3xEFaxEtbY4kFRHrmk1M7PG+m+aIPyAm1iXeV5Ytuwdk/b4arC8weg9rbWyoRFhfQ3KwSLF2bLxL89CpsK7NzM1Yn2KqfMrnTYr108v8uzp7qOvxUpHv0j9oDljbvubvW15P90+AAQAz/egmuzAEAwAAAABJRU5ErkJggg==";
    
    UIImage *placeholderImage = [UIImage imageWithData:[NSData dataWithBase64EncodedString:placeholder]];
    
    imageView.image = placeholderImage;
    
    
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    NSURL *tmpDirURL = [NSURL fileURLWithPath:NSTemporaryDirectory() isDirectory:YES];
    if([stickerID rangeOfString:@":sticker"].location != NSNotFound){
        
        fileMgr = [NSFileManager defaultManager];
        
        
        NSString *imgFolder = [NSString stringWithFormat:@"%@chat/sticker/images", tmpDirURL];
        imgFolder = [imgFolder stringByReplacingOccurrencesOfString:@"file://" withString:@""];
        if (![fileMgr fileExistsAtPath:imgFolder]) {
            if([fileMgr createDirectoryAtPath:imgFolder withIntermediateDirectories:YES attributes:nil error:nil]){
                //NSLog(@"Dir created");
            }
        }
        NSString *type = [stickerID stringByReplacingOccurrencesOfString:@":sticker" withString:@""];
        NSString *strPath = [NSString stringWithFormat:@"%@/%@", imgFolder, type];
        
        if ([fileMgr fileExistsAtPath:strPath]){
            NSData *imgData = [fileMgr contentsAtPath:strPath];
            imageView.image = [UIImage imageWithData:imgData];
            if(handler) {
                handler(YES);
            }
            
        }else{
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@file?id=%@&mediaType=sticker", kSERVER_HOST, type]];
            //NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            NSMutableURLRequest  *req =[NSMutableURLRequest requestWithURL:url];
            [req setCachePolicy:NSURLRequestReturnCacheDataElseLoad];
            [req setTimeoutInterval:20];
            [req setValue:[AppHelper getUserDataForKey:kADA_CLIENT] forHTTPHeaderField:kADA_CLIENT];
            
            __block UIImageView *imgView = imageView;
            [imageView setImageWithURLRequest:req
                                placeholderImage:[UIImage imageNamed:@"emo-76"]
                                         success:^(NSURLRequest * req, NSHTTPURLResponse * res, UIImage * image) {
                                             
                                             imgView.image = image;
                                             NSData *imgData = UIImagePNGRepresentation(imgView.image);
                                             if([imgData writeToFile:strPath atomically:YES]){
                                                 //NSLog(@"write success");
                                             }
                                             if(handler) {
                                                 handler(YES);
                                             }
                                         } failure:^(NSURLRequest * req, NSHTTPURLResponse * res, NSError * error) {
                                             //NSLog(@"%@", error);
                                             if(handler) {
                                                 handler(NO);
                                             }
                                         }];
            
        }

        
        return;
    }
    
    NSJSONSerialization *item = nil;
    NSString *codeString  = [NSString stringWithFormat:@"$sticker:%@", stickerID];
    NSString *stickerImageID = nil;
    
    
    for (NSJSONSerialization *stickerSet in self.stickerSet) {
        
        NSArray *details = [stickerSet valueForKey:@"detail"];
        
        NSArray *result = [details filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"code == %@", codeString]];
        if (result.count > 0) {
            item = result.firstObject;
            stickerImageID = [NSString stringWithFormat:@"%@", [stickerSet valueForKey:@"id"]];
            break;
        }
        
    }
    
    
    if (item) {
        dispatch_group_t group = dispatch_group_create();
        dispatch_group_enter(group);
        
        
        
        
        
        NSString *imgFolder = [NSString stringWithFormat:@"%@chat/stickerSet/%@", tmpDirURL, stickerImageID];
        imgFolder = [imgFolder stringByReplacingOccurrencesOfString:@"file://" withString:@""];
        if (![fileMgr fileExistsAtPath:imgFolder]) {
            if([fileMgr createDirectoryAtPath:imgFolder withIntermediateDirectories:YES attributes:nil error:nil]){
                //NSLog(@"Dir created");
            }
        }
        NSString *imageID = [item valueForKey:@"code"];
        NSString *strPath = [NSString stringWithFormat:@"%@/%@", imgFolder, imageID];
        
        
        
        
        if ([fileMgr fileExistsAtPath:strPath]){
            NSData *imgData = [fileMgr contentsAtPath:strPath];
            UIImage *image = [UIImage imageWithData:imgData];
            imageView.image = image;
            dispatch_group_leave(group);
            if(handler) {
                handler(YES);
            }
        }else{
            
            //NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            NSMutableURLRequest *req = [URLConnection requestFromURL:[NSString stringWithFormat:@"%@file", kSERVER_HOST]
                                        
                                                              method:@"GET"
                                                           parameter:@{
                                                                       @"id": [item valueForKey:@"image_id"],
                                                                       @"mediaType": @"chatSticker",
                                                                       @"raw": @"true"
                                                                       }];
            [req setCachePolicy:NSURLRequestReturnCacheDataElseLoad];
            [req setTimeoutInterval:20];
            [req setValue:[AppHelper getUserDataForKey:kADA_CLIENT] forHTTPHeaderField:kADA_CLIENT];
            __block UIImageView *imgView = imageView;
            
            AFImageResponseSerializer* serializer = (AFImageResponseSerializer*)[UIImageView sharedImageDownloader].sessionManager.responseSerializer;
            serializer.acceptableContentTypes = nil;
            //serializer.acceptableContentTypes = [serializer.acceptableContentTypes setByAddingObject:@"image/jpg"];
            
            [imageView setImageWithURLRequest:req
                           placeholderImage:placeholderImage
                                    success:^(NSURLRequest * req, NSHTTPURLResponse * res, UIImage * image) {
                                        
                                        if (image) {
                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                
                                                
                                                
                                                imgView.image = image;
                                                NSData *imgData = UIImagePNGRepresentation(image);
                                                if([imgData writeToFile:strPath atomically:YES]){
                                                    //NSLog(@"write success");
                                                }
                                            });
                                        }
                                        dispatch_group_leave(group);
                                        if(handler) {
                                            handler(YES);
                                        }
                                    } failure:^(NSURLRequest *req, NSHTTPURLResponse * res, NSError * error) {
                                        dispatch_group_leave(group);
                                        if(handler) {
                                            handler(NO);
                                        }
                                    }];
        }
        
        
        
        dispatch_group_notify(group, dispatch_get_main_queue(), ^{
            //success(usersWithImage);
        });
    }

}


- (void) loadStickerSet:(SuccessBlock)handler {
    
    if(!self.stickerSet){
        self.stickerSet = [[NSMutableArray alloc] init];
    }
    
    [ServiceCaller getJSONCache:[NSString stringWithFormat:@"%@sticker/get", kSERVER]
                         method:@"GET"
                         header:nil
                      parameter:nil
                        handler:^(NSJSONSerialization *json) {
                            BOOL finished = NO;
                            if(json){
                                [self.stickerSet removeAllObjects];
                                if([[json valueForKey:@"result"] boolValue] && [json valueForKey:@"data"] && [[json valueForKey:@"data"] count] > 0){
                                    [self.stickerSet addObjectsFromArray:[json valueForKey:@"data"]];
                                }
                                self.stickerLoaded = YES;
                                if(handler){
                                    handler(YES);
                                }
                                finished = YES;
                            }
                            [ServiceCaller callJSON:[NSString stringWithFormat:@"%@sticker/get", kSERVER]
                                             method:@"GET"
                                             header:nil
                                          parameter:nil
                                            handler:^(NSJSONSerialization *json) {
                                                if(json){
                                                    [self.stickerSet removeAllObjects];
                                                    if([[json valueForKey:@"result"] boolValue] && [json valueForKey:@"data"] && [[json valueForKey:@"data"] count] > 0){
                                                        [self.stickerSet addObjectsFromArray:[json valueForKey:@"data"]];
                                                    }
                                                    self.stickerLoaded = YES;
                                                    if(!finished){
                                                        if(handler){
                                                            handler(YES);
                                                        }
                                                    }
                                                }
                                            }];

                        }];

    
    
    }

NSString * toJSONString(id msg) {
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:msg
                                                       options:kNilOptions
                                                         error: nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData
                                                 encoding:NSUTF8StringEncoding];
    
    
    return jsonString;
}

- (void)markingReadStatus:(NSArray<ChatMessage*>*) messages {
    //    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    //NSLog(@"markingReadStatus");
    NSString *currentUser = [NSString stringWithFormat:@"%@-%@", self->ada.login.usid, self->ada.login.group];

    if(!messages){
        return ;
    }
    
    
    @try {
        for (ChatMessage *message in messages) {
            
            
            BOOL senderMsg = [message.senderID isEqualToString:self->ada.login.usid] && [message.senderGroup isEqualToString:self->ada.login.group];
            if (senderMsg) {
                continue;
            }
            
            if(!message.readUsers){
                
                message.readUsers = [[NSMutableSet<NSString*> alloc] init];
            }
            NSSet<NSString*> *readMembers = message.readUsers;
            BOOL found = NO;
            for (NSString *member in readMembers) {
                if([member isEqualToString:currentUser]){
                    found = YES;
                    break;
                }
            }
            if(!found){
                NSMutableSet<NSString*> *users = [NSMutableSet setWithSet: message.readUsers];
                [users addObject:currentUser];
                message.readUsers = users;
                NSDictionary *readMsg = @{
                                          kMessageID: [NSString stringWithFormat:@"%@",message.messageID],
                                          kRoomID: self->xmppRoomID,
                                          kAction: @"readMsg"
                                          };
                
                //NSLog(@"sending %@", readMsg);
                [self sendStreamMessage:toJSONString(readMsg)];
                
                NSString *keyReadUser = [NSString stringWithFormat:@"%@-%@", message.senderID, message.senderGroup];
                NSString *msgID = [NSString stringWithFormat:@"%@",message.messageID];
                NSString *roomID = self->xmppRoomID;
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"msgID == %@ and roomID == %@",
                                          msgID,
                                          roomID];
                
                NSMutableArray *readUserArr = nil;
                
                NSArray *msgLogs = self->userMessages;
                
                NSArray *msgArr = [msgLogs filteredArrayUsingPredicate:predicate];
                if (msgArr.count > 0) {
                    
                    MessageLog *msgLog = msgArr.firstObject;
                    NSJSONSerialization *jsonLog = [NSJSONSerialization JSONObjectWithData:msgLog.message
                                                                                   options:NSJSONReadingMutableContainers
                                                                                     error:nil];
                    
                    //NSLog(@"%@ %@", [jsonLog valueForKey:kMessage], [jsonLog valueForKey:kReadUser]);
                    
                    if(![jsonLog valueForKey:kReadUser]){
                        readUserArr = [NSMutableArray arrayWithArray:@[keyReadUser]];
                    }else{
                        readUserArr = [NSMutableArray arrayWithArray:[jsonLog valueForKey:kReadUser]];
                        if(![readUserArr containsObject:keyReadUser]){
                            [readUserArr addObject:keyReadUser];
                        }
                    }
                    [jsonLog setValue:readUserArr forKey:kReadUser];
                    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonLog
                                                                       options:NSJSONWritingPrettyPrinted
                                                                         error:nil];
                    msgLog.message = jsonData;
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [ManagedObjectContextFactory saveContext];
                    });
                }

            }
            
            
            
            
            
        }
    }
    @catch (NSException *exception) {
        NSLog(@"Error %@", exception);
    }
    @finally {
        
    }
    
    
    
    //});
}



#pragma mark AVPlayerViewControllerDelegate

- (void) playVideo:(ChatMessage*) message {
    if (message.messageType != ChatMessageTypeVideo) {
        return;
    }
    self->skipLeaveConnection = YES;
    NSString *strURL = [NSString stringWithFormat:@"%@file?id=%@&mediaType=%@",
                         kSERVER_HOST,
                         [message.videoMessage.videoID stringByReplacingOccurrencesOfString:@":video" withString:@""],
                         @"video"
                         ];
    //NSLog(@"opening : %@", strURL);
    NSURL *url = [NSURL URLWithString:strURL];
    
    AVPlayerViewController *playerViewController = [[AVPlayerViewController alloc] init];
    
    
    
    NSMutableDictionary * headers = [NSMutableDictionary dictionary];
    //NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [headers setObject:[AppHelper getUserDataForKey:kADA_CLIENT] forKey:kADA_CLIENT];
    AVURLAsset * asset = [AVURLAsset URLAssetWithURL:url options:@{@"AVURLAssetHTTPHeaderFieldsKey" : headers}];
    AVPlayerItem * item = [AVPlayerItem playerItemWithAsset:asset];
    
    
    
    self.player = [[AVPlayer alloc] initWithPlayerItem: item];
    
    playerViewController.player = _player;
    playerViewController.delegate = self;
    
    [[NSNotificationCenter defaultCenter]  addObserverForName:AVPlayerItemDidPlayToEndTimeNotification
                                                       object:item queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification * _Nonnull note) {
                                                           [playerViewController dismissViewControllerAnimated:YES completion:nil];
                                                       }];
    
    
    [playerViewController.view setFrame:CGRectMake(0, 0,
                                                   self.view.bounds.size.width,
                                                   self.view.bounds.size.width)];
    
    playerViewController.showsPlaybackControls = YES;
    
    
    [self presentViewController:playerViewController animated:YES completion:^{
        
        
        [self.player play];
    }];
    
}


- (void)itemDidFinishPlaying:(NSNotification *)notification {
    
    //NSLog(@"end...");
}

#pragma mark QuickViewDelegate

- (void) showImagePreview:(id) sender {
    ChatTapGestureRecognizer *gesture = (ChatTapGestureRecognizer*)sender;
    [self showImage:gesture.refObject];
    
}

- (void) showVideoPreview:(id) sender {
    ChatTapGestureRecognizer *gesture = (ChatTapGestureRecognizer*)sender;
    [self playVideo:gesture.refObject];
    
}

- (void) showImage:(ChatMessage*) message {
    if (message.messageType != ChatMessageTypeImage) {
        return;
    }
    self->skipLeaveConnection = YES;
    currentChatMessage = message;
    QLPreviewController *vc = [[QLPreviewController alloc] init];
    vc.dataSource = self;
    vc.delegate = self;
    [self presentViewController:vc animated:YES completion:^{
  
    }];

}

- (void) showFile:(ChatMessage*) message progress:(ProgressBlock) progress {
    if (message.messageType != ChatMessageTypeFile) {
        return;
    }
    [self->ada download:message.fileMessage.fileID
         saveName:message.fileMessage.name
            cache:YES
            progress:progress
          handler:^(NSData *data, NSString *filePath, NSError *error) {
              if(data){
                  self->skipLeaveConnection = YES;
                  self->currentChatMessage = message;
                  QLPreviewController *vc = [[QLPreviewController alloc] init];
                  vc.dataSource = self;
                  vc.delegate = self;
                  [self presentViewController:vc animated:YES completion:^{
                      
                  }];
              
              }
          }];
}

    
    


- (NSInteger)numberOfPreviewItemsInPreviewController:(QLPreviewController *)controller {
    return 1;
}


- (id <QLPreviewItem>)previewController:(QLPreviewController *)controller previewItemAtIndex:(NSInteger)index {
    self->skipLeaveConnection = YES;
    NSString *imageID = currentChatMessage.messageType == ChatMessageTypeImage ? currentChatMessage.imageMessage.imageID : currentChatMessage.fileMessage.fileID;
    
    
    
    NSInteger orientation = -1;
    if ([imageID rangeOfString:@"@"].location != NSNotFound) {
        NSArray *IDs = [imageID componentsSeparatedByString:@"@"];
        imageID = IDs.firstObject;
        orientation = [IDs.lastObject integerValue];
    }
    
    NSString *type = nil;
    
    if ([imageID rangeOfString:@":"].location != NSNotFound) {
        NSArray *IDs = [imageID componentsSeparatedByString:@":"];
        imageID = IDs.firstObject;
        type = IDs.lastObject;
    }
    
    
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    NSURL *tmpDirURL = [NSURL fileURLWithPath:NSTemporaryDirectory() isDirectory:YES];
    
    NSString *imgFolder = [NSString stringWithFormat:currentChatMessage.messageType == ChatMessageTypeImage ? @"%@image-cache/%@": @"%@download-cache/%@", tmpDirURL, type ? type : @"undefined"];
    imgFolder = [imgFolder stringByReplacingOccurrencesOfString:@"file://" withString:@""];
    if (![fileMgr fileExistsAtPath:imgFolder]) {
        if([fileMgr createDirectoryAtPath:imgFolder withIntermediateDirectories:YES attributes:nil error:nil]){
            //NSLog(@"Dir created");
        }
    }
    
    
    
    NSString *strPath = currentChatMessage.messageType == ChatMessageTypeImage ? [NSString stringWithFormat:@"%@/%@.jpg", imgFolder, imageID] :
    [NSString stringWithFormat:@"%@/%@", imgFolder, [currentChatMessage.fileMessage.name  stringByReplacingOccurrencesOfString:@"/" withString:@"-"]];
    
    if([fileMgr fileExistsAtPath:strPath]){
        //NSLog(@"has file");
    }
    
    //NSLog(@"opening : %@", strPath);
    return [NSURL fileURLWithPath:strPath];
    
    
}

#pragma mark - UIImagePickerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    self->skipLeaveConnection = NO;
    
    NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
    //video
    if (CFStringCompare ((__bridge CFStringRef) mediaType, kUTTypeMovie, 0) == kCFCompareEqualTo) {
        //NSLog(@"Movie %@", info);
        [self encodeVideo:[info objectForKey:UIImagePickerControllerMediaURL] handler:videoHandler];
        return;
    }
    
    
    
    if (picker.sourceType == UIImagePickerControllerSourceTypePhotoLibrary) {
        
        if(imageHandler) {
            UIImage *oImage = [info valueForKey:UIImagePickerControllerOriginalImage];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self dismissViewControllerAnimated:YES completion:^{
                    
                    CGSize imgSize = oImage.size;
                    //NSLog(@"oImage or %ld", (long)oImage.imageOrientation);
                    if (oImage.imageOrientation != UIImageOrientationUp && oImage.imageOrientation != UIImageOrientationDown) {
                        //imgSize = CGSizeMake(imgSize.height, imgSize.width);
                    }
                    
                    CGFloat ratio = imgSize.width/imgSize.height;
                    
                    int width = MIN(1024, MAX(1024, imgSize.width));//self.view.frame.size.width * 3;
                    int height = width * ratio;
                    
                    UIImage *image = [oImage scaleToSize:CGSizeMake(width, height)];
                    //NSLog(@"image or %ld", (long)image.imageOrientation);
                    ImageMessage *message = [ImageMessage new];
                    message.image = image;
                    message.width = width;
                    message.height = height;
                    message.sizeRatio = ratio;
                    self->imageHandler(message);
                }];
            });
            
            
        }
        
    }else{
        NSString *sourceType = [NSString stringWithFormat:@"%@", picker.mediaTypes];
        
        if([sourceType rangeOfString:(NSString*)kUTTypeMovie].location != NSNotFound){
            //NSLog(@"Movie %@", info);
            [self encodeVideo:[info objectForKey:UIImagePickerControllerMediaURL] handler:videoHandler];
        }else{
            if(imageHandler) {
                
                //camera photo
                UIImage *oImage = [info valueForKey:UIImagePickerControllerOriginalImage];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self dismissViewControllerAnimated:YES completion:^{
                        

                        CGSize imgSize = oImage.size;
                        //NSLog(@"oImage or %ld", (long)oImage.imageOrientation);
                        if (oImage.imageOrientation != UIImageOrientationUp && oImage.imageOrientation != UIImageOrientationDown) {
                            //imgSize = CGSizeMake(imgSize.height, imgSize.width);
                        }
                        
                        CGFloat ratio = imgSize.width/imgSize.height;
                        
                        int width = MIN(1024, MAX(1024, imgSize.width));//self.view.frame.size.width * 3;
                        int height = width * ratio;
        
                        UIImage *image = [oImage scaleToSize:CGSizeMake(width, height)];
                        //NSLog(@"image or %ld", (long)image.imageOrientation);
                        ImageMessage *message = [ImageMessage new];
                        message.image = image;
                        message.width = width;
                        message.height = height;
                        message.sizeRatio = ratio;
                        message.sourceCamera = YES;
                        self->imageHandler(message);
                    }];
                });
                
                
            }
        }
    }
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    self->skipLeaveConnection = NO;
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)encodeVideo:(NSURL *)videoURL handler:(ChatVideoBlock) handler{
    
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:videoURL options:nil];
    
    // Create the composition and tracks
    AVMutableComposition *composition = [AVMutableComposition composition];
    AVMutableCompositionTrack *videoTrack = [composition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
    AVMutableCompositionTrack *audioTrack = [composition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
    NSArray *assetVideoTracks = [asset tracksWithMediaType:AVMediaTypeVideo];
    if (assetVideoTracks.count <= 0)
    {
        NSLog(@"Error reading the transformed video track");
        return NO;
    }
    
    // Insert the tracks in the composition's tracks
    AVAssetTrack *assetVideoTrack = [assetVideoTracks firstObject];
    [videoTrack insertTimeRange:assetVideoTrack.timeRange ofTrack:assetVideoTrack atTime:CMTimeMake(0, 1) error:nil];
    [videoTrack setPreferredTransform:assetVideoTrack.preferredTransform];
    
    AVAssetTrack *assetAudioTrack = [[asset tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0];
    [audioTrack insertTimeRange:assetAudioTrack.timeRange ofTrack:assetAudioTrack atTime:CMTimeMake(0, 1) error:nil];
    
    
    //CGSize size = [assetVideoTrack naturalSize];
    //NSLog(@"size.width = %f size.height = %f", size.width, size.height);
    CGAffineTransform txf = [videoTrack preferredTransform];
    //NSLog(@"txf.a = %f txf.b = %f txf.c = %f txf.d = %f txf.tx = %f txf.ty = %f", txf.a, txf.b, txf.c, txf.d, txf.tx, txf.ty);
    
    int angle = txf.c == -1 ? 90 : txf.c == 1 ? -90 : txf.d == -1 ? 180 : 1 ;
    
    //NSLog(@"angle = %d", angle);
    // Export to mp4
    NSDateFormatter *videoDF = [NSDateFormatter instanceWithUSLocale];
    [videoDF setDateFormat:@"yyyyMMddHHmmss"];
    NSString *fileName = [NSString stringWithFormat:@"%@.mp4", [videoDF stringFromDate:[NSDate date]]];
    NSString *mp4Quality = AVAssetExportPresetMediumQuality;
    NSString *exportPath = [NSString stringWithFormat:@"%@/%@",
                            [NSHomeDirectory() stringByAppendingString:@"/tmp"],
                            fileName];
    
    
    NSURL *exportUrl = [NSURL fileURLWithPath:exportPath];
    AVAssetExportSession *exportSession = [[AVAssetExportSession alloc] initWithAsset:composition presetName:mp4Quality];
    exportSession.outputURL = exportUrl;
    CMTime start = CMTimeMakeWithSeconds(0.0, 0);
    CMTimeRange range = CMTimeRangeMake(start, [asset duration]);
    exportSession.timeRange = range;
    exportSession.outputFileType = AVFileTypeMPEG4;
    [exportSession exportAsynchronouslyWithCompletionHandler:^{
        switch ([exportSession status])
        {
            case AVAssetExportSessionStatusCompleted:
//                NSLog(@"MP4 Successful!");
                if(handler){
                    //handler(exportPath);
                    
                    VideoMessage *message = [VideoMessage new];
                    message.filePath = exportPath;
                    message.fileName = fileName;
                    message.angle = angle;
                    message.contentType = @"video/mp4";
                    handler(message);
                    
                }
                
                
                
                //[self uploadVideo: fileName path:exportPath angle:angle];
                
                break;
            case AVAssetExportSessionStatusFailed:
                NSLog(@"Export failed: %@", [[exportSession error] localizedDescription]);
                if(handler){
                    handler(nil);
                }
                break;
            case AVAssetExportSessionStatusCancelled:
                NSLog(@"Export canceled");
                if(handler){
                    handler(nil);
                }
                break;
            default:
                break;
        }
        
    }];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self dismissViewControllerAnimated:YES completion:nil];
    });
    return YES;
}


#pragma mark UIDocumentMenuViewDelegate

- (NSString*) mimeTypeForFileAtPath: (NSString *) path {
    if (![[NSFileManager defaultManager] fileExistsAtPath:path]) {
        return nil;
    }
    // Borrowed from http://stackoverflow.com/questions/5996797/determine-mime-type-of-nsdata-loaded-from-a-file
    // itself, derived from  http://stackoverflow.com/questions/2439020/wheres-the-iphone-mime-type-database
    CFStringRef UTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, (__bridge CFStringRef)[path pathExtension], NULL);
    CFStringRef mimeType = UTTypeCopyPreferredTagWithClass (UTI, kUTTagClassMIMEType);
    CFRelease(UTI);
    if (!mimeType) {
        return @"application/octet-stream";
    }
    return (__bridge NSString *)mimeType ;
}

- (void)documentPicker:(UIDocumentPickerViewController *)controller didPickDocumentAtURL:(NSURL *)url {
    
    if (url) {
        //NSLog(@"%@", url);
        NSString *exportPath = [[[NSString stringWithFormat:@"%@", url] stringByRemovingPercentEncoding]  stringByReplacingOccurrencesOfString:@"file://" withString:@""];
        
        exportPath = [exportPath stringByReplacingOccurrencesOfString:@" " withString:@"_"];
        NSError *error;
        if (![[NSFileManager defaultManager] copyItemAtURL:url toURL:[NSURL fileURLWithPath:exportPath] error:&error]) {
            NSLog(@"Error %@", error);
        }
        
        
        if(fileHandler){

            NSString *filePath = [exportPath stringByReplacingOccurrencesOfString:@"file://" withString:@""];
            NSArray *fileNames = [filePath componentsSeparatedByString:@"/"];
            NSString *fileName = fileNames.lastObject;
            FileMessage *message = [FileMessage new];
            message.filePath = filePath;
            message.name = fileName;
            fileHandler(message);
        }
        
        
    }
}

- (void)documentMenu:(UIDocumentMenuViewController *)documentMenu didPickDocumentPicker:(UIDocumentPickerViewController *)documentPicker {
    
    documentPicker.delegate = self;
    [self presentViewController:documentPicker animated:YES completion:nil];
    //[documentMenu dismissViewControllerAnimated:YES completion:nil];
}

- (void)documentMenuWasCancelled:(UIDocumentMenuViewController *)documentMenu {
    //[documentMenu dismissViewControllerAnimated:YES completion:nil];
}



# pragma mark add item to chat

- (void) openFilePicker:(ChatFileBlock) handler {
    self->skipLeaveConnection = YES;
    fileHandler = handler;
    NSArray *types = [FileMessage allowDocumentTypes];
    
    UIDocumentMenuViewController *documentMenu = [[UIDocumentMenuViewController alloc] initWithDocumentTypes:types
                                                                                                      inMode:UIDocumentPickerModeImport];
    documentMenu.delegate = self;
    //documentMenu.modalPresentationStyle = UIModalPresentationFormSheet;
    
    //documentMenu.popoverPresentationController.sourceView = self.view;
    //documentMenu.should have a non-nil sourceView or barButtonItem set before the presentation occurs
    [self presentViewController: documentMenu animated:YES completion: nil];
}

- (void) openImagePicker:(ChatImageBlock) handler {
    self->skipLeaveConnection = YES;
    imageHandler = handler;
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    [imagePicker setDelegate:self];
    
    [imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    
    [self presentViewController:imagePicker animated:YES completion:nil];
    
}
- (void) openCamera:(ChatImageBlock) handler {
    self->skipLeaveConnection = YES;
    imageHandler = handler;
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    [imagePicker setDelegate:self];
    
    [imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
    [imagePicker setMediaTypes:@[(NSString*)kUTTypeImage]];
    //[imagePicker setAllowsEditing:YES];
    
    [self presentViewController:imagePicker animated:YES completion:nil];
}
- (void) openVideo:(ChatVideoBlock) handler {
    self->skipLeaveConnection = YES;
    videoHandler = handler;
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    [imagePicker setDelegate:self];
    
    [imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
    [imagePicker setMediaTypes:@[(NSString*)kUTTypeMovie]];
    [imagePicker setVideoMaximumDuration:2 * 60];
    [self presentViewController:imagePicker animated:YES completion:nil];
}

- (void) openVideoPicker:(ChatVideoBlock) handler {
    self->skipLeaveConnection = YES;
    videoHandler = handler;
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    [imagePicker setDelegate:self];
    
    [imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    imagePicker.mediaTypes = @[(NSString *)kUTTypeMovie];
    [self presentViewController:imagePicker animated:YES completion:nil];

    
}

- (void) openSticker:(ChatStickerBlock) handler rewardSticker:(BOOL) rewardSticker {
    self->skipLeaveConnection = YES;
    stickerHandler = handler;
    if(rewardSticker){
        RewardStickerViewController *vc = [[RewardStickerViewController alloc] init];
        vc.adaConfig = self->ada;
        vc.delegate = self;
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
        vc.navigationItem.title = [AppHelper getLabelForThai:@"สติกเกอร์" eng:@"Sticker"];
        UIBarButtonItem *btnClose = [[UIBarButtonItem alloc] initWithTitle:[AppHelper getLabelForThai:@"ยกเลิก" eng:@"Cancel"] style:UIBarButtonItemStyleDone target:vc action:@selector(dismissViewController)];
        vc.navigationItem.leftBarButtonItem = btnClose;
        
        
        [self presentViewController:nav animated:YES completion:nil];
    }else{
        StickerViewController *vc = [[StickerViewController alloc] init];
        vc.stickerSet = self.stickerSet;
        vc.delegate = self;
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
        vc.navigationItem.title = [AppHelper getLabelForThai:@"สติกเกอร์" eng:@"Sticker"];
        UIBarButtonItem *btnClose = [[UIBarButtonItem alloc] initWithTitle:[AppHelper getLabelForThai:@"ยกเลิก" eng:@"Cancel"] style:UIBarButtonItemStyleDone target:vc action:@selector(dismissViewController)];
        vc.navigationItem.leftBarButtonItem = btnClose;
        
        
        [self presentViewController:nav animated:YES completion:nil];
    }
}

- (void)canSendRewardSticker:(SuccessBlock)handler {
    
    if(self.room.roomType != ChatRoomTypePrivate){
        if (handler) {
            handler(NO);
        }
        return;
    }
    
    [ServiceCaller callJSON:[NSString stringWithFormat:@"%@msg-reward-point/get-sticker-by-group/%@",
                             kSERVER, self->ada.login.group]
                     method:@"GET"
                     header:nil
                  parameter:nil
                    handler:^(NSJSONSerialization *json) {
                        if ([[json valueForKey:@"result"] boolValue]) {
                            if (handler) {
                                handler(YES);
                            }
                        }else{
                            if (handler) {
                                handler(NO);
                            }
                        }
                    }];
}

- (void)dontLeaveConnection {
    self->skipLeaveConnection = YES;
}

#pragma mark Sticker View Delegate
- (void) didSelectSticker:(id) sender withCode:(NSString*) code {
    if (stickerHandler) {
        StickerMessage *stickerMessage = [StickerMessage new];
        stickerMessage.stickerID = code;
        stickerHandler(stickerMessage);
    }
}

- (void)didSelectSticker:(NSString *)code reason:(NSString *)reason {
    if (stickerHandler) {
        StickerMessage *stickerMessage = [StickerMessage new];
        stickerMessage.stickerID = code;
        stickerMessage.message = reason;
        stickerMessage.point = @"1";
        stickerHandler(stickerMessage);
    }

}

#pragma mark member 

- (void) loadImage:(ChatMessage*) message withPlaceholder:(UIImage*) placeholder in:(UIImageView*) imageView {
    if (message.messageType != ChatMessageTypeImage) {
        return;
    }
 
    [self->ada loadImage:imageView imageID:message.imageMessage.imageID placeholder:placeholder  cache:YES];
    
    NSArray *gestures = imageView.gestureRecognizers;
    for(UIGestureRecognizer *g in gestures) {
        [imageView removeGestureRecognizer:g];
    }
    imageView.userInteractionEnabled = YES;
    ChatTapGestureRecognizer *tapGesture = [[ChatTapGestureRecognizer alloc] initWithTarget:self action:@selector(showImagePreview:)];
    tapGesture.refObject = message;
    [imageView addGestureRecognizer:tapGesture];
}
- (void) loadVideo:(ChatMessage*) message withPlaceholder:(UIImage*) placeholder  in:(UIImageView*) imageView {
    
    if (message.messageType != ChatMessageTypeVideo) {
        return;
    }
    
    [self loadVideoPreview:imageView withVideoID:message.videoMessage.videoID placeholder:placeholder handler:nil];
    
    NSArray *gestures = imageView.gestureRecognizers;
    for(UIGestureRecognizer *g in gestures) {
        [imageView removeGestureRecognizer:g];
    }
    imageView.userInteractionEnabled = YES;
    ChatTapGestureRecognizer *tapGesture = [[ChatTapGestureRecognizer alloc] initWithTarget:self action:@selector(showVideoPreview:)];
    tapGesture.refObject = message;
    [imageView addGestureRecognizer:tapGesture];
}

- (void)leaveGroup:(SuccessBlock)handler {
    [ServiceCaller callJSON:[NSString
                             stringWithFormat:@"%@msg-groups/response-invite-in-room/%@/%@/%@",
                             kSERVER,
                             self->xmppRoomID,
                             self->ada.login.group,
                             self->ada.login.usid]
                     method:@"POST"
                     header:nil
                  parameter:@{
                              @"action": [NSNumber numberWithInt:0]
                              }
                    handler:^(NSJSONSerialization *json) {
                        if (json && [[json valueForKey:@"result"] boolValue]) {
                            if (handler) {
                                handler(YES);
                            }
                        }else{
                            if (handler) {
                                handler(NO);
                            }
                        }
                    }];
}

- (void)addMember:(NSArray *)members handler:(SuccessBlock)handler {
    if (!members || members.count == 0) {
        handler(NO);
        return;
    }
    NSMutableArray *jsonMembers = [[NSMutableArray alloc] init];
    for (ADAMember *member in members) {
        [jsonMembers addObject:@{@"usid":member.userID, @"user_group" : member.group}];
    }
    NSDictionary *data = @{
                           @"invitee_list":jsonMembers
                           };
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:data
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:nil];
    //NSLog(@"param = %@", [[NSString alloc] initWithData:jsonData
    //                                           encoding:NSUTF8StringEncoding]);
    
    
    [ServiceCaller callJSON:[NSString
                             stringWithFormat:@"%@msg-groups/add-room-member/%@",
                             kSERVER,
                             self->xmppRoomID]
                     method:@"POST"
                     header:nil
                  parameter:@{
                              @"data": [[NSString alloc] initWithData:jsonData
                                                             encoding:NSUTF8StringEncoding]
                              } handler:^(NSJSONSerialization *json) {
                                  if (json && [[json valueForKey:@"result"] boolValue]) {
                                      if (handler) {
                                          handler(YES);
                                      }
                                  }else{
                                      if (handler) {
                                          handler(NO);
                                      }
                                  }
                              }];
}

- (void)setQueue:(dispatch_queue_t)queue {
    chatQueue = queue;
}

- (void) getRoomMembers:(ADAMembersBlock) handler{
    [ServiceCaller callJSON:[NSString
                             stringWithFormat:@"%@msg-groups/room-members/%@",
                             kSERVER,
                             self->xmppRoomID]
                     method:@"GET"
                     header:nil
                  parameter:nil
                    handler:^(NSJSONSerialization *json) {
                        if (json && [[json valueForKey:@"result"] boolValue]) {
                            
                            if(handler) {
                                handler([self doProcessADAMembers:[json valueForKey:@"data"]], nil);
                            }
                            
                        }else{
                            handler(nil, [[NSError alloc] initWithDomain:@"ADA" code:500 userInfo:nil]);
                        }
                        
                    }];
}

- (NSArray<ADAMember*>*) doProcessADAMembers:(NSArray*) arr {
    NSMutableArray<ADAMember*> *list = [[NSMutableArray alloc] init];
    //if(json && [[json valueForKey:@"result"] boolValue]){
    for(NSJSONSerialization *member in arr){
        ADAMember *m = [ADAMember new];
        m.name = [[[AppHelper getLabelForThai: [NSString stringWithFormat:@"%@ %@",
                                                [member valueForKey:@"firstname_th"],
                                                [member valueForKey:@"lastname_th"]]
                                          eng: [NSString stringWithFormat:@"%@ %@",
                                                [member valueForKey:@"firstname_en"],
                                                [member valueForKey:@"lastname_en"]]] stringByReplacingOccurrencesOfString:@"null" withString:@"" ]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        m.userID = [member valueForKey:@"user_id"];
        m.group = [member valueForKey:@"user_group"];
        m.imageURL = [member valueForKey:@"user_image"];
        [member setValue:[NSString stringWithFormat:@"%@:contact-cover", [member valueForKey:@"contact_cover_image"]] forKey:@"contact_cover_image"];
        m.contactInfo = [AppHelper getLabelForThai:[member valueForKey:@"contact_display"] eng:[member valueForKey:@"contact_display_en"]];
        m.userData = member;
        [list  addObject:m];
    }
    return list;
    //}else{
    //    return list;
    //}
}

#pragma mark chat delegate
- (void) chatServerConnectionState:(BOOL) connected {
    
}
- (void) onlineChatUsers:(NSArray*) users {
    
}
- (void) updateMessageState {
    
}
- (void) receivedMessage:(ChatMessage*) message {
    
}


@end
