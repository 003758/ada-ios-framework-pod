//
//  EventRegisterMember.m
//  ADAFramework
//
//  Created by Choldarong-r on 29/3/2561 BE.
//  Copyright © 2561 G-ABLE Company Limited. All rights reserved.
//

#import "EventRegisterMember.h"

@implementation EventRegisterMember


- (void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    [encoder encodeObject:[NSNumber numberWithInteger:self.memberID] forKey:@"memberID"];
    [encoder encodeObject:[NSNumber numberWithInteger:self.reviewTopic.count] forKey:@"topics"];
    long index = 0;
    for(EventReviewTopic *topic in self.reviewTopic) {
        [encoder encodeObject:[NSNumber numberWithInteger:topic.topicID] forKey:[NSString stringWithFormat:@"topic[%ld].id", index]];
        [encoder encodeObject:topic.topic forKey:[NSString stringWithFormat:@"topic[%ld].topic", index]];
        [encoder encodeObject:[NSNumber numberWithInteger:topic.topicType] forKey:[NSString stringWithFormat:@"topic[%ld].type", index]];
        index ++;
    }
    
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        self.memberID = [[decoder decodeObjectForKey:@"memberID"] integerValue];
        long count = [[decoder decodeObjectForKey:@"topics"] integerValue];
        NSMutableArray<EventReviewTopic*> *topics = [[NSMutableArray alloc] init];
        for(long index = 0; index < count; index++) {
            EventReviewTopic *topic = [EventReviewTopic new];
            topic.topicID = [[decoder decodeObjectForKey:[NSString stringWithFormat:@"topic[%ld].id", index]] integerValue];
            topic.topic = [decoder decodeObjectForKey:[NSString stringWithFormat:@"topic[%ld].topic", index]];
            topic.topicType = [[decoder decodeObjectForKey:[NSString stringWithFormat:@"topic[%ld].type", index]] integerValue];
            [topics addObject:topic];
        }
        self.reviewTopic = topics;
    }
    return self;
}

@end
