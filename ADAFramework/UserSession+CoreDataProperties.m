//
//  UserSession+CoreDataProperties.m
//  ADAFramework
//
//  Created by Choldarong-r on 2/12/2560 BE.
//  Copyright © 2560 G-ABLE ITS. All rights reserved.
//
//

#import "UserSession+CoreDataProperties.h"

@implementation UserSession (CoreDataProperties)

+ (NSFetchRequest<UserSession *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"UserSession"];
}

@dynamic key;
@dynamic data;
@dynamic stringData;
@dynamic boolData;
@dynamic intData;

@end
