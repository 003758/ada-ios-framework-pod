//
//  CompanyProfile.h
//  ADAFramework
//
//  Created by Choldarong-r on 25/7/2560 BE.
//  Copyright © 2560 G-ABLE ITS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Media.h"

@interface CompanyProfile : NSObject

@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *detail;
@property (nonatomic, retain) NSArray<Media*> *mediaList;

@end
