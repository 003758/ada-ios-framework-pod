//
//  Feature.m
//  ADAFramework
//
//  Created by Choldarong-r on 3/12/2559 BE.
//  Copyright © 2559 G-ABLE ITS. All rights reserved.
//

#import "Feature.h"

@implementation Feature

/*
 @property (nonatomic, retain) NSString      *name;
 @property (nonatomic, retain) NSString      *icon;
 @property (nonatomic, assign) FeatureType   type;
 @property (nonatomic, retain) NSString      *appLink;
 @property (nonatomic, retain) NSString      *url;
 */


- (void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc

    [encoder encodeObject:self.name forKey:@"name"];
    [encoder encodeObject:self.icon forKey:@"icon"];
    [encoder encodeObject:[NSNumber numberWithInteger:self.type] forKey:@"type"];
    [encoder encodeObject:self.appLink forKey:@"appLink"];
    [encoder encodeObject:self.url forKey:@"url"];

}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        self.type = [[decoder decodeObjectForKey:@"type"] integerValue];
        self.name = [decoder decodeObjectForKey:@"name"];
        self.icon = [decoder decodeObjectForKey:@"icon"];
        self.appLink = [decoder decodeObjectForKey:@"appLink"];
        self.url = [decoder decodeObjectForKey:@"url"];
            }
    return self;
}



+ (FeatureType) typeFromString:(NSString*) type {
    /*
     FeatureTypeContact             = 1,
     FeatureTypeNewsFeed            = 2,
     FeatureTypeChat                = 3,
     FeatureTypeInbox               = 4,
     FeatureTypePersonal            = 5,
     FeatureTypeShuttleBus          = 6,
     FeatureTypeWeatherForecast     = 7,
     FeatureTypeHelpDesk            = 8,
     FeatureTypeShareYourThought    = 9,
     FeatureTypeADACall             = 10,
     FeatureTypeCompanyProfile      = 11,
     FeatureTypeBOMs                = 12,
     FeatureTypeApps                = 13,
     FeatureTypeQuiz                = 14,
     FeatureTypeVote                = 15,
     FeatureTypeTaskAssignment      = 16,
     FeatureTypeCalendar            = 17,
     FeatureTypeWeb                 = 18
     */
    
    if([@"contact" isEqualToString:type]){
        return FeatureTypeContact;
    }
    if([@"news" isEqualToString:type]){
        return FeatureTypeNewsFeed;
    }
    if([@"bus" isEqualToString:type]){
        return FeatureTypeShuttleBus;
    }
    if([@"chat" isEqualToString:type] || [@"message" isEqualToString:type]){
        return FeatureTypeChat;
    }
    if([@"chatInvite" isEqualToString:type]){
        return FeatureTypeChatAction;
    }
    if([@"thought" isEqualToString:type] || [@"mail" isEqualToString:type]){
        return FeatureTypeShareYourThought;
    }
    if([@"personal" isEqualToString:type]){
        return FeatureTypePersonal;
    }
    if([@"company" isEqualToString:type]){
        return FeatureTypeCompanyProfile;
    }
    if([@"weather" isEqualToString:type]){
        return FeatureTypeWeatherForecast;
    }
    if([@"web" isEqualToString:type]){
        return FeatureTypeWeb;
    }
    if([@"boms" isEqualToString:type]){
        return FeatureTypeBOMs;
    }
    if([@"inbox" isEqualToString:type]){
        return FeatureTypeInbox;
    }
    if([@"quiz" isEqualToString:type]){
        return FeatureTypeQuiz;
    }
    if([@"vote" isEqualToString:type]){
        return FeatureTypeVote;
    }
    if([@"task" isEqualToString:type]){
        return FeatureTypeTaskAssignment;
    }
    if([@"helpdesk" isEqualToString:type]){
        return FeatureTypeHelpDesk;
    }
    if([@"app" isEqualToString:type]){
        return FeatureTypeApps;
    }
    if([@"call" isEqualToString:type]){
        return FeatureTypeADACall;
    }
    if([@"push" isEqualToString:type]){
        return FeatureTypeAppPush;
    }
    if([@"eventRegister" isEqualToString:type]){
        return FeatureTypeEventRegister;
    }
    if([@"roomReservation" isEqualToString:type]){
        return FeatureTypeRoomReservation;
    }
    if([@"generalInfo" isEqualToString:type]){
        return FeatureTypeGeneralInfo;
    }
    if([@"setting" isEqualToString:type]){
        return FeatureTypeSetting;
    }
    if([@"qr" isEqualToString:type]){
        return FeatureTypeQRScanner;
    }
    if([@"myEvent" isEqualToString:type]){
        return FeatureTypeMyEvent;
    }
    return FeatureTypeUnknow;
}

@end
