//
//  MessageLog+CoreDataClass.h
//  ADAFramework
//
//  Created by Choldarong-r on 21/12/2559 BE.
//  Copyright © 2559 G-ABLE ITS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class ADAUser;

NS_ASSUME_NONNULL_BEGIN

@interface MessageLog : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "MessageLog+CoreDataProperties.h"
