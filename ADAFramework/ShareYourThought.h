//
//  ShareYourThought.h
//  ADAFramework
//
//  Created by Choldarong-r on 13/7/2560 BE.
//  Copyright © 2560 G-ABLE ITS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ShareYourThoughtCategory.h"

@interface ShareYourThought : NSObject

@property (nonatomic, retain) ShareYourThoughtCategory *category;
@property (nonatomic, retain) NSString *subject;
@property (nonatomic, retain) NSString *message;
@property (nonatomic, retain) NSArray *images;
@property (nonatomic, assign) BOOL read;
@end
