//
//  ContactColumn.m
//  ADAFramework
//
//  Created by Choldarong-r on 6/12/2559 BE.
//  Copyright © 2559 G-ABLE ITS. All rights reserved.
//

#import "ContactColumn.h"

/*
 @property (nonatomic, retain) NSString *labelTH;
 @property (nonatomic, retain) NSString *labelEN;
 @property (nonatomic, retain) NSString *labelEN;
 @property (nonatomic, retain) NSString *labelEN;
 @property (nonatomic, retain) NSString *attributeEN;
 @property (nonatomic, assign) NSInteger sequence;
 @property (nonatomic, assign) BOOL sequence;
 @property (nonatomic, assign) BOOL preContactDisplay;
 @property (nonatomic, assign) BOOL preContactDisplay;
 @property (nonatomic, retain) NSString *templateValue;
 @property (nonatomic, assign) NSInteger sortSequence;
 @property (nonatomic, assign) BOOL sortAccending;
*/

@implementation ContactColumn
- (void)encodeWithCoder:(NSCoder *)coder {
    
    
    
    [coder encodeObject:_labelTH forKey:@"labelTH"];
    [coder encodeObject:_labelEN forKey:@"labelEN"];
    [coder encodeObject:_labelDB forKey:@"labelDB"];
    [coder encodeObject:_attributeTH forKey:@"attributeTH"];
    [coder encodeObject:_attributeEN forKey:@"attributeEN"];
    [coder encodeObject:_templateValue forKey:@"templateValue"];
    [coder encodeObject:[NSNumber numberWithInteger:_sequence] forKey:@"sequence"];
    [coder encodeObject:[NSNumber numberWithBool:_editable] forKey:@"editable"];
    [coder encodeObject:[NSNumber numberWithBool:_preContactDisplay] forKey:@"preContactDisplay"];
    [coder encodeObject:[NSNumber numberWithBool:_sortAccending] forKey:@"sortAccending"];
    
    [coder encodeObject:[NSNumber numberWithInteger:_sortSequence] forKey:@"sortSequence"];
}

- (instancetype)initWithCoder:(NSCoder *)coder {
    if (self = [super init]) {
        self.labelTH = [coder decodeObjectForKey:@"labelTH"];
        self.labelEN = [coder decodeObjectForKey:@"labelEN"];
        self.labelDB = [coder decodeObjectForKey:@"labelDB"];
        self.attributeTH = [coder decodeObjectForKey:@"attributeTH"];
        self.attributeEN = [coder decodeObjectForKey:@"attributeEN"];
        self.templateValue = [coder decodeObjectForKey:@"templateValue"];
        
        self.sequence = [[coder decodeObjectForKey:@"sequence"] integerValue];
        self.editable = [[coder decodeObjectForKey:@"editable"] boolValue];
        self.preContactDisplay = [[coder decodeObjectForKey:@"preContactDisplay"] boolValue];
        self.sortAccending = [[coder decodeObjectForKey:@"sortAccending"] boolValue];
        self.sortSequence = [[coder decodeObjectForKey:@"sortSequence"] integerValue];
        
        
    }
    return self;
}

@end
