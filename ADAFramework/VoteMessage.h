//
//  VoteMessage.h
//  ADAFramework
//
//  Created by Choldarong-r on 2/1/2562 BE.
//  Copyright © 2562 G-ABLE Company Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Vote.h"

NS_ASSUME_NONNULL_BEGIN

@interface VoteMessage : NSObject

@property (nonatomic, assign) long voteID;
@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSDate *endDate;

@end

NS_ASSUME_NONNULL_END
