//
//  RSSFeedChannel.h
//  ADAFramework
//
//  Created by Choldarong-r on 18/12/2559 BE.
//  Copyright © 2559 G-ABLE ITS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RSSFeedChannel : NSObject

@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *channelDescription;
@property (nonatomic, retain) NSString *date;
@property (nonatomic, retain) NSString *url;
@property (nonatomic, retain) NSString *imageUrl;
@end
