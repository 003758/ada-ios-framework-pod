//
//  O365Attendee.h
//  ADAFramework
//
//  Created by Choldarong-r on 14/7/2560 BE.
//  Copyright © 2560 G-ABLE ITS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ContactColumn.h"

@interface O365Attendee : NSObject

@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *email;
@property (nonatomic, assign) BOOL accept;
@property (nonatomic, retain) NSDictionary<NSString*, id> *userData;

@end
