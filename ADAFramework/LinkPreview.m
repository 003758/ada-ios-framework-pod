//
//  LinkPreview.m
//  ADAFramework
//
//  Created by Choldarong-r on 24/4/2561 BE.
//  Copyright © 2561 G-ABLE Company Limited. All rights reserved.
//

#import "LinkPreview.h"

@implementation LinkPreview

- (void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    [encoder encodeObject:self.linkURL forKey:@"linkURL"];
    [encoder encodeObject:self.imageURL forKey:@"imageURL"];
    [encoder encodeObject:self.linkDescription forKey:@"linkDescription"];
    [encoder encodeObject:self.title forKey:@"title"];
   
    
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        self.linkURL = [decoder decodeObjectForKey:@"linkURL"];
        self.imageURL = [decoder decodeObjectForKey:@"imageURL"];
        self.linkDescription = [decoder decodeObjectForKey:@"linkDescription"];
        self.title = [decoder decodeObjectForKey:@"title"];
        
    }
    return self;
}


@end
