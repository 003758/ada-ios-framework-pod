//
//  ADAUser+CoreDataProperties.h
//  ADAFramework
//
//  Created by Choldarong-r on 23/12/2559 BE.
//  Copyright © 2559 G-ABLE ITS. All rights reserved.
//

#import "ADAUser+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface ADAUser (CoreDataProperties)

+ (NSFetchRequest<ADAUser *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *userGroup;
@property (nullable, nonatomic, copy) NSString *usid;
@property (nullable, nonatomic, retain) NSSet<MessageLog *> *messages;
@property (nullable, nonatomic, retain) CacheService *cache;

@end

@interface ADAUser (CoreDataGeneratedAccessors)

- (void)addMessagesObject:(MessageLog *)value;
- (void)removeMessagesObject:(MessageLog *)value;
- (void)addMessages:(NSSet<MessageLog *> *)values;
- (void)removeMessages:(NSSet<MessageLog *> *)values;

@end

NS_ASSUME_NONNULL_END
