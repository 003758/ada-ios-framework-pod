//
//  NewsFeedDetail.h
//  ADAFramework
//
//  Created by Choldarong-r on 6/12/2559 BE.
//  Copyright © 2559 G-ABLE ITS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NewsFeedCategory.h"
#import "Media.h"

@interface NewsFeed : NSObject

@property (nonatomic, retain) NewsFeedCategory *category;
@property (nonatomic, retain) NSString *docno;
@property (nonatomic, retain) NSDate *dateFrom;
@property (nonatomic, retain) NSDate *dateTo;
@property (nonatomic, retain) NSString *image;
@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *source;
@property (nonatomic, retain) NSString *location;
@property (nonatomic, retain) NSString *detail;
@property (nonatomic, retain) NSString *author;
@property (nonatomic, retain) NSString *contactName;
@property (nonatomic, retain) NSString *contactDetail;
@property (nonatomic, retain) NSString *imageURL;
@property (nonatomic, retain) NSString *fileName;
@property (nonatomic, retain) NSString *fileDetail;
@property (nonatomic, retain) NSString *downloadFileURL;
@property (nonatomic, retain) NSString *categoryID;
@property (nonatomic, assign) CategoryType  categoryType;
@property (nonatomic, assign) MediaContentType  mediaContentType;

@end
