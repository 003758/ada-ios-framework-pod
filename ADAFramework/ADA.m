//
//  ADA.m
//  ADAFramework
//
//  Created by Choldarong-r on 2/12/2559 BE.
//  Copyright © 2559 G-ABLE ITS. All rights reserved.
//

#import "ADA.h"
#import "GAHelper.h"
#import "Feature.h"
#import "FeatureGroup.h"
#import "ChatRoom.h"
#import "ADAMember.h"
#import "ChatRoomGroup.h"
#import "ContactColumn.h"
#import "ContactPerson.h"
#import "NewsFeedCategory.h"
#import "NewsFeed.h"
#import "RSSFeed.h"
#import "RSSFeedChannel.h"
#import "FlashNews.h"
#import "O365Calendar.h"
#import "O365Attendee.h"
#import "AppHelper.h"
#import "ServiceCaller.h"
#import "LibSodiumHelper.h"
#import "URLConnection.h"
#import "NSData+Base64.h"
#import "NSData+Encryption.h"
#import "UIImage+Extend.h"
#import "NSString+HTML.h"
#import "NSString+XMLEntities.h"
#import "NSDateFormatter+Extend.h"
#import "CacheService+CoreDataClass.h"
#import "ManagedObjectContextFactory.h"
#import "Vote.h"
#import "VoteChoice.h"
#import "EventRegister.h"
#import "EventReviewTopic.h"
#import "EventRegisterMember.h"
#import "MeetingRoomReserved.h"

@import AFNetworking;
@import NAChloride;
@import UserNotifications;


@interface ADA() 

@end

@implementation ADA {
    SuccessBlock registerDeviceBlock;
    DataResponse tokenBlock;
    NSMutableArray<ADANotificationBlock> *pushBlocks;
    NSString *kNotificationKey;
    NSJSONSerialization *weatherIcon;
    NSArray<Feature*> *allFeatureList;
}

#ifdef DEBUG
#define RUN_DEV YES
#else
#define RUN_DEV NO
#endif

#define kSiginNotofication  @"SiginNotofication"

- (instancetype)init
{
    self = [super init];
    if (self) {
        NAChlorideInit();
    }
    
    NSURLCache *sharedCache = [[NSURLCache alloc] initWithMemoryCapacity:2 * 1024 * 1024
                                                            diskCapacity:100 * 1024 * 1024
                                                                diskPath:nil];
    [NSURLCache setSharedURLCache:sharedCache];
    
    NSString *filePath = [[NSBundle bundleForClass:[ADA class]] pathForResource:@"owm-icons" ofType:@"json"];
    NSString *myJSON = [[NSString alloc] initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:NULL];
    
    weatherIcon = [AppHelper jsonWithData:[myJSON dataUsingEncoding:NSUTF8StringEncoding]];
    
    pushBlocks = [[NSMutableArray alloc] init];
    [self changeLanguage:[ADA getCurrentLanguage] handler:nil];
    return self;
}

- (instancetype)initWithLanguage:(enum ADALanguage) lang {
    self = [self init];
    
    //NSUserDefaults *userDefaults = userDefaults;
    /*NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setValue:(lang == ADALanguageTH ? @"TH" : @"EN") forKey:kLanguage];
    dispatch_async(dispatch_get_main_queue(), ^{
        //[userDefaults synchronize];
        [userDefaults synchronize];
    });*/
    [AppHelper setUserData:(lang == ADALanguageTH ? @"TH" : @"EN") forKey:kLanguage];
    return self;
}

- (void)changeLanguage:(enum ADALanguage) lang handler:(SuccessBlock) handler {
    //NSUserDefaults *userDefaults = userDefaults;
    /*NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setValue:(lang == ADALanguageTH ? @"TH" : @"EN") forKey:kLanguage];
    dispatch_async(dispatch_get_main_queue(), ^{
        //[userDefaults synchronize];
        [userDefaults synchronize];
        if (handler) {
            handler(YES);
        }
    });
    */
    [AppHelper setUserData:(lang == ADALanguageTH ? @"TH" : @"EN") forKey:kLanguage];
    if (handler) {
        handler(YES);
    }
}

+(ADALanguage) getCurrentLanguage {
    return [AppHelper isThaiLanguage] ? ADALanguageTH : ADALanguageEN;
}

+ (void) getConfigurationWithCode:(NSString*) code handler:(JSONResponse) config {
    [ServiceCaller callJSON:[NSString stringWithFormat:@"%@%@", kCFG_SERVER, code]
                     method:@"GET"
                     header:@{@"enc":@"false"}
                  parameter:nil
                    handler:^(NSJSONSerialization *json) {
                        if(config){
                            config(json);
                        }
                    }];
}

+ (void) getClients:(BOOL) bomsClient handler:(ADAClientBlock) handler {
    ////NSLog(@"call : %@", [NSString stringWithFormat:@"%@%@", kCFG_SERVER, code]);
    [ServiceCaller callJSON:[NSString stringWithFormat:@"%@client-all", kSERVER_NAME]
                     method:@"GET"
                     header:@{@"enc":@"false"}
                  parameter:@{@"boms": bomsClient ? @"true" : @"false"}
                    handler:^(NSJSONSerialization *json) {
                        if(handler){
                            
                            if(json && [[json valueForKey:@"result"] boolValue]){
                                NSMutableArray<ADAClient*> *clients = [[NSMutableArray<ADAClient*> alloc] init];
                                for (NSJSONSerialization *client in [json valueForKey:@"data"]) {
                                    ADAClient *c = [ADAClient new];
                                    c.code = [client valueForKey:@"code"];
                                    c.name = [client valueForKey:@"name"];
                                    c.domain = [client valueForKey:@"domain"];
                                    [clients addObject:c];
                                }
                                handler(clients, nil);
                            }else{
                                handler(nil, [[NSError alloc] initWithDomain:@"ADA" code:500 userInfo:nil]);
                            }

                        }
                    }];
}

+ (void) getClientImageLogo:(NSString*) clientCode handler:(ImageBlock) handler {
    ADA *ada = [[ADA alloc] init];
    
    /*
    NSUserDefaults *userDetaults =[NSUserDefaults standardUserDefaults];
    [userDetaults setValue:clientCode forKey:kADA_CLIENT];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [userDetaults synchronize];
    });*/
    [AppHelper setUserData:clientCode forKey:kADA_CLIENT];
    
    [ada loadImage:[[UIImageView alloc] init]
          imageURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@logo/%@", kSERVER_NAME, clientCode]]
       placeholder:nil
    fitToImageView:NO
             cache:YES
           handler:^(UIImageView * _Nullable imageView, UIImage * _Nullable image) {
               if(handler) {
                   handler(image);
               }
           }];
}


#pragma mark Login/Logout
- (BOOL) isLoggedOn {
    @try {
        //NSUserDefaults *userDefaults = userDefaults;
        
        if(!self.login){
            Login *object = [AppHelper getUserDataForKey:@"login"];
            if(object) {
                self.login = object;
                
                kNotificationKey = [NSString stringWithFormat:@"%@-%@@notifications", self.login.usid, self.login.group];
                
            }else{
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                
                if([userDefaults.dictionaryRepresentation.allKeys containsObject:@"login"]) {
                    NSData *encodedObject = [userDefaults valueForKey:@"login"];
                    //NSLog(@"encodedObject %@", encodedObject);
                    if(encodedObject) {
                        Login *object = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
                        if(object) {
                            self.login = object;
                            [AppHelper setUserData:object forKey:@"login"];
                            kNotificationKey = [NSString stringWithFormat:@"%@-%@@notifications", self.login.usid, self.login.group];
                            [AppHelper setUserData:_login.group forKey:kADA_CLIENT];
                            [userDefaults setValue:_login.group forKey:kADA_CLIENT];
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [userDefaults synchronize];
                            });
                        }
                    }
                }else if([userDefaults.dictionaryRepresentation.allKeys containsObject:@"login-json"]) {
                    NSString *jsonString = [userDefaults valueForKey:@"login-json"];
                    NSJSONSerialization* json = [AppHelper jsonWithData:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
                    Login *l = [Login new];
                    NSMutableArray *array = [[NSMutableArray alloc] init];
                    
                    
                    [self setUserLogin:l withJSON:json];
                    
                    if(l) {
                        NSArray<Login*> *users = [self getExistsUsers];
                        if(users){
                            NSArray<Login*> *filteredUser = [users filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"usid = %@ and group = %@", l.usid, l.group]];
                            [array addObjectsFromArray:users];
                            if(filteredUser && filteredUser.count > 0) {
                                l = filteredUser.firstObject;
                            }
                        }
                        [self setUserLogin:l withJSON:json];
                        self.login = l;
                        [AppHelper setUserData:object forKey:@"login"];
                        [AppHelper setUserData:_login.group forKey:kADA_CLIENT];
                        kNotificationKey = [NSString stringWithFormat:@"%@-%@@notifications", self.login.usid, self.login.group];
                        NSData *encodedUsersObject = [NSKeyedArchiver archivedDataWithRootObject:array];
                        [userDefaults setObject:encodedUsersObject forKey: @"users"];
                        [userDefaults setValue:_login.group forKey:kADA_CLIENT];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [userDefaults synchronize];
                        });
                    }
                    
                }
            }
        }
    }
    @catch (NSException *exception) {
        NSLog(@"%@", exception.reason);
    }
    
    return self.login != nil;
}

- (void) reConnect:(SuccessBlock) handler {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        
        
        //NSLog(@"reConnect");
        if ([self isLoggedOn]) {
            //NSUserDefaults *userDefault = userDefaults;
            //__block NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            NSString *clientCode = [AppHelper getUserDataForKey:kADA_CLIENT];//[AppHelper getUserDataForKey:kADA_CLIENT];
            NSLog(@"reconnect clientCode : %@", clientCode);
            if(!clientCode) {
                if (handler) {
                    handler(NO);
                }
                return;
            }
            ////NSLog(@"clientCode %@", clientCode);
            
            
            
            NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[kSERVER_HOST stringByAppendingString:@"sodium-handshake"]]];
            [req setValue:clientCode forHTTPHeaderField:kADA_CLIENT];
            [LibSodiumHelper handshake:req handler:^(NSError *connectionError) {
                
                if (connectionError == nil) {

                    //NSUserDefaults *userDefaults = userDefaults;
                    NSString *usid = [AppHelper getUserDataForKey:kUSER];//[userDefaults valueForKey:kUSER];
                    NSString *passwd = [AppHelper getUserDataForKey:kPASSWORD];//[userDefaults valueForKey:kPASSWORD];
                    //NSString *group = [AppHelper getUserDataWithAttributeName:kGROUP];//[userDefaults valueForKey:kGROUP];
                    NSString *UDID = [UIDevice currentDevice].identifierForVendor.UUIDString;
                    //NSData *encodedObject = [userDefaults valueForKey:@"login"];
                    NSData *passwordData = [[NSData dataWithBase64EncodedString:passwd] AES256DecryptWithKey:UDID];
                    NSString *password = [[NSString alloc] initWithData:passwordData encoding:NSUTF8StringEncoding];
 
                    [self loginWithUser:usid password:password clientCode:clientCode handler:^(Login *login, NSError *error) {
                        if(login != nil){
                            if (handler) {
                                handler(YES);
                            }
                        }else{
                            if (handler) {
                                handler(NO);
                            }
                        }
                    }];
                    
                    
                    
                    
                }else{
                    //NSLog(@"Error %@", connectionError);
                    if (handler) {
                        handler(NO);
                    }
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        //NSLog(@"try again");
                        [self reConnect:handler];
                    });
                }
            }];
        }else{
            if (handler) {
                handler(NO);
            }
        }
    });
}

- (NSArray<Login*>*) getExistsUsers {
    
    NSArray<Login*> *users = [AppHelper getUserDataForKey:@"users"];
    if(users) {
        return users;
    }
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if([userDefaults.dictionaryRepresentation.allKeys containsObject:@"users"]) {
        if([userDefaults valueForKey:@"users"]){
            NSData *encodedObject = [userDefaults valueForKey:@"users"];
            if(encodedObject) {
                NSArray<Login*> *users = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
                return users;
            }
        }
    }
    return nil;
}

- (NSArray<Login*>* _Nullable) removeExistsUsers:(Login* _Nonnull) user {
    //NSUserDefaults *userDefault = userDefaults;
    
    NSArray<Login*> *users = [self getExistsUsers];
    if(users){
        NSMutableArray<Login*> *array = [NSMutableArray arrayWithArray:users];
        NSArray<Login*> *filteredUser = [users filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"usid = %@ and group = %@", user.usid, user.group]];
        
        [array removeObject:filteredUser.firstObject];
        
        //NSData *encodedUsersObject = [NSKeyedArchiver archivedDataWithRootObject:array];
        //[userDefaults setObject:encodedUsersObject forKey: @"users"];
        [AppHelper setUserData:array forKey:@"users"];
        //NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        //dispatch_async(dispatch_get_main_queue(), ^{
        //    [userDefaults synchronize];
            
        //});
        
        return array;
    }
    return nil;
}

- (void)setUserLogin:(Login*) l withJSON:(NSJSONSerialization *)json {
    if([json valueForKey:@"password"]) {
        NSData *passwordData = [[json valueForKey:@"password"] dataUsingEncoding:NSUTF8StringEncoding];
        NSString *UDID = [UIDevice currentDevice].identifierForVendor.UUIDString;
        NSData *encPassword = [passwordData AES256EncryptWithKey: UDID];
        l.userSerializeData = encPassword;
    }
    
    l.userName = [json valueForKey:@"userName"];
    l.userNameTH = [json valueForKey:@"userNameTH"];
    l.userNameEN = [json valueForKey:@"userNameEN"];
    l.unitName = [json valueForKey:@"org"];
    l.mapNearbyURL = [json valueForKey:@"map_nearby_url"];
    NSJSONSerialization *member = [json valueForKey:@"member"];
    if(member){
        l.memberID = [[member valueForKey:@"id"] integerValue];
        l.imageID = [[member valueForKey:@"picture"] stringByAppendingString:@":profilePicture"];
        l.usid = [member valueForKey:@"usid"];
        l.group = [member valueForKey:@"user_group"];
    }
    NSJSONSerialization *theme = [json valueForKey:@"theme"];
    if(theme){
        l.siteID = [[theme valueForKey:@"id"] integerValue];
        l.siteName = [theme valueForKey:@"site_name"];
        l.siteDescription = [theme valueForKey:@"site_detail"];
        l.chatServerHost = [theme valueForKey:@"chat_server"];
        l.chatServerDomain = [theme valueForKey:@"chat_domain"];
        l.chatServerPort = [[theme valueForKey:@"chat_server_port"] integerValue];
        l.bgColor = [UIColor colorWithHexString:[theme valueForKey:@"bg_color"]];
        l.tintColor = [UIColor colorWithHexString:[theme valueForKey:@"tint_color"]];
        if([theme valueForKey:@"logo_image_login"]){
            l.logoImageID = [[theme valueForKey:@"logo_image_login"] stringByAppendingString:@":icon"];
        }
        if([theme valueForKey:@"logo_image_nav"]){
            l.topLogoImageID = [[theme valueForKey:@"logo_image_nav"] stringByAppendingString:@":icon"];
        }
        
        
        
    }
}

- (void) loginWithUser:(NSString*) userid password:(NSString*) password clientCode:(NSString*) clientCode handler:(LoginBlock) handler {
    NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[kSERVER_HOST stringByAppendingString:@"sodium-handshake"]]];
    [req setValue:clientCode forHTTPHeaderField:kADA_CLIENT];
    [LibSodiumHelper handshake:req handler:^(NSError *connectionError) {
        NSLog(@"Client : %@", clientCode);
        if (!connectionError) {
            //__block NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];//userDefaults;
            //[userDefaults setValue:clientCode forKey:kADA_CLIENT];
            [AppHelper setUserData:clientCode forKey:kADA_CLIENT];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [ServiceCaller signinWithUser:userid password:password group:clientCode handler:^(NSJSONSerialization *json) {
                    
                    if (json && [[json valueForKey:@"result"] boolValue]) {
                        Login *l = nil;
                        NSMutableArray *array = [[NSMutableArray alloc] init];
                        NSArray<Login*> *users = [self getExistsUsers];
                        
                        if(users){
                            NSArray<Login*> *filteredUser = [users filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"usid = %@ and group = %@", userid, clientCode]];
                            [array addObjectsFromArray:users];
                            if(filteredUser && filteredUser.count > 0) {
                                l = filteredUser.firstObject;
                            }else{
                                l = [Login new];
                                [array addObject:l];
                            }
                        }else{
                            l = [Login new];
                            [array addObject:l];
                        }
                        [json setValue:password forKey:@"password"];
                        
                        [self setUserLogin:l withJSON:json];
                        
                        //NSData *encodedUsersObject = [NSKeyedArchiver archivedDataWithRootObject:array];
                        //[userDefaults setObject:encodedUsersObject forKey: @"users"];
                        [AppHelper setUserData:array forKey:@"users"];
                        self.login = l;
                        self->kNotificationKey = [NSString stringWithFormat:@"%@-%@@notifications", self.login.usid, self.login.group];
                        
                        [AppHelper setUserData:l forKey:@"login"];
                        //NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:l];
                        //[userDefaults setObject:encodedObject forKey: @"login"];
                        //[userDefaults setValue:[AppHelper stringWithJSON:json] forKeyPath:@"login-json"];
                        //[]
                        
                        
                        /*
                        dispatch_async(dispatch_get_main_queue(), ^{
                            //[userDefault synchronize];
                            [userDefaults synchronize];
                         
                        });
                        */
                        if(handler){
                            handler(l, nil);
                        }
                        
                        [[NSNotificationCenter defaultCenter] postNotificationName:kSiginNotofication object:nil userInfo:@{@"data" : self.login}];
                        
                    }else{
                        /*[userDefaults removeObjectForKey:kADA_CLIENT];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            //[userDefault synchronize];
                            [userDefaults synchronize];
                            if(handler){
                                handler(nil, [[NSError alloc] initWithDomain:@"ADA" code:500 userInfo:nil]);
                            }
                        });
                        
                        */
                        if(handler){
                            handler(nil, [[NSError alloc] initWithDomain:@"ADA" code:500 userInfo:nil]);
                        }
                    }
                }];
                
            });
        }
    }];
}

- (void) logout:(SuccessBlock)handler {
    NSString *UDID = [UIDevice currentDevice].identifierForVendor.UUIDString;
    
    //NSUserDefaults *userDefaults = userDefaults;
    
    //NSString *usid = [userDefaults valueForKey:kUSER];
    //NSString *group = [userDefaults valueForKey:kGROUP];
    NSString *usid = nil;
    NSString *group = nil;
    
    Login *object = [AppHelper getUserDataForKey:@"login"];
    usid = object.usid;
    group = object.group;
    
    
    
    [ServiceCaller callJSON:[NSString stringWithFormat:@"%@msg-member/signout/%@/%@/%@", kSERVER, UDID, usid, group]
                     method:@"POST"
                     header:nil
                  parameter:nil
                    handler:^(NSJSONSerialization *json) {
                        
                        if ([[json valueForKey:@"result"] boolValue]) {
                            self.login = nil;
                            [AppHelper removeUserDataForKey:@"login"];
                            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                            [userDefaults removeObjectForKey:@"login"];
                            [userDefaults removeObjectForKey:@"login-json"];
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [userDefaults synchronize];
                                if (handler) {
                                    handler(YES);
                                }
                            });
                            
                        }else{
                            if (handler) {
                                handler(NO);
                            }
                        }
                        
                    }];
}

- (void) setAppThemeColor {
    if (![self isLoggedOn]) {
        return;
    }
    
    
    UIColor *tintColor = self.login.tintColor;
    UIColor *bgColor = self.login.bgColor;
    
    if (!tintColor || !bgColor) {
        tintColor = [UIColor whiteColor];
        bgColor = [UIColor colorWithHexString:@"#d00037"];//[[UIColor blackColor] colorWithAlphaComponent:0.7];
    }
    
    
    /*
     size_t count = CGColorGetNumberOfComponents(bgColor.CGColor);
     const CGFloat *componentColors = CGColorGetComponents(bgColor.CGColor);
     CGFloat darknessScore = 0;
     if (count == 2) {
     darknessScore = (((componentColors[0]*255) * 299) + ((componentColors[0]*255) * 587) + ((componentColors[0]*255) * 114)) / 1000;
     } else if (count == 4) {
     darknessScore = (((componentColors[0]*255) * 299) + ((componentColors[1]*255) * 587) + ((componentColors[2]*255) * 114)) / 1000;
     }
     
     if (darknessScore >= 125) {
     //dark
     //NSLog(@"Dark color");
     [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
     }else{
     [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
     }
     */
    
    [[UITextField appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]] setTintColor:bgColor];
    
    
    
    if([[UIBarButtonItem appearanceWhenContainedInInstancesOfClasses:@[ [UISearchBar class], [UINavigationBar class]]] titleTextAttributesForState:UIControlStateNormal]){
        NSDictionary *titleText = [[UIBarButtonItem appearanceWhenContainedInInstancesOfClasses:@[ [UISearchBar class], [UINavigationBar class]]] titleTextAttributesForState:UIControlStateNormal];
        
        if(![titleText valueForKey:NSForegroundColorAttributeName]){
            NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:titleText];
            [dict setObject:tintColor forKey:NSForegroundColorAttributeName];
            
            [[UIBarButtonItem appearanceWhenContainedInInstancesOfClasses:@[ [UISearchBar class], [UINavigationBar class]]] setTitleTextAttributes:dict forState:UIControlStateNormal];
        }
        
        
    }else{
        
        [[UIBarButtonItem appearanceWhenContainedInInstancesOfClasses:@[ [UISearchBar class], [UINavigationBar class]]] setTitleTextAttributes:@{NSForegroundColorAttributeName :tintColor}
                                                                                                                                      forState:UIControlStateNormal];
    }
    
    /*
    if([UINavigationBar appearance].titleTextAttributes){
        NSDictionary *titleText = [UINavigationBar appearance].titleTextAttributes;
        NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:titleText];
        [dict setObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
        [[UINavigationBar appearance] setTitleTextAttributes:dict];
        
    }else{
        
        [[UINavigationBar appearance] setTitleTextAttributes:@{
                                                               NSForegroundColorAttributeName: [UIColor whiteColor]
                                                               
                                                               }];
    }
    */
    
    [[UIApplication sharedApplication] delegate].window.tintColor = bgColor;
    
    [UISearchBar appearance].tintColor = tintColor;
    [UISearchBar appearance].barTintColor = bgColor;
    
    //[[UITabBar appearance] setBackgroundImage:[UIImage imageWithColor:bgColor]];
    //[[UITabBar appearance] setTintColor:tintColor];
    
    [[UIStepper appearance] setTintColor:tintColor];
    
    
    
    UIImage *imgBgColor = [[UIImage imageWithColor:bgColor] imageByScalingAndCroppingForSize:(CGSizeMake(150, 35))];
    UIImage *imgBgColor7 = [[UIImage imageWithColor:[bgColor colorWithAlphaComponent:0.7]] imageByScalingAndCroppingForSize:(CGSizeMake(150, 35))];
    UIImage *imgBgColor5 = [[UIImage imageWithColor:[bgColor colorWithAlphaComponent:0.5]] imageByScalingAndCroppingForSize:(CGSizeMake(150, 35))];
    
    
    [[UIStepper appearance] setBackgroundImage:imgBgColor forState:UIControlStateNormal];
    [[UIStepper appearance] setBackgroundImage:imgBgColor7 forState:UIControlStateHighlighted];
    [[UIStepper appearance] setBackgroundImage:imgBgColor7 forState:UIControlStateReserved];
    [[UIStepper appearance] setBackgroundImage:imgBgColor7 forState:UIControlStateSelected];
    [[UIStepper appearance] setBackgroundImage:imgBgColor5 forState:UIControlStateDisabled];
    
    
    //[[UINavigationBar appearance] setBackgroundImage:[UIImage imageWithColor:bgColor] forBarMetrics:UIBarMetricsDefault];
    /*
    if(![UINavigationBar appearance].backgroundColor){
        [[UINavigationBar appearance] setShadowImage:[UIImage new]];
        [[UINavigationBar appearance] setBackgroundColor:bgColor];
        [[UINavigationBar appearance] setBarTintColor:bgColor];
        [[UINavigationBar appearance] setTintColor:tintColor];
        [[UINavigationBar appearance] setBarTintColor:bgColor];
    }
    */
    [[UISegmentedControl appearance] setBackgroundColor:tintColor];
    [[UISegmentedControl appearance] setTintColor:bgColor];
    
}


- (void) getUserSetting:(JSONResponse) handler {
    [ServiceCaller callJSON:[NSString stringWithFormat:@"%@user/get-setting", kSERVER]
                     method:@"GET"
                     header:nil
                  parameter:nil
                    handler:^(NSJSONSerialization *json) {
                        if ([[json valueForKey:@"result"] boolValue]) {
                            
                            if (handler) {
                                handler([json valueForKey:@"data"]);
                            }
                            
                        }else{
                            if (handler) {
                                handler(nil);
                            }
                        }
                    }
     ];
}
- (void) setUserSetting:(NSDictionary*) data :(SuccessBlock) handler {
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:data options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    [ServiceCaller callJSON:[NSString stringWithFormat:@"%@user/set-setting", kSERVER]
                     method:@"POST"
                     header:nil
                  parameter:@{@"json": jsonString}
                    handler:^(NSJSONSerialization *json) {
                        if ([[json valueForKey:@"result"] boolValue]) {
                            
                            if (handler) {
                                handler(YES);
                            }
                            
                        }else{
                            if (handler) {
                                handler(NO);
                            }
                        }
                    }
     ];
}

#pragma mark Notification
- (void) registerPushNotification:(NSDictionary*) launchOptions :(SuccessBlock) handler {
    
    
    if(launchOptions){
        NSDictionary *userInfo = [launchOptions valueForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
        [self saveNotification:[[ADANotification alloc] initWith:userInfo] handler:^(BOOL success) {
            
        }];
    }
    
    if([[UIApplication sharedApplication] respondsToSelector:@selector(isRegisteredForRemoteNotifications)])
    {
        [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:(UNAuthorizationOptionAlert | UNAuthorizationOptionBadge | UNAuthorizationOptionSound) completionHandler:^(BOOL granted, NSError * _Nullable error) {
            if(handler){
                handler(granted);
            }
        }];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    
    
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receivedRemoteMessage:)
                                                 name:kADARecivedRemoteMessage object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receivedRemoteBGMessage:)
                                                 name:kADARecivedRemoteBGMessage object:nil];
    
}

- (void) registerToServer {
    NSString *UDID = [UIDevice currentDevice].identifierForVendor.UUIDString;
    ////NSLog(@"Register token to server");
    [ServiceCaller callJSON:[NSString stringWithFormat:@"%@msg-member/register-device/%@/%@/ios/%@",
                             kSERVER,
                             self.login.group,
                             self.login.usid,
                             UDID
                             ]
                     method:@"POST"
                     header:@{@"showMsg":@"N"}
                  parameter:@{
                              @"device_key": _pushNotificationToken,
                              @"dev_mode": RUN_DEV ? @"1" : @"0"
                              }
                    handler:^(NSJSONSerialization *json) {
                        
                        if(self->registerDeviceBlock){
                            self->registerDeviceBlock(json && [[json valueForKey:@"result"] boolValue]);
                        }
                    }];
}

- (void) registerDeviceToken:(NSData*) deviceToken handler:(SuccessBlock) handler {
    
    
    if(tokenBlock){
        tokenBlock(deviceToken);
    }
    
    NSString *deviceTokenStr = [[[[deviceToken description]
                                  stringByReplacingOccurrencesOfString: @"<" withString: @""]
                                 stringByReplacingOccurrencesOfString: @">" withString: @""]
                                stringByReplacingOccurrencesOfString: @" " withString: @""];
    self.pushNotificationToken = deviceTokenStr;
    ////NSLog(@"%@", deviceTokenStr);
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kSiginNotofication
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(registerToServer)
                                                 name:kSiginNotofication object:nil];
    registerDeviceBlock = handler;
    if(deviceToken && self.login.group && self.login.usid){
        [self registerToServer];
    }
}




- (void) didDeviceTokenReceived:(DataResponse _Nullable) handler {
    tokenBlock = handler;
}


- (void) didNotificationReceived:(ADANotificationBlock) handler {
    if(handler){
        [pushBlocks addObject:handler];
    }
    
    
    
}

/*
 func saveNotification(_ notification: ADANotification) {
 let data = NSKeyedArchiver.archivedData(withRootObject: notification)
 let userDefaults = UserDefaults.standard
 if var notifications =  userDefaults.array(forKey: "notifications") {
 notifications.append(data)
 userDefaults.set(notifications, forKey: "notifications")
 }else{
 var notifications:[Data] = []
 notifications.append(data)
 userDefaults.set(notifications, forKey: "notifications")
 }
 DispatchQueue.main.async(execute: {
 userDefaults.synchronize()
 let cnt = self.getNotifications().count
 if self.mainView != nil {
 let tabbarItems = self.mainView?.tabBarController?.tabBar.items
 tabbarItems?.last?.badgeValue = cnt > 0 ? "\(cnt)" : nil
 }
 
 })
 }
 */

- (NSArray<ADANotification*>*) getNotifications {
    //NSUserDefaults *userDefaults = userDefaults;
    if(!kNotificationKey) {
        return @[];
    }
    //__block NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    //NSArray *dataList = (NSArray*)[userDefaults objectForKey:kNotificationKey];
    NSArray *dataList = (NSArray*)[AppHelper getUserDataForKey:kNotificationKey];
    if(dataList){
        NSMutableArray<ADANotification*> *notifications = [NSMutableArray<ADANotification*> new];
        for(NSData *data in dataList) {
            if(data) {
                ADANotification *notification = [NSKeyedUnarchiver unarchiveObjectWithData:data];
                if(notification.messageType) {
                    [notifications addObject:notification];
                }
            }
        }
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:notifications.count];
        return notifications;
    }
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    return @[];
}

- (void) removeNotifications:(ADANotification*) removeNotification handler:(SuccessBlock) handler {
    //NSUserDefaults *userDefaults = userDefaults;
    if(!kNotificationKey) {
        return;
    }
    
    //__block NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    //NSArray *dataList = (NSArray*)[userDefaults objectForKey:kNotificationKey];
    NSArray *dataList = (NSArray*)[AppHelper getUserDataForKey:kNotificationKey];
    if(dataList){
        NSMutableArray<NSData*> *notifications = [NSMutableArray<NSData*> new];
        for(NSData *data in dataList) {
            ADANotification *notification = [NSKeyedUnarchiver unarchiveObjectWithData:data];
            if(removeNotification.messageID == notification.messageID){
                continue;
            }
            [notifications addObject:data];
        }
        //[userDefaults setObject:notifications forKey:kNotificationKey];
        [AppHelper setUserData:notifications forKey:kNotificationKey];
        
        //dispatch_async(dispatch_get_main_queue(), ^{
            //[userDefaults synchronize];
            //[userDefaults synchronize];
            [self setUserSetting:@{@"aps-cnt": [NSNumber numberWithInteger:notifications.count]} :nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:kADANoficationUpdate object:nil];
            if(handler) {
                handler(YES);
            }
        //});
        
    }else{
        if(handler) {
            handler(NO);
        }
    }
    
}

- (void) saveNotification:(ADANotification*) notification handler:(SuccessBlock) handler {
    if(!kNotificationKey) {
        return;
    }
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:notification];
    //__block NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    //NSUserDefaults *userDefaults = userDefaults;
    //NSArray *notifications = (NSArray*)[userDefaults objectForKey:kNotificationKey];
    NSArray *notifications = (NSArray*)[AppHelper getUserDataForKey:kNotificationKey];
    NSLog(@"notifications %@", notifications);
    if(notifications){
        NSMutableArray<NSData*> *arr = [NSMutableArray arrayWithArray:notifications];
        BOOL canSave = YES;
        if(notification.json) {
            for(NSData *exData in arr) {
                ADANotification *exNoti = [NSKeyedUnarchiver unarchiveObjectWithData:exData];
                if(exNoti.json){
                    if([[ADA stringWithJSON:exNoti.json] isEqualToString:[ADA stringWithJSON:notification.json]]){
                        canSave = NO;
                        break;
                    }
                }
            }
        }
        
        if(canSave) {
            [arr addObject:data];
            //[userDefaults setObject:arr forKey:kNotificationKey];
            [AppHelper setUserData:arr forKey:kNotificationKey];
            [[UIApplication sharedApplication] setApplicationIconBadgeNumber:arr.count];
            //[userDefaults synchronize];
            [self setUserSetting:@{@"aps-cnt": [NSNumber numberWithInteger:arr.count]} :nil];
            //dispatch_async(dispatch_get_main_queue(), ^{
                //[userDefaults synchronize];
                NSLog(@"save update notification");
                //[userDefaults synchronize];
                [[NSNotificationCenter defaultCenter] postNotificationName:kADANoficationUpdate object:nil];
                if(handler) {
                    handler(YES);
                }
            //});
        }else{
            if(handler) {
                handler(NO);
            }
        }
    }else{
        NSMutableArray *arr = [NSMutableArray new];
        [arr addObject:data];
        //[userDefaults setObject:arr forKey:kNotificationKey];
        [AppHelper setUserData:arr forKey:kNotificationKey];
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:arr.count];
        //[userDefaults synchronize];
        [self setUserSetting:@{@"aps-cnt": [NSNumber numberWithInteger:arr.count]} :nil];
        //dispatch_async(dispatch_get_main_queue(), ^{
            //[userDefaults synchronize];
            NSLog(@"save new notification");
            //[userDefaults synchronize];
            if(handler) {
                handler(YES);
            }
        //});
    }
    
}


- (void) receivedRemoteBGMessage:(NSDictionary*) userInfo {
    ADANotification *push = [[ADANotification alloc] initWith:userInfo];
    push.foregroundMessage = NO;
    [self saveNotification: push handler:^(BOOL success) {
    
        if(success) {
            for (ADANotificationBlock block in self->pushBlocks) {
                if(block){
                    block(push, nil);
                }
            }
        }
    }];
    
}


- (void) receivedRemoteMessage:(NSDictionary*) userInfo {
    
    ADANotification *push = [[ADANotification alloc] initWith:userInfo];
    push.foregroundMessage = YES;
    [self saveNotification: push handler:^(BOOL success) {
        if(success) {
            for (ADANotificationBlock block in self->pushBlocks) {
                if(block){
                    block(push, nil);
                }
            }
        }
    }];
    
    
    ////NSLog(@"object %@", object);
    
    ////NSLog(@"userInfo %@", userInfo);
}


NSDictionary * jsonToDictionary(NSJSONSerialization *msg) {
    
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:msg
                                                       options:kNilOptions
                                                         error: nil];
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:jsonData
                                                         options:kNilOptions
                                                           error:nil];
    return json;
}


#pragma mark Local Notification
- (void) removeLocalNotification:(ADANotification*) notification handler:(SuccessBlock) handler {
    
    //if (NSClassFromString(@"UNUserNotificationCenter")) {
    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_9_x_Max) {
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        
        //remove old notification
        [center getPendingNotificationRequestsWithCompletionHandler:^(NSArray<UNNotificationRequest *> * _Nonnull requests) {
            
            BOOL found = NO;
            for(UNNotificationRequest *req in requests) {
                NSDictionary *userInfo = req.content.userInfo;
                
                if([notification.messageType isEqualToString:[userInfo valueForKey:@"messageType"]]){
                    if(notification.json && [jsonToDictionary(notification.json) isEqualToDictionary:jsonToDictionary([userInfo valueForKey:@"json"])]) {
                        [center removePendingNotificationRequestsWithIdentifiers:@[req.identifier]];
                        found = YES;
                        
                    }else if(notification.userInfo && [notification.userInfo isEqualToDictionary:userInfo]) {
                        [center removePendingNotificationRequestsWithIdentifiers:@[req.identifier]];
                        found = YES;
                        
                    }
                }
            }
            
            if (handler) {
                handler(found);
            }
        }];
        
    }else{
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
        NSArray<UILocalNotification *> *notifications = [UIApplication sharedApplication].scheduledLocalNotifications;
        BOOL found = NO;
        for(UILocalNotification *noti in notifications) {
            NSDictionary *userInfo = noti.userInfo;
            if([notification.messageType isEqualToString:[userInfo valueForKey:@"messageType"]] ){
                if(notification.json && [jsonToDictionary(notification.json) isEqualToDictionary:jsonToDictionary([userInfo valueForKey:@"json"])]) {
                    
                    [[UIApplication sharedApplication] cancelLocalNotification:noti];
                    found = true;
                    
                }else if(notification.json &&
                         [notification.userInfo isEqualToDictionary:userInfo]){
                    [[UIApplication sharedApplication] cancelLocalNotification:noti];
                    found = true;
                    
                }
            }
        }
#pragma clang diagnostic pop
        if (handler) {
            handler(found);
        }
    }
    
}

- (void) getLocalNotification:(NSString*) messageType handler:(ADANotificationsBlock) handler {
    NSMutableArray<ADANotification*> *results = [NSMutableArray<ADANotification*> new];
    //if (NSClassFromString(@"UNUserNotificationCenter")) {
    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_9_x_Max) {
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        
        //remove old notification
        [center getPendingNotificationRequestsWithCompletionHandler:^(NSArray<UNNotificationRequest *> * _Nonnull requests) {
            
            
            for(UNNotificationRequest *req in requests) {
                NSDictionary *userInfo = req.content.userInfo;
                if(userInfo) {
                    if([messageType isEqualToString:[userInfo valueForKey:@"messageType"]]) {
                        ADANotification *n = [[ADANotification alloc] initWith:@{@"userInfo" : userInfo}];
                        n.messageID = [req.identifier intValue];
                        n.message = req.content.body;
                        UNCalendarNotificationTrigger *trigger = (UNCalendarNotificationTrigger*)req.trigger;
                        n.date = [trigger nextTriggerDate];
                        [results addObject:n];
                    }
                }
            }
            
            if (handler) {
                handler(results, nil);
            }
        }];
        
    }else{
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
        NSArray<UILocalNotification *> *notifications = [UIApplication sharedApplication].scheduledLocalNotifications;
        
        for(UILocalNotification *noti in notifications) {
            NSDictionary *userInfo = noti.userInfo;
            if([messageType isEqualToString:[userInfo valueForKey:@"messageType"]]) {
                
                [results addObject:[[ADANotification alloc] initWith:@{@"userInfo" : userInfo}]];
            }
        }
#pragma clang diagnostic pop
        if (handler) {
            handler(results, nil);
        }
    }
}

- (void) addLocalNotification:(ADANotification*) notification handler:(SuccessBlock) handler  {
    
    
    // do stuff for iOS 9 and newer
    

    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_9_x_Max) {
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        
        UNMutableNotificationContent *content = [[UNMutableNotificationContent alloc] init];
        //content.title = notification.message;
        content.body = notification.message;
        content.categoryIdentifier = @"alarm";
        if(notification.userInfo){
            NSMutableDictionary *userInfo = [NSMutableDictionary dictionaryWithDictionary:notification.userInfo];
            
            [userInfo setValue:notification.messageType forKey:@"messageType"];
            
            content.userInfo = userInfo;
        }else{
            content.userInfo = @{@"messageType": notification.messageType};
        }
        notification.userInfo = content.userInfo;
        content.sound = [UNNotificationSound  soundNamed:notification.sound];
        
        
        NSCalendar *calendar = [NSCalendar currentCalendar];
        unsigned unitFlags =  NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay |NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
        
        NSDateComponents *component = [calendar components:unitFlags fromDate:notification.date];
        UNCalendarNotificationTrigger *trigger = [UNCalendarNotificationTrigger triggerWithDateMatchingComponents:component repeats:NO];
        
        UNNotificationRequest *req =  [UNNotificationRequest requestWithIdentifier:[NSString stringWithFormat:@"%ld", notification.messageID]  content:content trigger:trigger];
        NSLog(@"reqID %ld", notification.messageID);
        [center addNotificationRequest:req withCompletionHandler:^(NSError * _Nullable error) {
            if(error) {
                NSLog(@"Error : %@", error);
            }
            if(handler) {
                handler(error == nil);
            }
        }];
    }else{
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
        UILocalNotification *req = [[UILocalNotification alloc] init];
        req.fireDate = notification.date;
        //req.alertTitle = notification.title;
        req.alertBody = notification.message;
        if(notification.userInfo){
            NSMutableDictionary *userInfo = [NSMutableDictionary dictionaryWithDictionary:notification.userInfo];
            [userInfo setValue:notification.messageType forKey:@"messageType"];
            req.userInfo = userInfo;
        }else{
            req.userInfo = @{@"messageType": notification.messageType};
        }
        notification.userInfo = req.userInfo;
        req.soundName = notification.sound;
        
        [[UIApplication sharedApplication] scheduleLocalNotification:req];
        if(handler) {
            handler(YES);
        }
#pragma clang diagnostic pop
        
    }
        

    
    
    
    
    //if (NSClassFromString(@"UNUserNotificationCenter")) {
    
}


#pragma mark Utils



- (void) trackScreenName:(NSString* _Nonnull) screenName trackerID:(NSString* _Nonnull) trackID {
    [GAHelper trackID:trackID screen:screenName framework:self];
}


+ (NSDateFormatter*) getDateFormatter {
    return [ADA isThaiLanguage] ? [NSDateFormatter instanceWithTHLocale] : [NSDateFormatter instanceWithUSLocale];
}

+ (NSString *)mimeTypeForData:(NSData *)data {
    uint8_t c;
    [data getBytes:&c length:1];
    
    switch (c) {
        case 0xFF:
            return @"image/jpg";
            break;
        case 0x89:
            return @"image/png";
            break;
        case 0x47:
            return @"image/gif";
            break;
        case 0x49:
        case 0x4D:
            return @"image/tiff";
            break;
        case 0x25:
            return @"application/pdf";
            break;
        case 0xD0:
            return @"application/vnd";
            break;
        case 0x46:
            return @"text/plain";
            break;
        default:
            return @"application/octet-stream";
    }
    return nil;
}

- (NSString*) mimeTypeForFileAtPath: (NSString *) path {
    if (![[NSFileManager defaultManager] fileExistsAtPath:path]) {
        return nil;
    }
    // Borrowed from http://stackoverflow.com/questions/5996797/determine-mime-type-of-nsdata-loaded-from-a-file
    // itself, derived from  http://stackoverflow.com/questions/2439020/wheres-the-iphone-mime-type-database
    CFStringRef UTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, (__bridge CFStringRef)[path pathExtension], NULL);
    CFStringRef mimeType = UTTypeCopyPreferredTagWithClass (UTI, kUTTagClassMIMEType);
    CFRelease(UTI);
    if (!mimeType) {
        return @"application/octet-stream";
    }
    return (__bridge NSString *)mimeType ;
}


- (void) loadImage:(UIImageView*) imageView imageID:(NSString*) imageID placeholder:(UIImage*) placeholder cache:(BOOL) doCache {
    [self loadImage:imageView imageID:imageID placeholder:placeholder type:nil cache:doCache];
}

- (void) loadImage:(UIImageView*) imageView imageID:(NSString*) imageID placeholder:(UIImage*) placeholder fitToImageView:(BOOL) fitToImageView cache:(BOOL) doCache {
    [self loadImage:imageView imageID:imageID placeholder:placeholder type:nil fitToImageView:fitToImageView cache:doCache handler:nil];
}

- (void) loadImage:(UIImageView*) imageView imageID:(NSString*) imageID placeholder:(UIImage*) placeholder type:(NSString*) type cache:(BOOL) doCache {
    [self loadImage:imageView imageID:imageID placeholder:placeholder type:nil cache:doCache handler:nil];
}

- (void) loadImage:(UIImageView*) imageView imageID:(NSString*) imageID placeholder:(UIImage*) placeholder  cache:(BOOL) doCache  handler:(LoadedImageViewBlock) handler{
    [self loadImage:imageView imageID:imageID placeholder:placeholder type:nil cache:doCache handler:handler];
}


- (void) loadImage:(UIImageView*) imageView imageID:(NSString*) imageID placeholder:(UIImage*) placeholder type:(NSString*) type cache:(BOOL) doCache handler:(LoadedImageViewBlock) handler{
    [self loadImage:imageView imageID:imageID placeholder:placeholder type:type fitToImageView:YES cache:doCache handler:handler];
}

- (NSString*) stringURL:(NSString*) imageID {
    return [self stringURL: imageID type:nil];
}

- (NSString*) stringURL:(NSString*) imageID type:(NSString*) type {
    if(!imageID) {
        return nil;
    }
    
    if (!type && [imageID rangeOfString:@":"].location != NSNotFound) {
        NSArray *IDs = [imageID componentsSeparatedByString:@":"];
        imageID = IDs.firstObject;
        type = IDs.lastObject;
    }
    return [NSString stringWithFormat:@"%@file?id=%@&mediaType=%@&_c=%@&client-code=%@", kSERVER_HOST, imageID, type, self.login.group, self.login.group];
}

- (NSURLRequest* _Nonnull) requestFromStringURLSafe:(NSString* _Nonnull) url {
    
    
//    NSUserDefaults *session = [NSUserDefaults standardUserDefaults];
    
    //NSData *encodedObject = [AppHelper getUserDataForKey:@"login"];//[session valueForKey:@"login"];
    NSString *group = [AppHelper getUserDataForKey:kADA_CLIENT];//[session valueForKey:kADA_CLIENT];
    NSString *lang = [AppHelper getUserDataForKey:kLanguage];//[session valueForKey:kLanguage];
    if(!self.login) {
        Login *object = [AppHelper getUserDataForKey:@"login"];
        group = object.group;
        
    }
    
    NSURLComponents *comp = [NSURLComponents componentsWithString:[NSString stringWithFormat:@"%@proxy-call", kSERVER_HOST]];
    comp.queryItems = @[[NSURLQueryItem queryItemWithName:@"_c" value:group],[NSURLQueryItem queryItemWithName:@"url" value:url]];
    
    NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:comp.URL];
    
    [req setValue:[UIDevice currentDevice].identifierForVendor.UUIDString forHTTPHeaderField:kSECURE_DEVICE];
    [req setValue:@"iOS" forHTTPHeaderField:@"device_os"];
    [req setValue:@"iOS" forHTTPHeaderField:@"device-os"];
    [req setValue:group forHTTPHeaderField:kADA_CLIENT];
    [req setValue:lang forHTTPHeaderField:kLanguage];
    [req setValue:[[NSBundle bundleForClass:[ADA class]].infoDictionary valueForKey:@"CFBundleShortVersionString"]  forHTTPHeaderField:@"sdk-version"];
    NSString *version = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    [req setValue:version forHTTPHeaderField:@"app-version"];
    return req;
}



- (void) loadImage:(UIImageView*) iv imageID:(NSString*) ID placeholder:(UIImage*) p type:(NSString*) t fitToImageView:(BOOL) fitToImageView cache:(BOOL) doCache handler:(LoadedImageViewBlock) handler{
    
    __block NSString *imageID = ID;
    __block UIImageView *imageView = iv;
    __block UIImage *placeholder = p;
    __block NSString *type = t;
    dispatch_async(dispatch_get_main_queue(), ^{
    
        if(!imageID) {
            if(handler) {
                handler(imageView, placeholder);
            }
            return;
        }
        AFImageResponseSerializer* serializer = (AFImageResponseSerializer*)[UIImageView sharedImageDownloader].sessionManager.responseSerializer;
        serializer.acceptableContentTypes = nil;
        
        
        //NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@file", kSERVER_HOST]];
        
        CGSize imageSize = CGSizeMake(0, 0);
        if ([imageID rangeOfString:@"#"].location != NSNotFound) {
            NSArray *IDs = [imageID componentsSeparatedByString:@"#"];
            imageID = IDs.firstObject;
            NSArray *size = [IDs.lastObject componentsSeparatedByString:@","];
            imageSize = CGSizeMake([size.firstObject floatValue], [size.lastObject floatValue]);
        }
        
        
        NSInteger orientation = -1;
        if ([imageID rangeOfString:@"@"].location != NSNotFound) {
            NSArray *IDs = [imageID componentsSeparatedByString:@"@"];
            imageID = IDs.firstObject;
            orientation = [IDs.lastObject integerValue];
        }
        
        if (!type && [imageID rangeOfString:@":"].location != NSNotFound) {
            NSArray *IDs = [imageID componentsSeparatedByString:@":"];
            imageID = IDs.firstObject;
            type = IDs.lastObject;
        }
        
        
        
        NSFileManager *fileMgr = [NSFileManager defaultManager];
        NSURL *tmpDirURL = [NSURL fileURLWithPath:NSTemporaryDirectory() isDirectory:YES];
        
        NSString *imgFolder = [NSString stringWithFormat:@"%@image-cache/%@", tmpDirURL, type ? type : @"undefined"];
        imgFolder = [imgFolder stringByReplacingOccurrencesOfString:@"file://" withString:@""];
        if (![fileMgr fileExistsAtPath:imgFolder]) {
            if([fileMgr createDirectoryAtPath:imgFolder withIntermediateDirectories:YES attributes:nil error:nil]){
            }
        }
        NSString *strPath = [NSString stringWithFormat:@"%@/%@.jpg", imgFolder, imageID];
        
        if([fileMgr fileExistsAtPath:strPath]) {
            NSData *imageData = [fileMgr contentsAtPath:strPath];
            UIImage *image = [UIImage imageWithData:imageData];
            if (handler) {
                handler(iv, image);
            }
        }
        
        
        NSMutableURLRequest *req = [URLConnection requestFromURL:[NSString stringWithFormat:@"%@file", kSERVER_HOST]
                                                          method:@"GET"
                                                       parameter:
                                    type?@{@"id": imageID, @"mediaType" : type}
                                                                :@{@"id": imageID}
                                    ];
        
        
        NSString *lang = [AppHelper getUserDataForKey:kLanguage];//[AppHelper getUserDataWithAttributeName:kLanguage];
        [req setCachePolicy:doCache ? NSURLRequestReturnCacheDataElseLoad : NSURLRequestReloadIgnoringLocalAndRemoteCacheData];
        [req setTimeoutInterval:20];
        [req setValue:[AppHelper getUserDataForKey:kADA_CLIENT] forHTTPHeaderField:kADA_CLIENT];
        [req setValue:lang forHTTPHeaderField:kLanguage];
        [req setValue:[UIDevice currentDevice].identifierForVendor.UUIDString forHTTPHeaderField:kSECURE_DEVICE];
        [req setValue:@"iOS" forHTTPHeaderField:@"device_os"];
        [req setValue:@"iOS" forHTTPHeaderField:@"device-os"];
        [req setValue:[[NSBundle bundleForClass:[ADA class]].infoDictionary valueForKey:@"CFBundleShortVersionString"]  forHTTPHeaderField:@"sdk-version"];
        NSString *version = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
        [req setValue:version forHTTPHeaderField:@"app-version"];
        [ServiceCaller setUserAgent:req];
        
        
        [imageView setImageWithURLRequest:req
                         placeholderImage:placeholder
                                  success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull img) {
                                      __block UIImage *image = img;
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          if(imageSize.width != 0 && imageSize.height != 0){
                                              image = [image scaleToSize:imageSize];
                                          }
                                          
                                          CGSize imageSize = image.size;
                                          if(orientation != -1){
                                              image = [UIImage imageWithCGImage:image.CGImage scale:1.0 orientation:orientation];
                                          }else{
                                              
                                              if(fitToImageView){
                                                  CGSize imageViewSize = iv.frame.size;
                                                  CGFloat scale  = [UIScreen mainScreen].scale;
                                                  
                                                  if(imageSize.width <= imageSize.height){
                                                      CGFloat ratio = imageViewSize.width / imageViewSize.height;
                                                      image = [UIImage imageByCroppingImage:image
                                                                                     toSize:CGSizeMake(imageSize.width * scale, imageSize.width * ratio * scale)];
                                                  }else{
                                                      //CGFloat ratio = imageViewSize.height / imageViewSize.width;
                                                      
                                                      
                                                      
                                                      if(imageSize.width  * scale > imageViewSize.width) {
                                                          image = [image scaleToSize: CGSizeMake(imageViewSize.width  * scale, imageViewSize.height * scale)];
                                                      }
                                                      
                                                      if(imageSize.height < imageViewSize.height) {
                                                          
                                                          UIImage *trailImage = [UIImage imageWithColor:[UIColor clearColor]];
                                                          trailImage = [trailImage scaleToSize: CGSizeMake(imageViewSize.width, imageViewSize.height - imageSize.height)];
                                                          image = [UIImage mergeImage:image withImage:trailImage];
                                                      }
                                                      
                                                      
                                                      image = [image imageByScalingAndCroppingForSize:CGSizeMake(imageViewSize.width * scale, imageViewSize.height * scale)];
                                                      
                                                  }
                                              }
                                              
                                          }
                                          iv.image = image;
                                          
                                          
                                          NSData *imgData = nil;
                                          if([@"logo" isEqualToString:type]){
                                              if([image.contentType rangeOfString:@"/png"].location != NSNotFound){
                                                  imgData = UIImagePNGRepresentation(image);
                                              }else{
                                                  imgData = UIImageJPEGRepresentation(image, 1);
                                              }
                                          }else{
                                              imgData = UIImageJPEGRepresentation(image, 1);
                                          }
                                          [imgData writeToFile:strPath atomically:YES];
                                          
                                          
                                          if (handler) {
                                              handler(iv, image);
                                          }
                                      });
                                  } failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          if(error) {
                                              NSLog(@"Load image error %@", error);
                                          }
                                          if(response) {
                                              NSLog(@"Response %@", response);
                                          }
                                          if (handler) {
                                              handler(iv, nil);
                                          }
                                      });
                                  }];
    });
                                                 
}

- (void) loadImage:(UIImageView*) imageView imageURL:(NSURL*) imageURL placeholder:(UIImage*) placeholder cache:(BOOL) doCache {
    [self loadImage:imageView imageURL:imageURL placeholder:placeholder cache:doCache handler:nil];
}

- (void) loadImage:(UIImageView*) imageView imageURL:(NSURL*) imageURL placeholder:(UIImage*) placeholder cache:(BOOL) doCache handler:(LoadedImageViewBlock) handler {
    [self loadImage:imageView imageURL:imageURL placeholder:placeholder fitToImageView:YES cache:doCache handler:handler];
}

- (void) loadImage:(UIImageView*) imageView imageURL:(NSURL*) imageURL placeholder:(UIImage*) placeholder fitToImageView:(BOOL) fitToImageView cache:(BOOL) doCache handler:(LoadedImageViewBlock) handler{
    
    
    //NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSString *clientCode = [AppHelper getUserDataForKey:kADA_CLIENT];
    AFImageResponseSerializer* serializer = (AFImageResponseSerializer*)[UIImageView sharedImageDownloader].sessionManager.responseSerializer;

    serializer.acceptableContentTypes = nil;
    if(imageView && imageURL) {
        NSMutableURLRequest *req =  nil;
        if([imageURL.absoluteString rangeOfString: kSERVER_NAME].location != NSNotFound){
            req = [URLConnection requestFromURL:[imageURL.absoluteString stringByRemovingPercentEncoding]
                                         method:@"GET"
                                      parameter:@{@"client-code": clientCode}
                   ];
        }else{
            req = [URLConnection requestFromURL:[NSString stringWithFormat:@"%@proxy-call", kSERVER_HOST]
                                         method:@"GET"
                                      parameter:@{@"url": [imageURL.absoluteString stringByRemovingPercentEncoding]
                                                  }
                   ];
        }
        
        [req setCachePolicy:doCache ? NSURLRequestReturnCacheDataElseLoad : NSURLRequestReloadIgnoringLocalAndRemoteCacheData];
        [req setTimeoutInterval:20];
        [req setValue:[AppHelper getUserDataForKey:kADA_CLIENT] forHTTPHeaderField:kADA_CLIENT];
        [req setValue:[UIDevice currentDevice].identifierForVendor.UUIDString forHTTPHeaderField:kSECURE_DEVICE];

        __block UIImageView *targetImageView = imageView;
        
        
        [UIImageView sharedImageDownloader].downloadPrioritizaton = AFImageDownloadPrioritizationLIFO;
        //[albumImageRequest start];
        
        
        [imageView setImageWithURLRequest:req placeholderImage:placeholder success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
            CGSize imageSize = image.size;
            CGSize imageViewSize = targetImageView.frame.size;
            CGFloat ratio = imageViewSize.width / imageViewSize.height;
            CGFloat scale  = [UIScreen mainScreen].scale;
            
            /*
             if(imageSize.width > imageViewSize.width) {
             image = [image scaleToSize:CGSizeMake(imageViewSize.width * scale,
             (imageViewSize.width * imageSize.height/imageSize.width)  * scale)];
             ratio = 1;
             }
             */
            if(fitToImageView){
                if(imageSize.width <= imageSize.height){
                    image = [UIImage imageByCroppingImage:image
                                                   toSize:CGSizeMake(imageSize.width * scale, imageSize.width * ratio * scale)];
                }else{
                    ratio = imageViewSize.height / imageViewSize.width;
                    image = [UIImage imageByCroppingImage:image
                                                   toSize:CGSizeMake(imageSize.height * ratio * scale, imageSize.height * scale)];
                }
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                targetImageView.image = image;
                if (handler) {
                    handler(targetImageView, image);
                }
            });
            
        } failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
            if(error) {
                NSLog(@"Load image error %@", error);
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if (handler) {
                    handler(targetImageView, nil);
                }
            });
        }];
        return;
    }
  
}

- (void) saveFile: (NSData* _Nonnull) data withFileName:(NSString* _Nonnull) fileName handler:(SuccessBlock _Nullable) handler {
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    NSURL *tmpDirURL = [NSURL fileURLWithPath:NSTemporaryDirectory() isDirectory:YES];
    
    NSString *imgFolder = [NSString stringWithFormat:@"%@/cache", tmpDirURL];
    imgFolder = [imgFolder stringByReplacingOccurrencesOfString:@"file://" withString:@""];
    if (![fileMgr fileExistsAtPath:imgFolder]) {
        if([fileMgr createDirectoryAtPath:imgFolder withIntermediateDirectories:YES attributes:nil error:nil]){
        }
    }
    NSString *strPath = [NSString stringWithFormat:@"%@/%@", imgFolder, fileName];
    if([data writeToFile:strPath atomically:YES]){
        if (handler) {
            handler(YES);
        }
    }else{
        if (handler) {
            handler(NO);
        }
    }
}
- (void) getFileWithName:(NSString* _Nonnull) fileName handler:(DataResponse _Nullable) handler {
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    NSURL *tmpDirURL = [NSURL fileURLWithPath:NSTemporaryDirectory() isDirectory:YES];
    
    NSString *imgFolder = [NSString stringWithFormat:@"%@/cache", tmpDirURL];
    imgFolder = [imgFolder stringByReplacingOccurrencesOfString:@"file://" withString:@""];
    if (![fileMgr fileExistsAtPath:imgFolder]) {
        if([fileMgr createDirectoryAtPath:imgFolder withIntermediateDirectories:YES attributes:nil error:nil]){
            ////NSLog(@"Dir created");
        }
    }
    ////NSLog(@"File Name : %@", imageID);
    NSString *strPath = [NSString stringWithFormat:@"%@/%@", imgFolder, fileName];
    if ([fileMgr fileExistsAtPath:strPath]){
        NSData *data = [fileMgr contentsAtPath:strPath];
        if (handler) {
            handler(data);
        }
    }else{
        if (handler) {
            handler(nil);
        }
    }
}

- (void) getImage:(NSString*) imageID cache:(BOOL) doCache handler:(ImageBlock) handler {
    [self getImage:imageID type:nil cache:doCache handler:handler];
}
- (void) getImage:(NSString*) imageID type:(NSString*) type cache:(BOOL) doCache handler:(ImageBlock) handler {
    
    
    if (!type && [imageID rangeOfString:@":"].location != NSNotFound) {
        NSArray *IDs = [imageID componentsSeparatedByString:@":"];
        imageID = IDs.firstObject;
        type = IDs.lastObject;
    }
    
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    NSURL *tmpDirURL = [NSURL fileURLWithPath:NSTemporaryDirectory() isDirectory:YES];
    
    NSString *imgFolder = [NSString stringWithFormat:@"%@/image-cache/%@", tmpDirURL, type ? type : @"undefined"];
    imgFolder = [imgFolder stringByReplacingOccurrencesOfString:@"file://" withString:@""];
    if (![fileMgr fileExistsAtPath:imgFolder]) {
        if([fileMgr createDirectoryAtPath:imgFolder withIntermediateDirectories:YES attributes:nil error:nil]){
            ////NSLog(@"Dir created");
        }
    }
    ////NSLog(@"File Name : %@", imageID);
    NSString *strPath = [NSString stringWithFormat:@"%@/%@", imgFolder, imageID];
    
    if (doCache && [fileMgr fileExistsAtPath:strPath]){
        NSData *imgData = [fileMgr contentsAtPath:strPath];
        if(handler){
            handler([UIImage imageWithData:imgData]);
        }
        return;
    }else{
        [ServiceCaller callData:[NSString stringWithFormat:@"%@file", kSERVER_HOST]
                         method:@"GET"
                         header:nil
                      parameter:
         type?@{@"id": imageID, @"mediaType" : type}
                               :@{@"id": imageID}
                        handler:^(NSData *data) {
                            if(handler){
                                if(data && data.length > 0){
                                    
                                    handler([UIImage imageWithData:data]);
                                    if (doCache) {
                                        [data writeToFile:strPath atomically:YES];
                                    }
                                }else{
                                    handler(nil);
                                }
                            }
                            
                        }
         ];
    }
}


- (void) getImageURL:(NSURL*) imageURL cache:(BOOL) doCache handler:(ImageBlock) handler {
    
    
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    NSURL *tmpDirURL = [NSURL fileURLWithPath:NSTemporaryDirectory() isDirectory:YES];
    
    NSString *imgFolder = [NSString stringWithFormat:@"%@/image-cache", tmpDirURL];
    imgFolder = [imgFolder stringByReplacingOccurrencesOfString:@"file://" withString:@""];
    if (![fileMgr fileExistsAtPath:imgFolder]) {
        if([fileMgr createDirectoryAtPath:imgFolder withIntermediateDirectories:YES attributes:nil error:nil]){
            ////NSLog(@"Dir created");
        }
    }
    ////NSLog(@"File Name : %@", imageID);
    NSArray *fileNames = [imageURL.absoluteString componentsSeparatedByString:@"/"];
    NSString *fileName = fileNames.lastObject;
    ////NSLog(@"File Name : %@", imageID);
    NSString *strPath = [NSString stringWithFormat:@"%@/%@", imgFolder, fileName];
    
    if (doCache && [fileMgr fileExistsAtPath:strPath]){
        NSData *imgData = [fileMgr contentsAtPath:strPath];
        if(handler){
            handler([UIImage imageWithData:imgData]);
        }
        return;
    }else{
        if([imageURL.absoluteString rangeOfString: kSERVER_NAME].location != NSNotFound){
            //NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [ServiceCaller callData:imageURL.absoluteString
                             method:@"GET"
                             header:nil
                          parameter:@{@"client-code": [AppHelper getUserDataForKey:kADA_CLIENT]}
                            handler:^(NSData *data) {
                                if(handler){
                                    if(data && data.length > 0){
                                        
                                        handler([UIImage imageWithData:data]);
                                        if (doCache) {
                                            [data writeToFile:strPath atomically:YES];
                                        }
                                    }else{
                                        handler(nil);
                                    }
                                }
                                
                            }
             ];
            
        }else{
            
            
            [ServiceCaller callData:[NSString stringWithFormat:@"%@news-feed/image-proxy", kSERVER]
                             method:@"GET"
                             header:nil
                          parameter:@{@"url": imageURL.absoluteString}
                            handler:^(NSData *data) {
                                if(handler){
                                    if(data && data.length > 0){
                                        
                                        handler([UIImage imageWithData:data]);
                                        if (doCache) {
                                            [data writeToFile:strPath atomically:YES];
                                        }
                                    }else{
                                        handler(nil);
                                    }
                                }
                                
                            }
             ];
            
        }
    }
    
    
}


- (void) download:(NSString*) fileID  saveName:(NSString*) saveName cache:(BOOL) doCache progress:(ProgressBlock) progress handler:(DownloadBlock) handler {
    [self download:fileID type:nil saveName:saveName cache:doCache progress:progress handler:handler];
}

- (void) download:(NSString*) fileID type:(NSString*) type saveName:(NSString*) saveName cache:(BOOL) doCache progress:(ProgressBlock) progress handler:(DownloadBlock) handler {
    
    if (!type && [fileID rangeOfString:@":"].location != NSNotFound) {
        NSArray *IDs = [fileID componentsSeparatedByString:@":"];
        fileID = IDs.firstObject;
        type = IDs.lastObject;
    }
    
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    NSURL *tmpDirURL = [NSURL fileURLWithPath:NSTemporaryDirectory() isDirectory:YES];
    
    NSString *imgFolder = [NSString stringWithFormat:@"%@download-cache/%@", tmpDirURL, type ? type : @"undefined"];
    imgFolder = [imgFolder stringByReplacingOccurrencesOfString:@"file://" withString:@""];
    if (![fileMgr fileExistsAtPath:imgFolder]) {
        if([fileMgr createDirectoryAtPath:imgFolder withIntermediateDirectories:YES attributes:nil error:nil]){
            ////NSLog(@"Dir created");
        }
    }
    ////NSLog(@"File Name : %@", imageID);
    NSString *fileName = fileID;
    if(saveName){
        fileName = [saveName stringByReplacingOccurrencesOfString:@"/" withString:@"-"];
    }
    NSString *strPath = [NSString stringWithFormat:@"%@/%@", imgFolder, fileName];
    
    if (doCache && [fileMgr fileExistsAtPath:[strPath stringByRemovingPercentEncoding]]){
        ////NSLog(@"doCache: %@", [strPath stringByRemovingPercentEncoding]);
        NSData *imgData = [fileMgr contentsAtPath:[strPath stringByRemovingPercentEncoding]];
        if (progress) {
            progress(imgData.length, imgData.length);
        }
        if(handler){
            handler(imgData, [strPath stringByRemovingPercentEncoding], nil);
        }
        return;
    }else{
        
        NSString *clientCode = [AppHelper getUserDataForKey:kADA_CLIENT];
        AFHTTPRequestSerializer *serializer = [AFHTTPRequestSerializer serializer];
        
        NSString *strURL = type ? [NSString stringWithFormat:@"%@file?id=%@&mediaType=%@", kSERVER_HOST, fileID, type] :
        [NSString stringWithFormat:@"%@file?id=%@", kSERVER_HOST, fileID];
        NSMutableURLRequest *request = [URLConnection requestFromURL:strURL method:@"GET" parameter:nil];
        [request setValue:clientCode forHTTPHeaderField:kADA_CLIENT];
        [serializer requestBySerializingRequest:request withParameters:nil error:nil];
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        manager.requestSerializer = serializer;
        [manager.requestSerializer setValue:clientCode forHTTPHeaderField:kADA_CLIENT];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        manager.responseSerializer.acceptableContentTypes = nil;
        //[manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
        
        [manager GET:strURL
          parameters:@{}
            progress:^(NSProgress * _Nonnull uploadProgress) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (progress) {
                        progress((long)uploadProgress.completedUnitCount, (long)uploadProgress.totalUnitCount);
                    }
                });
                
            } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                ////NSLog(@"Success: %@", responseObject);
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (progress) {
                        progress([responseObject length], [responseObject length]);
                    }
                });
                
                if (doCache) {
                    [responseObject writeToFile:[strPath stringByRemovingPercentEncoding] atomically:YES];
                }
                handler(responseObject, [strPath stringByRemovingPercentEncoding], nil);
                
                
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                if (progress) {
                    progress(0, 0);
                }
                if(handler){
                    handler(nil, nil, error);
                }
                
                
            }];
    }
    
}


- (void)downLoadFromURL:(BOOL)doCache fileMgr:(NSFileManager *)fileMgr fileName:(NSString *)fileName fileURL:(NSURL * _Nonnull)fileURL handler:(DownloadBlock _Nullable)handler progress:(ProgressBlock _Nullable)progress tmpDirURL:(NSURL *)tmpDirURL {
    NSString *imgFolder = [NSString stringWithFormat:@"%@download-cache", tmpDirURL];
    imgFolder = [imgFolder stringByReplacingOccurrencesOfString:@"file://" withString:@""];
    if (![fileMgr fileExistsAtPath:imgFolder]) {
        if([fileMgr createDirectoryAtPath:imgFolder withIntermediateDirectories:YES attributes:nil error:nil]){
            ////NSLog(@"Dir created");
        }
    }
    //BOOL pdfFile = [[fileName lowercaseString] rangeOfString:@".pdf"].location != NSNotFound;
    __block NSString *strPath = [NSString stringWithFormat:@"%@/%@", imgFolder, fileName];
    NSLog(@"%@", strPath);
    if (doCache && [fileMgr fileExistsAtPath:[strPath stringByRemovingPercentEncoding]]){
        NSData *imgData = [fileMgr contentsAtPath:[strPath stringByRemovingPercentEncoding]];
        if (progress) {
            progress(imgData.length, imgData.length);
        }
        if(handler){
            handler(imgData, [strPath stringByRemovingPercentEncoding], nil);
        }
        return;
    }else{
        NSString *clientCode = [AppHelper getUserDataForKey:kADA_CLIENT];
        AFHTTPRequestSerializer *serializer = [AFHTTPRequestSerializer serializer];
        
        NSString *strURL = [NSString stringWithFormat:@"%@proxy-call?_c=%@&url=%@", kSERVER_HOST, clientCode, fileURL.absoluteString];
        if([fileURL.absoluteString containsString:@"file?id="]) {
            strURL = fileURL.absoluteString;
        }
        
        //NSLog(@"clientCode : %@", clientCode);
        
        NSMutableURLRequest *request = [URLConnection requestFromURL:strURL method:@"GET" parameter:nil];
        [request setValue:clientCode forHTTPHeaderField:kADA_CLIENT];
        [serializer requestBySerializingRequest:request withParameters:nil error:nil];
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        manager.requestSerializer = serializer;
        [manager.requestSerializer setValue:clientCode forHTTPHeaderField:kADA_CLIENT];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        manager.responseSerializer.acceptableContentTypes = nil;
        //[manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
        //[manager.responseSerializer.acceptableContentTypes setByAddingObject:@"application/pdf"];
        
        [manager GET:strURL
          parameters:nil
            progress:^(NSProgress * _Nonnull downloadProgress) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (progress) {
                        progress((long)downloadProgress.completedUnitCount, (long)downloadProgress.totalUnitCount);
                    }
                });
                
            } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                ////NSLog(@"Success: %@", responseObject);
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (progress) {
                        progress([responseObject length], [responseObject length]);
                    }
                });
                //NSString *mimeType = [ADA mimeTypeForData:responseObject];
                //NSLog(@"mimeType %@", mimeType);
                /*
                 NSString *mimeType = [ADA mimeTypeForData:responseObject];
                 if(mimeType) {
                 if([mimeType containsString:@"/pdf"]){
                 strPath = [strPath stringByAppendingString:@".pdf"];
                 }else if([mimeType containsString:@"/jpg"]){
                 strPath = [strPath stringByAppendingString:@".jpg"];
                 }else if([mimeType containsString:@"/png"]){
                 strPath = [strPath stringByAppendingString:@".png"];
                 }else if([mimeType containsString:@"/gif"]){
                 strPath = [strPath stringByAppendingString:@".gif"];
                 }
                 }
                 */
                
                if (doCache) {
                    [responseObject writeToFile:[strPath stringByRemovingPercentEncoding] atomically:YES];
                }
                handler(responseObject, [strPath stringByRemovingPercentEncoding], nil);
                
                
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                if (progress) {
                    progress(0, 0);
                }
                if(handler){
                    handler(nil, nil, error);
                }
                
                
            }];
        
        
    }
}

- (void) downloadFromURL:(NSURL*) fileURL cache:(BOOL) doCache progress:(ProgressBlock) progress handler:(DownloadBlock) handler {
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    NSURL *tmpDirURL = [NSURL fileURLWithPath:NSTemporaryDirectory() isDirectory:YES];
    
    
    
    if([fileURL.absoluteString containsString:@"file?id="]) {
        //NSLog(@"url %@", [fileURL.absoluteString stringByAppendingString:@"&outputType=file-info"]);
        [ServiceCaller callJSON: [fileURL.absoluteString stringByAppendingString:@"&outputType=file-info"]
                         method:@"GET"
                         header:@{
                                  @"enc": @"false"
                                  }
                      parameter:nil
                        handler:^(id target, NSJSONSerialization *json) {
                            //NSLog(@"%@", json);
                            if(json) {
                                NSString *fileName = [[json valueForKey:@"name"] stringByReplacingOccurrencesOfString:@" " withString:@"_"];
                                [self downLoadFromURL:doCache fileMgr:fileMgr fileName:fileName fileURL:fileURL handler:handler progress:progress tmpDirURL:tmpDirURL];
                            }
                        } target:nil];
    }else{
        NSArray *fileNames = [fileURL.absoluteString componentsSeparatedByString:@"/"];
        NSString *fileName = fileNames.lastObject;
        [self downLoadFromURL:doCache fileMgr:fileMgr fileName:fileName fileURL:fileURL handler:handler progress:progress tmpDirURL:tmpDirURL];
    }
    
    
    
}





- (void) download:(NSString*) fileID  saveName:(NSString*) saveName cache:(BOOL) doCache progress:(ProgressBlock) progress handler:(DownloadRefBlock) handler ref:(id) ref {
    [self download:fileID
              type:nil
          saveName:saveName
             cache:doCache
          progress:progress
           handler:handler
               ref:ref];
}

- (void) download:(NSString*) fileID type:(NSString*) type saveName:(NSString*) saveName cache:(BOOL) doCache progress:(ProgressBlock) progress handler:(DownloadRefBlock) handler ref:(id) ref {
    
    if (!type && [fileID rangeOfString:@":"].location != NSNotFound) {
        NSArray *IDs = [fileID componentsSeparatedByString:@":"];
        fileID = IDs.firstObject;
        type = IDs.lastObject;
    }
    
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    NSURL *tmpDirURL = [NSURL fileURLWithPath:NSTemporaryDirectory() isDirectory:YES];
    
    NSString *imgFolder = [NSString stringWithFormat:@"%@download-cache/%@", tmpDirURL, type ? type : @"undefined"];
    imgFolder = [imgFolder stringByReplacingOccurrencesOfString:@"file://" withString:@""];
    if (![fileMgr fileExistsAtPath:imgFolder]) {
        if([fileMgr createDirectoryAtPath:imgFolder withIntermediateDirectories:YES attributes:nil error:nil]){
            ////NSLog(@"Dir created");
        }
    }
    ////NSLog(@"File Name : %@", imageID);
    NSString *fileName = fileID;
    if(saveName){
        fileName = [saveName stringByReplacingOccurrencesOfString:@"/" withString:@"-"];
    }
    NSString *strPath = [NSString stringWithFormat:@"%@/%@", imgFolder, fileName];
    
    if (doCache && [fileMgr fileExistsAtPath:[strPath stringByRemovingPercentEncoding]]){
        ////NSLog(@"doCache: %@", [strPath stringByRemovingPercentEncoding]);
        NSData *imgData = [fileMgr contentsAtPath:[strPath stringByRemovingPercentEncoding]];
        if (progress) {
            progress(imgData.length, imgData.length);
        }
        if(handler){
            handler(ref, imgData, [strPath stringByRemovingPercentEncoding], nil);
        }
        return;
    }else{
        
        NSString *clientCode = [AppHelper getUserDataForKey:kADA_CLIENT];
        AFHTTPRequestSerializer *serializer = [AFHTTPRequestSerializer serializer];
        
        NSString *strURL = type ? [NSString stringWithFormat:@"%@file?id=%@&mediaType=%@", kSERVER_HOST, fileID, type] :
        [NSString stringWithFormat:@"%@file?id=%@", kSERVER_HOST, fileID];
        NSMutableURLRequest *request = [URLConnection requestFromURL:strURL method:@"GET" parameter:nil];
        [request setValue:clientCode forHTTPHeaderField:kADA_CLIENT];
        [serializer requestBySerializingRequest:request withParameters:nil error:nil];
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        manager.requestSerializer = serializer;
        [manager.requestSerializer setValue:clientCode forHTTPHeaderField:kADA_CLIENT];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        manager.responseSerializer.acceptableContentTypes = nil;
        //[manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
        
        [manager GET:strURL
          parameters:@{}
            progress:^(NSProgress * _Nonnull uploadProgress) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (progress) {
                        progress((long)uploadProgress.completedUnitCount, (long)uploadProgress.totalUnitCount);
                    }
                });
                
            } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                ////NSLog(@"Success: %@", responseObject);
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (progress) {
                        progress([responseObject length], [responseObject length]);
                    }
                });
                
                if (doCache) {
                    [responseObject writeToFile:[strPath stringByRemovingPercentEncoding] atomically:YES];
                }
                handler(ref, responseObject, [strPath stringByRemovingPercentEncoding], nil);
                
                
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                if (progress) {
                    progress(0, 0);
                }
                if(handler){
                    handler(ref, nil, nil, error);
                }
                
                
            }];
    }
    
}


- (void)downLoadFromURL:(BOOL)doCache fileMgr:(NSFileManager *)fileMgr fileName:(NSString *)fileName fileURL:(NSURL * _Nonnull)fileURL handler:(DownloadRefBlock _Nullable)handler progress:(ProgressBlock _Nullable)progress tmpDirURL:(NSURL *)tmpDirURL ref:(id) ref {
    NSString *imgFolder = [NSString stringWithFormat:@"%@download-cache", tmpDirURL];
    imgFolder = [imgFolder stringByReplacingOccurrencesOfString:@"file://" withString:@""];
    if (![fileMgr fileExistsAtPath:imgFolder]) {
        if([fileMgr createDirectoryAtPath:imgFolder withIntermediateDirectories:YES attributes:nil error:nil]){
            ////NSLog(@"Dir created");
        }
    }
    //BOOL pdfFile = [[fileName lowercaseString] rangeOfString:@".pdf"].location != NSNotFound;
    __block NSString *strPath = [NSString stringWithFormat:@"%@/%@", imgFolder, fileName];
    NSLog(@"%@", strPath);
    if (doCache && [fileMgr fileExistsAtPath:[strPath stringByRemovingPercentEncoding]]){
        NSData *imgData = [fileMgr contentsAtPath:[strPath stringByRemovingPercentEncoding]];
        if (progress) {
            progress(imgData.length, imgData.length);
        }
        if(handler){
            handler(ref, imgData, [strPath stringByRemovingPercentEncoding], nil);
        }
        return;
    }else{
        NSString *clientCode = [AppHelper getUserDataForKey:kADA_CLIENT];
        AFHTTPRequestSerializer *serializer = [AFHTTPRequestSerializer serializer];
        
        NSString *strURL = [NSString stringWithFormat:@"%@proxy-call?_c=%@&url=%@", kSERVER_HOST, clientCode, fileURL.absoluteString];
        if([fileURL.absoluteString containsString:@"file?id="]) {
            strURL = fileURL.absoluteString;
        }
        
        //NSLog(@"clientCode : %@", clientCode);
        
        NSMutableURLRequest *request = [URLConnection requestFromURL:strURL method:@"GET" parameter:nil];
        [request setValue:clientCode forHTTPHeaderField:kADA_CLIENT];
        [serializer requestBySerializingRequest:request withParameters:nil error:nil];
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        manager.requestSerializer = serializer;
        [manager.requestSerializer setValue:clientCode forHTTPHeaderField:kADA_CLIENT];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        manager.responseSerializer.acceptableContentTypes = nil;
        //[manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
        //[manager.responseSerializer.acceptableContentTypes setByAddingObject:@"application/pdf"];
        
        [manager GET:strURL
          parameters:nil
            progress:^(NSProgress * _Nonnull downloadProgress) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (progress) {
                        progress((long)downloadProgress.completedUnitCount, (long)downloadProgress.totalUnitCount);
                    }
                });
                
            } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                ////NSLog(@"Success: %@", responseObject);
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (progress) {
                        progress([responseObject length], [responseObject length]);
                    }
                });
                //NSString *mimeType = [ADA mimeTypeForData:responseObject];
                //NSLog(@"mimeType %@", mimeType);
                /*
                 NSString *mimeType = [ADA mimeTypeForData:responseObject];
                 if(mimeType) {
                 if([mimeType containsString:@"/pdf"]){
                 strPath = [strPath stringByAppendingString:@".pdf"];
                 }else if([mimeType containsString:@"/jpg"]){
                 strPath = [strPath stringByAppendingString:@".jpg"];
                 }else if([mimeType containsString:@"/png"]){
                 strPath = [strPath stringByAppendingString:@".png"];
                 }else if([mimeType containsString:@"/gif"]){
                 strPath = [strPath stringByAppendingString:@".gif"];
                 }
                 }
                 */
                
                if (doCache) {
                    [responseObject writeToFile:[strPath stringByRemovingPercentEncoding] atomically:YES];
                }
                handler(ref, responseObject, [strPath stringByRemovingPercentEncoding], nil);
                
                
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                if (progress) {
                    progress(0, 0);
                }
                if(handler){
                    handler(ref, nil, nil, error);
                }
                
                
            }];
        
        
    }
}

- (void) downloadFromURL:(NSURL*) fileURL cache:(BOOL) doCache progress:(ProgressBlock) progress handler:(DownloadRefBlock) handler ref:(id) ref {
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    NSURL *tmpDirURL = [NSURL fileURLWithPath:NSTemporaryDirectory() isDirectory:YES];
    
    
    
    if([fileURL.absoluteString containsString:@"file?id="]) {
        //NSLog(@"url %@", [fileURL.absoluteString stringByAppendingString:@"&outputType=file-info"]);
        [ServiceCaller callJSON: [fileURL.absoluteString stringByAppendingString:@"&outputType=file-info"]
                         method:@"GET"
                         header:@{
                                  @"enc": @"false"
                                  }
                      parameter:nil
                        handler:^(id target, NSJSONSerialization *json) {
                            //NSLog(@"%@", json);
                            if(json) {
                                NSString *fileName = [[json valueForKey:@"name"] stringByReplacingOccurrencesOfString:@" " withString:@"_"];
                                [self downLoadFromURL:doCache fileMgr:fileMgr fileName:fileName fileURL:fileURL handler:handler progress:progress tmpDirURL:tmpDirURL ref:ref];
                            }
                        } target:nil];
    }else{
        NSArray *fileNames = [fileURL.absoluteString componentsSeparatedByString:@"/"];
        NSString *fileName = fileNames.lastObject;
        [self downLoadFromURL:doCache fileMgr:fileMgr fileName:fileName fileURL:fileURL handler:handler progress:progress tmpDirURL:tmpDirURL ref:ref];
    }
    
    
    
}


- (void) saveImage:(UIImage*) image progress:(ProgressBlock) progress handler:(UploadBlock) handler {
    [self saveImage:image type:nil progress:progress handler:handler];
}
- (void) saveData:(NSData*) data progress:(ProgressBlock) progress handler:(UploadBlock) handler {
    [self saveData:data type:nil progress:progress handler:handler];
}

- (void) saveImage:(UIImage*) image type:(NSString*) type progress:(ProgressBlock) progress handler:(UploadBlock) handler {
    NSData *imgData = UIImageJPEGRepresentation(image, 1.0);
    [self saveData:imgData type:type progress:progress handler:handler];
}
- (void) saveData:(NSData*) data type:(NSString*) type progress:(ProgressBlock) progress handler:(UploadBlock) handler {
    
    
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    NSURL *tmpDirURL = [NSURL fileURLWithPath:NSTemporaryDirectory() isDirectory:YES];
    
    NSString *imgFolder = [NSString stringWithFormat:@"%@temp-upload/%@", tmpDirURL, type ? type : @"undefined"];
    imgFolder = [imgFolder stringByReplacingOccurrencesOfString:@"file://" withString:@""];
    if (![fileMgr fileExistsAtPath:imgFolder]) {
        if([fileMgr createDirectoryAtPath:imgFolder withIntermediateDirectories:YES attributes:nil error:nil]){
            ////NSLog(@"Dir created");
        }
    }
    ////NSLog(@"File Name : %@", imageID);
    NSString *strPath = [NSString stringWithFormat:@"%@/%ld", imgFolder, (unsigned long)data.length];
    
    [data writeToFile:strPath atomically:YES];
    
    NSData *fileData = [fileMgr contentsAtPath:strPath];
    long fileSize = fileData.length;
    if (fileSize == 0) {
        if(handler){
            handler(nil, [[NSError alloc] initWithDomain:@"ADA" code:204 userInfo:nil]);
        }
        return;
    }
    
    NSArray *fileNames = [strPath componentsSeparatedByString:@"/"];
    NSString *fileName = fileNames.lastObject;
    NSString *fileType = [ADA mimeTypeForData:data];//[self mimeTypeForFileAtPath:strPath];
    
    //NSLog(@"file : %@, fileType: %@, size: %ld", fileName, fileType,fileSize);
    
    
    
    // 1. Create `AFHTTPRequestSerializer` which will create your request.
    AFHTTPRequestSerializer *serializer = [AFHTTPRequestSerializer serializer];
    
    // 2. Create an `NSMutableURLRequest`.
    NSString *strURL = [kSERVER_HOST stringByAppendingString:@"upload-file?uploadType=form"];
    if (type) {
        strURL = [[strURL stringByAppendingString:@"&mediaType="] stringByAppendingString:type];
    }
    // 3. Create and use `AFHTTPRequestOperationManager` to create an `AFHTTPRequestOperation` from the `NSMutableURLRequest` that we just created.
    
    
    
    NSString *clientCode = [AppHelper getUserDataForKey:kADA_CLIENT];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.requestSerializer = serializer;
    manager.responseSerializer.acceptableContentTypes = nil;
    //[manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    manager.requestSerializer = serializer;
    [manager.requestSerializer setValue:clientCode forHTTPHeaderField:kADA_CLIENT];
    [manager.requestSerializer setValue:@"multipart/form-data" forHTTPHeaderField:@"Content-Type"];
    [manager POST:strURL
       parameters:@{}
constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
    [formData appendPartWithFileData:fileData name:@"file" fileName:fileName mimeType:fileType];
}
         progress:^(NSProgress * _Nonnull uploadProgress) {
             long writen = (long)((double)fileData.length * uploadProgress.fractionCompleted);
             long total = (long)fileData.length;
             
             if (progress) {
                 progress(writen, total);
             }
         } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
             long total = (long)fileData.length;
             if (progress) {
                 progress(total, total);
             }
             //NSLog(@"response %@", responseObject);
             NSJSONSerialization *json = [AppHelper jsonWithData:responseObject];
             
             if(handler){
                 handler([json valueForKey:@"id"], nil);
             }
             [[NSFileManager defaultManager] removeItemAtPath:strPath error:nil];
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
             if(handler){
                 handler(nil, error);
             }
             [[NSFileManager defaultManager] removeItemAtPath:strPath error:nil];
         }];
    
}


- (void) upload:(NSURL*) url progress:(ProgressBlock) progress handler:(UploadBlock) handler {
    [self upload:url type:nil progress:progress handler:handler];
}
- (void) upload:(NSURL*) url type:(NSString*) type progress:(ProgressBlock) progress handler:(UploadBlock) handler {
    NSString *exportPath = [[[NSString stringWithFormat:@"%@", url] stringByRemovingPercentEncoding]  stringByReplacingOccurrencesOfString:@"file://" withString:@""];
    
    exportPath = [exportPath stringByReplacingOccurrencesOfString:@" " withString:@"_"];
    NSError *error;
    if (![[NSFileManager defaultManager] copyItemAtURL:url toURL:[NSURL fileURLWithPath:exportPath] error:&error]) {
        //NSLog(@"Error %@", error);
    }
    
    
    unsigned long long fileSize = [[[NSFileManager defaultManager] attributesOfItemAtPath:exportPath error:nil] fileSize];
    
    NSString *filePath = [exportPath stringByReplacingOccurrencesOfString:@"file://" withString:@""];
    NSData *fileData = [[NSFileManager defaultManager] contentsAtPath:filePath];
    fileSize = fileData.length;
    if (fileSize == 0) {
        if(handler){
            handler(nil, [[NSError alloc] initWithDomain:@"ADA" code:204 userInfo:nil]);
        }
        return;
    }
    
    NSArray *fileNames = [filePath componentsSeparatedByString:@"/"];
    NSString *fileName = fileNames.lastObject;
    NSString *fileType = [ADA mimeTypeForData:fileData];//[self mimeTypeForFileAtPath:filePath];
    
    //NSLog(@"file : %@, fileType: %@, size: %llu", fileName, fileType,fileSize);
    
    
    
    // 1. Create `AFHTTPRequestSerializer` which will create your request.
    AFHTTPRequestSerializer *serializer = [AFHTTPRequestSerializer serializer];
    
    // 2. Create an `NSMutableURLRequest`.
    NSString *strURL = [kSERVER_HOST stringByAppendingString:@"upload-file?uploadType=form"];
    if (type) {
        strURL = [[strURL stringByAppendingString:@"&mediaType="] stringByAppendingString:type];
    }
    // 3. Create and use `AFHTTPRequestOperationManager` to create an `AFHTTPRequestOperation` from the `NSMutableURLRequest` that we just created.
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.requestSerializer = serializer;
    manager.responseSerializer.acceptableContentTypes = nil;
    //[manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    manager.requestSerializer = serializer;
    [manager.requestSerializer setValue:[AppHelper getUserDataForKey:kADA_CLIENT] forHTTPHeaderField:kADA_CLIENT];
    [manager.requestSerializer setValue:@"multipart/form-data" forHTTPHeaderField:@"Content-Type"];
    [manager POST:strURL
       parameters:@{}
constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
    [formData appendPartWithFileData:fileData name:@"file" fileName:fileName mimeType:fileType];
}
         progress:^(NSProgress * _Nonnull uploadProgress) {
             long writen = (long)((double)fileData.length * uploadProgress.fractionCompleted);
             long total = (long)fileData.length;
             
             if (progress) {
                 progress(writen, total);
             }
         } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
             long total = (long)fileData.length;
             if (progress) {
                 progress(total, total);
             }
             
             NSJSONSerialization *json = [AppHelper jsonWithData:responseObject];
             
             if(handler){
                 handler([json valueForKey:@"id"], nil);
             }
             [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
             if(handler){
                 handler(nil, error);
             }
             [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
         }];
    
    
}

- (void) clearCache:(SuccessBlock) handler {
    
    
    
    AFImageDownloader *imageDownloader = [AFImageDownloader defaultInstance];
    NSURLCache *urlCache = imageDownloader.sessionManager.session.configuration.URLCache;
    [urlCache removeAllCachedResponses];
    [imageDownloader.imageCache removeAllImages];
    
    
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    
    NSArray* tmpDirectory = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:NSTemporaryDirectory() error:NULL];
    for (NSString *file in tmpDirectory) {
        
        if([file containsString:@"ada-user-data"]){
            continue;
        }
        
        [fileMgr removeItemAtPath:[NSString stringWithFormat:@"%@%@", NSTemporaryDirectory(), file] error:NULL];
    }
    
    NSArray *array = [ManagedObjectContextFactory getAllObjectsFromEntity:@"CacheService" sortedBy:@{}];
    for (CacheService *cache in array) {
        [ManagedObjectContextFactory deleteObject:cache];
    }
    [ManagedObjectContextFactory saveContext];
    
    if(handler){
        handler(YES);
    }
    
    
}



#pragma mark Feature

- (void) getAppOverview:(AppOverviewBlock _Nullable) handler {
    [ServiceCaller callJSON:[NSString stringWithFormat:@"%@features/get-tutorial", kSERVER]
                     method:@"GET"
                     header:nil
                  parameter:nil
                    handler:^(NSJSONSerialization *json) {
                        
                        if ([[json valueForKey:@"result"] boolValue]) {
                            NSMutableArray<NSString*> *images = [[NSMutableArray alloc] init];
                            for(NSString *imageID in [[json valueForKey:@"data"] valueForKey:@"images"]) {
                                [images addObject:imageID];
                            }
                            //NSLog(@"%@", json);
                            NSMutableArray<Version*> *versions = [[NSMutableArray alloc] init];
                            for(NSJSONSerialization *note in [[json valueForKey:@"data"]  valueForKey:@"notes"]) {
                                Version *v = [Version new];
                                v.version = [note valueForKey:@"version"];
                                v.note = [note valueForKey:@"note"];
                                [versions addObject:v];
                            }
                            
                            if (handler) {
                                
                                
                                handler(versions, images, nil);
                            }
                            
                        }else{
                            NSError *error = [[NSError alloc] initWithDomain:@"ADA" code:500 userInfo:nil];
                            handler(nil, nil, error);
                        }
                    }
     ];
}

- (void) getGroupFeatures:(FeatureGroupBlock) handler {
 
    
    [ServiceCaller getJSONCache:[NSString stringWithFormat:@"%@features/get-group", kSERVER]
                         method:@"GET"
                         header:nil
                      parameter:nil
                        handler:^(NSJSONSerialization *json) {
                            BOOL cacheLoaded = NO;
    
                            if ([[json valueForKey:@"result"] boolValue]) {
                                NSMutableArray *features = [[NSMutableArray alloc] init];
                                [self doProcessGroupFeature:json features:features];
                                if (handler) {
                                    handler(features, nil);
                                }
                                cacheLoaded = YES;
                            }
                            [ServiceCaller callJSON:[NSString stringWithFormat:@"%@features/get-group", kSERVER]
                                             method:@"GET"
                                             header:nil
                                          parameter:nil
                                            handler:^(NSJSONSerialization *json) {
                                                if(cacheLoaded) {
                                                    return;
                                                }
                                                if ([[json valueForKey:@"result"] boolValue]) {
                                                    
                                                    NSMutableArray *features = [[NSMutableArray alloc] init];
                                                    [self doProcessGroupFeature:json features:features];
                                                    if (handler) {
                                                        handler(features, nil);
                                                    }
                                                    
                                                }else{
                                                    NSError *error = [[NSError alloc] initWithDomain:@"ADA" code:500 userInfo:nil];
                                                    handler(nil, error);
                                                }
                                            }
                             ];
                        }
     ];
    
    
}

- (void) doProcessGroupFeature:(NSJSONSerialization*) json features:(NSMutableArray*) groupList {
    
    NSArray *data = [json valueForKey:@"data"];
    
    for (NSJSONSerialization *group in data) {
        
        FeatureGroup *g = [FeatureGroup new];
        g.groupName = [AppHelper getLabelForThai:[group valueForKey:@"group_name_th"]
                                             eng:[group valueForKey:@"group_name_en"]];
        g.groupDescription = [AppHelper getLabelForThai:[group valueForKey:@"group_desc_th"]
                                                    eng:[group valueForKey:@"group_desc_en"]];
        g.coverImage = [NSString stringWithFormat:@"%@:feature-cover", [group valueForKey:@"cover_image_id"]];
        
        
        
        NSMutableArray *features = [NSMutableArray new];
        for (NSJSONSerialization *feature in [group valueForKey:@"features"]) {
            
            
            if([[feature valueForKey:@"type"] isEqualToString:@"web"]){
                if([feature valueForKey:@"url"]){
                    Feature *f = [Feature new];
                    f.name = [feature valueForKey:@"name"];
                    f.type = getFeatureTypeByCode([feature valueForKey:@"type"]);
                    f.icon = [feature valueForKey:@"icon"];
                    f.url = [feature valueForKey:@"url"];
                    [features addObject:f];
                }
            }else if([[feature valueForKey:@"type"] isEqualToString:@"link"]){
                if([feature valueForKey:@"ios_app_link"]){
                    Feature *f = [Feature new];
                    f.name = [feature valueForKey:@"name"];
                    f.type = getFeatureTypeByCode([feature valueForKey:@"type"]);
                    f.icon = [feature valueForKey:@"icon"];
                    f.appLink = [feature valueForKey:@"ios_app_link"];
                    [features addObject:f];
                }
            }else{
                Feature *f = [Feature new];
                f.name = [feature valueForKey:@"name"];
                f.type = getFeatureTypeByCode([feature valueForKey:@"type"]);
                f.icon = [feature valueForKey:@"icon"];
                [features addObject:f];
            }
        }
        g.features = features;
        [groupList addObject:g];
    }
}

- (void) getMainFeatures:(FeatureBlock) handler {
 
    [self doRetrieveFeature:1 handler:handler];
    
}

- (void) getFeatures:(FeatureBlock)handler {
    
 
    
    [self doRetrieveFeature:0 handler:handler];
}

- (void) getAllFeatures:(FeatureBlock)handler {
    
 
    [self doRetrieveFeature:-1 handler:handler];
}



- (void)doRetrieveFeature:(NSInteger) mainMenu handler:(FeatureBlock)handler {
    
    [ServiceCaller getJSONCache:[NSString stringWithFormat:@"%@features/get", kSERVER]
                         method:@"GET"
                         header:nil
                      parameter:nil
                        handler:^(NSJSONSerialization *json) {
                            BOOL cacheLoaded = NO;
                            if (json && [[json valueForKey:@"result"] boolValue]) {
                                NSMutableArray *features = [[NSMutableArray alloc] init];
                                self->allFeatureList = features;
                                [self doProcessFeature:mainMenu json:json features:features];
                                
                                if (handler) {
                                    handler(features, nil);
                                }
                                cacheLoaded = YES;
                            }
                            [ServiceCaller callJSON:[NSString stringWithFormat:@"%@features/get", kSERVER]
                                             method:@"GET"
                                             header:nil
                                          parameter:nil
                                            handler:^(NSJSONSerialization *json) {
                                                if(cacheLoaded) {
                                                    return;
                                                }
                                                if ([[json valueForKey:@"result"] boolValue]) {
                                                    NSMutableArray *features = [[NSMutableArray alloc] init];
                                                    [self doProcessFeature:mainMenu json:json features:features];
                                                    self->allFeatureList = features;
                                                    if (handler) {
                                                        handler(features, nil);
                                                    }
                                                    
                                                }else{
                                                    NSError *error = [[NSError alloc] initWithDomain:@"ADA" code:500 userInfo:nil];
                                                    handler(nil, error);
                                                }
                                            }
                             ];
                        }
     ];
    
}

- (void) doProcessFeature:(NSInteger) mainMenu json:(NSJSONSerialization*) json features:(NSMutableArray*) features {
    
    NSArray *data = [[json valueForKey:@"data"] valueForKey:@"features"];
    if(mainMenu == 0){
        data = [data filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"hot_menu = 0"]];
    }else if(mainMenu == 1){
        data = [data filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"hot_menu > 0"]];
        data = [data sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"hot_menu" ascending:YES]]];
    }
    
    for (NSJSONSerialization *feature in data) {
        if([[feature valueForKey:@"type"] isEqualToString:@"helpdesk"]){
            continue;
        }
        
        
        if([[feature valueForKey:@"type"] isEqualToString:@"web"]){
            if([feature valueForKey:@"url"]){
                Feature *f = [Feature new];
                f.name = [feature valueForKey:@"name"];
                f.type = getFeatureTypeByCode([feature valueForKey:@"type"]);
                f.icon = [feature valueForKey:@"icon"];
                f.url = [feature valueForKey:@"url"];
                f.appKey = [feature valueForKey:@"app_key"];
                f.webID = [feature valueForKey:@"web_id"];
                f.proxyName = [feature valueForKey:@"proxy_name"];
                [features addObject:f];
            }
        }else if([[feature valueForKey:@"type"] isEqualToString:@"link"]){
            if([feature valueForKey:@"ios_app_link"]){
                Feature *f = [Feature new];
                f.name = [feature valueForKey:@"name"];
                f.type = getFeatureTypeByCode([feature valueForKey:@"type"]);
                f.icon = [feature valueForKey:@"icon"];
                f.appLink = [feature valueForKey:@"ios_app_link"];
                [features addObject:f];
            }
        }else{
            Feature *f = [Feature new];
            f.name = [feature valueForKey:@"name"];
            f.type = getFeatureTypeByCode([feature valueForKey:@"type"]);
            f.icon = [feature valueForKey:@"icon"];
            [features addObject:f];
        }
    }
}

/*
 @{
 @"news": @"newsNav",
 @"bus":@"shuttleNav",
 @"contact":@"contactNav",
 @"chat":@"messagingNav",
 @"mail":@"sendMailNav",
 @"app":@"appNav",
 @"personal":@"personNav",
 @"setting":@"settingsNav",
 @"helpdesk":@"helpdeskNav",
 @"company":@"companyNav",
 @"phone":@"adaCallNav",
 @"bus":@"busNav",
 @"weather":@"weatherNav",
 };
 
 */

FeatureType getFeatureTypeByCode(NSString *code) {
    
    return [Feature typeFromString:code];
}

#pragma mark FlashNews
- (void) getFlashNews:(FlashNewsBlock) handler {
    
 
    [ServiceCaller getJSONCache:[NSString stringWithFormat:@"%@ads/get-ads", kSERVER]
                         method:@"GET"
                         header:@{@"showMsg":@"N"}
                      parameter:nil
                        handler:^(NSJSONSerialization *json) {
                            BOOL cacheLoaded = NO;
                            if(json && [[json valueForKey:@"result"] boolValue]){
                                NSMutableArray *list = [[NSMutableArray alloc] init];
                                [self doProcessFlashNews:list data:json];
                                cacheLoaded = YES;
                                if (handler) {
                                    handler(list, nil);
                                }
                            }
                            [ServiceCaller callJSON:[NSString stringWithFormat:@"%@ads/get-ads", kSERVER]
                                             method:@"GET"
                                             header:@{@"showMsg":@"N"}
                                          parameter:nil
                                            handler:^(NSJSONSerialization *json) {
                                                if(!cacheLoaded) {
                                                    if(json && [[json valueForKey:@"result"] boolValue]){
                                                        NSMutableArray *list = [[NSMutableArray alloc] init];
                                                        [self doProcessFlashNews:list data:json];
                                                        if (handler) {
                                                            handler(list, nil);
                                                        }
                                                    }else{
                                                        if (handler) {
                                                            handler(nil, [[NSError alloc] initWithDomain:@"ADA" code:500 userInfo:nil]);
                                                        }
                                                    }
                                                }
                                            }];
                            
                            
                        }];
    
}

- (void) doProcessFlashNews:(NSMutableArray*) list data:(NSJSONSerialization*) json {
    NSArray *data = [json valueForKey:@"data"];
    
    NSDateFormatter *parser = [NSDateFormatter instanceWithUSLocale];
    [parser setDateFormat:@"yyyy-MM-dd HH:mm:ss.S"];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"d MMMM YYYY"];
    for(NSJSONSerialization *item in data){
        FlashNews *f = [FlashNews new];
        f.newsID = [[item valueForKey:@"id"] longValue];
        f.title = [AppHelper getLabelForThai:[item valueForKey:@"title"]
                                         eng:[item valueForKey:@"title_en"]];
        f.detail = [AppHelper getLabelForThai:[item valueForKey:@"detail"]
                                          eng:[item valueForKey:@"detail_en"]];
        f.author = [item valueForKey:@"author"];
        
        NSDate *startDate = [parser dateFromString:[item valueForKey:@"start_date"]];
        if(startDate){
            
            f.startDate = [formatter stringFromDate:startDate];//[item valueForKey:@"str_start_date"];
        }
        if([item valueForKey:@"image_id"]){
            f.imageID = [NSString stringWithFormat:@"%@:ads", [item valueForKey:@"image_id"]];
        }
        f.important = [[item valueForKey:@"action"] boolValue];
        [list addObject:f];
    }
}

#pragma mark Chat


- (void) getEvent:(ChatRoomsBlock) handler {
    [ServiceCaller callJSON:[NSString stringWithFormat:@"%@msg-groups/get-active-event-room",
                                 kSERVER]
                         method:@"GET"
                         header:nil
                      parameter:@{}
                        handler:^(NSJSONSerialization *json) {
                            if(json){
                                NSMutableArray<ChatRoom*> *eventList = [[NSMutableArray<ChatRoom*> alloc] init];
                                [self doProcessEventGroup:eventList fromJSON:json];
                                if(handler) {
                                    handler(eventList, nil);
                                }
                            }else{
                                if(handler) {
                                    handler(nil, [[NSError alloc] initWithDomain:@"ADA" code:500 userInfo:nil]);
                                }
                            }
                        }];
}


- (void) doProcessEventGroup:(NSMutableArray*) roomsList fromJSON:(NSJSONSerialization*) json {
    if([[json valueForKey:@"result"] boolValue]){
        //[chatGroups removeAllObjects];
        NSArray *rooms = [json valueForKey:@"data"];
        
        NSInteger seq = 1;
        NSDateFormatter *parser = [NSDateFormatter instanceWithUSLocale];
        parser.dateFormat = @"yyyy-MM-dd HH:mm:ss.0";
        for(NSJSONSerialization *room in rooms){
            ChatRoom *r = [ChatRoom new];
            r.roomType = ChatRoomTypeEvent;
            r.seq = seq++;
            r.eventID = [[room valueForKey:@"id"] integerValue];
            r.roomName = [room valueForKey:@"event_name"];
            r.roomDecription = [room valueForKey:@"event_detail"];
            r.lastMessage = nil;
            r.startDate = [parser dateFromString:[room valueForKey:@"start_date"]];
            r.endDate = [parser dateFromString:[room valueForKey:@"end_date"]];
            r.eventOwnerName = [room valueForKey:@"name"];
            r.chatBot = [[room valueForKey:@"bot"] boolValue];
            if([room valueForKey:@"event_image"]){
                r.eventImageID = [NSString stringWithFormat:@"%@:chatEvent@%@", [room valueForKey:@"event_image"], [room valueForKey:@"event_image_orientation"]];
            }
            if([room valueForKey:@"profile_image"]){
                r.imageID = [NSString stringWithFormat:@"%@:chatEvent@%@", [room valueForKey:@"profile_image"], [room valueForKey:@"profile_image_orientation"]];
            }
            
            r.roomOwner = [[room valueForKey:@"create_by"] isEqualToString:self.login.usid];
            
            [roomsList addObject:r];
            
        }
    }
}


- (void) chatWithUser:(NSString*) usid group:(NSString*) group handler:(ChatRoomIDBlock) handler {
    [ServiceCaller callJSON:[kSERVER stringByAppendingString:@"msg-groups/get-private-room-members"]
                     method:@"GET"
                     header:nil parameter:@{
                                            @"member1": self.login.usid,
                                            @"group1": self.login.group,
                                            @"member2": usid,
                                            @"group2": group,
                                            }
                    handler:^(NSJSONSerialization *json) {
                        ////NSLog(@"json %@",json);
                        if (json) {
                            if (handler) {
                                handler([[json valueForKey:@"id"] stringValue]);
                            }
                        }else{
                            if (handler) {
                                handler(nil);
                            }
                        }
                    }];
}

- (void) getChatRoomWithID:(NSString* _Nonnull) roomID handler:(ChatRoomBlock _Nullable) handler {
    [ServiceCaller getJSONCache:[NSString stringWithFormat:@"%@msg-groups/get-related-rooms/%@/%@",
                                 kSERVER, self.login.group, self.login.usid]
                         method:@"GET"
                         header:nil
                      parameter:@{@"status":@"A"}
                        handler:^(NSJSONSerialization *json) {
                            
                            if(json){
                                ChatRoom *room = [self doProcessWith:roomID fromJSON:json];
                                if(room) {
                                    if(handler) {
                                        handler(room);
                                    }
                                    return;
                                }
                            }
                            [ServiceCaller callJSON:[NSString stringWithFormat:@"%@msg-groups/get-related-rooms/%@/%@",
                                                     kSERVER, self.login.group, self.login.usid]
                                             method:@"GET"
                                             header:nil
                                          parameter:@{@"status":@"A"}
                                            handler:^(NSJSONSerialization *json) {
                                                
                                                if(json){
                                                    
                                                    ChatRoom *room = [self doProcessWith:roomID fromJSON:json];
                                                    if(handler) {
                                                        handler(room);
                                                    }
                                                }
                                            }];
                        }];
}

- (void) getChatRoomWithUser:(NSString*) usid group:(NSString*) group handler:(ChatRoomBlock) handler {
    [self chatWithUser:usid group:group handler:^(NSString * _Nullable roomID) {
        [ServiceCaller getJSONCache:[NSString stringWithFormat:@"%@msg-groups/get-related-rooms/%@/%@",
                                     kSERVER, self.login.group, self.login.usid]
                             method:@"GET"
                             header:nil
                          parameter:@{@"status":@"A"}
                            handler:^(NSJSONSerialization *json) {
                                
                                if(json){
                                    ChatRoom *room = [self doProcessWith:roomID fromJSON:json];
                                    if(room) {
                                        if(handler) {
                                            handler(room);
                                        }
                                        return;
                                    }
                                }
                                [ServiceCaller callJSON:[NSString stringWithFormat:@"%@msg-groups/get-related-rooms/%@/%@",
                                                         kSERVER, self.login.group, self.login.usid]
                                                 method:@"GET"
                                                 header:nil
                                              parameter:@{@"status":@"A"}
                                                handler:^(NSJSONSerialization *json) {
                                                    
                                                    if(json){
                                                        
                                                        ChatRoom *room = [self doProcessWith:roomID fromJSON:json];
                                                        if(handler) {
                                                            handler(room);
                                                        }
                                                    }
                                                }];
                            }];
    }];
}

- (ChatRoom*) doProcessWith:(NSString*) roomID fromJSON:(NSJSONSerialization*) json {
    if([[json valueForKey:@"result"] boolValue]){
        
        NSArray *rooms = [json valueForKey:@"data"];
        
        NSInteger seq = 1;
        
        NSDateFormatter *parser = [NSDateFormatter instanceWithUSLocale];
        parser.dateFormat = @"yyyy-MM-dd HH:mm:ss.0";
        
        for (NSJSONSerialization *room in rooms) {
            NSString *roomNumber = [NSString stringWithFormat:@"%@", [room valueForKey:@"id"]];
            if(![roomNumber isEqualToString:roomID]) {
                continue;
            }
            NSString *member = [room valueForKey:@"members"];
            
            NSMutableSet *memberSet = [[NSMutableSet alloc] init];
            NSArray *members = [member componentsSeparatedByString:@","];
            for (NSString *m in members) {
                [memberSet addObject:[[m stringByAppendingString:@"@"] stringByAppendingString:self.login.chatServerDomain]];
            }
 
            ChatRoom *r = [ChatRoom new];
            

            r.memberOnline = NO;
            r.seq = seq++;
            r.roomID = [NSString stringWithFormat:@"%@", [room valueForKey:@"id"]];
            r.roomName = [room valueForKey:@"group_name"];
            r.roomDecription = [room valueForKey:@"group_desc"];
            r.targetUserID = [room valueForKey:@"usid"];
            r.targetUserGroup = [room valueForKey:@"user_group"];
            r.lastMessage = [room valueForKey:@"last_message"];
            r.members = memberSet;
            if([room valueForKey:@"group_picture"] && ![[room valueForKey:@"group_picture"] isEqualToString:@""]){
                r.imageID = [NSString stringWithFormat:@"%@:profilePicture", [room valueForKey:@"group_picture"]];
            }
            r.notification = [[room valueForKey:@"notify"] boolValue];
            r.joinRoom = [[room valueForKey:@"join_status"] boolValue];
            r.roomType = [[room valueForKey:@"group_type"] integerValue];
            r.roomOwner = [[room valueForKey:@"create_by"] isEqualToString:self.login.usid];
            if(r.roomType == ChatRoomTypePrivate) {
                ADAMember *member = [ADAMember new];
                
                member.userID = r.targetUserID;
                member.group = r.targetUserGroup;
                NSString *name = [self getContactNickname:member];
                if(name) {
                    r.roomName = name;
                }
            }
            
            
            if([room valueForKey:@"event"]) {
                NSJSONSerialization *roomJSON = [room valueForKey:@"event"];
                //NSLog(@"%@", roomJSON);
                r.eventOwner = [[roomJSON valueForKey:@"create_by"] isEqualToString:self.login.usid];
                r.eventID = [[roomJSON valueForKey:@"id"] integerValue];
                if(r.eventOwner) {
                    r.roomName = [NSString stringWithFormat:@"%@@%@", [roomJSON valueForKey:@"event_name"], [room valueForKey:@"member_name"]];
                }else{
                    r.roomName = [roomJSON valueForKey:@"event_name"];
                }
                r.roomDecription = [roomJSON valueForKey:@"event_detail"];
                r.chatBot = [[roomJSON valueForKey:@"bot"] boolValue];
                r.startDate = [parser dateFromString:[roomJSON valueForKey:@"start_date"]];
                r.endDate = [parser dateFromString:[roomJSON valueForKey:@"end_date"]];
                r.eventOwnerName = [roomJSON valueForKey:@"name"];
                r.eventActive = [[roomJSON valueForKey:@"event_status"] isEqualToString:@"A"];
                r.eventExpired = [[roomJSON valueForKey:@"event_status"] isEqualToString:@"E"];
                if([roomJSON valueForKey:@"event_image"] && ![[roomJSON valueForKey:@"event_image"] isEqualToString:@""]){
                    r.eventImageID = [NSString stringWithFormat:@"%@:chatEvent", [roomJSON valueForKey:@"event_image"]];
                }
                if([roomJSON valueForKey:@"profile_image"] && ![[roomJSON valueForKey:@"profile_image"] isEqualToString:@""]){
                    r.imageID = [NSString stringWithFormat:@"%@:chatEvent", [roomJSON valueForKey:@"profile_image"]];
                }
                
                
            }
            
            
            return r;
        }
    }
    return nil;
}
    
- (void) canSendRewardSticker:(SuccessBlock) handler {
    [ServiceCaller callJSON:[NSString stringWithFormat:@"%@msg-reward-point/get-sticker-by-group/%@",
                             kSERVER, self.login.group]
                     method:@"GET"
                     header:nil
                  parameter:nil
                    handler:^(NSJSONSerialization *json) {
                        if ([[json valueForKey:@"result"] boolValue]) {
                            NSArray *stickers = [json valueForKey:@"data"];
                            if(stickers.count > 0){
                                if(handler) {
                                    handler(YES);
                                    
                                }
                                return;
                            }
                            
                        }
                        if(handler) {
                            handler(NO);
                            
                        }
                        return;
                    
                    }];
     
    
}
    
 
- (void) canChatWithUser:(NSString*) usid group:(NSString*) group handler:(SuccessBlock) handler {
    
    if (![self isLoggedOn]) {
        if (handler) {
            handler(NO);
        }
        return;
    }
    
    [ServiceCaller callJSON:[NSString stringWithFormat:@"%@msg-member/get-by-userid/%@/%@",
                             kSERVER,
                             group,
                             usid
                             ]
                     method:@"GET"
                     header:nil
                  parameter:nil
                    handler:^(NSJSONSerialization *json) {
                        if (json && [[json valueForKey:@"result"] boolValue]) {
                            if (handler) {
                                handler(YES);
                            }
                            
                            
                            
                        }else{
                            if (handler) {
                                handler(NO);
                            }
                        }
                    }];
    
    
    
    
    
    
}

- (void) getADAMember:(NSString* _Nonnull) userID group:(NSString* _Nonnull) group handler:(ADAMemberBlock _Nullable) handler {
    
    [ServiceCaller getJSONCache:[NSString stringWithFormat:@"%@user/profile-new/%@/%@",
                                 kSERVER,
                                 group,
                                 userID]
                         method:@"GET"
                         header:nil
                      parameter:nil
                        handler:^(NSJSONSerialization *json) {
                            if(handler){
                                handler([self doProcessADAMember: json], nil);
                            }
                            [ServiceCaller callJSON:[NSString stringWithFormat:@"%@user/profile-new/%@/%@",
                                                     kSERVER,
                                                     group,
                                                     userID]
                                             method:@"GET"
                                             header:nil
                                          parameter:nil
                                            handler:^(NSJSONSerialization *json) {
                                                if(handler){
                                                    handler([self doProcessADAMember: json], nil);
                                                }
                                            }];
                            
                        }];
    
    
    
}

- (void) searchADAMember:(NSString*) keyword handler:(ContactPersonBlock) handler {
    [ServiceCaller callJSON:[kSERVER stringByAppendingString:@"msg-member/get-by-keyword"]
                     method:@"GET"
                     header:nil
                  parameter:@{
                              @"keyword":keyword,
                              @"includeAll": @"Y"
                              }
                    handler:^(NSJSONSerialization *json) {
                        
                        if (json && [[json valueForKey:@"result"] boolValue]) {
                            if (handler) {
                                
                                
                                
                                handler([self doProcessContact:json], nil);
                            }
                            
                        }else{
                            if (handler) {
                                handler(nil, [[NSError alloc] initWithDomain:@"ADA" code:500 userInfo:nil]);
                            }
                        }
                    }];
}

- (void) createGroupChatRoom:(ChatRoom*) room handler:(SuccessBlock) handler {
    
    
    if (!room.adaMembers || room.adaMembers.count == 0 || !room.roomName) {
        handler(NO);
        return;
    }
    
    NSMutableDictionary *data =  [NSMutableDictionary dictionaryWithDictionary:@{
                                                                                 @"group_name" : room.roomName,
                                                                                 @"group_desc" : room.roomDecription ? room.roomDecription : @""
                                                                                 }];
    if (room.image) { // has group image
        
        [self saveImage:room.image type:@"profilePicture" progress:nil handler:^(NSString *fileID, NSError *error) {
            if (!error && fileID) {
                [data setValue:fileID
                        forKey:@"group_picture"];
                [data setValue:[NSNumber numberWithInteger:room.image.imageOrientation]
                        forKey:@"group_picture_orientation"];
            }
            NSMutableArray *members = [[NSMutableArray alloc] init];
            for (ADAMember *member in room.adaMembers) {
                [members addObject:@{@"usid":member.userID, @"user_group" : member.group}];
            }
            [data setValue:members forKey:@"invitee_list"];
            
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:data
                                                               options:NSJSONWritingPrettyPrinted
                                                                 error:nil];
            [ServiceCaller callJSON:[NSString stringWithFormat:@"%@msg-groups/get-room-member/%@/%@",
                                     kSERVER, self.login.group, self.login.usid
                                     ]
                             method:@"POST"
                             header:nil
                          parameter:@{
                                      @"data": [[NSString alloc] initWithData:jsonData
                                                                     encoding:NSUTF8StringEncoding]
                                      }handler:^(NSJSONSerialization *json) {
                                          ////NSLog(@"%@", json);
                                          if (json && [[json valueForKey:@"result"] boolValue]) {
                                              if (handler) {
                                                  handler(YES);
                                              }
                                          }else{
                                              if (handler) {
                                                  handler(NO);
                                              }
                                          }
                                      }];
            
        }];
        
        
    }else{ // no group image
        NSMutableArray *members = [[NSMutableArray alloc] init];
        for (ADAMember *member in room.members) {
            [members addObject:@{@"usid":member.userID, @"user_group" : member.group}];
        }
        [data setValue:members forKey:@"invitee_list"];
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:data
                                                           options:NSJSONWritingPrettyPrinted
                                                             error:nil];
        
        
        [ServiceCaller callJSON:[NSString stringWithFormat:@"%@msg-groups/get-room-member/%@/%@",
                                 kSERVER, self.login.group, self.login.usid
                                 ]
                         method:@"POST"
                         header:nil
                      parameter:@{
                                  @"data": [[NSString alloc] initWithData:jsonData
                                                                 encoding:NSUTF8StringEncoding]
                                  }handler:^(NSJSONSerialization *json) {
                                      if (json && [[json valueForKey:@"result"] boolValue]) {
                                          if (handler) {
                                              handler(YES);
                                          }
                                      }else{
                                          if (handler) {
                                              handler(NO);
                                          }
                                      }
                                      
                                  }];
    }
    
}

- (void) createChatRoomWithUser:(NSString *)userID group:(NSString *)group handler:(SuccessBlock)handler {
    
    [ServiceCaller callJSON:[kSERVER stringByAppendingString:@"msg-groups/get-private-room-members"]
                     method:@"GET"
                     header:nil parameter:@{
                                            @"member1": self.login.usid,
                                            @"group1": self.login.group,
                                            @"member2": userID,
                                            @"group2": group,
                                            }
                    handler:^(NSJSONSerialization *json) {
                        ////NSLog(@"json %@",json);
                        if (json && [json valueForKey:@"group_id"]) {
                            if (handler) {
                                handler(YES);
                            }
                        }else{
                            if (handler) {
                                handler(NO);
                            }
                        }
                    }];
    
}

- (void) updateChatGroup:(ChatRoom* _Nonnull) room handler:(SuccessBlock _Nullable) handler {
    
    __block NSMutableDictionary<NSString*, id> *json = [[NSMutableDictionary alloc] init];
    if(room.roomName) {
        [json setObject:room.roomName forKey:@"group_name"];
    }
    if(room.roomDecription) {
        [json setObject:room.roomDecription forKey:@"group_desc"];
    }
    if(room.image) {
        
        [self saveImage:room.image type:@"profilePicture" progress:^(long progress, long total) {
            
        } handler:^(NSString * _Nullable fileID, NSError * _Nullable error) {
            if(error) {
                NSLog(@"save image fail : %@", error);
            }
            if(fileID) {
                room.imageID = [fileID stringByAppendingString:@":profilePicture"];
                [json setObject:fileID forKey:@"group_picture"];
                [json setObject:[NSNumber numberWithInt:room.image.imageOrientation]   forKey:@"group_picture_orientation"];
            }
            
            
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:json
                                                               options:kNilOptions
                                                                 error: nil];
            NSString *jsonString = [[NSString alloc] initWithData:jsonData
                                                         encoding:NSUTF8StringEncoding];
//            NSLog(@"update : %@", jsonString);
//            NSLog(@"url %@", [NSString stringWithFormat:@"%@msg-groups/update-room/%@",kSERVER, room.roomID]);
            [ServiceCaller callJSON:[NSString stringWithFormat:@"%@msg-groups/update-room/%@",kSERVER, room.roomID]
                             method:@"POST"
                             header:nil
                          parameter:@{
                                      @"data":jsonString
                                      }
                            handler:^(NSJSONSerialization *jsonWS) {
                                if (jsonWS && [[jsonWS valueForKey:@"result"] boolValue]) {
                                    if (handler) {
                                        handler(YES);
                                    }
                                }else{
                                    if (handler) {
                                        handler(NO);
                                    }
                                }
                            }];
        }];
    }else{
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:json
                                                           options:kNilOptions
                                                             error: nil];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData
                                                     encoding:NSUTF8StringEncoding];
        //NSLog(@"update : %@", jsonString);
        [ServiceCaller callJSON:[NSString stringWithFormat:@"%@msg-groups/update-room/%@",kSERVER, room.roomID]
                         method:@"POST"
                         header:nil
                      parameter:@{
                                  @"data":jsonString
                                  }
                        handler:^(NSJSONSerialization *jsonWS) {
                            
                            //NSLog(@"%@", jsonWS);
                            
                            if (jsonWS && [[jsonWS valueForKey:@"result"] boolValue]) {
                                if (handler) {
                                    handler(YES);
                                }
                            }else{
                                if (handler) {
                                    handler(NO);
                                }
                            }
                        }];
    }
}

- (void)removeChatRoom:(ChatRoom *)chatRoom handler:(SuccessBlock)handler {
    [ServiceCaller callJSON:[NSString stringWithFormat:@"%@msg-groups/remove-room",kSERVER]
                     method:@"POST"
                     header:nil
                  parameter:@{
                              @"roomID":chatRoom.roomID
                              }
                    handler:^(NSJSONSerialization *jsonWS) {
                        if (jsonWS && [[jsonWS valueForKey:@"result"] boolValue]) {
                            if (handler) {
                                handler(YES);
                            }
                        }else{
                            if (handler) {
                                handler(NO);
                            }
                        }
                    }];
    
}
- (void) inviteMember:(ChatRoom*) room member:(NSArray<ADAMember*>*) members handler:(SuccessBlock) handler {
    
    NSMutableArray<NSDictionary<NSString*, NSString*>*>* users = [[NSMutableArray alloc] init];
    for(ADAMember *m in members) {
        [users addObject:@{@"usid": m.userID, @"user_group": m.group}];
    }
    
    
    NSDictionary *data = @{
                           @"invitee_list": users
                           };
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:data
                                                       options:NSJSONWritingPrettyPrinted
                                                         error: nil];
    
    
    [ServiceCaller callJSON:[NSString
                             stringWithFormat:@"%@msg-groups/add-room-member/%@",
                             kSERVER,
                             room.roomID]
                     method:@"POST"
                     header:nil
                  parameter:@{
                              @"data": [[NSString alloc] initWithData:jsonData
                                                             encoding:NSUTF8StringEncoding]
                              } handler:^(NSJSONSerialization *json) {
                                  if ([[json valueForKey:@"result"] boolValue]) {
                                      if(handler){
                                          handler(YES);
                                      }
                                  }else{
                                      if(handler){
                                          handler(NO);
                                      }
                                  }
                              }];
}

- (void) declineChatInvitation:(ChatRoom*) room handler:(SuccessBlock) handler {
    
    [ServiceCaller callJSON:[NSString
                             stringWithFormat:@"%@msg-groups/response-invite-in-room/%@/%@/%@",
                             kSERVER,
                             room.roomID,
                             self.login.group,
                             self.login.usid]
                     method:@"POST"
                     header:nil
                  parameter:@{@"action":@"0"}
                    handler:^(NSJSONSerialization *json) {
                        if (json && [[json valueForKey:@"result"] boolValue]) {
                            if (handler) {
                                handler(YES);
                            }
                        }else{
                            if (handler) {
                                handler(NO);
                            }
                        }
                    }];
    
}

- (void) acceptChatInvitation:(ChatRoom*) room handler:(SuccessBlock) handler {
    [ServiceCaller callJSON:[NSString
                             stringWithFormat:@"%@msg-groups/response-invite-in-room/%@/%@/%@",
                             kSERVER,
                             room.roomID,
                             self.login.group,
                             self.login.usid]
                     method:@"POST"
                     header:nil
                  parameter:@{@"action":@"1"}
                    handler:^(NSJSONSerialization *json) {
                        if (json && [[json valueForKey:@"result"] boolValue]) {
                            if (handler) {
                                handler(YES);
                            }
                        }else{
                            if (handler) {
                                handler(NO);
                            }
                        }
                    }];
}

- (void) leaveChatGroup:(ChatRoom*) room handler:(SuccessBlock) handler {
    
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:@{@"`leave`":[NSNumber numberWithBool:true]}
                                                       options:kNilOptions
                                                         error: nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData
                                                 encoding:NSUTF8StringEncoding];
    
    [ServiceCaller callJSON:[NSString stringWithFormat:@"%@ห้",
                             kSERVER,
                             room.roomID,
                             self.login.group,
                             self.login.usid]
                     method:@"POST"
                     header:nil
                  parameter:@{
                              @"data": jsonString
                              }
                    handler:^(NSJSONSerialization *json) {
                        if (json && [[json valueForKey:@"result"] boolValue]) {
                            if (handler) {
                                handler(YES);
                            }
                        }else{
                            if (handler) {
                                handler(NO);
                            }
                        }
                        
                    }];
}

- (void) turnChatNotification:(ChatRoom*) room turn:(BOOL) on handler:(SuccessBlock) handler {
    
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:@{@"notify":[NSNumber numberWithBool:on]}
                                                       options:kNilOptions
                                                         error: nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData
                                                 encoding:NSUTF8StringEncoding];
    
    [ServiceCaller callJSON:[NSString stringWithFormat:@"%@msg-group-member/update-by-userid/%@/%@/%@",
                             kSERVER,
                             room.roomID,
                             self.login.group,
                             self.login.usid]
                     method:@"POST"
                     header:nil
                  parameter:@{
                              @"data": jsonString
                              }
                    handler:^(NSJSONSerialization *json) {
                        if (json && [[json valueForKey:@"result"] boolValue]) {
                            if (handler) {
                                handler(YES);
                            }
                        }else{
                            if (handler) {
                                handler(NO);
                            }
                        }
                        
                    }];
}

- (void) createEventRoom:(ChatRoom* _Nonnull) room handler:(SuccessBlock _Nullable) handler {
    
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setValue:room.roomName forKey:@"event_name"];
    if(room.roomDecription) {
        [dic setValue:room.roomDecription forKey:@"event_detail"];
    }
    if(room.eventOwnerName) {
        [dic setValue:room.eventOwnerName forKey:@"name"];
    }
    if(room.imageID) {
        [dic setValue:room.imageID forKey:@"profile_image"];
    }
    if(room.eventImageID) {
        [dic setValue:room.eventImageID forKey:@"event_image"];
    }
    [dic setValue:self.login.usid forKey:@"create_by"];
    [dic setValue:self.login.group forKey:@"create_group"];
    NSDateFormatter *formatter = [NSDateFormatter instanceWithUSLocale];
    formatter.dateFormat = @"yyyy-MM-dd HH:mm";
    if(room.startDate) {
        [dic setValue:[formatter stringFromDate:room.startDate] forKey:@"start_date"];
    }
    if(room.endDate) {
        [dic setValue:[formatter stringFromDate:room.endDate] forKey:@"end_date"];
    }
    
    //NSLog(@"%@", room.image);
    //NSLog(@"%@", room.eventImage);
    
    
    
    if (room.image && room.eventImage) { // has group image
        
        [self saveImage:room.image type:@"chatEvent" progress:nil handler:^(NSString *fileID, NSError *error) {
            if (!error && fileID) {
                //NSLog(@"fileID : %@", fileID);
                
                [dic setValue:fileID forKey:@"profile_image"];
                [dic setValue:[NSNumber numberWithInteger:room.image.imageOrientation] forKey:@"profile_image_orientation"];
            }
            
            
            [self saveImage:room.eventImage type:@"chatEvent" progress:nil handler:^(NSString *fileID2, NSError *error) {
                if (!error && fileID2) {
                    //NSLog(@"fileID2 : %@", fileID2);
                    [dic setValue:fileID2 forKey:@"event_image"];
                    [dic setValue:[NSNumber numberWithInteger:room.eventImage.imageOrientation] forKey:@"event_image_orientation"];
                }
                [self createEvent:dic handler:handler];
            }];
            
        }];
    }else if(room.image){
        [self saveImage:room.image type:@"chatEvent" progress:nil handler:^(NSString *fileID, NSError *error) {
            if (!error && fileID) {
                [dic setValue:fileID forKey:@"profile_image"];
                [dic setValue:[NSNumber numberWithInteger:room.image.imageOrientation] forKey:@"profile_image_orientation"];
            }
            
            
            [self createEvent:dic handler:handler];
            
        }];
    }else if(room.eventImage){
        [self saveImage:room.eventImage type:@"chatEvent" progress:nil handler:^(NSString *fileID, NSError *error) {
            if (!error && fileID) {
                [dic setValue:fileID forKey:@"event_image"];
                [dic setValue:[NSNumber numberWithInteger:room.eventImage.imageOrientation] forKey:@"event_image_orientation"];
            }
            [self createEvent:dic handler:handler];
        }];
    }else{
        [self createEvent:dic handler:handler];
    }
}

- (void) createEvent:(NSDictionary *)dic handler:(SuccessBlock _Nullable) handler {
    //NSLog(@"%@", dic);
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:nil];
    [ServiceCaller callJSON:[NSString stringWithFormat:@"%@msg-groups/create-event-room",
                             kSERVER
                             ]
                     method:@"POST"
                     header:nil
                  parameter:@{
                              @"data": [[NSString alloc] initWithData:jsonData
                                                             encoding:NSUTF8StringEncoding]
                              }handler:^(NSJSONSerialization *json) {
                                  if (json && [[json valueForKey:@"result"] boolValue]) {
                                      if (handler) {
                                          handler(YES);
                                      }
                                  }else{
                                      if (handler) {
                                          handler(NO);
                                      }
                                  }
                              }];
}

- (void) cancelEventRoom:(ChatRoom* _Nonnull) room handler:(SuccessBlock _Nullable) handler {
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:@{@"event_status": @"C"}
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData
                                                 encoding:NSUTF8StringEncoding];

    [ServiceCaller callJSON:[NSString stringWithFormat:@"%@msg-groups/update-event-room/%ld",
                             kSERVER, (long)room.eventID]
                     method:@"POST"
                     header:nil
                  parameter: @{
                               @"data": jsonString
                               }
                    handler:^(NSJSONSerialization *json) {
                        //NSLog(@"%@", json);
                        if (json && [[json valueForKey:@"result"] boolValue]) {
                            if (handler) {
                                handler(YES);
                            }
                        }else{
                            if (handler) {
                                handler(NO);
                            }
                        }
                    }];
}

- (void) updateEventRoom:(ChatRoom* _Nonnull) room handler:(SuccessBlock _Nullable) handler {
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setValue:room.roomName forKey:@"event_name"];
    if(room.roomDecription) {
        [dic setValue:room.roomDecription forKey:@"event_detail"];
    }
    if(room.eventOwnerName) {
        [dic setValue:room.eventOwnerName forKey:@"name"];
    }
    /*if(room.imageID) {
        [dic setValue:room.imageID forKey:@"profile_image"];
    }
    if(room.eventImageID) {
        [dic setValue:room.eventImageID forKey:@"event_image"];
    }
     */
    [dic setValue:self.login.usid forKey:@"create_by"];
    [dic setValue:self.login.group forKey:@"create_group"];
    NSDateFormatter *formatter = [NSDateFormatter instanceWithUSLocale];
    formatter.dateFormat = @"yyyy-MM-dd HH:mm";
    if(room.startDate) {
        [dic setValue:[formatter stringFromDate:room.startDate] forKey:@"start_date"];
    }
    if(room.endDate) {
        [dic setValue:[formatter stringFromDate:room.endDate] forKey:@"end_date"];
    }
    
    //NSLog(@"%@", room.image);
    //NSLog(@"%@", room.eventImage);
    
    
    
    if (room.image && room.eventImage) { // has group image
        if(room.image != room.oImage) {
            [self saveImage:room.image type:@"chatEvent" progress:nil handler:^(NSString *fileID, NSError *error) {
                if (!error && fileID) {
                    //NSLog(@"fileID: %@", fileID);
                    [dic setValue:fileID forKey:@"profile_image"];
                    [dic setValue:[NSNumber numberWithInteger:room.image.imageOrientation] forKey:@"profile_image_orientation"];
                }
                
                if (room.eventImage) { // has group image
                    if(room.eventImage != room.oEventImage) {
                        [self saveImage:room.eventImage type:@"chatEvent" progress:nil handler:^(NSString *fileID2, NSError *error) {
                            if (!error && fileID2) {
                                //NSLog(@"fileID2: %@", fileID2);
                                [dic setValue:fileID2 forKey:@"event_image"];
                                [dic setValue:[NSNumber numberWithInteger:room.eventImage.imageOrientation] forKey:@"event_image_orientation"];
                            }
                            [self updateEvent:room.eventID :dic handler:handler];
                        }];
                    }else{
                        [self updateEvent:room.eventID :dic handler:handler];
                    }
                }else{
                    [self updateEvent:room.eventID :dic handler:handler];
                }
            }];
        }else{
            if (room.eventImage) { // has group image
                if(room.eventImage != room.oEventImage) {
                    [self saveImage:room.eventImage type:@"chatEvent" progress:nil handler:^(NSString *fileID2, NSError *error) {
                        if (!error && fileID2) {
                            //NSLog(@"fileID2: %@", fileID2);
                            [dic setValue:fileID2 forKey:@"event_image"];
                            [dic setValue:[NSNumber numberWithInteger:room.eventImage.imageOrientation] forKey:@"event_image_orientation"];
                        }
                        [self updateEvent:room.eventID :dic handler:handler];
                    }];
                }else{
                    [self updateEvent:room.eventID :dic handler:handler];
                }
            }else{
                [self updateEvent:room.eventID :dic handler:handler];
            }
        }
    }else if(room.image){
        if(room.image != room.oImage) {
            [self saveImage:room.image type:@"chatEvent" progress:nil handler:^(NSString *fileID, NSError *error) {
                if (!error && fileID) {
                    [dic setValue:fileID forKey:@"profile_image"];
                    [dic setValue:[NSNumber numberWithInteger:room.image.imageOrientation] forKey:@"profile_image_orientation"];
                }
                [self updateEvent:room.eventID :dic handler:handler];
            }];
        }else{
            [self updateEvent:room.eventID :dic handler:handler];
        }
    }else if(room.eventImage){
        if(room.eventImage != room.oEventImage) {
            [self saveImage:room.eventImage type:@"chatEvent" progress:nil handler:^(NSString *fileID, NSError *error) {
                if (!error && fileID) {
                    [dic setValue:fileID forKey:@"event_image"];
                    [dic setValue:[NSNumber numberWithInteger:room.eventImage.imageOrientation] forKey:@"event_image_orientation"];
                }
                [self updateEvent:room.eventID :dic handler:handler];
            }];
        }else{
            [self updateEvent:room.eventID :dic handler:handler];
        }
    }else{
        [self updateEvent:room.eventID :dic handler:handler];
    }
}

- (void) updateEvent:(NSInteger) eventID :(NSDictionary *)dic handler:(SuccessBlock _Nullable) handler {
    ////NSLog(@"%@", dic);
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData
                                                 encoding:NSUTF8StringEncoding];

    [ServiceCaller callJSON:[NSString stringWithFormat:@"%@msg-groups/update-event-room/%ld",
                             kSERVER, (long)eventID]
                     method:@"POST"
                     header:nil
                  parameter: @{
                              @"data": jsonString
                              }
                    handler:^(NSJSONSerialization *json) {
                        //NSLog(@"%@", json);
                                  if (json && [[json valueForKey:@"result"] boolValue]) {
                                      if (handler) {
                                          handler(YES);
                                      }
                                  }else{
                                      if (handler) {
                                          handler(NO);
                                      }
                                  }
                              }];
}

- (void) joinEvent:(ChatRoom *)room handler:(ChatRoomIDBlock _Nullable) handler {
    
    
    
    
    [ServiceCaller callJSON:[NSString stringWithFormat:@"%@msg-groups/join-event-room/%ld",
                             kSERVER, (long)room.eventID
                             ]
                     method:@"POST"
                     header:nil
                  parameter:@{
                              @"userID": self.login.usid,
                              @"groupID": self.login.group
                              }
                    handler:^(NSJSONSerialization *json) {
                        //NSLog(@"%@", json);
                        if (json && [[json valueForKey:@"result"] boolValue]) {
                            if (handler) {
                                handler([NSString stringWithFormat:@"%@", [[json valueForKey:@"data"] valueForKey:@"room_id"]]);
                            }
                        }else{
                            if (handler) {
                                handler(nil);
                            }
                        }
                    }];
}

- (void) getEventRoom:(ChatRoom *)room handler:(ChatRoomBlock _Nullable) handler {
    
    
    [self joinEvent:room handler:^(NSString * _Nullable roomID) {
        if(roomID) {
            [ServiceCaller getJSONCache:[NSString stringWithFormat:@"%@msg-groups/get-related-rooms/%@/%@",
                                         kSERVER, self.login.group, self.login.usid]
                                 method:@"GET"
                                 header:nil
                              parameter:@{@"status":@"A"}
                                handler:^(NSJSONSerialization *json) {
                                    if(json){
                                        
                                        ChatRoom *room = [self doProcessWith:roomID fromJSON:json];
                                        if(room) {
                                            if(handler) {
                                                handler(room);
                                            }
                                            return;
                                        }
                                    }
                                    [ServiceCaller callJSON:[NSString stringWithFormat:@"%@msg-groups/get-related-rooms/%@/%@",
                                                             kSERVER, self.login.group, self.login.usid]
                                                     method:@"GET"
                                                     header:nil
                                                  parameter:@{@"status":@"A"}
                                                    handler:^(NSJSONSerialization *json) {
                                                        if(json){
                                                            
                                                            ChatRoom *room = [self doProcessWith:roomID fromJSON:json];
                                                            if(handler) {
                                                                handler(room);
                                                            }
                                                        }
                                                    }];
                                }];
        }
    }];
    
}

- (void) fetchEventInfo:(ChatRoom *)room handler:(SuccessBlock _Nullable) handler {
    
    
    [ServiceCaller callJSON:[NSString stringWithFormat:@"%@msg-groups/get-event-info/%@",
                             kSERVER, room.roomID
                             ]
                     method:@"GET"
                     header:nil
                  parameter:@{@"roomID": room.roomID}
                    handler:^(NSJSONSerialization *json) {
                        
                        if (json && [[json valueForKey:@"result"] boolValue]) {
                            
                            NSJSONSerialization *roomJSON = [json valueForKey:@"data"];
                            NSDateFormatter *parser = [NSDateFormatter instanceWithUSLocale];
                            parser.dateFormat = @"yyyy-MM-dd HH:mm:ss.0";
                            
                            room.eventID = [[roomJSON valueForKey:@"id"] integerValue];
                            room.roomName = [roomJSON valueForKey:@"event_name"];
                            room.roomDecription = [roomJSON valueForKey:@"event_detail"];
                            
                            room.startDate = [parser dateFromString:[roomJSON valueForKey:@"start_date"]];
                            room.endDate = [parser dateFromString:[roomJSON valueForKey:@"end_date"]];
                            room.eventOwnerName = [roomJSON valueForKey:@"name"];
                            
                            if([roomJSON valueForKey:@"event_image"]){
                                room.eventImageID = [NSString stringWithFormat:@"%@:chatEvent", [roomJSON valueForKey:@"event_image"]];
                            }
                            if([roomJSON valueForKey:@"profile_image"]){
                                room.imageID = [NSString stringWithFormat:@"%@:chatEvent", [roomJSON valueForKey:@"profile_image"]];
                            }
                            
                            room.roomOwner = [[roomJSON valueForKey:@"create_by"] isEqualToString:self.login.usid];
                            
                            
                            if (handler) {
                                handler(YES);
                            }
                        }else{
                            if (handler) {
                                handler(NO);
                            }
                        }
                    }];
}


- (void) getEventMember:(ChatRoom*) room handler:(ADAMembersBlock) handler {
    [ServiceCaller callJSON:[NSString stringWithFormat:@"%@msg-groups/event-member/%ld", kSERVER, (long)room.eventID]
                     method:@"GET"
                     header:nil
                  parameter:@{
                              
                              }
                    handler:^(NSJSONSerialization *json) {
                        
                        if (json && [[json valueForKey:@"result"] boolValue]) {
                            if (handler) {
                                handler([self doProcessADAMembers:[json valueForKey:@"data"]], nil);
                            }
                            
                        }else{
                            if (handler) {
                                handler(nil, [[NSError alloc] initWithDomain:@"ADA" code:500 userInfo:nil]);
                            }
                        }
                    }];
}



- (void) sendMessageTextToAllMembers:(ChatRoom*) room message:(ChatMessage*) message handler:(SuccessBlock) handler {
    
    [self sendPlainChatMessage:message handler:^(NSString * _Nullable value) {
        
        [ServiceCaller callJSON:[NSString stringWithFormat:@"%@msg-groups/event-room-broadcast/%ld", kSERVER, (long)room.eventID]
                         method:@"POST"
                         header:nil
                      parameter:@{
                                  @"udid": [UIDevice currentDevice].identifierForVendor.UUIDString,
                                  @"memberID" : [NSNumber numberWithInteger: self.login.memberID],
                                  @"message": value
                                  }
                        handler:^(NSJSONSerialization *json) {
                            
                            if (json && [[json valueForKey:@"result"] boolValue]) {
                                if (handler) {
                                    handler(YES);
                                }
                                
                            }else{
                                if (handler) {
                                    handler(NO);
                                }
                            }
                        }];
    }];

    
}

- (void) sendMessageImageToAllMembers:(ChatRoom*) room message:(ImageMessage*) message handler:(SuccessBlock) handler {
    
    CGSize imgSize = message.image.size;
    
    
    CGFloat ratio = imgSize.width/imgSize.height;
    
    int width = MIN(1024, MAX(1024, imgSize.width));//self.view.frame.size.width * 3;
    int height = width * ratio;
    
    UIImage *image = [message.image scaleToSize:CGSizeMake(width, height)];
    ////NSLog(@"image or %ld", (long)image.imageOrientation);
    message.image = image;
    
    [self sendImage:message handler:^(NSString * _Nullable value) {
        
        ChatMessage *m = [ChatMessage new];
        m.message = value;
        [self sendMessageTextToAllMembers:room message:m handler:handler];

    }];
}
- (void) sendMessageVideoToAllMembers:(ChatRoom*) room message:(VideoMessage*) message handler:(SuccessBlock) handler {
    [self sendVideo:message handler:^(NSString * _Nullable value) {
        ChatMessage *m = [ChatMessage new];
        m.message = value;
        [self sendMessageTextToAllMembers:room message:m handler:handler];
    }];
}
- (void) sendMessageFileToAllMembers:(ChatRoom*) room message:(FileMessage*) message handler:(SuccessBlock) handler {
    [self sendFile:message handler:^(NSString * _Nullable value) {
        ChatMessage *m = [ChatMessage new];
        m.message = value;
        [self sendMessageTextToAllMembers:room message:m handler:handler];
    }];
}
- (void) sendMessageLocationToAllMembers:(ChatRoom*) room message:(LocationMessage*) message handler:(SuccessBlock) handler {
    [self sendLocation:message handler:^(NSString * _Nullable value) {
        ChatMessage *m = [ChatMessage new];
        m.message = value;
        [self sendMessageTextToAllMembers:room message:m handler:handler];
    }];
}
- (void) sendMessageStickerToAllMembers:(ChatRoom*) room message:(StickerMessage*) message handler:(SuccessBlock) handler {
    [self sendSticker:message handler:^(NSString * _Nullable value) {
        ChatMessage *m = [ChatMessage new];
        m.message = value;
        [self sendMessageTextToAllMembers:room message:m handler:handler];
    }];
}


- (void) sendImage:(ImageMessage*) message handler:(StringBlock) handler {
    
    
    NSData *imageData = UIImageJPEGRepresentation(message.image, 1.0);//UIImagePNGRepresentation(image);
    
    
    // 1. Create `AFHTTPRequestSerializer` which will create your request.
    AFHTTPRequestSerializer *serializer = [AFHTTPRequestSerializer serializer];
    
    // 2. Create an `NSMutableURLRequest`.
    NSString *strURL = [kSERVER_HOST stringByAppendingString:@"upload-file?uploadType=form"];
    
    
    //NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.requestSerializer = serializer;
    manager.responseSerializer.acceptableContentTypes = nil;
    //[manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    manager.requestSerializer = serializer;
    [manager.requestSerializer setValue:[AppHelper getUserDataForKey:kADA_CLIENT] forHTTPHeaderField:kADA_CLIENT];
    [manager.requestSerializer setValue:@"multipart/form-data" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    
    [manager POST:strURL
       parameters:@{}
constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
    
    
    
    [formData appendPartWithFileData:imageData name:@"file" fileName:[@"image" stringByAppendingString:message.image.extension] mimeType:message.image.contentType];
}
         progress:^(NSProgress * _Nonnull uploadProgress) {
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 //long writen = (long)((double)imageData.length * uploadProgress.fractionCompleted);
                 //long total = (long)imageData.length;
                 
             });
         } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
             ////NSLog(@"Success: %@", responseObject);
             [AppHelper logData:responseObject];
             NSJSONSerialization *json = [AppHelper jsonWithData:responseObject];
             
             NSString *jsonSend = [NSString stringWithFormat:@"{\"messageImageID\":\"%@\", \"messageImageOr\":%li, \"width\":%f, \"height\":%f, \"type\":\"%@\", \"src\":\"%@\"}",
                                   [json valueForKey:@"id"],
                                   (long)message.image.imageOrientation,
                                   message.image.size.width,
                                   message.image.size.height,
                                   [json valueForKey:@"type"],
                                   message.sourceCamera ? @"camera" : @""
                                   ];
             /*
             NSMutableDictionary *msg = [NSMutableDictionary dictionaryWithDictionary: @{
                                                                                         kMessage: jsonSend,
                                                                                         kSender: self.login.usid
                                                                                         }];
             
             
             NSData *jsonData = [NSJSONSerialization dataWithJSONObject:msg
                                                                options:NSJSONWritingPrettyPrinted
                                                                  error: nil];
             NSString *jsonString = [[NSString alloc] initWithData:jsonData
                                                          encoding:NSUTF8StringEncoding];
             */
             if(handler) {
                 handler(jsonSend);
             }
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
             //NSLog(@"Failure %@", error.description);
             if(handler) {
                 handler(nil);
             }
         }];
}

- (void) sendVideo:(VideoMessage*) message handler:(StringBlock) handler {
    //self->progressHandler = progress;
    //(long)unsigned long long fileSize = [[[NSFileManager defaultManager] attributesOfItemAtPath:message.filePath error:nil] fileSize];
    //NSLog(@"export path %@", message.filePath);
    //NSLog(@"export size %llu KB", fileSize / 1024);
    
    NSString *mp4Path = [message.filePath stringByReplacingOccurrencesOfString:@"file://" withString:@""];
    NSData *imageData = [[NSFileManager defaultManager] contentsAtPath:mp4Path];//UIImagePNGRepresentation(image);
    
    
    
    
    // 1. Create `AFHTTPRequestSerializer` which will create your request.
    AFHTTPRequestSerializer *serializer = [AFHTTPRequestSerializer serializer];
    
    // 2. Create an `NSMutableURLRequest`.
    NSString *strURL = [kSERVER_HOST stringByAppendingString:@"upload-file?uploadType=form&mediaType=video"];
    strURL = [NSString stringWithFormat:@"%@&angle=%ld", strURL, (long)message.angle];
    
    
    //NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = nil;
    //[manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    manager.requestSerializer = serializer;
    [manager.requestSerializer setValue:[AppHelper getUserDataForKey:kADA_CLIENT] forHTTPHeaderField:kADA_CLIENT];
    
    [manager POST:strURL
       parameters:@{}
constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
    [formData appendPartWithFileData:imageData name:@"file" fileName:message.fileName mimeType:@"video/mp4"];
}
         progress:^(NSProgress * _Nonnull uploadProgress) {
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 /*long writen = (long)((double)imageData.length * uploadProgress.fractionCompleted);
                 long total = (long)imageData.length;
                 
                 if(progress){
                     progress(writen, total);
                 }
                 */
             });
         } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
             ////NSLog(@"Success: %@", responseObject);
             
             NSJSONSerialization *json = [AppHelper jsonWithData:responseObject];
             
             NSString *jsonSend = [NSString stringWithFormat:@"{\"videoID\":\"%@\", \"type\":\"%@\"}",
                                   [json valueForKey:@"id"],
                                   [json valueForKey:@"type"]
                                   ];
             

             //[self sendStreamMessage: jsonString];
             if(handler){
                 handler(jsonSend);
             }
             [[NSFileManager defaultManager] removeItemAtPath:mp4Path error:nil];
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
             //NSLog(@"Failure %@", error.description);
             if(handler){
                 handler(nil);
             }
         }];
    
}

- (void) sendFile:(FileMessage*) message handler:(StringBlock) handler {
    //self->progressHandler = progress;
    
    unsigned long long fileSize = [[[NSFileManager defaultManager] attributesOfItemAtPath:message.filePath error:nil] fileSize];
    
    NSString *filePath = message.filePath;
    NSData *fileData = [[NSFileManager defaultManager] contentsAtPath:filePath];
    fileSize = fileData.length;
    if (fileSize == 0) {
        [AppHelper showErrorMessage:@"Cannot send this file" completion:nil];
        return;
    }
    
    NSArray *fileNames = [filePath componentsSeparatedByString:@"/"];
    NSString *fileName = fileNames.lastObject;
    NSString *fileType = [self mimeTypeForFileAtPath:filePath];
    
    
    // 1. Create `AFHTTPRequestSerializer` which will create your request.
    AFHTTPRequestSerializer *serializer = [AFHTTPRequestSerializer serializer];
    
    // 2. Create an `NSMutableURLRequest`.
    NSString *strURL = [kSERVER_HOST stringByAppendingString:@"upload-file?uploadType=form&mediaType=document"];
    strURL = [NSString stringWithFormat:@"%@", strURL];
    
    
    //NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = nil;
    //[manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    manager.requestSerializer = serializer;
    [manager.requestSerializer setValue:[AppHelper getUserDataForKey:kADA_CLIENT] forHTTPHeaderField:kADA_CLIENT];
    
    [manager POST:strURL
       parameters:@{}
constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
    [formData appendPartWithFileData:fileData name:@"file" fileName:fileName mimeType: fileType];
}
         progress:^(NSProgress * _Nonnull uploadProgress) {
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 /*long writen = (long)((double)fileData.length * uploadProgress.fractionCompleted);
                 long total = (long)fileData.length;
                 
                 if(progress){
                     progress(writen, total);
                 }
                 */
             });
         } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
             ////NSLog(@"Success: %@", responseObject);
             
             NSJSONSerialization *json = [AppHelper jsonWithData:responseObject];
             
             NSString *jsonSend = [NSString stringWithFormat:@"{\"fileID\":\"%@\", \"name\": \"%@\", \"type\":\"%@\", \"size\": \"%@\"}",
                                   [json valueForKey:@"id"],
                                   fileName,
                                   [json valueForKey:@"type"],
                                   [json valueForKey:@"size"]
                                   ];
             
             if(handler){
                 handler(jsonSend);
             }
             [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
             //NSLog(@"Failure %@", error.description);
             if(handler){
                 handler(nil);
             }
             
         }];
    
}

- (void) sendSticker:(StickerMessage*) message handler:(StringBlock) handler {
    
    
    NSMutableDictionary *msg = nil;
    if (message.message || message.point) {
        msg = [NSMutableDictionary dictionaryWithDictionary: @{
                                                               kMessage: [NSString stringWithFormat:@"$reward:%@;%@;%@", message.stickerID, message.point, message.message],
                                                               kSender: self.login.usid
                                                               }];
    }else{
        
        
        msg = [NSMutableDictionary dictionaryWithDictionary: @{
                                                               kMessage: message.stickerID,
                                                               kSender: self.login.usid
                                                               }];
    }
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:msg
                                                       options:NSJSONWritingPrettyPrinted
                                                         error: nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData
                                                 encoding:NSUTF8StringEncoding];
    
    
    //[self sendStreamMessage: jsonString];
    if(handler){
        handler(jsonString);
    }
}


- (void) sendLocation:(LocationMessage*) message handler:(StringBlock) handler {
    
    
    NSString *jsonSend = [NSString stringWithFormat:@"{\"locationDesc\":\"%@\",\"locationName\":\"%@\",\"latitude\":%f,\"longitude\":%f}",
                          [message.address stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""],
                          [message.placeName stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""],
                          message.latitude,
                          message.longitude
                          ];
    
    
    
    
    if(handler){
        handler(jsonSend);
    }
    
    
}



- (void) sendPlainChatMessage:(ChatMessage*) message handler:(StringBlock) handler {
    
    NSString *usid = self.login.usid;
    NSString *userGroup = self.login.group;
    NSString *userName = self.login.userName;
    NSString *imageID = self.login.imageID;
    NSDateFormatter *dfMsgID = [NSDateFormatter instanceWithUSLocale];
    [dfMsgID setDateFormat:@"yyyyMMddHHmmssSSS"];
    
    
    NSDateFormatter *df = [NSDateFormatter instanceWithUSLocale];
    [df setDateFormat:@"dd-MM-yyyy"];
    NSDateFormatter *df3 = [NSDateFormatter instanceWithUSLocale];
    [df3 setDateFormat:@"HH:mm"];
    
    NSString *msgID = [NSString stringWithFormat:@"%@-%@%@",
                       usid,
                       userGroup,
                       [dfMsgID stringFromDate:[NSDate date]]];
    
    NSDictionary *msg = nil;
    //BOOL sendReadStatus = NO;
    
    if(imageID){
        
        msg = @{
                kMessageID: msgID,
                kMessage: message.message,
                kSender: usid,
                kSenderName: userName,
                kGroupID: userGroup,
                kImageID: imageID,
                kTime: [df3 stringFromDate:[NSDate date]],
                kDate: [df stringFromDate:[NSDate date]],
                kBroadCast: [NSNumber numberWithBool:YES]
                };
    }else{
        msg = @{
                kMessageID: msgID,
                kMessage: message.message,
                kSender: usid,
                kSenderName: userName,
                kGroupID: userGroup,
                kTime: [df3 stringFromDate:[NSDate date]],
                kDate: [df stringFromDate:[NSDate date]],
                kBroadCast: [NSNumber numberWithBool:YES]
                };
    }
    
    
    
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:msg
                                                       options:NSJSONWritingPrettyPrinted
                                                         error: nil];
    
    NSString *jsonString = [[NSString alloc] initWithData:jsonData
                                                 encoding:NSUTF8StringEncoding];
    if(handler){
        handler(jsonString);
    }
}


- (void) getNearbyLocation:(CLLocationCoordinate2D) location handler:(NearbyLocationBlock _Nullable) handler {
    
    
        [ADA getConfigurationWithCode:@"ismart" handler:^(NSJSONSerialization * _Nullable json) {
            NSLog(@"json: %@", json);
            NSString *url = [[json valueForKey:@"additional"] valueForKey:@"map_nearby_url"];
            NSLog(@"url: %@", url);
            [ServiceCaller callJSON:url
                             method:@"GET"
                             header:@{@"enc":@"false"}
                          parameter:@{
                                      @"input":@"",
                                      @"lat":[NSString stringWithFormat:@"%f", location.latitude],
                                      @"lon":[NSString stringWithFormat:@"%f", location.longitude],
                                      @"serviceType": @"False",
                                      @"rowNum":@"30",
                                      @"radius":@"5.0",
                                      @"sort":@"th-asc"
                                      }
                            handler:^(NSJSONSerialization *json) {
                                NSMutableArray<LocationMessage*> *list = [[NSMutableArray alloc] init];
                                if(json) {
                                    NSArray *docs = [[json valueForKey:@"response"] valueForKey:@"docs"];
                                    for(NSJSONSerialization *jsonLoc in docs) {
                                        LocationMessage *message = [LocationMessage new];
                                        
                                        NSArray *latLon = [[jsonLoc valueForKey:@"latlon"] componentsSeparatedByString:@","];
                                        message.placeName = [jsonLoc valueForKey:@"displayth"];
                                        message.address = [jsonLoc valueForKey:@"location_t"];
                                        message.latitude = [latLon.firstObject doubleValue];
                                        message.longitude = [latLon.lastObject doubleValue];
                                        
                                        
                                        NSNumber *distance = [AppHelper distFromLat:[latLon.firstObject floatValue]
                                                                       FromLng:[latLon.lastObject floatValue]
                                                                         ToLat:location.latitude
                                                                         ToLng:location.longitude];
                                        
                                        message.distance = [NSString stringWithFormat:@"%.2f %@",
                                                            distance.floatValue / 1000, [AppHelper getLabelForThai:@"กม." eng:@"Km."]];
                                        
                                        [list addObject:message];
                                    }
                                }
                                if(handler) {
                                    handler(list, nil);
                                }
                                
                            }];
        }];
    
    
}

- (void) searchLocation:(NSString* _Nullable) keyword handler:(NearbyLocationBlock _Nullable) handler {
    
    
    [ADA getConfigurationWithCode:@"ismart" handler:^(NSJSONSerialization * _Nullable json) {
        NSLog(@"json: %@", json);
        NSString *url = [[json valueForKey:@"additional"] valueForKey:@"map_nearby_url"];
        NSLog(@"url: %@", url);
        [ServiceCaller callJSON:url
                         method:@"GET"
                         header:@{@"enc":@"false"}
                      parameter:@{
                                  @"input":keyword ? keyword : @"",
                                  //@"lat":[NSString stringWithFormat:@"%f", location.latitude],
                                  //@"lon":[NSString stringWithFormat:@"%f", location.longitude],
                                  @"serviceType": @"False",
                                  @"rowNum":@"30",
                                  @"radius":@"5.0",
                                  @"sort":@"th-asc"
                                  }
                        handler:^(NSJSONSerialization *json) {
                            NSMutableArray<LocationMessage*> *list = [[NSMutableArray alloc] init];
                            if(json) {
                                NSArray *docs = [[json valueForKey:@"response"] valueForKey:@"docs"];
                                for(NSJSONSerialization *jsonLoc in docs) {
                                    LocationMessage *message = [LocationMessage new];
                                    
                                    NSArray *latLon = [[jsonLoc valueForKey:@"latlon"] componentsSeparatedByString:@","];
                                    message.placeName = [jsonLoc valueForKey:@"displayth"];
                                    message.address = [jsonLoc valueForKey:@"location_t"];
                                    message.latitude = [latLon.firstObject doubleValue];
                                    message.longitude = [latLon.lastObject doubleValue];
                                    
                                    
                                    message.distance = @"";
                                    
                                    [list addObject:message];
                                }
                            }
                            if(handler) {
                                handler(list, nil);
                            }
                            
                        }];
    }];
    
    
}

 


- (void)doProcessLinkPreview:(NSJSONSerialization *)json result:(NSMutableArray<LinkPreview *> *)result {
    for(NSJSONSerialization *link in [json valueForKey:@"data"]){
        LinkPreview *p = [LinkPreview new];
        p.title = [link valueForKey:@"title"];
        p.linkDescription = [link valueForKey:@"description"];
        p.linkURL = [link valueForKey:@"link"];
        p.imageURL = [link valueForKey:@"image"];
        if(p.imageURL) {
            
            p.imageWidth = [[NSString stringWithFormat:@"%@", [link valueForKey:@"imageWidth"]] floatValue];
            p.imageHeight = [[NSString stringWithFormat:@"%@", [link valueForKey:@"imageHeight"]] floatValue];
            
        }
        //NSLog(@"w : %f, h : %f", p.imageWidth, p.imageHeight);
        [result addObject:p];
    }
}

- (void) getLinkPreview:(NSString* _Nonnull) content refObject:(id _Nullable) refObject handler:(LinkPreviewBlock _Nonnull) handler {
    
    [ServiceCaller getJSONCache:[NSString stringWithFormat:@"%@proxy/preview", kSERVER]
                     method:@"POST"
                     header:nil
                  parameter:@{@"content" : content}
                    handler:^(NSJSONSerialization *json) {
                        BOOL cacheLoaded = NO;
                        if (json) {
                            NSMutableArray<LinkPreview*> *result = [[NSMutableArray alloc] init];
                            [self doProcessLinkPreview:json result:result];
                            cacheLoaded = YES;
                            if(handler) {
                                handler(result, refObject, nil);
                            }
                        }
                        if(!cacheLoaded) {
                            [ServiceCaller callJSON:[NSString stringWithFormat:@"%@proxy/preview", kSERVER]
                                             method:@"POST"
                                             header:nil
                                          parameter:@{@"content" : content}
                                            handler:^(NSJSONSerialization *json) {
                                                if (json && [[json valueForKey:@"result"] boolValue]) {
                                                    NSMutableArray<LinkPreview*> *result = [[NSMutableArray alloc] init];
                                                    [self doProcessLinkPreview:json result:result];
                                                    
                                                    if(handler) {
                                                        handler(result, refObject, nil);
                                                    }
                                                }else{
                                                    if (handler) {
                                                        handler(nil, refObject, [[NSError alloc] initWithDomain:@"ADA" code:500 userInfo:nil]);
                                                    }
                                                }
                                            }];
                        
                        }
                        
                    }];
    
    
}


- (void) translate:(NSString* _Nonnull) content handler:(StringBlock _Nonnull) handler {
    
    NSData *param = [NSJSONSerialization dataWithJSONObject:@{
                                                              @"content" : content,
                                                              @"lang" : [AppHelper isThaiLanguage] ? @"th" : @"en"
                                                              }
                                                    options:NSJSONWritingPrettyPrinted
                                                      error:nil];
    
    [ServiceCaller getJSONCache:[NSString stringWithFormat:@"%@proxy/translate", kSERVER]
                         method:@"POST"
                         header:nil
                      parameter: param
                        handler:^(NSJSONSerialization *json) {
                            BOOL cacheLoaded = NO;
                            if (json) {
                                
                                cacheLoaded = YES;
                                if(handler) {
                                    handler([json valueForKey:@"data"]);
                                }
                            }
                            if(!cacheLoaded) {
                                [ServiceCaller callJSON:[NSString stringWithFormat:@"%@proxy/translate", kSERVER]
                                                 method:@"POST"
                                                 header:nil
                                              parameter: param
                                                handler:^(NSJSONSerialization *json) {
                                                    if (json && [[json valueForKey:@"result"] boolValue]) {
                                                        
                                                        if(handler) {
                                                            handler([json valueForKey:@"data"]);
                                                        }
                                                    }else{
                                                        if (handler) {
                                                            handler(content);
                                                        }
                                                    }
                                                }];
                                
                            }
                            
                        }];
    
    
}


#pragma mark Contact



- (NSString*) getLastestSearchContactKeyword {
    
    if (![self isLoggedOn]) {
        
        return nil;
    }
    
    
    NSString *kCacheService = [@"lastSearchContact-%@"  stringByAppendingString:self.login.usid];
    CacheService *cache = [ManagedObjectContextFactory
                           getObjectFromEntity:@"CacheService"
                           withValue:kCacheService
                           fromFieldName:@"serviceKey"];
    if (cache) {
        return [[NSString alloc] initWithData:cache.serviceData encoding:NSUTF8StringEncoding];
    }
    return nil;
}

- (void) getContactColumns:(ContactColumnBlock) handler {
    [ServiceCaller callJSON:[NSString stringWithFormat:@"%@user/user-mapper", kSERVER]
                     method:@"GET"
                     header:nil
                  parameter:nil
                    handler:^(NSJSONSerialization *json) {
                        if (json && [json valueForKey:@"data"]) {
                            NSDictionary *data = @{@"columns" : [json valueForKey:@"data"]};
                            
                            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:data
                                                                               options:NSJSONWritingPrettyPrinted
                                                                                 error:nil];
                            ;
                            ////NSLog(@"%@", [AppHelper jsonWithData:jsonData]);
                            if (handler) {
                                handler([self doProcessColumns:[AppHelper jsonWithData:jsonData]], nil);
                            }
                        }else{
                            if (handler) {
                                handler(nil, [[NSError alloc] initWithDomain:@"ADA" code:500 userInfo:nil]);
                            }
                        }
                    }];
    
}

- (void) searchContactWithKeyword:(NSString*) keyword lookInCache:(BOOL) doCache handler:(ContactBlock) handler {
    
    if (![self isLoggedOn]) {
        if (handler) {
            NSError *error = [[NSError alloc] initWithDomain:@"ADA" code:401 userInfo:nil];
            handler(nil, nil, error);
        }
        return;
    }
    
    NSString *kCacheService = [@"lastSearchContact-%@"  stringByAppendingString:self.login.usid];
    CacheService *cache = [ManagedObjectContextFactory
                           getObjectFromEntity:@"CacheService"
                           withValue:kCacheService
                           fromFieldName:@"serviceKey"];
    if (!cache) {
        cache = (CacheService*)[ManagedObjectContextFactory
                                newEntityForName:@"CacheService"];
        cache.serviceKey = kCacheService;
    }
    cache.serviceData = [keyword dataUsingEncoding:NSUTF8StringEncoding];
    [ManagedObjectContextFactory saveContext];
    if (doCache) {
        [ServiceCaller getJSONCache:[NSString stringWithFormat:@"%@user/search-contact-new", kSERVER]
                             method:@"POST"
                             header:nil
                          parameter:@{@"keyword":keyword}
                            handler:^(NSJSONSerialization *json) {
                                
                                if(json && [[json valueForKey:@"result"] boolValue]){
                                    if (handler) {
                                        handler([self doProcessColumns:json], [self doProcessContact:json], nil);
                                    }
                                }
                                ////NSLog(@"keyword = %@", keyword);
                                [ServiceCaller callJSON:[NSString stringWithFormat:@"%@user/search-contact-new", kSERVER]
                                                 method:@"POST"
                                                 header:nil
                                              parameter:@{@"keyword":keyword}
                                                handler:^(NSJSONSerialization *json) {
                                                    
                                                    if(json && [[json valueForKey:@"result"] boolValue]){
                                                        if (handler) {
                                                            handler([self doProcessColumns:json], [self doProcessContact:json], nil);
                                                        }
                                                    }else{
                                                        if (handler) {
                                                            handler(nil, nil, [[NSError alloc] initWithDomain:@"ADA" code:500 userInfo:nil]);
                                                        }
                                                    }
                                                }];
                            }
         ];
    }else{
        
        [ServiceCaller callJSON:[NSString stringWithFormat:@"%@user/search-contact-new", kSERVER]
                         method:@"POST"
                         header:nil
                      parameter:@{@"keyword":keyword}
                        handler:^(NSJSONSerialization *json) {
                            
                            if(json && [[json valueForKey:@"result"] boolValue]){
                                if (handler) {
                                    handler([self doProcessColumns:json], [self doProcessContact:json], nil);
                                }
                            }else{
                                if (handler) {
                                    handler(nil, nil, [[NSError alloc] initWithDomain:@"ADA" code:500 userInfo:nil]);
                                }
                            }
                        }];
        
        
    }
}

- (NSArray*) doProcessColumns:(NSJSONSerialization*) json {
    NSMutableArray *columns = [[NSMutableArray alloc] init];
    for (NSJSONSerialization *col in [json valueForKey:@"columns"]) {
        ContactColumn *c = [ContactColumn new];
        c.labelEN = [col valueForKey:@"label_en"];
        c.labelTH = [col valueForKey:@"label_th"];
        c.labelDB = [col valueForKey:@"label_db"];
        c.attributeEN = [col valueForKey:@"attr_en"];
        c.attributeTH = [col valueForKey:@"attr_th"];
        c.editable = [[col valueForKey:@"editable"] boolValue];
        c.contactDisplay = [[col valueForKey:@"contact_display"] boolValue];
        c.preContactDisplay = [[col valueForKey:@"pre_contact_display"] boolValue];
        c.sortSequence = [col valueForKey:@"sort_seq"]  ? [[col valueForKey:@"sort_seq"] integerValue] : -1;
        c.sortAccending = ([col valueForKey:@"sort_direction"]  ? [[col valueForKey:@"sort_direction"] integerValue] : 0) == 1;
        c.templateValue = [col valueForKey:@"value_template"];
        [columns addObject:c];
    }
    
    return columns;
}



- (ADAMember*) doProcessADAMember:(id) json {
    if(json && [[json valueForKey:@"result"] boolValue]){
        NSJSONSerialization* member  = json;
        /*ADAMember *m = [ADAMember new];
        m.name = [[[AppHelper getLabelForThai: [NSString stringWithFormat:@"%@ %@",
                                                [member valueForKey:@"firstname_th"],
                                                [member valueForKey:@"lastname_th"]]
                                          eng: [NSString stringWithFormat:@"%@ %@",
                                                [member valueForKey:@"firstname_en"],
                                                [member valueForKey:@"lastname_en"]]] stringByReplacingOccurrencesOfString:@"null" withString:@"" ]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        m.userID = [member valueForKey:@"user_id"];
        m.group = [member valueForKey:@"user_group"];
        m.imageURL = [member valueForKey:@"user_image"];
        [json setValue:[NSString stringWithFormat:@"%@:contact-cover", [json valueForKey:@"contact_cover_image"]] forKey:@"contact_cover_image"];
        m.contactInfo = [AppHelper getLabelForThai:[member valueForKey:@"contact_display"] eng:[member valueForKey:@"contact_display_en"]];
        m.userData = json;*/
        return [ADAMember withDictionary:(NSDictionary<NSString*, id>*) member];
    }else{
        return nil;
    }
}

- (NSArray<ADAMember*>*) doProcessADAMembers:(NSArray*) arr {
    NSMutableArray<ADAMember*> *list = [[NSMutableArray alloc] init];
    //if(json && [[json valueForKey:@"result"] boolValue]){
        for(NSJSONSerialization *member in arr){
            /*ADAMember *m = [ADAMember new];
            m.name = [[[AppHelper getLabelForThai: [NSString stringWithFormat:@"%@ %@",
                                                    [member valueForKey:@"firstname_th"],
                                                    [member valueForKey:@"lastname_th"]]
                                              eng: [NSString stringWithFormat:@"%@ %@",
                                                    [member valueForKey:@"firstname_en"],
                                                    [member valueForKey:@"lastname_en"]]] stringByReplacingOccurrencesOfString:@"null" withString:@"" ]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            m.userID = [member valueForKey:@"user_id"];
            m.group = [member valueForKey:@"user_group"];
            m.imageURL = [member valueForKey:@"user_image"];
            [member setValue:[NSString stringWithFormat:@"%@:contact-cover", [member valueForKey:@"contact_cover_image"]] forKey:@"contact_cover_image"];
            m.contactInfo = [AppHelper getLabelForThai:[member valueForKey:@"contact_display"] eng:[member valueForKey:@"contact_display_en"]];
            m.userData = member;*/
            [list  addObject:[ADAMember withDictionary:(NSDictionary<NSString*, id>*) member]];
        }
        return list;
    //}else{
    //    return list;
    //}
}

- (NSArray<ContactPerson*>*) doProcessContact:(NSJSONSerialization*) json {
    //NSLog(@"doProcessContact");
    ////NSLog(@"json %@", json);
    
    
    NSArray *columns = [json valueForKey:@"columns"] ? [json valueForKey:@"columns"] : @[];
    NSArray *array = [json valueForKey:@"data"];
    
    NSArray *sortArr = [columns filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"sort_seq != nil"]];
    
    NSString *sortAttr = @"unitid";
    NSString *sortNameAttr = @"fulldept";
    if (sortArr) {
        sortArr = [sortArr sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"sort_seq" ascending:YES]]];
        
        sortAttr = [sortArr.firstObject valueForKey:[AppHelper getLabelForThai:@"attr_th" eng:@"attr_en"]];
        sortNameAttr = [sortArr.firstObject valueForKey:[AppHelper getLabelForThai:@"attr_th" eng:@"attr_en"]];
    }
    
    if(!sortAttr){
        sortAttr = @"unitid";
        sortNameAttr = @"fulldept";
    }
    
    
    array = [array sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:sortAttr ascending:YES]]];
    
    
    if(array){
        
        NSMutableDictionary *contacts = [NSMutableDictionary new];
        NSMutableArray *groups = [NSMutableArray new];
        NSMutableArray *groupNames = [NSMutableArray new];
        
        NSMutableArray *contactList = [NSMutableArray new];
        for (NSJSONSerialization *json in array) {
            if(![groups containsObject:[json valueForKey:sortAttr]]){
                [groups addObject:[json valueForKey:sortAttr]];
                [groupNames addObject:[json valueForKey:sortNameAttr]];
                [contacts setObject:[[NSMutableArray alloc] init] forKey:[json valueForKey:sortAttr]];
            }
            [[contacts objectForKey:[json valueForKey:sortAttr]] addObject:json];
            
        }
        
        for (NSString *group in groups) {
            ContactPerson *c = [ContactPerson new];
            c.groupTitle = [groupNames objectAtIndex:[groups indexOfObject:group]];
            NSMutableArray *contactGroupList = [NSMutableArray new];
            NSArray *listInGroup  = [contacts objectForKey:group];
            for (NSJSONSerialization *contact in listInGroup) {
                NSMutableDictionary *row  = [[NSMutableDictionary alloc] init];
                if([contact valueForKey:@"user_image"]){
                    [row setValue:[contact valueForKey:@"user_image"] forKey:@"user_image"];
                }
                for (NSJSONSerialization *col in columns) {
                    
                    
                    [row setValue:[contact valueForKey:[col valueForKey:@"attr_th"]]  forKey:[col valueForKey:@"attr_th"]];
                    [row setValue:[contact valueForKey:[col valueForKey:@"attr_en"]]  forKey:[col valueForKey:@"attr_en"]];
                    
                }
                
                [row setValue:[contact valueForKey:@"contact_display"]  forKey:@"contact_display"];
                [row setValue:[contact valueForKey:@"contact_display_en"]  forKey:@"contact_display_en"];
                if([contact valueForKey:@"contact_cover_image"]){
                    [row setValue:[NSString stringWithFormat:@"%@:contact-cover", [contact valueForKey:@"contact_cover_image"]]  forKey:@"contact_cover_image"];
                }
                [contactGroupList addObject:row];
                
            }
            c.contactList = contactGroupList;
            [contactList addObject:c];
        }
        
        
        
        
        //NSLog(@"done");
        return contactList;
        
    }
    return nil;
}


- (void) setContactNickname:(NSString *_Nonnull) nickname with:(id) user {
    if(user) {
        
        NSString *nameJSON = [AppHelper getUserDataForKey:@"phone-book-name"];
        NSMutableDictionary *dict = [NSMutableDictionary new];
        if(nameJSON) {
            NSJSONSerialization *json = [AppHelper jsonWithData:[nameJSON dataUsingEncoding:NSUTF8StringEncoding]];
            [dict setDictionary:jsonToDictionary(json)];
        }
        
        if([user isKindOfClass:[ADAMember class]]) {
            ADAMember *member = (ADAMember*)user;
            if(member.userData){
                NSString *mobile = [member.userData valueForKey:@"mobile"];
                mobile = [mobile stringByReplacingOccurrencesOfString:@" " withString:@""];
                mobile = [mobile stringByReplacingOccurrencesOfString:@"-" withString:@""];
                mobile = [mobile stringByReplacingOccurrencesOfString:@"," withString:@""];
                [dict setValue:nickname forKey: mobile];
            }
            NSString *userID = [[NSString stringWithFormat:@"%@-%@", member.userID, member.group] uppercaseString];
            [dict setValue:nickname forKey: userID];
        }else if([user isKindOfClass:[NSDictionary class]]) {
            NSDictionary *member = (NSDictionary*)user;
            NSString *mobile = [member valueForKey:@"mobile"];
            mobile = [mobile stringByReplacingOccurrencesOfString:@" " withString:@""];
            mobile = [mobile stringByReplacingOccurrencesOfString:@"-" withString:@""];
            mobile = [mobile stringByReplacingOccurrencesOfString:@"," withString:@""];
            [dict setValue:nickname forKey: mobile];
            NSString *userID = [[NSString stringWithFormat:@"%@-%@",
                                 [member valueForKey:@"user_id"],
                                 [member valueForKey:@"user_group"]] uppercaseString];
            [dict setValue:nickname forKey: userID];
        }
        //NSLog(@"%@", dict);
        NSString *jsonString = [AppHelper stringWithJSON:[AppHelper jsonWithdictionary:dict]];
        [AppHelper setUserData:jsonString forKey:@"phone-book-name"];
    }
}

- (NSString* _Nullable) getContactNickname:(id) user {
    if(user) {
        
        NSString *nameJSON = [AppHelper getUserDataForKey:@"phone-book-name"];
        NSMutableDictionary *dict = [NSMutableDictionary new];
        if(nameJSON) {
            NSJSONSerialization *json = [AppHelper jsonWithData:[nameJSON dataUsingEncoding:NSUTF8StringEncoding]];
            [dict setDictionary:jsonToDictionary(json)];
        }
        
        if([user isKindOfClass:[ADAMember class]]) {
            ADAMember *member = (ADAMember*)user;
            if(member.userData){
                NSString *mobile = [member.userData valueForKey:@"mobile"];
                mobile = [mobile stringByReplacingOccurrencesOfString:@" " withString:@""];
                mobile = [mobile stringByReplacingOccurrencesOfString:@"-" withString:@""];
                mobile = [mobile stringByReplacingOccurrencesOfString:@"," withString:@""];
                if([dict valueForKey:mobile]) {
                    return [dict valueForKey:mobile];
                }
            }
            NSString *userID = [[NSString stringWithFormat:@"%@-%@", member.userID, member.group] uppercaseString];
            if([dict valueForKey:userID]) {
                return [dict valueForKey:userID];
            }
        }else if([user isKindOfClass:[NSDictionary class]]) {
            NSDictionary *member = (NSDictionary*)user;
            NSString *mobile = [member valueForKey:@"mobile"];
            mobile = [mobile stringByReplacingOccurrencesOfString:@" " withString:@""];
            mobile = [mobile stringByReplacingOccurrencesOfString:@"-" withString:@""];
            mobile = [mobile stringByReplacingOccurrencesOfString:@"," withString:@""];
            if([dict valueForKey:mobile]) {
                return [dict valueForKey:mobile];
            }
            NSString *userID = [[NSString stringWithFormat:@"%@-%@",
                                 [member valueForKey:@"user_id"],
                                 [member valueForKey:@"user_group"]] uppercaseString] ;
            if([dict valueForKey:userID]) {
                return [dict valueForKey:userID];
            }
        }
        
    }
    return nil;
}


- (void) getAllPhonebook:(SuccessBlock _Nullable) handler {
 
    [ServiceCaller callJSON:[NSString stringWithFormat:@"%@user/get-all-phonebook", kSERVER]
                     method:@"GET"
                     header:nil
                  parameter:nil
                    handler:^(NSJSONSerialization *json) {
                        //NSLog(@"json %@", json);
                        NSLog(@"result %@", [[json valueForKey:@"result"] boolValue] ? @"Y" : @"N");
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if (json && [[json valueForKey:@"result"] boolValue]) {
                                //NSMutableArray<Phonebook*> *phonebooks = [[NSMutableArray alloc] init];
                                //NSArray *data = [json valueForKey:@"data"];
                                
                                
                                
                                NSUserDefaults *userDefaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.com.g-able.ada"];
                                //[userDefaults synchronize];
                                
                                NSString *nameJSON = [AppHelper getUserDataForKey:@"phone-book-name"];
                                NSMutableDictionary *dict = [NSMutableDictionary new];
                                if(nameJSON) {
                                    NSJSONSerialization *json = [AppHelper jsonWithData:[nameJSON dataUsingEncoding:NSUTF8StringEncoding]];
                                    [dict setDictionary:jsonToDictionary(json)];
                                }
                                
                                for(NSJSONSerialization * j in [json valueForKey:@"data"]){
                                    NSString *nickname = [dict valueForKey:[j valueForKey:@"mobile"]];
                                    if(nickname && ![nickname isEqualToString:@""]) {
                                        [j setValue:[dict valueForKey:[j valueForKey:@"mobile"]] forKey:@"contact_display"];
                                    }
                                }
                                
                                
                                [userDefaults setValue:[AppHelper stringWithJSON:json] forKey:@"phonebooks"];
                                //dispatch_async(dispatch_get_main_queue(), ^{
                                //[userDefaults synchronize];
                                //if([userDefaults synchronize]){
                                    if (handler) {
                                        handler(YES);
                                    }
                                /*}else{
                                    if (handler) {
                                        handler(NO);
                                    }
                                }*/
                                //});
                                
                            }else{
                                if (handler) {
                                    handler(NO);
                                }
                            }
                        });
                    }];
    
}

- (NSString *)stringByStrippingHTML:(NSString*) input {
    NSRange r;
    NSString *s = [input copy];
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    return s;
}


#pragma mark News Feed
- (void) getNewsFeedCategory:(NewsFeedCategoryBlock) handler {
    if (![self isLoggedOn]) {
        if (handler) {
            NSError *error = [[NSError alloc] initWithDomain:@"ADA" code:401 userInfo:nil];
            handler(nil, error);
        }
        return;
    }
    
    
    
    [ServiceCaller getJSONCache:[NSString stringWithFormat:@"%@news-feed/get", kSERVER]
                         method:@"GET"
                         header:nil
                      parameter:nil
                        handler:^(NSJSONSerialization *json) {
                            if (json && [[json valueForKey:@"result"] boolValue]) {
                                NSMutableArray *categories = [[NSMutableArray alloc] init];
                                [self doProcessNewsFeedCategory:categories inJSON:json];
                                if (handler) {
                                    handler(categories, nil);
                                }
                                
                            }
                            [ServiceCaller callJSON:[NSString stringWithFormat:@"%@news-feed/get", kSERVER]
                                             method:@"GET"
                                             header:nil
                                          parameter:nil
                                            handler:^(NSJSONSerialization *json) {
                                                if ([[json valueForKey:@"result"] boolValue]) {
                                                    NSMutableArray *categories = [[NSMutableArray alloc] init];
                                                    [self doProcessNewsFeedCategory:categories inJSON:json];
                                                    if (handler) {
                                                        handler(categories, nil);
                                                    }
                                                    
                                                }else{
                                                    if (handler) {
                                                        handler(nil, [[NSError alloc] initWithDomain:@"ADA" code:500 userInfo:nil]);
                                                    }
                                                }
                                            }];
                        }];
    
    
}

- (void) doProcessNewsFeedCategory:(NSMutableArray*) categories inJSON:(NSJSONSerialization*) json {
    NSJSONSerialization *jsonData = [json valueForKey:@"data"];
    NSArray *array = [jsonData valueForKey:@"categories"];
    for(NSJSONSerialization *cate in array) {
        NewsFeedCategory *news = [NewsFeedCategory new];
        news.categoryID = [NSString stringWithFormat:@"%@:%@",
                           [cate valueForKey:@"site_id"],
                           [cate valueForKey:@"seq"]] ;
        news.icon = [cate valueForKey:@"icon"];
        news.title = [cate valueForKey:@"cat_name"];
        news.detail = [cate valueForKey:@"cat_desc"];
        news.categoryType = [[cate valueForKey:@"cat_type"] integerValue];
        [categories addObject:news];
    }
}


- (void) getNewsFeedList:(NewsFeedCategory*) category inPeroid:(NSString*) peroid handler:(NewsFeedBlock) handler {
    if (![self isLoggedOn]) {
        if (handler) {
            NSError *error = [[NSError alloc] initWithDomain:@"ADA" code:401 userInfo:nil];
            handler(nil, error);
        }
        return;
    }
    
    NSArray *ID = [category.categoryID componentsSeparatedByString:@":"];
    if(peroid && [peroid isEqualToString:@""]){
        peroid = nil;
    }
 
    [ServiceCaller getJSONCache:[NSString stringWithFormat:@"%@news-feed/get-by-category/%@/%@",
                                 kSERVER,
                                 ID.firstObject,
                                 ID.lastObject]
                         method:@"GET"
                         header:nil
                      parameter:peroid ? @{@"period" : peroid} : nil
                        handler:^(NSJSONSerialization *json) {
                            
                            if (json && [[json valueForKey:@"result"] boolValue]) {
                                NSMutableArray *newsList = [[NSMutableArray alloc] init];
                                [self doProcessNewsFeedList:category list:newsList fromJSON:json];
                                if (handler) {
                                    handler(newsList, nil);
                                }
                            }
                            
 
                            [ServiceCaller callJSON:[NSString stringWithFormat:@"%@news-feed/get-by-category/%@/%@",
                                                     kSERVER,
                                                     ID.firstObject,
                                                     ID.lastObject]
                                             method:@"GET"
                                             header:nil
                                          parameter:peroid ? @{@"period" : peroid} : nil
                                            handler:^(NSJSONSerialization *json) {
                                                if ([[json valueForKey:@"result"] boolValue]) {
                                                    NSMutableArray *newsList = [[NSMutableArray alloc] init];
                                                    [self doProcessNewsFeedList:category list:newsList fromJSON:json];
                                                    if (handler) {
                                                        handler(newsList, nil);
                                                    }
                                                }else{
                                                    if (handler) {
                                                        handler(nil, [[NSError alloc] initWithDomain:@"ADA" code:500 userInfo:nil]);
                                                    }
                                                }
                                            }];
                            
                        }];
    
}

- (void) doProcessNewsFeedList:(NewsFeedCategory*) category list:(NSMutableArray*) newsList fromJSON:(NSJSONSerialization*) json {
    
    
    
    NSDateFormatter *df = [NSDateFormatter instanceWithUSLocale];
    [df setDateFormat:@"yyyy-MM-dd"];
    for(NSJSONSerialization *news in [json valueForKey:@"data"]) {
        NewsFeed *detail = [NewsFeed new];
        detail.category = category;
        detail.categoryID = [NSString stringWithFormat:@"%@", [news valueForKey:@"cate_id"]];
        detail.categoryType = category.categoryType;
        detail.docno = [news valueForKey:@"docno"];
        detail.source = [news valueForKey:@"company"];
        detail.image = [news valueForKey:@"image"];
        detail.contactName = [news valueForKey:@"contact_name"];
        detail.contactDetail = [news valueForKey:@"contact_detail"];
        detail.dateFrom = [news valueForKey:@"date_from"]  && [[news valueForKey:@"date_from"] length] == 10? [df dateFromString:[news valueForKey:@"date_from"]] :nil;
        detail.dateTo = [news valueForKey:@"date_to"]  && [[news valueForKey:@"date_to"] length] == 10? [df dateFromString:[news valueForKey:@"date_to"]] : nil;
        detail.title = [AppHelper getLabelForThai:[news valueForKey:@"title_th"] eng:[news valueForKey:@"title_en"] ];
        detail.detail = [AppHelper getLabelForThai:[news valueForKey:@"detail_th"] eng:[news valueForKey:@"detail_en"] ];
        detail.location = [news valueForKey:@"location"];
        if([news valueForKey:@"attach_file"] && ![@"" isEqualToString:[news valueForKey:@"attach_file"]]){
            detail.downloadFileURL = [news valueForKey:@"attach_file"];
            detail.fileName = [detail.downloadFileURL componentsSeparatedByString:@"/"].lastObject;
            if([@"pdf" isEqualToString:[news valueForKey:@"attach_type"]]) {
                detail.mediaContentType = MediaContentTypePDF;
            }else if([@"video" isEqualToString:[news valueForKey:@"attach_type"]]) {
                detail.mediaContentType = MediaContentTypeVideo;
            }
            if([news valueForKey:@"attach_name"] && ![@"" isEqualToString:[news valueForKey:@"attach_name"]]) {
                detail.fileName = [news valueForKey:@"attach_name"];
            }
            
        }
        [newsList addObject:detail];
    }
}

- (void) getNewsFeedDetail:(NewsFeed*) newsFeed handler:(NewsFeedDetailBlock) handler {
    
    if (![self isLoggedOn]) {
        if (handler) {
            NSError *error = [[NSError alloc] initWithDomain:@"ADA" code:401 userInfo:nil];
            handler(nil, error);
        }
        return;
    }
    NSArray *ID = [newsFeed.categoryID componentsSeparatedByString:@":"];
    
    
    [ServiceCaller getJSONCache:[NSString stringWithFormat:@"%@news-feed/get-detail-by-docno/%@",
                                 kSERVER,
                                 ID.lastObject]
                         method:@"GET"
                         header:nil
                      parameter:@{
                                  @"docno": newsFeed.docno
                                  }handler:^(NSJSONSerialization *json) {
                                      
                                      if (json && [[json valueForKey:@"result"] boolValue]) {
                                          
                                          NewsFeedDetail *d = [self doProcessNewsFeedDetail:newsFeed inJSON:json];
                                          if(d){
                                              handler(d, nil);
                                          } else{
                                              handler(nil, [[NSError alloc] initWithDomain:@"ADA" code:404 userInfo:nil]);
                                          }
                                          
                                      }
                                   
                                      [ServiceCaller callJSON:[NSString stringWithFormat:@"%@news-feed/get-detail-by-docno/%@",
                                                               kSERVER,
                                                               ID.lastObject]
                                                       method:@"GET"
                                                       header:nil
                                                    parameter:@{
                                                                @"docno": newsFeed.docno
                                                                }handler:^(NSJSONSerialization *json) {
                                                                    
                                                                    if ([[json valueForKey:@"result"] boolValue]) {
                                                                        
                                                                        NewsFeedDetail *d = [self doProcessNewsFeedDetail:newsFeed inJSON:json];
                                                                        if(d){
                                                                            handler(d, nil);
                                                                        } else{
                                                                            handler(nil, [[NSError alloc] initWithDomain:@"ADA" code:404 userInfo:nil]);
                                                                        }
                                                                        
                                                                    }else{
                                                                        if (handler) {
                                                                            handler(nil, [[NSError alloc] initWithDomain:@"ADA" code:500 userInfo:nil]);
                                                                        }
                                                                    }
                                                                    
                                                                    
                                                                }];
                                  }];
    
    
    
}

- (NewsFeedDetail*) doProcessNewsFeedDetail:(NewsFeed*) newsFeed inJSON:(NSJSONSerialization*) json {
    NSJSONSerialization *detail = [[json valueForKey:@"data"] firstObject];
    
    if(!detail){
        return nil;
    }
    NewsFeedDetail *d = [NewsFeedDetail new];
    if(![[detail valueForKey:@"has_file_content"] boolValue]){
        NSArray *images = [detail valueForKey:@"item_file"];
        d.imageURLs = images;
    }else{
        d.imageURLs = @[];
        NSString *b64String = [detail valueForKey:@"item_file_content"];
        NSData *data = [NSData dataWithBase64EncodedString:b64String];
        d.imageData = data;
    }
    
    d.categoryID = newsFeed.categoryID;
    d.categoryType = newsFeed.categoryType;
    d.docno = newsFeed.docno;
    d.source = newsFeed.source;
    d.contactName = newsFeed.contactName;
    d.contactDetail = newsFeed.contactDetail;
    d.dateFrom = newsFeed.dateFrom;
    d.dateTo = newsFeed.dateTo;
    d.title = newsFeed.title;
    if([detail valueForKey:@"detail"]){
        d.detail = [detail valueForKey:@"detail"];
    }else{
        d.detail = newsFeed.detail;
    }
    d.fileName = [detail valueForKey:@"item_file"];
    d.fileDetail = [detail valueForKey:@"item_detail"];
    d.location = newsFeed.location;
    d.downloadFileURL = newsFeed.downloadFileURL;
    return d;
}

- (void) getRSSFeed:(RSSFeedBlock) handler {
    if (![self isLoggedOn]) {
        if (handler) {
            NSError *error = [[NSError alloc] initWithDomain:@"ADA" code:401 userInfo:nil];
            handler(nil, error);
        }
        return;
    }
    
    [ServiceCaller getJSONCache:[NSString stringWithFormat:@"%@news-feed/rss-all", kSERVER]
                         method:@"GET"
                         header:nil
                      parameter:nil
                        handler:^(NSJSONSerialization *json) {
                            
                            if (json && [[json valueForKey:@"result"] boolValue]) {
                                NSMutableArray *rssfeedList = [[NSMutableArray alloc] init];
                                [self doProcessRSS:rssfeedList fromJSON:json];
                                if(handler) {
                                    handler(rssfeedList, nil);
                                }
                                
                            }
                            [ServiceCaller callJSON:[NSString stringWithFormat:@"%@news-feed/rss-all", kSERVER]
                                             method:@"GET"
                                             header:nil
                                          parameter:nil
                                            handler:^(NSJSONSerialization *json) {
                                                
                                                
                                                
                                                if ([[json valueForKey:@"result"] boolValue]) {
                                                    NSMutableArray *rssfeedList = [[NSMutableArray alloc] init];
                                                    [self doProcessRSS:rssfeedList fromJSON:json];
                                                    if(handler) {
                                                        handler(rssfeedList, nil);
                                                    }
                                                    
                                                }else{
                                                    if(handler) {
                                                        handler(nil, [[NSError alloc] initWithDomain:@"ADA" code:500 userInfo:nil]);
                                                    }
                                                }
                                            }];
                            
                        }];
    
    
    
}

- (void) doProcessRSS:(NSMutableArray*) rssFeedList fromJSON:(NSJSONSerialization*) json {
    for (NSJSONSerialization *rss in [json valueForKey:@"data"]) {
        
        RSSFeed *feed = [RSSFeed new];
        feed.ID = [NSString stringWithFormat:@"%@", [rss valueForKey:@"id"]];
        feed.title = [rss valueForKey:@"channel"];
        feed.url = [rss valueForKey:@"url"];
        feed.type = [[rss valueForKey:@"rss_type"] integerValue];
        [rssFeedList addObject:feed];
        
    }
}

- (void) getRSSFeedChannel:(RSSFeed*) rssFeed handler:(RSSFeedChannelBlock) handler {
    
    NSMutableArray *result = [[NSMutableArray alloc] init];
    
    if (rssFeed.url) {
        
        
        [ServiceCaller getJSONCache:[NSString stringWithFormat:@"%@news-feed/rss-by-url",
                                     kSERVER]
                             method:@"GET"
                             header:nil
                          parameter:@{
                                      @"url":rssFeed.url
                                      }
                            handler:^(NSJSONSerialization *json) {
                                [self doProccessRSS:json result:result];
                                handler(result, nil);
                                [ServiceCaller callJSON:[NSString stringWithFormat:@"%@news-feed/rss-by-url",
                                                         kSERVER]
                                                 method:@"GET"
                                                 header:nil
                                              parameter:@{
                                                          @"url":rssFeed.url
                                                          }
                                                handler:^(NSJSONSerialization *json) {
                                                    [self doProccessRSS:json result:result];
                                                    if(![[json valueForKey:@"result"] boolValue]){
                                                        if(handler) {
                                                            handler(nil, [[NSError alloc] initWithDomain:@"ADA" code:500 userInfo:nil]);
                                                        }
                                                    }else{
                                                        if(handler){
                                                            handler(result, nil);
                                                        }
                                                    }
                                                    
                                                }];
                            }];
    }else{
        
        
        [ServiceCaller getJSONCache:[NSString stringWithFormat:@"%@news-feed/rss/%@",
                                     kSERVER, rssFeed.ID]
                             method:@"GET"
                             header:nil
                          parameter:nil
                            handler:^(NSJSONSerialization *json) {
                                [self doProccessRSS:json result:result];
                                handler(result, nil);
                                [ServiceCaller callJSON:[NSString stringWithFormat:@"%@news-feed/rss/%@",
                                                         kSERVER, rssFeed.ID]
                                                 method:@"GET"
                                                 header:nil
                                              parameter:nil
                                                handler:^(NSJSONSerialization *json) {
                                                    [self doProccessRSS:json result:result];
                                                    if(![[json valueForKey:@"result"] boolValue]){
                                                        if(handler) {
                                                            handler(nil, [[NSError alloc] initWithDomain:@"ADA" code:500 userInfo:nil]);
                                                        }
                                                    }else{
                                                        handler(result, nil);
                                                    }
                                                    
                                                }];
                            }];
        
    }
}

- (void) doProccessRSS:(NSJSONSerialization *)json result:(NSMutableArray*) result {
    [result removeAllObjects];
    if([[json valueForKey:@"result"] boolValue]){
        if ([[NSString stringWithFormat:@"%@", json] rangeOfString:@"rdf:RDF"].location != NSNotFound) {
            for(NSJSONSerialization *item in [[json valueForKey:@"rdf:RDF"]valueForKey:@"item"]){
                RSSFeedChannel *r = [RSSFeedChannel new];
                r.title = [[[item valueForKey:@"title"]  stringByDecodingHTMLEntities] stringByConvertingHTMLToPlainText];
                r.channelDescription = [[[item valueForKey:@"description"] stringByDecodingHTMLEntities] stringByConvertingHTMLToPlainText];
                r.date = [item valueForKey:@"pubDate"];
                if([item valueForKey:@"link"]){
                    r.url = [item valueForKey:@"link"];
                }else{
                    r.url = [[item valueForKey:@"guid"] valueForKey:@"content"];
                }
                if([item valueForKey:@"thumbnail"]){
                    r.imageUrl = [item valueForKey:@"thumbnail"];
                    
                }else if([item valueForKey:@"image"]){
                    r.imageUrl = [item valueForKey:@"image"];
                }else if([item valueForKey:@"enclosure"]){
                    r.imageUrl = [[item valueForKey:@"enclosure"] valueForKey:@"url"];
                    
                }else if([item valueForKey:@"media:thumbnail"]){
                    r.imageUrl = [[item valueForKey:@"media:thumbnail"] valueForKey:@"url"];
                    
                }else if([item valueForKey:@"media:group"] && [[item valueForKey:@"media:group"] valueForKey:@"media:content"] && [[[item valueForKey:@"media:group"] valueForKey:@"media:content"] count] > 0){
                    r.imageUrl = [[[[item valueForKey:@"media:group"] valueForKey:@"media:content"] firstObject] valueForKey:@"url"];
                }
                
                [result addObject:r];
            }
            
            
        }else{
            for(NSJSONSerialization *item in [[[json valueForKey:@"rss"] valueForKey:@"channel"] valueForKey:@"item"]){
                RSSFeedChannel *r = [RSSFeedChannel new];
                r.title = [[[item valueForKey:@"title"]  stringByDecodingHTMLEntities] stringByConvertingHTMLToPlainText];
                r.channelDescription = [[[item valueForKey:@"description"] stringByDecodingHTMLEntities] stringByConvertingHTMLToPlainText];
                r.date = [item valueForKey:@"pubDate"];
                if([item valueForKey:@"link"]){
                    r.url = [item valueForKey:@"link"];
                }else{
                    r.url = [[item valueForKey:@"guid"] valueForKey:@"content"];
                }
                if([item valueForKey:@"thumbnail"]){
                    r.imageUrl = [item valueForKey:@"thumbnail"];
                    
                }else if([item valueForKey:@"image"]){
                    r.imageUrl = [item valueForKey:@"image"];
                }else if([item valueForKey:@"enclosure"]){
                    r.imageUrl = [[item valueForKey:@"enclosure"] valueForKey:@"url"];
                    
                }else if([item valueForKey:@"media:thumbnail"]){
                    r.imageUrl = [[item valueForKey:@"media:thumbnail"] valueForKey:@"url"];
                    
                }else if([item valueForKey:@"media:group"] && [[item valueForKey:@"media:group"] valueForKey:@"media:content"] && [[[item valueForKey:@"media:group"] valueForKey:@"media:content"] count] > 0){
                    r.imageUrl = [[[[item valueForKey:@"media:group"] valueForKey:@"media:content"] firstObject] valueForKey:@"url"];
                }
                [result addObject:r];
            }
            
        }
        
        
    }
}
#pragma mark Office 365

- (void) getO365CalendarWithUser:(O365User*) user dateFrom: (NSDate*) dateFrom to:(NSDate*) dateTo handler:(O365CalendarBlock) handler {
    if (![self isLoggedOn] || !user) {
        if (handler) {
            NSError *error = [[NSError alloc] initWithDomain:@"ADA" code:401 userInfo:nil];
            handler(nil, error);
        }
        return;
    }
    NSDateFormatter *formatter = [NSDateFormatter instanceWithUSLocale];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    /*[ServiceCaller getJSONCache:[NSString stringWithFormat:@"%@o365/calendar", kSERVER]
     method:@"POST"
     header:nil
     parameter:@{
     @"user": user.username,
     @"password": user.password,
     @"dateFrom": [formatter stringFromDate:dateFrom],
     @"dateTo": [formatter stringFromDate:dateTo]
     }
     handler:^(NSJSONSerialization *json) {
     
     if (json && [[json valueForKey:@"result"] boolValue]) {
     NSMutableArray *calendarList = [[NSMutableArray alloc] init];
     [self doProccessO365Calendar: json result:calendarList];
     if(handler) {
     handler(calendarList, nil);
     }
     
     }*/
    
    
    [ServiceCaller callJSON:[NSString stringWithFormat:@"%@o365/calendar", kSERVER]
                     method:@"POST"
                     header:@{
                              @"internal-talk" : @"ada-cloud",
                              @"enc" : @"false"
                              }
                  parameter:@{
                              @"user": user.username,
                              @"password": user.password,
                              @"dateFrom": [formatter stringFromDate:dateFrom],
                              @"dateTo": [formatter stringFromDate:dateTo]
                              }
                    handler:^(NSJSONSerialization *json) {
                        
                        
                        
                        if ([[json valueForKey:@"result"] boolValue]) {
                            NSMutableArray *calendarList = [[NSMutableArray alloc] init];
                            [self doProccessO365Calendar: json result:calendarList];
                            if(handler) {
                                handler(calendarList, nil);
                            }
                            
                        }else{
                            if(handler) {
                                handler(nil, [[NSError alloc] initWithDomain:@"ADA" code:500 userInfo:nil]);
                            }
                        }
                    }];
    
    //}];
    
    
    
}

NSString* formatDateTimeString(NSString* str) {
    
    
    return [[str stringByReplacingOccurrencesOfString:@"T" withString:@" "] stringByReplacingOccurrencesOfString:@"Z" withString:@""];
}

- (void) doProccessO365Calendar:(NSJSONSerialization *)json result:(NSMutableArray*) result {
    [result removeAllObjects];
    if([[json valueForKey:@"result"] boolValue]){
        NSDateFormatter *dateFormatter = [NSDateFormatter instanceWithUSLocale];
        
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        for(NSJSONSerialization *item in [json valueForKey:@"data"]){
            ////NSLog(@"%@", item);
            O365Calendar *r = [O365Calendar new];
            r.subject = [item valueForKey:@"Subject"];
            r.body = [[item valueForKey:@"Body"] valueForKey:@"Content"];
            r.bodyPreview = [item valueForKey:@"BodyPreview"];
            r.organizerName = [[[item valueForKey:@"Organizer"] valueForKey:@"EmailAddress"] valueForKey:@"Name"];
            r.organizerAddress = [[[item valueForKey:@"Organizer"] valueForKey:@"EmailAddress"] valueForKey:@"Address"];
            
            r.organizerUserData = [[item valueForKey:@"Organizer"] valueForKey:@"UserData"];
            
            r.location = [[item valueForKey:@"Location"] valueForKey:@"DisplayName"];
            
            r.startDate = [dateFormatter dateFromString:formatDateTimeString([item valueForKey:@"Start"])];
            
            r.startDate = [NSCalendar.currentCalendar dateByAddingUnit:NSCalendarUnitHour value:7 toDate:r.startDate  options:NSCalendarWrapComponents];
            
            r.endDate = [dateFormatter dateFromString:formatDateTimeString([item valueForKey:@"End"])];
            
            r.endDate = [NSCalendar.currentCalendar dateByAddingUnit:NSCalendarUnitHour value:7 toDate:r.endDate  options:NSCalendarWrapComponents];
            
            
            
            if([item valueForKey:@"Attendees"]){
                NSMutableArray *members = [[NSMutableArray alloc] init];
                NSArray *attendees = [item valueForKey:@"Attendees"];
                for(NSJSONSerialization *att in attendees){
                    
                    /*if([r.organizerAddress isEqualToString:[[att valueForKey:@"EmailAddress"] valueForKey:@"Address"]]){
                     continue;
                     }*/
                    
                    O365Attendee *attendee = [O365Attendee new];
                    attendee.name = [[att valueForKey:@"EmailAddress"] valueForKey:@"Name"];
                    attendee.email = [[att valueForKey:@"EmailAddress"] valueForKey:@"Address"];
                    attendee.userData = [att valueForKey:@"UserData"];
                    attendee.accept = [[[att valueForKey:@"Status"] valueForKey:@"Response"] isEqualToString:@"Accepted"];
                    [members addObject:attendee];
                }
                
                [members sortUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"accept" ascending:NO],
                                                [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES]]];
                r.attendees = members;
            }
            
            //if([r.endDate compare:[NSDate date]] == NSOrderedDescending){
            [result addObject:r];
            //}
        }
        [result sortUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"startDate" ascending:YES]]];
    }
    
}

#pragma mark Personal


- (void) getPersonalInfoWithUser:(NSString*) userid group:(NSString*)group handler:(PersonalBlock) handler {
    
    if (![self isLoggedOn]) {
        if (handler) {
            NSError *error = [[NSError alloc] initWithDomain:@"ADA" code:401 userInfo:nil];
            handler(nil, error);
        }
        return;
    }
    
    [ServiceCaller getJSONCache:[NSString stringWithFormat:@"%@user/profile-new/%@/%@",
                                 kSERVER,
                                 group,
                                 userid]
                         method:@"GET"
                         header:nil
                      parameter:nil
                        handler:^(NSJSONSerialization *json) {
                            
                            if(handler){
                                handler([self doProcessPersonalData:json], nil);
                            }
                            [ServiceCaller callJSON:[NSString stringWithFormat:@"%@user/profile-new/%@/%@",
                                                     kSERVER,
                                                     group,
                                                     userid]
                                             method:@"GET"
                                             header:nil
                                          parameter:nil
                                            handler:^(NSJSONSerialization *json) {
                                                if(handler){
                                                    handler([self doProcessPersonalData:json], nil);
                                                }
                                            }];
                            
                        }];
    
    //?groupid=2&empno=003758&year=2017
    
}


- (void) getPersonalInfo:(PersonalBlock) handler {
    
    
    [self getPersonalInfoWithUser:self.login.usid group:self.login.group handler:handler];
}

- (void) submitPersonalData:(Personal*) personal handler:(SuccessBlock) handler {
    
    NSJSONSerialization *_json = personal.personalData;
    NSNumber *rowID = [_json valueForKey:@"id"];
    NSString *password = [_json valueForKey:@"password"];
    NSArray *cols = [_json valueForKey:@"columns"];
    [_json setValue:nil forKey:@"result"];
    [_json setValue:nil forKey:@"columns"];
    [_json setValue:nil forKey:@"password"];
    [_json setValue:nil forKey:@"data-date"];
    
    
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:_json options:NSJSONWritingPrettyPrinted error:nil];
    //remove no edtiable permission attribute
    NSJSONSerialization *updateJSON = [AppHelper jsonWithData:jsonData];
    for(ContactColumn *col in personal.columns){
        if(!col.editable && ![col.attributeTH isEqualToString:@"id"] && ![col.attributeEN isEqualToString:@"id"]){
            [updateJSON setValue:nil forKey:col.attributeTH];
            [updateJSON setValue:nil forKey:col.attributeEN];
        }
    }
    
    [updateJSON setValue:rowID forKey:@"id"];
    
    jsonData = [NSJSONSerialization dataWithJSONObject:updateJSON options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    
    [ServiceCaller callJSON:[NSString stringWithFormat:@"%@user/update-account", kSERVER]
                     method:@"POST"
                     header:nil
                  parameter:@{
                              @"json": jsonString
                              }handler:^(NSJSONSerialization *json) {
                                  
                                  if([[json valueForKey:@"result"] boolValue]){
                                      
                                      if(handler){
                                          handler(YES);
                                      }
                                      /*[AppHelper showLoadingWithMessage:[AppHelper getLabelForThai:@"บันทึกสำเร็จ" eng:@"Save Success"] withIcon:@""];
                                       
                                       dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                                       [AppHelper hideLoading];
                                       });
                                       */
                                      
                                      
                                  }else{
                                      if(handler){
                                          handler(NO);
                                      }
                                      /*[AppHelper showAlert:[AppHelper getLabelForThai:@"ไม่สามารถบันทึกได้" eng:@"Cannot Save Data"] handler:^(UIAlertAction * _Nullable action) {
                                       }];*/
                                      
                                  }
                                  [_json setValue:password forKey:@"password"];
                                  [_json setValue:cols forKey:@"columns"];
                                  [_json setValue:[NSNumber numberWithBool:YES] forKey:@"result"];
                                  
                              }];
    
    
}



- (Personal*)doProcessPersonalData:(NSJSONSerialization *)json {
    //NSLog(@"doProcessPersonalData %@", json);
    if ([[json valueForKey:@"result"] boolValue]) {
        
        
        
        Personal *p = [Personal new];
        
        
        if([json valueForKey:@"contact_cover_image"]){
            [json setValue:[NSString stringWithFormat:@"%@:contact-cover", [json valueForKey:@"contact_cover_image"]]  forKey:@"contact_cover_image"];
        }
        
        
        if([json valueForKey:@"recent_cover_image"]){
            p.recentTabImageID = [[json valueForKey:@"recent_cover_image"] stringByAppendingString:@":contact-cover"];
        }
        if([json valueForKey:@"people_cover_image"]){
            p.peopleTabImageID = [[json valueForKey:@"people_cover_image"] stringByAppendingString:@":contact-cover"];
        }
        if([json valueForKey:@"service_cover_image"]){
            p.serviceTabImageID = [[json valueForKey:@"service_cover_image"] stringByAppendingString:@":contact-cover"];
        }
        if([json valueForKey:@"message_cover_image"]){
            p.inboxTabImageID = [[json valueForKey:@"message_cover_image"] stringByAppendingString:@":contact-cover"];
        }
        
        
        p.backgroundImageID = [json valueForKey:@"contact_cover_image"];

        
        p.personalImageURL = [json valueForKey:@"user_image"];
        p.personalInfo = [AppHelper getLabelForThai:[json valueForKey:@"contact_display"] eng:[json valueForKey:@"contact_display_en"]];
        
        
        NSArray *columns = [self doProcessColumns:json];
        p.columns = columns;
        
        //[json setNilValueForKey:@"columns"];
        
        p.personalData = json;
        
        /*
         [ServiceCaller callJSON:[NSString stringWithFormat:@"%@personal/get", del.SERVER]
         method:@"GET"
         header:nil
         parameter:nil
         handler:^(NSJSONSerialization *json) {
         if ([[json valueForKey:@"result"] boolValue]) {
         
         [serviceList removeAllObjects];
         
         [serviceList addObjectsFromArray:[[json valueForKey:@"data"] valueForKey:@"categories"]];
         
         
         //[self.collectionView reloadData];
         [_tableView reloadData];
         }
         }];*/
        
        return p;
    }
    return nil;
}


#pragma mark Shuttle Bus

- (void)doProcessShuttleBusStation:(NSJSONSerialization *)json list:(NSMutableArray<ShuttleBusStation *> *)list {
    NSArray *data = [json valueForKey:@"data"];
    for(NSJSONSerialization *station in data) {
        ShuttleBusStation *s = [ShuttleBusStation new];
        s.stationID = [[station valueForKey:@"id"] intValue];
        s.name = [station valueForKey:@"name"];
        s.coordinate = CLLocationCoordinate2DMake([[station valueForKey:@"lat"] floatValue], [[station valueForKey:@"lon"] floatValue]);
        [list addObject:s];
    }
}

- (void) getShuttleBusStation:(ShuttleBusStationBlock _Nullable) handler {
    NSMutableArray<ShuttleBusStation*> *list = [[NSMutableArray<ShuttleBusStation*> alloc] init];

    [ServiceCaller getJSONCache:[NSString stringWithFormat:@"%@shuttle-bus/get-station", kSERVER]
                     method:@"GET"
                     header:nil
                  parameter:nil
                    handler:^(NSJSONSerialization *json) {
                        [list removeAllObjects];
                        BOOL foundInCache = NO;
                        if(json && [[json valueForKey:@"result"] boolValue]) {
                            [self doProcessShuttleBusStation:json list:list];
                            if(handler) {
                                handler(list, nil);
                            }
                            foundInCache = YES;
                        }
                        
                        [ServiceCaller callJSON:[NSString stringWithFormat:@"%@shuttle-bus/get-station", kSERVER]
                                         method:@"GET"
                                         header:nil
                                      parameter:nil
                                        handler:^(NSJSONSerialization *json) {
                                            if(!foundInCache) {
                                                [list removeAllObjects];
                                                if(json && [[json valueForKey:@"result"] boolValue]) {
                                                    [self doProcessShuttleBusStation:json list:list];
                                                    if(handler) {
                                                        handler(list, nil);
                                                    }
                                                }else{
                                                    if(handler) {
                                                        handler(nil, [[NSError alloc] initWithDomain:@"ADA" code:500 userInfo:nil]);
                                                    }
                                                }
                                            }
                                        }];
                        
                        
                    }];

    
    

}

- (void) shuttleBusTracking:(ShuttleBusRoute* _Nonnull) route handler:(ShuttleBusLocationBlock _Nullable) handler {
    [ServiceCaller callJSON:[NSString stringWithFormat:@"%@shuttle-bus/tracking/%d", kSERVER, route.routeID]
                     method:@"GET"
                     header:nil
                  parameter:nil
                    handler:^(NSJSONSerialization *json) {
                        if(json && [[json valueForKey:@"result"] boolValue]) {
                            NSJSONSerialization *data = [json valueForKey:@"data"];
                            ShuttleBusLocation *location = [ShuttleBusLocation new];
                            location.routeID = route.routeID;
                            location.name = [data valueForKey:@"name"];
                            location.coordinate = CLLocationCoordinate2DMake([[data valueForKey:@"lat"] floatValue], [[data valueForKey:@"lon"] floatValue]);
                            location.speed = [[data valueForKey:@"speed"] intValue];
                            location.direction = [[data valueForKey:@"direction"] intValue];
                            location.engine = [[data valueForKey:@"engine"] intValue];
                            NSDateFormatter *dateFormat = [ADA getDateFormatter];
                            dateFormat.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"us-EN"];
                            [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
                            location.date = [dateFormat dateFromString:[data valueForKey:@"date"]];
                            if(handler) {
                                handler(location, nil);
                            }
                        }else{
                            if(handler) {
                                handler(nil, [[NSError alloc] initWithDomain:@"ADA" code:500 userInfo:nil]);
                            }
                        }
                    }];
    
}

- (void) getShuttleBusRoute:(ShuttleBusBlock) handler {

    /*
    if (![self isLoggedOn]) {
        if (handler) {
            NSError *error = [[NSError alloc] initWithDomain:@"ADA" code:401 userInfo:nil];
            handler(nil, error);
        }
        return;
    }
    */
    NSMutableArray<ShuttleBusRoute*> *list = [[NSMutableArray<ShuttleBusRoute*> alloc] init];
  
    [self getShuttleBusStation:^(NSArray<ShuttleBusStation *> * _Nullable stations, NSError * _Nullable error) {
        if(!stations || stations.count == 0) {
        
            if(handler) {
                handler(nil, nil);
            }
            return;
        }
        
        
        [ServiceCaller getJSONCache:[NSString stringWithFormat:@"%@shuttle-bus/get-place", kSERVER]
                         method:@"GET"
                         header:nil
                      parameter:nil
                        handler:^(NSJSONSerialization *json) {
                            [list removeAllObjects];
                            BOOL foundInCache = NO;
                            if(json && [[json valueForKey:@"result"] boolValue]) {
                                [self doProcessShuttleBus:json stations:stations list:list handler:handler];
                                foundInCache = YES;
                            }
                            [ServiceCaller callJSON:[NSString stringWithFormat:@"%@shuttle-bus/get-place", kSERVER]
                                             method:@"GET"
                                             header:nil
                                          parameter:nil
                                            handler:^(NSJSONSerialization *json) {
                                                if(!foundInCache) {
                                                    [list removeAllObjects];
                                                    [self doProcessShuttleBus:json stations:stations list:list handler:handler];
                                                }
                                            }];
                        }];
        
        
    }];
    //}];
    
}

- (void)doProcessShuttleBus:(NSJSONSerialization *)json stations:(NSArray<ShuttleBusStation*>*) stations list:(NSMutableArray<ShuttleBusRoute*>*) list handler:(ShuttleBusBlock) handler {
    
    //NSLog(@"doProcessShuttleBus");
    if (json && [[json valueForKey:@"result"] boolValue]) {
        NSArray *places = [json valueForKey:@"data"];
        
        __block NSInteger placeCount = places.count;
        for (NSJSONSerialization *place in places) {
            ShuttleBusRoute *r = [ShuttleBusRoute new];
            r.routeID = [[place valueForKey:@"id"] intValue];
            r.routeName = [AppHelper getLabelForThai:[place valueForKey:@"name"] eng:[place valueForKey:@"name_en"]] ;
            if([place valueForKey:@"start_station_id"]) {
                NSArray* station = [stations filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"stationID == %d", [[place valueForKey:@"start_station_id"] intValue]]];
                if(station.count > 0) {
                    
                    r.startPoint = station.firstObject;
                }
            }
            if([place valueForKey:@"end_station_id"]) {
                NSArray* station = [stations filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"stationID == %d", [[place valueForKey:@"end_station_id"] intValue]]];
                if(station.count > 0) {
                    r.endPoint = station.firstObject;
                }
            }
            
            //r.startLocationName = [place valueForKey:@"start_name"];
            //r.endLocationName = [place valueForKey:@"end_name"];
            r.trackingAvailable = [place valueForKey:@"nostra_service"] != nil;
            
            [ServiceCaller callJSON:[NSString stringWithFormat:@"%@shuttle-bus/get-time-table/%ld",
                                     kSERVER, [[place valueForKey:@"id"] longValue]]
                             method:@"GET"
                             header:nil
                          parameter:nil
                            handler:^(id target, NSJSONSerialization *json) {
                                
                                ShuttleBusRoute *route = (ShuttleBusRoute*)r;
                                route.timetable = [self doProcessBusRoute: json];
                                placeCount--;
                                if(placeCount <= 0){
                                    if(handler){
                                        handler(list, nil);
                                    }
                                }
                            } target:r];
            [list addObject:r];
        }
    }
}


- (NSArray*)doProcessBusRoute:(NSJSONSerialization *)json {
    NSMutableArray *timetable = [[NSMutableArray alloc] init];
    if ([[json valueForKey:@"result"] boolValue]) {
        NSArray *times = [json valueForKey:@"data"];
        for (NSJSONSerialization *json in times) {
            ShuttleBusTimeTable *t = [ShuttleBusTimeTable new];
            t.time = [json valueForKey:@"time"];
            t.remark = [json valueForKey:@"remark"];
            [timetable addObject:t];
        }
        return timetable;
    }else{
        return timetable;
    }
}

#pragma mark Quiz

- (void) submitQuiz:(QuizSet*) quizSet :(SuccessBlock) handler {
    if (![self isLoggedOn]) {
        if (handler) {
            //NSError *error = [[NSError alloc] initWithDomain:@"ADA" code:401 userInfo:nil];
            handler(NO);
        }
        return;
    }
    
    NSMutableDictionary *jsonDict = [NSMutableDictionary dictionaryWithDictionary:@{@"answer": [NSMutableArray new]}];
    for (Quiz *quiz in quizSet.quizList) {
        if(quiz.answer != nil){
            for(id choiceID in quiz.answer){
                [[jsonDict valueForKey:@"answer"] addObject:@{
                                                              @"questions_id": [NSNumber numberWithLong:quiz.questionID],
                                                              @"questions_choice_id" : choiceID}];
            }
        }
    }
    
    ////NSLog(@"%@", jsonDict);
    NSJSONSerialization *json = [AppHelper jsonWithData:[NSJSONSerialization dataWithJSONObject:jsonDict options:NSJSONWritingPrettyPrinted error:nil]];
    
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:json
                                                       options:kNilOptions
                                                         error: nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData
                                                 encoding:NSUTF8StringEncoding];
    
    [ServiceCaller callJSON:[NSString stringWithFormat:@"%@quiz/submit-answers", kSERVER]
                     method:@"POST"
                     header:nil
                  parameter:@{@"json": jsonString}
                    handler:^(NSJSONSerialization *json) {
                        ////NSLog(@"%@", json);
                        if(json){
                            if(handler) {
                                handler([[json valueForKey:@"result"] boolValue]);
                            }
                        }else{
                            if(handler) {
                                handler(NO);
                            }
                        }
                    }];
    
}

- (void) getQuiz:(QuizState) state handler:(QuizSetBlock _Nullable) handler{
//- (void) getQuiz:(NSArray* _Nullable) state handler:(QuizSetBlock _Nullable) handler;{
    if (![self isLoggedOn]) {
        if (handler) {
            NSError *error = [[NSError alloc] initWithDomain:@"ADA" code:401 userInfo:nil];
            handler(nil, error);
        }
        return;
    }
    
    NSMutableArray<NSString*> *param = [[NSMutableArray alloc] init];
    
    //if(state != nil) {
        
            if(state & QuizStateSubmited) {
                [param addObject:@"S"];
            }
            if(state & QuizStateExpired) {
                [param addObject:@"E"];
            }
            if(state & QuizStateNotAnswered) {
                [param addObject:@"N"];
            }
        
    //}
    NSString *paramString = [param componentsJoinedByString:@","];

    if([paramString isEqualToString:@""]) {
        [ServiceCaller callJSON:[NSString stringWithFormat:@"%@quiz/get-all", kSERVER]
                         method:@"GET"
                         header:nil
                      parameter:@{@"state": paramString}
                        handler:^(NSJSONSerialization *json) {
                      
                            if(!json || ![[json valueForKey:@"result"] boolValue]){
                                if(handler) {
                                    handler(nil, [[NSError alloc] initWithDomain:@"ADA" code:500 userInfo:nil]);
                                }
                            }
                            NSMutableArray *list = [self doProcessQuiz:json];
                            if(handler){
                                handler(list, nil);
                            }

                        }];
    }else{
        [ServiceCaller getJSONCache:[NSString stringWithFormat:@"%@quiz/get-all", kSERVER]
                             method:@"GET"
                             header:nil
                          parameter:@{@"state": paramString}
                            handler:^(NSJSONSerialization *json) {
                                BOOL loadCache = NO;
                                if(!json || ![[json valueForKey:@"result"] boolValue]){
                                }else{
                                    NSMutableArray *list = [self doProcessQuiz:json];
                                    if(handler){
                                        handler(list, nil);
                                    }
                                    loadCache = YES;
                                }
                                
                                [ServiceCaller callJSON:[NSString stringWithFormat:@"%@quiz/get-all", kSERVER]
                                                 method:@"GET"
                                                 header:nil
                                              parameter:@{@"state": paramString}
                                                handler:^(NSJSONSerialization *json) {
                                                    if(!loadCache) {
                                                        if(!json || ![[json valueForKey:@"result"] boolValue]){
                                                            if(handler) {
                                                                handler(nil, [[NSError alloc] initWithDomain:@"ADA" code:500 userInfo:nil]);
                                                            }
                                                        }
                                                        NSMutableArray *list = [self doProcessQuiz:json];
                                                        if(handler){
                                                            handler(list, nil);
                                                        }
                                                    }
                                                    //[self doProcessShuttleBus:json list:list handler:handler];
                                                }];
                            }];
    }
    
}

- (void) getQuizResult:(QuizSet*) quizSet handler:(QuizResultBlock _Nullable) handler{
    //- (void) getQuiz:(NSArray* _Nullable) state handler:(QuizSetBlock _Nullable) handler;{
    if (![self isLoggedOn]) {
        if (handler) {
            NSError *error = [[NSError alloc] initWithDomain:@"ADA" code:401 userInfo:nil];
            handler(0, nil, error);
        }
        return;
    }
    
    
    [ServiceCaller callJSON:[NSString stringWithFormat:@"%@quiz/get-result/%ld", kSERVER, quizSet.setID]
                     method:@"GET"
                     header:nil
                  parameter:@{@"ownPoint": @"true"}
                    handler:^(NSJSONSerialization *json) {
                        ////NSLog(@"%@", json);
                        if(!json || ![[json valueForKey:@"result"] boolValue]){
                            if(handler) {
                                handler(0, nil, [[NSError alloc] initWithDomain:@"ADA" code:500 userInfo:nil]);
                            }
                        }
                        NSArray<ADAMember*> *list = [self doProcessADAMembers:[json valueForKey:@"winner"]];
                        
                        NSInteger point = 0;
                        point = [[json valueForKey:@"point"] integerValue];
                        if(handler){
                            handler(point, list, nil);
                        }
                        //[self doProcessShuttleBus:json list:list handler:handler];
                    }];
    
}
/*
- (void) getQuizPoll:(QuizSet* _Nonnull) quizSet handler:(QuizPollBlock _Nullable) handler{
    //- (void) getQuiz:(NSArray* _Nullable) state handler:(QuizSetBlock _Nullable) handler;{
    if (![self isLoggedOn]) {
        if (handler) {
            NSError *error = [[NSError alloc] initWithDomain:@"ADA" code:401 userInfo:nil];
            handler(nil, error);
        }
        return;
    }
    
    
    [ServiceCaller callJSON:[NSString stringWithFormat:@"%@quiz/get-pool/%ld", kSERVER, quizSet.setID]
                     method:@"GET"
                     header:nil
                  parameter:@{}
                    handler:^(NSJSONSerialization *json) {
                        if(!json || ![[json valueForKey:@"result"] boolValue]){
                            if(handler) {
                                handler(nil, [[NSError alloc] initWithDomain:@"ADA" code:500 userInfo:nil]);
                            }
                        }
                        NSMutableArray *list = [self doProcessPoll:json];
                        if(handler){
                            handler(list, nil);
                        }
                        //[self doProcessShuttleBus:json list:list handler:handler];
                    }];
    
}

- (NSArray<Quiz*>*) doProcessPoll:(NSJSONSerialization*) json {
    NSMutableArray<Quiz*> *quizs = [[NSMutableArray alloc] init];
    for(NSJSONSerialization *question in [json valueForKey:@"data"]){
        Quiz *quiz = [Quiz new];
        quiz.questionID = [[question valueForKey:@"questions_id"] longValue];
        quiz.seq = [[question valueForKey:@"seq"] intValue];
        quiz.title = [AppHelper getLabelForThai:[question valueForKey:@"questions_th"]
                                            eng:[question valueForKey:@"questions_en"]];
        
        //quiz.answer = [NSSet setWithArray:@[@"1"]];
        quiz.mulitpleChoice = [[question valueForKey:@"questions_type"] intValue] == 2;
        NSMutableArray *choices = [[NSMutableArray alloc] init];
        for(NSJSONSerialization *choiceJSON in [question valueForKey:@"choices"]){
            QuizChoice *choice = [QuizChoice new];
            choice.choiceID = [[choiceJSON valueForKey:@"questions_choice_id"] longValue];
            choice.seq = [[choiceJSON valueForKey:@"seq"] longValue];
            choice.title = [AppHelper getLabelForThai:[choiceJSON valueForKey:@"questions_choice_th"]
                                                  eng:[choiceJSON valueForKey:@"questions_choice_en"]];
            choice.totalPerson = [[choiceJSON valueForKey:@"answers"] count];
            choice.percent = 0;
            [choices addObject:choice];
        }
        quiz.choices = [NSArray arrayWithArray:choices];
        int totalPerson = 0;
        for(QuizChoice *choice in quiz.choices) {
            totalPerson += choice.totalPerson;
        }
        if(totalPerson > 0) {
            for(QuizChoice *choice in quiz.choices) {
                choice.percent = (choice.totalPerson / totalPerson) * 100;
            }
        }
        [quizs addObject:quiz];
    }
    return quizs;
}
*/
/*
- (void) getArchiveQuiz:(QuizSetBlock) handler {
    if (![self isLoggedOn]) {
        if (handler) {
            NSError *error = [[NSError alloc] initWithDomain:@"ADA" code:401 userInfo:nil];
            handler(nil, error);
        }
        return;
    }
    
    [ServiceCaller callJSON:[NSString stringWithFormat:@"%@quiz/get", kSERVER]
                     method:@"GET"
                     header:nil
                  parameter:@{@"active": @"false"}
                    handler:^(NSJSONSerialization *json) {
                        
                        if(!json || ![[json valueForKey:@"result"] boolValue]){
                            if(handler) {
                                handler(nil, [[NSError alloc] initWithDomain:@"ADA" code:500 userInfo:nil]);
                            }
                        }
                        
                        NSMutableArray *list = [self doProcessQuiz:json];
                        
                        if(handler){
                            handler(list, nil);
                        }
                        //[self doProcessShuttleBus:json list:list handler:handler];
                    }];
}
*/
- (NSMutableArray *)doProcessQuiz:(NSJSONSerialization *)json {
    NSDateFormatter *parser = [NSDateFormatter instanceWithUSLocale];
    [parser setDateFormat:@"yyyy-MM-dd HH:mm:ss.S"];
    [parser setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:3600*7]];
    
    NSMutableArray *list = [[NSMutableArray alloc] init];
    for(NSJSONSerialization *jsonSet in [json valueForKey:@"data"]){
        QuizSet *set = [QuizSet new];
        set.setID = [[jsonSet valueForKey:@"questions_set_id"] longValue];
        set.imageID = [jsonSet valueForKey:@"questions_set_image"] ? [NSString stringWithFormat:@"%@:quiz" ,[jsonSet valueForKey:@"questions_set_image"]] : nil;
        set.quizName = [AppHelper getLabelForThai:[jsonSet valueForKey:@"questions_set_th"]
                                              eng:[jsonSet valueForKey:@"questions_set_en"]];
        set.quizDescription = [AppHelper getLabelForThai:[jsonSet valueForKey:@"questions_set_detail_th"]
                                                     eng:[jsonSet valueForKey:@"questions_set_detail_en"]];
        set.startDate = [parser dateFromString:[jsonSet valueForKey:@"questions_set_start_time"]];
        set.endDate = [parser dateFromString:[jsonSet valueForKey:@"questions_set_end_time"]];
        set.answeredMembers = [[jsonSet valueForKey:@"member_answered"] integerValue];
        set.answered = [[jsonSet valueForKey:@"answered"] boolValue];
        set.createDate = [parser dateFromString:[jsonSet valueForKey:@"create_date"]];
        set.answered = [[jsonSet valueForKey:@"answered"] integerValue] == 1;
        set.expired = [[jsonSet valueForKey:@"expired"] integerValue] == 1;
        set.poll = [[jsonSet valueForKey:@"questions_set_type"] integerValue] == 2;
        
        if([jsonSet valueForKey:@"creator_user_id"] && [jsonSet valueForKey:@"creator_user_group"]){
        
            ADAMember *m =  [ADAMember new];
            
            m.name = [[[AppHelper getLabelForThai: [NSString stringWithFormat:@"%@ %@",
                                                    [jsonSet valueForKey:@"firstname_th"],
                                                    [jsonSet valueForKey:@"lastname_th"]]
                                              eng: [NSString stringWithFormat:@"%@ %@",
                                                    [jsonSet valueForKey:@"firstname_en"],
                                                    [jsonSet valueForKey:@"lastname_en"]]] stringByReplacingOccurrencesOfString:@"null" withString:@"" ]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            m.userID = [jsonSet valueForKey:@"user_id"];
            m.group = [jsonSet valueForKey:@"user_group"];
            m.imageURL = [jsonSet valueForKey:@"user_image"];
            m.contactInfo = [AppHelper getLabelForThai:[jsonSet valueForKey:@"contact_display"] eng:[jsonSet valueForKey:@"contact_display_en"]];
            set.creator = m;
        
        }
        
        
        
        set.active = [[jsonSet valueForKey:@"active"] boolValue];
        NSMutableArray *quizList = [[NSMutableArray alloc] init];
        
      
        for(NSJSONSerialization *question in [jsonSet valueForKey:@"questions"]){
            Quiz *quiz = [Quiz new];
            quiz.questionID = [[question valueForKey:@"questions_id"] longValue];
            quiz.seq = [[question valueForKey:@"seq"] intValue];
            quiz.title = [AppHelper getLabelForThai:[question valueForKey:@"questions_th"]
                                                eng:[question valueForKey:@"questions_en"]];
            
            //quiz.answer = [NSSet setWithArray:@[@"1"]];
            quiz.mulitpleChoice = [[question valueForKey:@"questions_type"] intValue] == 2;
            
            
            
            NSMutableArray *choices = [[NSMutableArray alloc] init];
            int totalPerson = 0;
            for(NSJSONSerialization *choiceJSON in [question valueForKey:@"choices"]){
                QuizChoice *choice = [QuizChoice new];
                choice.choiceID = [[choiceJSON valueForKey:@"questions_choice_id"] longValue];
                choice.seq = [[choiceJSON valueForKey:@"seq"] longValue];
                choice.title = [AppHelper getLabelForThai:[choiceJSON valueForKey:@"questions_choice_th"]
                                                      eng:[choiceJSON valueForKey:@"questions_choice_en"]];
                choice.totalPerson = [[choiceJSON valueForKey:@"answers"] count];
                choice.percent = 0;
                totalPerson += choice.totalPerson;
                [choices addObject:choice];
            }
            quiz.choices = [NSArray arrayWithArray:choices];
            
            if(totalPerson > 0) {
                for(QuizChoice *choice in quiz.choices) {
                    choice.percent = (double)((double)choice.totalPerson / (double)totalPerson) * 100;
                }
            }
            
            /*
            NSMutableArray *choices = [[NSMutableArray alloc] init];
            for(NSJSONSerialization *choiceJSON in [question valueForKey:@"choices"]){
                QuizChoice *choice = [QuizChoice new];
                choice.choiceID = [[choiceJSON valueForKey:@"questions_choice_id"] longValue];
                choice.seq = [[choiceJSON valueForKey:@"seq"] longValue];
                choice.title = [AppHelper getLabelForThai:[choiceJSON valueForKey:@"questions_choice_th"]
                                                      eng:[choiceJSON valueForKey:@"questions_choice_en"]];
                choice.correctAnswer = [[choiceJSON valueForKey:@"questions_answer"] integerValue] == 1;
                [choices addObject:choice];
            }
            quiz.choices = [NSArray arrayWithArray:choices];
             */
            [quizList addObject:quiz];
        }
        set.quizList = [NSArray arrayWithArray:quizList];
        [list addObject:set];
    }
    
    
    
    __block BOOL hasNotification = NO;
    [self getLocalNotification:@"quiz" handler:^(NSArray<ADANotification *> * _Nullable nofication, NSError * _Nullable error) {
        hasNotification = YES;
        for (QuizSet *quiz in list) {
            if(nofication){
                for (ADANotification *n in nofication) {
                    //NSLog(@"%@", n.userInfo);
                    if(n.userInfo) {
                        if([[n.userInfo valueForKey:@"setID"] longValue] == quiz.setID) {
                            
                            [self removeLocalNotification:n handler:^(BOOL success) {
                                NSLog(@"remove notification @ %@", n.date);
                            }];
                        }
                    }
                }
            }
            
        }
    }];
    
    /*
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        Feature *quizFeature = [allFeatureList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"type == %d", FeatureTypeQuiz]].firstObject;
        NSDateFormatter *formatter = [NSDateFormatter instanceWithUSLocale];
        formatter.dateFormat = @"dd-MM-yyyy";
        NSDate *now = [NSDate date];
        for (QuizSet *quiz in list) {
            
            if(![[formatter stringFromDate:quiz.endDate] isEqualToString:[formatter stringFromDate:now]]){
                continue;
            }
            
            NSDate *warningDate = [quiz.endDate dateByAddingTimeInterval:-60*15];
            NSLog(@"warningDate %@", warningDate);
            if(warningDate.timeIntervalSince1970 > [NSDate date].timeIntervalSince1970) {
                ADANotification *warning = [[ADANotification alloc] init];
                
                warning.message = [NSString stringWithFormat:@"%@ \"%@\" %@",
                                   quizFeature.name,
                                   quiz.quizName,
                                   [ADA getLabelForThai:@"จะหมดเวลาส่งคำตอบใน 15 นาที" eng:@"will expiring in 15 minutes."]];
                warning.userInfo = @{
                                     @"setID": [NSNumber numberWithLong:quiz.setID],
                                     @"title": quiz.quizName,
                                     @"alarm": warning.message,
                                     @"detail": quiz.quizDescription
                                     };
                warning.messageType = @"quiz";
                warning.featureType = FeatureTypeQuiz;
                warning.date = warningDate;
                __block ADA *ada = self;
                [self addLocalNotification:warning handler:^(BOOL success) {
                    if(success) {
                        NSLog(@"add notification @ %@", warning.date);
                    }
                    if(quiz.endDate.timeIntervalSince1970 > [NSDate date].timeIntervalSince1970) {
                        ADANotification *alarm = [[ADANotification alloc] init];
                        alarm.messageID = alarm.messageID + 80;
                        alarm.message = [NSString stringWithFormat:@"%@ \"%@\" %@",
                                         quizFeature.name,
                                         quiz.quizName,
                                         [ADA getLabelForThai:@"หมดเวลาแล้ว" eng:@"has expired."]];
                        alarm.userInfo = @{
                                           @"setID": [NSNumber numberWithLong:quiz.setID],
                                           @"title": quiz.quizName,
                                           @"alarm": alarm.message,
                                           @"detail": quiz.quizDescription
                                           };
                        alarm.messageType = @"quiz";
                        alarm.featureType = FeatureTypeQuiz;
                        alarm.date = quiz.endDate;
                        
                        [ada addLocalNotification:alarm  handler:^(BOOL success) {
                            NSLog(@"add notification @ %@", alarm.date);
                        }];
                    }
                }];
                
            }else{
                
                if(quiz.endDate.timeIntervalSince1970 > [NSDate date].timeIntervalSince1970) {
                    ADANotification *alarm = [[ADANotification alloc] init];
                    alarm.messageID = alarm.messageID + 80;
                    alarm.message = [NSString stringWithFormat:@"%@ \"%@\" %@",
                                     quizFeature.name,
                                     quiz.quizName,
                                     [ADA getLabelForThai:@"หมดเวลาแล้ว" eng:@"has expired."]];
                    alarm.userInfo = @{
                                       @"setID": [NSNumber numberWithLong:quiz.setID],
                                       @"title": quiz.quizName,
                                       @"alarm": alarm.message,
                                       @"detail": quiz.quizDescription
                                       };
                    alarm.messageType = @"quiz";
                    alarm.featureType = FeatureTypeQuiz;
                    alarm.date = quiz.endDate;
                    
                    [self addLocalNotification:alarm  handler:^(BOOL success) {
                        NSLog(@"add notification @ %@", alarm.date);
                    }];
                }
            }
        }
    });
    */
    
    
    return list;
}

#pragma mark Vote

- (void) submitVote:(Vote*) vote :(SuccessBlock) handler {
    if (![self isLoggedOn]) {
        if (handler) {
            //NSError *error = [[NSError alloc] initWithDomain:@"ADA" code:401 userInfo:nil];
            handler(NO);
        }
        return;
    }
    
    
    NSMutableDictionary *jsonDict = [NSMutableDictionary dictionaryWithDictionary:@{@"answer": [NSMutableArray new]}];
    NSArray<VoteChoice*> *choices = [vote.choices sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"ansSeq" ascending:YES]]];
    for (VoteChoice *choice in choices) {
        if(choice.ansSeq > 0){
            
            [[jsonDict valueForKey:@"answer"] addObject:@{
                                                          @"choice_id": [NSNumber numberWithLong:choice.choiceID],
                                                          @"user_id" : self.login.usid,
                                                          @"user_group": self.login.group}];
            
        }
    }
    
    ////NSLog(@"%@", jsonDict);
    NSJSONSerialization *json = [AppHelper jsonWithData:[NSJSONSerialization dataWithJSONObject:jsonDict options:NSJSONWritingPrettyPrinted error:nil]];
    
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:json
                                                       options:kNilOptions
                                                         error: nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData
                                                 encoding:NSUTF8StringEncoding];
    
    [ServiceCaller callJSON:[NSString stringWithFormat:@"%@vote/submit/%ld", kSERVER, vote.voteID]
                     method:@"POST"
                     header:nil
                  parameter:@{@"json": jsonString}
                    handler:^(NSJSONSerialization *json) {
                        ////NSLog(@"%@", json);
                        if(json){
                            if(handler) {
                                handler([[json valueForKey:@"result"] boolValue]);
                            }
                        }else{
                            if(handler) {
                                handler(NO);
                            }
                        }
                    }];
    
}

- (void) getVote:(VoteState) state handler:(VoteBlock _Nullable) handler {
    if (![self isLoggedOn]) {
        if (handler) {
            NSError *error = [[NSError alloc] initWithDomain:@"ADA" code:401 userInfo:nil];
            handler(nil, error);
        }
        return;
    }
    
    NSMutableArray<NSString*> *param = [[NSMutableArray alloc] init];
    
    //if(state != nil) {
    
    if(state & VoteStateSubmited) {
        [param addObject:@"S"];
    }
    if(state & VoteStateExpired) {
        [param addObject:@"E"];
    }
    if(state & VoteStateNotAnswered) {
        [param addObject:@"N"];
    }
    
    //}
    NSString *paramString = [param componentsJoinedByString:@","];
    if([paramString isEqualToString:@""]) {
        [ServiceCaller callJSON:[NSString stringWithFormat:@"%@vote/get-all", kSERVER]
                         method:@"GET"
                         header:nil
                      parameter:@{@"state": paramString}
                        handler:^(NSJSONSerialization *json) {
                            ////NSLog(@"%@", json);
                            
                                if(!json || ![[json valueForKey:@"result"] boolValue]){
                                    if(handler) {
                                        handler(nil, [[NSError alloc] initWithDomain:@"ADA" code:500 userInfo:nil]);
                                    }
                                }
                                NSMutableArray *list = [self doProcessVote:json];
                                if(handler){
                                    handler(list, nil);
                                }
                            
                        }];
    }else{
        ////NSLog(@"%@", paramString);
        [ServiceCaller getJSONCache:[NSString stringWithFormat:@"%@vote/get-all", kSERVER]
                             method:@"GET"
                             header:nil
                          parameter:@{@"state": paramString}
                            handler:^(NSJSONSerialization *json) {
                                ////NSLog(@"%@", json);
                                BOOL loadCache = NO;
                                if(!json || ![[json valueForKey:@"result"] boolValue]){
                                    if(handler) {
                                        handler(nil, [[NSError alloc] initWithDomain:@"ADA" code:500 userInfo:nil]);
                                    }
                                    
                                }else{
                                    NSMutableArray *list = [self doProcessVote:json];
                                    if(handler){
                                        handler(list, nil);
                                    }
                                    loadCache = YES;
                                }
                                
                                [ServiceCaller callJSON:[NSString stringWithFormat:@"%@vote/get-all", kSERVER]
                                                 method:@"GET"
                                                 header:nil
                                              parameter:@{@"state": paramString}
                                                handler:^(NSJSONSerialization *json) {
                                                    ////NSLog(@"%@", json);
                                                    if(!loadCache) {
                                                        if(!json || ![[json valueForKey:@"result"] boolValue]){
                                                            if(handler) {
                                                                handler(nil, [[NSError alloc] initWithDomain:@"ADA" code:500 userInfo:nil]);
                                                            }
                                                        }
                                                        NSMutableArray *list = [self doProcessVote:json];
                                                        if(handler){
                                                            handler(list, nil);
                                                        }
                                                    }
                                                }];
                            }];
    }
}

- (void) getVoteWithID:(long) voteID handler:(VoteResultBlock _Nullable) handler {
    if (![self isLoggedOn]) {
        if (handler) {
            NSError *error = [[NSError alloc] initWithDomain:@"ADA" code:401 userInfo:nil];
            handler(nil, error);
        }
        return;
    }
    
    [ServiceCaller callJSON:[NSString stringWithFormat:@"%@vote/get/%ld", kSERVER, voteID]
                     method:@"GET"
                     header:nil
                  parameter: nil
                    handler:^(NSJSONSerialization *json) {
                        ////NSLog(@"%@", json);
                        
                        if(!json || ![[json valueForKey:@"result"] boolValue]){
                            if(handler) {
                                handler(nil, [[NSError alloc] initWithDomain:@"ADA" code:500 userInfo:nil]);
                            }
                        }
                        NSMutableArray *list = [self doProcessVote:json];
                        if(handler){
                            handler([list firstObject], nil);
                        }
                    }];
}

- (void) addVote:(Vote*) vote :(VoteResultBlock) handler {
    if (![self isLoggedOn]) {
        if (handler) {
            
            handler(nil,  [[NSError alloc] initWithDomain:@"ADA" code:500 userInfo:nil]);
        }
        return;
    }
    
    NSDateFormatter *formatter =  [NSDateFormatter instanceWithUSLocale];
    formatter.dateFormat = @"dd/MM/yyyy HH:mm";
    
    
    NSMutableDictionary *json =  [NSMutableDictionary dictionaryWithDictionary:@{
                                                                                 @"title" : vote.title,
                                                                                 @"detail" : vote.detail ? vote.detail : @"",
                                                                                 @"view_mode" : [NSNumber numberWithInteger:vote.voteType],
                                                                                 @"start_time" : [formatter stringFromDate:vote.startDate],
                                                                                 @"end_time" : [formatter stringFromDate:vote.endDate],
                                                                                 @"publish": [NSNumber numberWithInteger:1]
                                                                                 }];
    if(vote.imageID) {
        [json setValue:vote.imageID forKey:@"image_id"];
    }
    
    [json setObject:[NSMutableArray new] forKey:@"choices"];
    for(VoteChoice *choice in vote.choices) {
        
        [[json valueForKey:@"choices"] addObject:@{
                                                   @"detail" : choice.detail
                                                   }];
    }
    
    [json setObject:[NSMutableArray new] forKey:@"members"];
    for(ADAMember *member in vote.members) {
        
        [[json valueForKey:@"members"] addObject:@{
                                                   @"user_id" : member.userID,
                                                   @"user_group": member.group
                                                   }];
    }
    
    
    ////NSLog(@"%@", [AppHelper stringWithJSON:[AppHelper jsonWithdictionary:json]]);
    
    [ServiceCaller callJSON:[NSString stringWithFormat:@"%@vote/add", kSERVER]
                     method:@"POST"
                     header:nil
                  parameter:@{@"json" : [AppHelper stringWithJSON:[AppHelper jsonWithdictionary:json]]}
                    handler:^(NSJSONSerialization *json) {
                        ////NSLog(@"%@", json);
                        if(json && [[json valueForKey:@"result"] boolValue]){
                            if(handler) {
                                vote.voteID = [[json valueForKey:@"id"] longValue];
                                handler(vote, nil);
                            }
                        }else{
                            if(handler) {
                                handler(vote, [[NSError alloc] initWithDomain:@"ADA" code:500 userInfo:nil]);
                            }
                        }
                        
                    }];
    
}

- (void) updateVote:(Vote*) vote :(VoteResultBlock) handler {
    if (![self isLoggedOn]) {
        if (handler) {
            
            handler(nil,  [[NSError alloc] initWithDomain:@"ADA" code:500 userInfo:nil]);
        }
        return;
    }
    
    NSDateFormatter *formatter = [NSDateFormatter instanceWithUSLocale];
    formatter.dateFormat = @"dd/MM/yyyy HH:mm";
    
    
    NSMutableDictionary *json =  [NSMutableDictionary dictionaryWithDictionary:@{
                                                                                 @"title" : vote.title,
                                                                                 @"detail" : vote.detail ? vote.detail : @"",
                                                                                 @"view_mode" : [NSNumber numberWithInteger:vote.voteType],
                                                                                 @"start_time" : [formatter stringFromDate:vote.startDate],
                                                                                 @"end_time" : [formatter stringFromDate:vote.endDate],
                                                                                 @"publish": [NSNumber numberWithInteger:1]
                                                                                 }];
    if(vote.imageID) {
        [json setValue:vote.imageID forKey:@"image_id"];
    }
    [json setObject:[NSMutableArray new] forKey:@"choices"];
    for(VoteChoice *choice in vote.choices) {
        [[json valueForKey:@"choices"] addObject:@{
                                                   @"detail" : choice.detail
                                                   }];
    }
    
    [json setObject:[NSMutableArray new] forKey:@"members"];
    for(ADAMember *member in vote.members) {
        [[json valueForKey:@"members"] addObject:@{
                                                   @"user_id" : member.userID,
                                                   @"user_group": member.group
                                                   }];
    }
    
    
    
    
    [ServiceCaller callJSON:[NSString stringWithFormat:@"%@vote/update/%ld", kSERVER, vote.voteID]
                     method:@"POST"
                     header:nil
                  parameter:@{@"json" : [AppHelper stringWithJSON:[AppHelper jsonWithdictionary:json]]}
                    handler:^(NSJSONSerialization *json) {
                        ////NSLog(@"%@", json);
                        if(json && [[json valueForKey:@"result"] boolValue]){
                            if(handler) {
                                if(handler) {
                                    vote.voteID = [[json valueForKey:@"id"] longValue];
                                    handler(vote, nil);
                                }
                            }
                        }else{
                            if(handler) {
                                handler(vote, [[NSError alloc] initWithDomain:@"ADA" code:500 userInfo:nil]);
                            }
                        }
                        
                    }];
    
}

- (void) deleteVote:(Vote*) vote :(SuccessBlock) handler {
    
    [ServiceCaller callJSON:[NSString stringWithFormat:@"%@vote/delete/%ld", kSERVER, vote.voteID]
                     method:@"POST"
                     header:nil
                  parameter:@{@"id": @(vote.voteID)}
                    handler:^(NSJSONSerialization *json) {
                        if(json && [[json valueForKey:@"result"] boolValue]){
                            if(handler) {
                                handler(YES);
                            }
                        }else{
                            if(handler) {
                                handler(NO);
                            }
                        }
                        
                    }];
    
}



- (NSMutableArray *)doProcessVote:(NSJSONSerialization *)json {
    NSDateFormatter *parser = [NSDateFormatter instanceWithUSLocale];
    [parser setDateFormat:@"yyyy-MM-dd HH:mm:ss.S"];
    [parser setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:3600*7]];
    
    NSMutableArray *list = [[NSMutableArray alloc] init];
    ////NSLog(@"%@", json);
    
    for(NSJSONSerialization *jsonSet in [json valueForKey:@"data"]){
        Vote *vote = [Vote new];
        vote.voteID = [[jsonSet valueForKey:@"id"] longValue];
        vote.imageID = [jsonSet valueForKey:@"image_id"] ? [NSString stringWithFormat:@"%@:vote" ,[jsonSet valueForKey:@"image_id"]] : nil;
        vote.title = [jsonSet valueForKey:@"title"];
        vote.detail = [jsonSet valueForKey:@"detail"];
        vote.startDate = [parser dateFromString:[jsonSet valueForKey:@"start_time"]];
        vote.endDate = [parser dateFromString:[jsonSet valueForKey:@"end_time"]];
        vote.createDate = [parser dateFromString:[jsonSet valueForKey:@"create_time"]];
        vote.answered = [[jsonSet valueForKey:@"answered"] integerValue] == 1;
        vote.expired = [[jsonSet valueForKey:@"expired"] integerValue] == 1;

        if([[jsonSet valueForKey:@"view_mode"] intValue] == 1){
            vote.voteType = VoteNormal;
        }else if([[jsonSet valueForKey:@"view_mode"] intValue] == 2){
            vote.voteType = VoteSequence;
        }else if([[jsonSet valueForKey:@"view_mode"] intValue] == 3){
            vote.voteType = VotePrivate;
        }
        
        NSMutableArray<VoteChoice*> *choices = [NSMutableArray new];
        for(NSJSONSerialization *choice in [jsonSet valueForKey:@"choices"]){
            VoteChoice *c = [VoteChoice new];
            c.choiceID = [[choice valueForKey:@"id"] longValue];
            c.seq = [[choice valueForKey:@"seq"] intValue];
            c.detail = [choice valueForKey:@"detail"];
            [choices addObject:c];
        }
        vote.choices = [NSArray arrayWithArray:choices];
        
        NSMutableArray<ADAMember*> *answeredMembers = [NSMutableArray new];
        NSMutableArray<ADAMember*> *members = [NSMutableArray new];
        for(NSJSONSerialization *member in [jsonSet valueForKey:@"members"]){
            
            ADAMember *m = nil;
            if([[member valueForKey:@"answer"] boolValue]) {
                m = [VoteAnswerMember new];
                NSMutableArray<VoteChoice*> *choices = [NSMutableArray new];
                ////NSLog(@"%@", [member valueForKey:@"ansChoices"]);
                for(NSJSONSerialization *choice in [member valueForKey:@"ansChoices"]){
                    VoteChoice *c = [VoteChoice new];
                    c.choiceID = [[choice valueForKey:@"id"] longValue];
                    c.seq = [[choice valueForKey:@"seq"] intValue];
                    c.detail = [choice valueForKey:@"detail"];
                    [choices addObject:c];
                }
                ((VoteAnswerMember*)m).answers = choices;
                
            }else{
                m = [ADAMember new];
            }
            m.name = [[[AppHelper getLabelForThai: [NSString stringWithFormat:@"%@ %@",
                                                    [member valueForKey:@"firstname_th"],
                                                    [member valueForKey:@"lastname_th"]]
                                              eng: [NSString stringWithFormat:@"%@ %@",
                                                    [member valueForKey:@"firstname_en"],
                                                    [member valueForKey:@"lastname_en"]]] stringByReplacingOccurrencesOfString:@"null" withString:@"" ]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            m.userID = [member valueForKey:@"user_id"];
            m.group = [member valueForKey:@"user_group"];
            m.imageURL = [member valueForKey:@"user_image"];
            m.contactInfo = [AppHelper getLabelForThai:[member valueForKey:@"contact_display"] eng:[member valueForKey:@"contact_display_en"]];
            
            if([[member valueForKey:@"answer"] boolValue]) {
                [answeredMembers addObject:m];
            }
            [members addObject:m];
            
            if([[jsonSet valueForKey:@"creator_id"] isEqualToString:m.userID] && [[jsonSet valueForKey:@"creator_group"] isEqualToString:m.group]) {
                vote.creator = m;
            }
        }
        
        vote.answeredMembers = [NSArray arrayWithArray:answeredMembers];
        vote.members = [NSArray arrayWithArray:members];
        
        [list addObject:vote];
    }
    
    /*
    __block BOOL hasNotification = NO;
    [self getLocalNotification:@"vote" handler:^(NSArray<ADANotification *> * _Nullable nofication, NSError * _Nullable error) {
        hasNotification = YES;
        for (Vote *vote in list) {
            
            if(nofication){
                for (ADANotification *n in nofication) {
                    //NSLog(@"%@", n.userInfo);
                    if(n.userInfo) {
                        if([[n.userInfo valueForKey:@"voteID"] longValue] == vote.voteID) {
                            
                            [self removeLocalNotification:n handler:^(BOOL success) {
                                NSLog(@"remove notification @ %@", n.date);
                            }];
                        }
                    }
                }
            }
            
        }
    }];
    
    
    Feature *voteFeature = [allFeatureList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"type == %d", FeatureTypeVote]].firstObject;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        NSDateFormatter *formatter = [NSDateFormatter instanceWithUSLocale];
        formatter.dateFormat = @"dd-MM-yyyy";
        NSDate *now = [NSDate date];
        //NSLog(@"now %@", [formatter stringFromDate:now]);
        for (Vote *vote in list) {
            //NSLog(@"end vote %@", [formatter stringFromDate:vote.endDate]);
            if(![[formatter stringFromDate:vote.endDate] isEqualToString:[formatter stringFromDate:now]]){
                continue;
            }

            NSDate *warningDate = [vote.endDate dateByAddingTimeInterval:-60*15];
            
            if(warningDate.timeIntervalSince1970 > [NSDate date].timeIntervalSince1970) {
                ADANotification *warning = [[ADANotification alloc] init];
                
                warning.message = [NSString stringWithFormat:@"%@ \"%@\" %@",
                                   voteFeature.name,
                                   vote.title,
                                   [ADA getLabelForThai:@"จะหมดเวลาส่งคำตอบใน 15 นาที" eng:@"will expiring in 15 minutes."]];
                warning.userInfo = @{
                                     @"voteID": [NSNumber numberWithLong:vote.voteID],
                                     @"title": vote.title,
                                     @"alarm": warning.message,
                                     @"detail": vote.detail
                                     };
                warning.messageType = @"vote";
                warning.featureType = FeatureTypeVote;
                warning.date = warningDate;
                __block ADA *ada = self;
                [self addLocalNotification:warning handler:^(BOOL success) {
                    if(success) {
                        NSLog(@"add notification @ %@", warning.date);
                    }
                    if(vote.endDate.timeIntervalSince1970 > [NSDate date].timeIntervalSince1970) {
                        ADANotification *alarm = [[ADANotification alloc] init];
                        alarm.messageID = alarm.messageID + 80;
                        alarm.message = [NSString stringWithFormat:@"%@ \"%@\" %@",
                                         voteFeature.name,
                                         vote.title,
                                         [ADA getLabelForThai:@"หมดเวลาแล้ว" eng:@"has expired."]];
                        alarm.userInfo = @{
                                           @"voteID": [NSNumber numberWithLong:vote.voteID],
                                           @"title": vote.title,
                                           @"alarm": alarm.message,
                                           @"detail": vote.detail
                                           };
                        alarm.messageType = @"vote";
                        alarm.featureType = FeatureTypeVote;
                        alarm.date = vote.endDate;
                        
                        [ada addLocalNotification:alarm  handler:^(BOOL success) {
                            NSLog(@"add notification @ %@", alarm.date);
                        }];
                    }
                }];
                
            }else{
                
                if(vote.endDate.timeIntervalSince1970 > [NSDate date].timeIntervalSince1970) {
                    ADANotification *alarm = [[ADANotification alloc] init];
                    
                    alarm.message = [NSString stringWithFormat:@"%@ \"%@\" %@",
                                     voteFeature.name,
                                     vote.title,
                                     [ADA getLabelForThai:@"หมดเวลาแล้ว" eng:@"has expired."]];
                    alarm.userInfo = @{
                                       @"voteID": [NSNumber numberWithLong:vote.voteID],
                                       @"title": vote.title,
                                       @"alarm": alarm.message,
                                       @"detail": vote.detail
                                       };
                    alarm.messageType = @"vote";
                    alarm.featureType = FeatureTypeVote;
                    alarm.date = vote.endDate;
                    [self addLocalNotification:alarm  handler:^(BOOL success) {
                        NSLog(@"add notification @ %@", alarm.date);
                    }];
                }
            }
        }
    });
    */

    return list;
}


#pragma mark Thought
- (void) getThoughtCategory:(ShareYourThoughtCategoryBlock) handler {
    [ServiceCaller callJSON:[NSString stringWithFormat:@"%@system-config/site", kSERVER]
                     method:@"GET"
                     header:nil
                  parameter:nil
                    handler:^(NSJSONSerialization *json) {
                        if (json) {
                            NSMutableArray *list = [[NSMutableArray alloc] init];
                            NSString *categories = [[json valueForKey:@"data"] valueForKey:@"contact_category"];
                            NSData *jsonData = [categories dataUsingEncoding:NSUTF8StringEncoding];
                            NSArray *categoriesList = [AppHelper jsonArrayWithData:jsonData];
                            for(NSJSONSerialization *cate in categoriesList) {
                                ShareYourThoughtCategory *s = [ShareYourThoughtCategory new];
                                
                                s.titleTH = [cate valueForKey:@"title_th"];
                                s.titleEN = [cate valueForKey:@"title_en"];
                                s.icon = [cate valueForKey:@"icon"];
                                s.title = [AppHelper getLabelForThai:s.titleTH
                                                                 eng:s.titleEN];
                                [list addObject:s];
                            }
                            if (handler) {
                                handler(list, nil);
                            }
                        }
                    }];
    
}
- (void) submitThought:(ShareYourThought*) thought handler:(ProgressBlock) progress {
    [ServiceCaller callJSON:[NSString stringWithFormat:@"%@user/profile/%@/%@",
                             kSERVER, self.login.group, self.login.usid]
                     method:@"GET"
                     header:nil
                  parameter:nil
                    handler:^(NSJSONSerialization *json) {
                        if(!json){
                            if(progress){
                                progress(0, 0);
                            }
                            return ;
                        }
                        
                        NSMutableArray *attach = [[NSMutableArray alloc] init];
                        
                        
                        for (UIImage *image in thought.images) {
                            NSData *imgData = UIImageJPEGRepresentation(image, 0.6);
                            [attach addObject:imgData];
                        }
                        
                        NSDictionary *mailBody = @{
                                                   @"from": [json valueForKey:@"EMAILADDR"],
                                                   @"subject": [NSString stringWithFormat:@"[%@ - %@]: %@",
                                                                thought.category.titleTH, thought.category.titleEN, thought.subject] ,
                                                   @"message": [NSString stringWithFormat:@"[ผู้ส่ง - Sender: %@]<br>%@",
                                                                [AppHelper getLabelForThai:[json valueForKey:@"THAINAME"] eng:[json valueForKey:@"ENGNAME"]],
                                                                thought.message],
                                                   @"attach": [NSNumber numberWithInteger:attach.count]
                                                   };
                        
                        ////NSLog(@"%@", mailBody);
                        
                        NSData *data = [NSJSONSerialization dataWithJSONObject:mailBody
                                                                       options:NSJSONWritingPrettyPrinted
                                                                         error:nil];
                        
                        //NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                        // 1. Create `AFHTTPRequestSerializer` which will create your request.
                        AFHTTPRequestSerializer *serializer = [AFHTTPRequestSerializer serializer];
                        
                        // 2. Create an `NSMutableURLRequest`.
                        NSString *strURL = [kSERVER stringByAppendingString:@"mail/send-binary"];
                        
                        NSMutableURLRequest *request =
                        [serializer requestWithMethod:@"POST" URLString:strURL
                                           parameters:nil
                                                error:nil];
                        [request setValue:[AppHelper getUserDataForKey:kADA_CLIENT] forHTTPHeaderField:kADA_CLIENT];
                        NSMutableString *strData = [[NSMutableString alloc] init];
                        [strData appendString:@"data="];
                        [strData appendString:[[[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]  stringByReplacingOccurrencesOfString:@"&" withString:@"%26"] stringByReplacingOccurrencesOfString:@"#" withString:@"%23"]];
                        
                        
                        ////NSLog(@"message length : %ld", (unsigned long)strData.length);
                        
                        [request setHTTPBody:[strData dataUsingEncoding:NSUTF8StringEncoding]];
                        
                        
                        
                        //[request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
                        [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)strData.length] forHTTPHeaderField:@"Content-Length"];
                        
                        
                        [request setValue:[AppHelper getUserDataForKey:kADA_CLIENT]
                       forHTTPHeaderField:kADA_CLIENT];
                        
                        // 3. Create and use `AFHTTPRequestOperationManager` to create an `AFHTTPRequestOperation` from the `NSMutableURLRequest` that we just created.
                        
                        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
                        manager.requestSerializer = serializer;
                        [manager.requestSerializer setValue:[AppHelper getUserDataForKey:kADA_CLIENT] forHTTPHeaderField:kADA_CLIENT];
                        //[manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
                        [manager.requestSerializer setValue:[UIDevice currentDevice].identifierForVendor.UUIDString forHTTPHeaderField:@"secure-device"];
                        
                        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
                        manager.responseSerializer.acceptableContentTypes = nil;
                        //[manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
                        
                        
                        [manager.requestSerializer setValue:@"multipart/form-data"
                                         forHTTPHeaderField:@"Content-Type"];
                        [manager POST:strURL
                           parameters:@{}
            constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
                [formData appendPartWithFileData:data
                                            name:@"message"
                                        fileName:@"message.json"
                                        mimeType:@"application/json"];
                int i = 1;
                for(NSData *file in attach) {
                    [formData appendPartWithFileData:file
                                                name:[NSString stringWithFormat:@"image-%i", i]
                                            fileName:[NSString stringWithFormat:@"image-%i.jpg", i]
                                            mimeType:@"image/jpg"];
                    i++;
                }
            }
                         
                             progress:^(NSProgress * _Nonnull uploadProgress) {
                                 dispatch_async(dispatch_get_main_queue(), ^{
                                     long writen = (long)((double)strData.length * uploadProgress.fractionCompleted);
                                     long total = (long)strData.length;
                                     
                                     if (progress) {
                                         progress(writen, total);
                                     }                                 });
                             } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                 ////NSLog(@"responseObject : %@", responseObject);
                                 long total = (long)strData.length;
                                 if (progress) {
                                     progress(total, total);
                                 }
                                 
                             } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                 if(progress){
                                     progress(0, 0);
                                 }
                                 
                             }];
                        
                        
                    }];
    
}

- (void) getSummitedThought:(ShareYourThoughtBlock) handler {
    [ServiceCaller callJSON:[NSString stringWithFormat:@"%@mail/get-thought", kSERVER]
                     method:@"GET"
                     header:nil
                  parameter:@{@"showOwner": @"true"}
                    handler:^(NSJSONSerialization *json) {
                        NSMutableArray *list = [[NSMutableArray alloc] init];
                        if (json && [[json valueForKey:@"result"] boolValue]) {
                            
                            NSArray *thoughts = [json valueForKey:@"data"];
                            for(NSJSONSerialization *thought in thoughts) {
                                ShareYourThought *s = [ShareYourThought new];
                                NSString *subject = [thought valueForKey:@"subject"];
                                NSArray *subjectArr = [subject componentsSeparatedByString:@":"];
                                NSString *cateString = subjectArr.firstObject;
                                cateString = [cateString stringByReplacingOccurrencesOfString:@"[" withString:@""];
                                cateString = [cateString stringByReplacingOccurrencesOfString:@"]" withString:@""];
                                NSArray *cateArr = [cateString componentsSeparatedByString:@"-"];
                                
                                s.subject = subjectArr.lastObject;
                                
                                NSString *message = [thought valueForKey:@"message"];
                                NSArray *messageArr = [message componentsSeparatedByString:@"<br>"];
                                s.message = messageArr.lastObject;
                                
                                ShareYourThoughtCategory *cate = [ShareYourThoughtCategory new];
                                if(cateArr.count == 2) {
                                    cate.titleTH = [cateArr.firstObject stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                                    cate.titleEN = [cateArr.firstObject stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                                    cate.title = [AppHelper getLabelForThai:cate.titleTH eng:cate.titleEN];
                                }else{
                                    cate.titleTH = cateString;
                                    cate.titleEN = cateString;
                                    cate.title = [AppHelper getLabelForThai:cate.titleTH eng:cate.titleEN];
                                }
                                s.category = cate;
                                s.read = [[thought valueForKey:@"read_status"] intValue] == 1;
                                s.images = [NSMutableArray new];
                                if([thought valueForKey:@"attach_files"]){
                                    NSArray *images = [AppHelper jsonArrayWithData:[[thought valueForKey:@"attach_files"] dataUsingEncoding:NSUTF8StringEncoding]];
                                    for(NSString *imageID in images){
                                        [ServiceCaller callData:[NSString stringWithFormat:@"%@file", kSERVER_HOST]
                                                         method:@"GET"
                                                         header:nil
                                                      parameter:@{@"id": imageID, @"mediaType": @"thought"}
                                                        handler:^(id target, NSData *data) {
                                                            ShareYourThought *s = target;
                                                            [((NSMutableArray*)s.images) addObject:[UIImage imageWithData:data]];
                                                        } target:s];
                                    }
                                }
                                
                                [list addObject:s];
                            }
                            
                        }
                        if (handler) {
                            handler(list, nil);
                        }
                    }];
}

#pragma mark Inbox
- (void) getInbox:(InboxMessageBlock) handler {
    if (![self isLoggedOn]) {
        if (handler) {
            NSError *error = [[NSError alloc] initWithDomain:@"ADA" code:401 userInfo:nil];
            handler(nil, error);
        }
        return;
    }
    [ServiceCaller getJSONCache:[NSString stringWithFormat:@"%@inbox/get-inbox", kSERVER]
                         method:@"GET"
                         header:@{
                                  @"showMsg":@"N"
                                  }
                      parameter:nil
                        handler:^(NSJSONSerialization *json) {
                            [self doProcessInbox:json handler:^(NSArray *arrays, NSError *error) {
                                if(handler){
                                    handler(arrays, nil);
                                }
                            }];
                            
                            
                            
                            [ServiceCaller callJSON:[NSString stringWithFormat:@"%@inbox/get-inbox", kSERVER]
                                             method:@"GET"
                                             header:@{
                                                      @"showMsg":@"N"
                                                      }
                                          parameter:nil
                                            handler:^(NSJSONSerialization *json) {
                                                [self doProcessInbox:json handler:^(NSArray *arrays, NSError *error) {
                                                    if(handler){
                                                        handler(arrays, nil);
                                                    }
                                                }];
                                                
                                            }
                             ];
                        }
     ];
    
    
    
}

- (void)doProcessInbox:(NSJSONSerialization *)json handler:(InboxMessageBlock) handler {
    
    if (!json) {
        return ;
    }
    
    NSArray *inboxs = [json valueForKey:@"data"];
        
    NSMutableArray *list = [NSMutableArray new];
    NSDateFormatter *parser = [NSDateFormatter instanceWithUSLocale];
    parser.dateFormat = @"yyyy-MM-dd HH:mm:ss.S";
    for (NSJSONSerialization *msg in inboxs) {
        
        InboxMessage *inbox = [InboxMessage new];
        inbox.messageID = [[msg valueForKey:@"id"] longValue];
        inbox.title = [msg valueForKey:@"title"];
        inbox.message = [msg valueForKey:@"message"];
        inbox.author = [msg valueForKey:@"author"];
        inbox.date =  [parser dateFromString:[msg valueForKey:@"date"]] ;
        if([msg valueForKey:@"image_id"] && ![[msg valueForKey:@"image_id"] isEqualToString:@""]){
            
            Media *media = [Media new];
            media.mediaID = [[msg valueForKey:@"image_id"] stringByAppendingString:@":inbox"];
            media.mediaType = MediaContentTypePicture;
            
            inbox.image = media;
        }
        if([msg valueForKey:@"file_id"] && ![[msg valueForKey:@"file_id"] isEqualToString:@""]){
            
            Media *media = [Media new];
            media.mediaID = [[msg valueForKey:@"file_id"] stringByAppendingString:@":inbox"];
            media.title = [msg valueForKey:@"file_name"];
            media.mediaType = MediaContentTypePDF;
            
            inbox.pdf = media;
        }
        
        inbox.read = [msg valueForKey:@"read"] && [[msg valueForKey:@"read"] boolValue];
        
        [list addObject:inbox];
    }
    
    
    [list sortUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"date" ascending:NO],
                                 [NSSortDescriptor sortDescriptorWithKey:@"read" ascending:NO]]];
    
    handler(list, nil);
    
    
    
    
    
    
}


- (void) readInboxMessage:(NSArray<InboxMessage*>*) message handler:(SuccessBlock) handler {
    if (![self isLoggedOn]) {
        if (handler) {
            handler(NO);
        }
        return;
    }
    
    NSMutableDictionary *dict = [NSMutableDictionary new];
    for (InboxMessage *msg in message) {
        [dict setObject:@"true" forKey:[NSString stringWithFormat:@"inbox-read-%ld", msg.messageID]];
    }
    
    
    
    
    //NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *kNotificationKey = [NSString stringWithFormat:@"%@-%@@notifications", self.login.usid, self.login.group];
    
    //NSArray *notifications = (NSArray*)   [userDefaults valueForKey:kNotificationKey];
    NSArray *notifications = (NSArray*) [AppHelper getUserDataForKey:kNotificationKey];
    
    __block ADA *_self = self;
    [self setUserSetting:dict :^(BOOL success) {
        if(success){
            //NSLog(@"update read success");
            for (InboxMessage *msg in message) {
                msg.read = YES;
                
                
                if(notifications){
                    NSMutableArray<NSData*> *arr = [NSMutableArray arrayWithArray:notifications];
                    
                    for(NSData *data in arr) {
                        ADANotification *notification = [NSKeyedUnarchiver unarchiveObjectWithData:data];
                        if(notification.json && notification.featureType == FeatureTypeInbox) {
                            long inboxID = (long)[[NSString stringWithFormat:@"%@", [notification.json valueForKey:@"id"]] intValue];
                            if(inboxID == msg.messageID) {
                                [_self removeNotifications:notification handler:nil];
                                break;
                            }
                        }
                    }
                }
                
            }
        }
        if(handler){
            
            handler(success);
        }
    }];
    
}
- (void) removeInboxMessage:(NSArray<InboxMessage*>*) message handler:(SuccessBlock) handler {
    if (![self isLoggedOn]) {
        if (handler) {
            handler(NO);
        }
        return;
    }
    
    NSMutableDictionary *dict = [NSMutableDictionary new];
    for (InboxMessage *msg in message) {
        [dict setObject:@"true" forKey:[NSString stringWithFormat:@"inbox-remove-%ld", msg.messageID]];
    }
    
    [self setUserSetting:dict :^(BOOL success) {
        
        if(handler){
            handler(success);
        }
    }];
}
#pragma mark Company Profile
- (void) getCompanyProfile:(CompanyProfileBlock) handler{
    
    
    [ServiceCaller getJSONCache:[NSString stringWithFormat:@"%@user/profile/%@/%@",
                                 kSERVER, self.login.group, self.login.usid]
                         method:@"GET"
                         header:nil
                      parameter:nil
                        handler:^(NSJSONSerialization *json) {
                            
                            if (json) {
                                NSJSONSerialization *site = [json valueForKey:@"site"] ;
                                CompanyProfile *company = [self doProcessCompanyProfile:site];
                                [self doProcessCompanyMedia:company handler:handler];
                                
                            }
                            [ServiceCaller callJSON:[NSString stringWithFormat:@"%@user/profile/%@/%@",
                                                     kSERVER, self.login.group, self.login.usid]
                                             method:@"GET"
                                             header:nil
                                          parameter:nil
                                            handler:^(NSJSONSerialization *json) {
                                                
                                                if (json) {
                                                    NSJSONSerialization *site = [json valueForKey:@"site"] ;
                                                    CompanyProfile *company = [self doProcessCompanyProfile:site];
                                                    [self doProcessCompanyMedia:company handler:handler];
                                                }
                                            }];
                            
                        }];
    
    
    
}

- (CompanyProfile*) doProcessCompanyProfile:(NSJSONSerialization *) json {
    
    CompanyProfile *company = [CompanyProfile new];
    company.title = [json valueForKey:@"site_name"];
    company.detail = [json valueForKey:@"site_detail"];
    
    return company;
}

- (void) doProcessCompanyMedia:(CompanyProfile*) company handler:(CompanyProfileBlock) handler {
    [ServiceCaller callJSON:[NSString stringWithFormat:@"%@company/get-company-profile", kSERVER]
                     method:@"GET"
                     header:nil
                  parameter:nil
                    handler:^(NSJSONSerialization *json) {
                        
                        if ([[json valueForKey:@"result"] boolValue]) {
                            
                            NSArray *list = [json valueForKey:@"data"];
                            NSMutableArray<Media*> *mediaList = [NSMutableArray<Media*> new];
                            for(NSJSONSerialization *item in list) {
                                Media *m = [Media new];
                                m.title = [item valueForKey:@"name"];
                                m.detail = [item valueForKey:@"detail"];
                                m.icon= [item valueForKey:@"icon"];
                                NSString *type = [item valueForKey:@"file_type"];
                                if([type isEqualToString:@"web"]){
                                    m.mediaType = MediaContentTypeWeb;
                                    m.url = [NSURL URLWithString:m.detail];
                                }else if([type isEqualToString:@"mp4"]){
                                    m.mediaType = MediaContentTypeVideo;
                                    m.mediaID = [NSString stringWithFormat:@"%@:%@", [item valueForKey:@"media_id"], [item valueForKey:@"media_type"]];
                                }else {
                                    m.mediaType = MediaContentTypePDF;
                                    m.mediaID = [NSString stringWithFormat:@"%@:%@", [item valueForKey:@"media_id"], [item valueForKey:@"media_type"]];
                                }
                                [mediaList addObject:m];
                            }
                            company.mediaList = mediaList;
                        }
                        if(handler){
                            handler(company, nil);
                        }
                    }];
}

#pragma mark weather forecast
- (void) getWeatherForecast:(CLLocationCoordinate2D) coordinate handler:(WeatherForecastBlock) handler {
    [ServiceCaller callJSON:[NSString stringWithFormat:@"%@weather/forecast", kSERVER]
                     method:@"GET"
                     header:nil
                  parameter:@{
                              @"lat": [NSString stringWithFormat:@"%.4f", coordinate.latitude],
                              @"lon": [NSString stringWithFormat:@"%.4f", coordinate.longitude]
                              }
                    handler:^(NSJSONSerialization *json) {
                        
                        if(json){
                            
                            WeatherData *data = [self doProcessWeatherForecast: json];
                            if([json valueForKey:@"forecast"]) {
                                NSMutableArray<WeatherData*> *forecasts = [[NSMutableArray<WeatherData*> alloc] init];
                                NSArray *list = [json valueForKey:@"forecast"];
                                for(NSJSONSerialization *forecast in list) {
                                    WeatherData *f = [self doProcessWeatherForecast: forecast];
                                    [forecasts addObject:f];
                                }
                                data.forecast = forecasts;
                            }
                            if(handler) {
                                handler(data, nil);
                            }
                            
                        }else{
                            if(handler){
                                handler(nil, [[NSError alloc] initWithDomain:@"ADA" code:500 userInfo:nil]);
                            }
                        }
                    }];
}

#pragma mark weather forecast
- (NSString*) getWeatherIcon:(NSString*) icon {
    NSScanner *scanner = [NSScanner scannerWithString: [weatherIcon valueForKey:[NSString stringWithFormat:@"%@", icon]]];
    unsigned  value;
    if  ([scanner scanHexInt:&value]){
        NSString *strC = [NSString stringWithFormat: @"%C", (unichar)value];
        return strC;
    }
    return nil;
}

- (WeatherData*) doProcessWeatherForecast:(NSJSONSerialization *) json {
    NSJSONSerialization *main = [json valueForKey:@"main"];
    WeatherData *data = [[WeatherData alloc] init];
    NSDateFormatter *formatter = [NSDateFormatter instanceWithUSLocale];
    formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    if([json valueForKey:@"fetch_date"]) {
        
        data.updateDate = [formatter dateFromString:[NSString stringWithFormat:@"%@ %@:00", [json valueForKey:@"fetch_date"], [json valueForKey:@"fetch_time"]]];
    }
    
    if([json valueForKey:@"dt_txt"]) {
        
        data.weatherDate = [formatter dateFromString:[json valueForKey:@"dt_txt"]];
    }
    
    
    data.temp = [[main valueForKey:@"temp"] floatValue];
    data.tempMax = [[main valueForKey:@"temp_max"] floatValue];
    data.tempMin = [[main valueForKey:@"temp_min"] floatValue];
    
    NSJSONSerialization *weather = [[json valueForKey:@"weather"] firstObject];
    if(weather) {
        data.weather = [weather valueForKey:@"main"];
        data.weatherDescription = [weather valueForKey:@"description"];
        data.iconID = [weather valueForKey:@"id"];
        data.icon =  [self getWeatherIcon:[weather valueForKey:@"id"]];
    }
    
    NSJSONSerialization *cloud = [json valueForKey:@"clouds"];
    if(cloud) {
        data.clouds = [[cloud valueForKey:@"all"] floatValue];
    }
    
    NSJSONSerialization *wind = [json valueForKey:@"wind"];
    if(wind) {
        data.windSpped = [[wind valueForKey:@"speed"] floatValue];
        data.windDegree = [[wind valueForKey:@"deg"] floatValue];
    }
    return data;
}

#pragma mark event register
- (void) getEventRegister:(NSInteger) eventID handler:(EventRegisterBlock) handler{
    [ServiceCaller callJSON:[NSString stringWithFormat:@"%@event-register/get-event/%ld", kSERVER, (long)eventID]
                     method:@"GET"
                     header:nil
                  parameter:nil
                    handler:^(NSJSONSerialization *json) {
                        
                        if(json){
                            
                            EventRegister *data = [EventRegister new];
                            if([json valueForKey:@"data"]) {
                                json = [json valueForKey:@"data"];
                                data.eventID = [[json valueForKey:@"id"] integerValue];
                                data.name = [json valueForKey:@"name"];
                                data.detail = [json valueForKey:@"detail"];
                                data.eventImageID = [NSString stringWithFormat:@"%@:event", [json valueForKey:@"event_image"]];
                                NSDateFormatter *formatter = [ADA getDateFormatter];
                                formatter.dateFormat = @"yyyy-MM-dd HH:mm";
                                data.startDate = [formatter dateFromString:[json valueForKey:@"start_date"]];
                                data.endDate = [formatter dateFromString:[json valueForKey:@"end_date"]];
                                
                                
                                
                                NSMutableArray<EventReviewTopic*> *reviewTopic = [[NSMutableArray alloc] init];
                                
                                NSArray<NSJSONSerialization*> *reviews = [json valueForKey:@"reviews"];
                                
                                for(NSJSONSerialization *topic in [json valueForKey:@"topics"]) {
                                    EventReviewTopic *t = [EventReviewTopic new];
                                    t.topicID = [[topic valueForKey:@"id"] integerValue];
                                    t.topic = [topic valueForKey:@"topic"];
                                    if([[topic valueForKey:@"topic_type"] isEqualToString:@"rating"]) {
                                        t.topicType = EventReviewTopicTypeRating;
                                    }else{
                                        t.topicType = EventReviewTopicTypeComment;
                                    }
                                    [reviewTopic addObject:t];
                                    
                                    NSArray *filtered = [reviews filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"topic_id == %d", t.topicID]];
                                    if(filtered.count > 0) {
                                        if([filtered.firstObject valueForKey:@"rating"]) {
                                            t.rating = [[filtered.firstObject valueForKey:@"rating"] integerValue];
                                        }
                                        t.comment = [filtered.firstObject valueForKey:@"comment"];
                                    }
                                    
                                }
                                
                                EventRegisterMember *member = [EventRegisterMember new];
                                member.reviewTopic = reviewTopic;
                                member.seat = [[json valueForKey:@"seat"] valueForKey:@"seat"];
                                member.memberID = [[[json valueForKey:@"member"] valueForKey:@"id"] integerValue];
                                member.doReview = [reviews count] > 0 || reviewTopic.count == 0;
                                data.member = member;
                                
                            }
                            if(handler) {
                                handler(data, nil);
                            }
                            
                        }else{
                            if(handler){
                                handler(nil, [[NSError alloc] initWithDomain:@"ADA" code:500 userInfo:nil]);
                            }
                        }
                    }];
}

- (NSMutableArray<EventRegister *> *)doProcessMyEventRegister:(NSJSONSerialization *)json {
    NSMutableArray<EventRegister*> *list = [[NSMutableArray alloc] init];
    if([json valueForKey:@"data"]) {
        
        for(NSJSONSerialization *event in [json valueForKey:@"data"]) {
            
            EventRegister *data = [EventRegister new];
            data.eventID = [[event valueForKey:@"id"] integerValue];
            data.name = [event valueForKey:@"name"];
            data.detail = [event valueForKey:@"detail"];
            data.eventImageID = [NSString stringWithFormat:@"%@:event", [event valueForKey:@"event_image"]];
            NSDateFormatter *formatter = [ADA getDateFormatter];
            formatter.dateFormat = @"yyyy-MM-dd HH:mm";
            data.startDate = [formatter dateFromString:[event valueForKey:@"start_date"]];
            data.endDate = [formatter dateFromString:[event valueForKey:@"end_date"]];
            
            NSMutableArray<EventReviewTopic*> *reviewTopic = [[NSMutableArray alloc] init];
            
            NSArray<NSJSONSerialization*> *reviews = [event valueForKey:@"reviews"];
            
            for(NSJSONSerialization *topic in [event valueForKey:@"topics"]) {
                EventReviewTopic *t = [EventReviewTopic new];
                t.topicID = [[topic valueForKey:@"id"] integerValue];
                t.topic = [topic valueForKey:@"topic"];
                if([[topic valueForKey:@"topic_type"] isEqualToString:@"rating"]) {
                    t.topicType = EventReviewTopicTypeRating;
                }else{
                    t.topicType = EventReviewTopicTypeComment;
                }
                [reviewTopic addObject:t];
                
                NSArray *filtered = [reviews filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"topic_id == %d", t.topicID]];
                //NSLog(@"%@", filtered);
                if(filtered.count > 0) {
                    if([filtered.firstObject valueForKey:@"rating"]) {
                        t.rating = [[filtered.firstObject valueForKey:@"rating"] integerValue];
                    }
                    t.comment = [filtered.firstObject valueForKey:@"comment"];
                }
                
            }
            
            data.rating = (int)[[event valueForKey:@"rating"] floatValue];
            EventRegisterMember *member = [EventRegisterMember new];
            member.reviewTopic = reviewTopic;
            member.memberID = [[[event valueForKey:@"member"] valueForKey:@"id"] integerValue];
            member.seat = [[event valueForKey:@"seat"] valueForKey:@"seat"];
            member.doReview = [reviewTopic count] == 0 || [reviews count] > 0;
            data.member = member;
            
            [list addObject:data];
            
        }
        
        
        
        __block BOOL hasNotification = NO;
        [self getLocalNotification:@"eventRegister" handler:^(NSArray<ADANotification *> * _Nullable nofication, NSError * _Nullable error) {
            hasNotification = YES;
            for (EventRegister *event in list) {
                
                if(nofication){
                    for (ADANotification *n in nofication) {
                        //NSLog(@"%@", n.userInfo);
                        if(n.userInfo) {
                            if([[n.userInfo valueForKey:@"memberID"] longValue] == event.member.memberID) {
                                
                                [self removeLocalNotification:n handler:^(BOOL success) {
                                    NSLog(@"remove notification @ %@", n.date);
                                }];
                            }
                        }
                    }
                }
                
            }
        }];
        
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            NSDateFormatter *formatter = [NSDateFormatter instanceWithUSLocale];
            formatter.dateFormat = @"dd-MM-yyyy";
            for (EventRegister *event in list) {
                
                //NSLog(@"name %@ do review %@", event.name, event.member.doReview ? @"Y" : @"N");
                
                if(event.member.doReview) {
                    continue;
                }
                if(event.endDate.timeIntervalSince1970 > [NSDate date].timeIntervalSince1970) {
                    
                    ADANotification *alarm = [[ADANotification alloc] init];
                    
                    alarm.message = [NSString stringWithFormat:@"%@ \"%@\" %@",
                                     [ADA getLabelForThai:@"งานกิจกรรม" eng: @"Event"],
                                     event.name,
                                     [ADA getLabelForThai:@"หมดเวลาแล้ว กรุณาเข้าไปแสดงความคิดเห็น" eng:@"has expired, please review this event."]];
                    alarm.userInfo = @{
                                       @"memberID": [NSNumber numberWithInteger:event.member.memberID],
                                       @"name": event.name,
                                       @"alarm": alarm.message,
                                       @"detail": event.detail
                                       };
                    alarm.messageType = @"eventRegister";
                    alarm.featureType = FeatureTypeEventRegister;
                    alarm.date = event.endDate;
                    [self addLocalNotification:alarm  handler:^(BOOL success) {
                        NSLog(@"add notification @ %@", alarm.date);
                    }];
                }
                
            }
        });
        
    }
    return list;
}

- (void) getMyEventRegister:(EventRegistersBlock _Nullable) handler {
    [ServiceCaller callJSON:[NSString stringWithFormat:@"%@event-register/my-event", kSERVER]
                     method:@"GET"
                     header:nil
                  parameter:nil
                    handler:^(NSJSONSerialization *json) {
                        
                        if(json){
                            
                                
                            NSMutableArray<EventRegister *> * list = [self doProcessMyEventRegister:json];
                            if(handler) {
                                handler(list, nil);
                            }
                            
                        }else{
                            if(handler){
                                handler(nil, [[NSError alloc] initWithDomain:@"ADA" code:500 userInfo:nil]);
                            }
                        }
                    }];
}

- (void) registerEvent:(EventRegister* _Nonnull) event handler:(RegisterResultBlock _Nullable) handler {
    
    NSString *jsonString = nil;
    if(CLLocationCoordinate2DIsValid(event.location)) {
        NSJSONSerialization *json = [AppHelper jsonWithdictionary:@{
                                                                @"lat" : [NSNumber numberWithDouble:event.location.latitude],
                                                                @"lon" : [NSNumber numberWithDouble:event.location.longitude]
                                                                }];
        jsonString = [AppHelper stringWithJSON:json];
    }
    
    
    [ServiceCaller callJSON:[NSString stringWithFormat:@"%@event-register/register/%ld", kSERVER, (long)event.eventID]
                     method:@"POST"
                     header:nil
                  parameter:jsonString ? @{@"json" : jsonString} : nil
                    handler:^(NSJSONSerialization *json) {
                        if(json){
                            //NSLog(@"%@", json);
                            if([[json valueForKey:@"result"] boolValue]) {
                                
                                    if([[json valueForKey:@"registered"] boolValue]) {
                                        EventRegisterMember *member = [EventRegisterMember new];
                                        member.memberID = [[json valueForKey:@"id"] integerValue];
                                        if(handler) {
                                            handler(member, nil);
                                        }
                                    }else{
                                        if(handler) {
                                            handler(nil, nil);
                                        }
                                    }
                            }else{
                                if(handler){
                                    handler(nil, [[NSError alloc] initWithDomain:@"ADA" code:500 userInfo:nil]);
                                }
                            }
                            
                            
                        }else{
                            if(handler){
                                handler(nil, [[NSError alloc] initWithDomain:@"ADA" code:500 userInfo:nil]);
                            }
                        }
                    }];
}

- (void) reviewEvent:(EventRegisterMember* _Nonnull) eventMember handler:(SuccessBlock _Nullable) handler {
    
    
    NSMutableArray *reviews = [[NSMutableArray alloc] init];
    for(EventReviewTopic *topic in eventMember.reviewTopic) {
        if(topic.topicType == EventReviewTopicTypeRating) {
            [reviews addObject:@{
                                 @"topic_id" : [NSNumber numberWithInteger:topic.topicID] ,
                                 @"rating" : [NSNumber numberWithInteger:topic.rating]
                                 }];
        }else{
            [reviews addObject:@{
                                 @"topic_id" : [NSNumber numberWithInteger:topic.topicID] ,
                                 @"comment": topic.comment ? topic.comment : @""
                                 }];
        }
    }
    
    NSJSONSerialization *json = [AppHelper jsonWithdictionary:@{
                                                                @"clientCode": self.login.group,
                                                                @"reviews": reviews
                                                                }];
    
    [ServiceCaller callJSON:[NSString stringWithFormat:@"%@event-register/review/%ld", kSERVER, (long)eventMember.memberID]
                     method:@"POST"
                     header:nil
                  parameter:@{
                              @"json" : [AppHelper stringWithJSON:json]
                              }
                    handler:^(NSJSONSerialization *json) {
                        if(json){
                            
                            if(handler) {
                                handler([[json valueForKey:@"result"] boolValue]);
                            }
                            
                            
                        }else{
                            if(handler){
                                handler(NO);
                            }
                        }
                    }];
}

#pragma mark meeting room reservation

- (void) verifyEWSWith:(NSString*) user password:(NSString*) password handler:(SuccessBlock _Nullable) handler {
    NSDictionary *param = @{
                            @"mailUser" : user,
                            @"mailPassword" : password
                            };
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:param options:NSJSONWritingPrettyPrinted error:nil];
    [ServiceCaller callJSON:[NSString stringWithFormat:@"%@ews/verify", kSERVER]
                     method:@"POST"
                     header:nil
                  parameter: jsonData
                    handler:^(NSJSONSerialization *json) {
                        if(json){
                            if(handler) {
                                handler([[json valueForKey:@"result"] boolValue]);
                            }
                        }else{
                            if(handler) {
                                handler(NO);
                            }
                        }
                    }];
}

- (void) getBuilding:(BuildingBlock _Nullable) handler {
    
    
    [ServiceCaller getJSONCache:[NSString stringWithFormat:@"%@room-reservation/get-buildings", kSERVER]
                         method:@"GET"
                         header:nil
                      parameter: nil
                        handler:^(NSJSONSerialization *json) {
                            BOOL foundInCache = NO;
                            if(json){
                                
                                NSMutableArray<Building*> *buildings = [[NSMutableArray alloc] init];
                                if([[json valueForKey:@"result"] boolValue]) {
                                    
                                    for(NSJSONSerialization *building in [json valueForKey: @"data"]) {
                                        
                                        Building *b = [Building new];
                                        b.buildingID = [[building valueForKey:@"id"] integerValue];
                                        b.name = [building valueForKey:@"buildingName"];
                                        
                                        [buildings addObject:b];
                                    }
                                    if(handler) {
                                        handler(buildings, nil);
                                    }
                                    foundInCache = YES;
                                }
                                
                            }
                            [ServiceCaller callJSON:[NSString stringWithFormat:@"%@room-reservation/get-buildings", kSERVER]
                                             method:@"GET"
                                             header:nil
                                          parameter: nil
                                            handler:^(NSJSONSerialization *json) {
                                                if(foundInCache) {
                                                    return;
                                                }
                                                if(json){
                                                    NSMutableArray<Building*> *buildings = [[NSMutableArray alloc] init];
                                                    if([[json valueForKey:@"result"] boolValue]) {
                                                        
                                                        for(NSJSONSerialization *building in [json valueForKey: @"data"]) {
                                                            
                                                            Building *b = [Building new];
                                                            b.buildingID = [[building valueForKey:@"id"] integerValue];
                                                            b.name = [building valueForKey:@"buildingName"];
                                                            
                                                            [buildings addObject:b];
                                                        }
                                                        if(handler) {
                                                            handler(buildings, nil);
                                                        }
                                                        return;
                                                    }
                                                }
                                                if(handler) {
                                                    handler(nil, [[NSError alloc] initWithDomain:@"ADA" code:500 userInfo:nil]);
                                                }
                                            }];
                        }];
}

- (void) searchAvailableRoom:(ReserveRoom* _Nonnull) room handler:(MeetingRoomBlock _Nullable) handler {
    
    NSDate *beginTime = room.beginTime;
    NSDate *endTime = room.endTime;
    
    NSDateFormatter *formatter = [NSDateFormatter instanceWithUSLocale];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSMutableArray<NSNumber*> *buildings = [[NSMutableArray alloc] init];
    for(Building *building in room.buildings){
        [buildings addObject: [NSNumber numberWithInteger:building.buildingID]];
    }
        
        
    NSDictionary *param = @{
                            @"beginTime" : [formatter stringFromDate:beginTime],
                            @"endTime" : [formatter stringFromDate:endTime],
                            @"buildings":  buildings,
                            @"showReserved": [NSNumber numberWithBool:room.showReserved]
                            };
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:param options:NSJSONWritingPrettyPrinted error:nil];
    
    [ServiceCaller callJSON:[NSString stringWithFormat:@"%@room-reservation/search-room", kSERVER]
                                                method:@"POST"
                                                header:nil
                                             parameter: jsonData
                                               handler:^(NSJSONSerialization *json) {
                                                   if(json){
                                                       NSMutableArray<MeetingRoom*> *rooms = [[NSMutableArray alloc] init];
                                                       if([[json valueForKey:@"result"] boolValue]) {
                                                           
                                                           for(NSJSONSerialization *room in [json valueForKey: @"data"]) {
                                                               MeetingRoom *r = [MeetingRoom new];
                                                               r.name = [room valueForKey:@"name"];
                                                               r.detail = [room valueForKey:@"detail"];
                                                               r.roomID = [[room valueForKey:@"roomID"] integerValue];
                                                               r.numberOfSeat = [[room valueForKey:@"numberOfSeat"] integerValue];
                                                               r.assetCode = [room valueForKey:@"assetCode"];
                                                               r.floorCode = [room valueForKey:@"floorCode"];
                                                               r.floorName = [room valueForKey:@"floorName"];
                                                               NSJSONSerialization *building = [room valueForKey:@"building"];
                                                               Building *b = [Building new];
                                                               b.buildingID = [[building valueForKey:@"id"] integerValue];
                                                               b.name = [building valueForKey:@"buildingName"];
                                                               r.building = b;
                                                               
                                                               r.floorPlanURL = [room valueForKey:@"svgURL"];
                                                               [rooms addObject:r];
                                                           }
                                                           
                                                           if(room.showReserved) {
                                                               for(NSJSONSerialization *room in [json valueForKey: @"busyRooms"]) {
                                                                   MeetingRoomReserved *r = [MeetingRoomReserved new];
                                                                   r.name = [room valueForKey:@"name"];
                                                                   r.detail = [room valueForKey:@"detail"];
                                                                   r.roomID = [[room valueForKey:@"roomID"] integerValue];
                                                                   r.numberOfSeat = [[room valueForKey:@"numberOfSeat"] integerValue];
                                                                   r.assetCode = [room valueForKey:@"assetCode"];
                                                                   r.floorCode = [room valueForKey:@"floorCode"];
                                                                   r.floorName = [room valueForKey:@"floorName"];
                                                                   NSJSONSerialization *building = [room valueForKey:@"building"];
                                                                   Building *b = [Building new];
                                                                   b.buildingID = [[building valueForKey:@"id"] integerValue];
                                                                   b.name = [building valueForKey:@"buildingName"];
                                                                   r.building = b;
                                                                   
                                                                   r.floorPlanURL = [room valueForKey:@"svgURL"];
                                                                   NSArray *timetables = [room valueForKey:@"reserve"];
                                                                   NSMutableArray<ReserveRoom*> *reserved = [[NSMutableArray alloc] init];
                                                                   for(NSJSONSerialization *timetable in timetables) {
                                                                       ReserveRoom *reserve = [ReserveRoom new];
                                                                       reserve.usedTitle = [timetable valueForKey:@"usedTitle"];
                                                                       reserve.usedDetail = [timetable valueForKey:@"usedDetail"];
                                                                       reserve.beginTime = [formatter dateFromString:[timetable valueForKey:@"beginTime"]];
                                                                       reserve.endTime = [formatter dateFromString:[timetable valueForKey:@"endTime"]];
                                                                       if([timetable valueForKey:@"reserveBy"]) {
                                                                           NSJSONSerialization *reserveBy = [timetable valueForKey:@"reserveBy"];
                                                                           [reserveBy setValue:[NSNumber numberWithBool:YES] forKey:@"result"];
                                                                           reserve.reserveTo = [self doProcessADAMember:reserveBy];
                                                                       }
                                                                       [reserved addObject:reserve];
                                                                   }
                                                                   r.reservedRooms = reserved;
                                                                   [rooms addObject:r];
                                                               }
                                                           }
                                                           
                                                           
                                                           
                                                           if(handler) {
                                                               handler(rooms, nil);
                                                           }
                                                           return;
                                                       }
                                                   }
                                                   if(handler) {
                                                       handler(nil, [[NSError alloc] initWithDomain:@"ADA" code:500 userInfo:nil]);
                                                   }
                                               }];

}
- (void) getReservedRoom:(ReserveRoom* _Nonnull) room handler:(ReserveRoomBlock _Nullable) handler {
    NSDateFormatter *formatter = [NSDateFormatter instanceWithUSLocale];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSDate *beginTime = room.beginTime;
    NSDate *endTime = room.endTime;
    NSDictionary *param = @{
                            @"beginTime" : [formatter stringFromDate:beginTime],
                            @"endTime" : [formatter stringFromDate:endTime],
                            @"owner" : [NSNumber numberWithBool: YES]
                            };
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:param options:NSJSONWritingPrettyPrinted error:nil];
    
    [ServiceCaller callJSON:[NSString stringWithFormat:@"%@room-reservation/get-reserved", kSERVER]
                     method:@"POST"
                     header:nil
                  parameter: jsonData
                    handler:^(NSJSONSerialization *json) {
                        if(json){
                            NSMutableArray<ReserveRoom*> *rooms = [[NSMutableArray alloc] init];
                            
                            if([[json valueForKey:@"result"] boolValue]) {
                                
                                for(NSJSONSerialization *timetable in [json valueForKey: @"data"]) {
                                    ReserveRoom *reserve = [ReserveRoom new];
                                    reserve.timeTableData = [timetable valueForKey:@"timeTableData"];
                                    reserve.usedTitle = [timetable valueForKey:@"usedTitle"];
                                    reserve.usedDetail = [timetable valueForKey:@"usedDetail"];
                                    reserve.beginTime = [formatter dateFromString:[timetable valueForKey:@"beginTime"]];
                                    reserve.endTime = [formatter dateFromString:[timetable valueForKey:@"endTime"]];
                                    
                                    NSJSONSerialization *room = [timetable valueForKey:@"room"];
                                    MeetingRoom *r = [MeetingRoom new];
                                    r.name = [room valueForKey:@"name"];
                                    r.detail = [room valueForKey:@"detail"];
                                    r.roomID = [[room valueForKey:@"roomID"] integerValue];
                                    r.numberOfSeat = [[room valueForKey:@"numberOfSeat"] integerValue];
                                    r.assetCode = [room valueForKey:@"assetCode"];
                                    r.floorCode = [room valueForKey:@"floorCode"];
                                    r.floorName = [room valueForKey:@"floorName"];
                                    NSJSONSerialization *building = [room valueForKey:@"building"];
                                    Building *b = [Building new];
                                    b.buildingID = [[building valueForKey:@"id"] integerValue];
                                    b.name = [building valueForKey:@"buildingName"];
                                    r.building = b;
                                    //r.buildingName = [room valueForKey:@"buildingName"];
                                    r.floorPlanURL = [room valueForKey:@"svgURL"];
                                    reserve.room = r;
                                    
                                    
                                    if([room valueForKey:@"attendees"]) {
                                        reserve.attendees = [self doProcessADAMembers:[room valueForKey:@"attendees"]];
                                    }
                                    
                                    [rooms addObject:reserve];
                                }
                                if(handler) {
                                    handler(rooms, nil);
                                }
                                return;
                            }
                        }
                        if(handler) {
                            handler(nil, [[NSError alloc] initWithDomain:@"ADA" code:500 userInfo:nil]);
                        }
                    }];
}
- (void) reserveRoom:(ReserveRoom* _Nonnull) room handler:(SuccessBlock _Nullable) handler {
    NSDateFormatter *formatter = [NSDateFormatter instanceWithUSLocale];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithDictionary:@{
                            @"beginTime" : [formatter stringFromDate:room.beginTime],
                            @"endTime" : [formatter stringFromDate:room.endTime],
                            @"usedTitle" : room.usedTitle,
                            @"usedDetail" : room.usedDetail,
                            @"room": @{
                                    @"name": room.room.name,
                                    @"detail": room.room.detail ? room.room.detail : @"",
                                    @"roomID": [NSNumber numberWithInteger:room.room.roomID],
                                    @"floorCode": room.room.floorCode,
                                    @"floorName": room.room.floorName,
                                    @"buildingName": room.room.building.name
                                    }
                            }];
    if(room.mailUser && room.mailPassword) {
        [param setObject:room.mailUser forKey:@"mailUser"];
        [param setObject:room.mailPassword forKey:@"mailPassword"];
    }
    if(room.attendees) {
        NSMutableArray<NSString*> *attendees = [[NSMutableArray alloc] init];
        for(ADAMember *member in room.attendees) {
            [attendees addObject:[member.userData valueForKey:@"email"]];
        }
        [param setObject:attendees forKey:@"attendees"];
    }
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:param options:NSJSONWritingPrettyPrinted error:nil];
    
    [ServiceCaller callJSON:[NSString stringWithFormat:@"%@room-reservation/create", kSERVER]
                     method:@"POST"
                     header:nil
                  parameter: jsonData
                    handler:^(NSJSONSerialization *json) {
                        if(json){
                            if([[json valueForKey:@"result"] boolValue]) {
                                if(handler) {
                                    handler(YES);
                                }
                                return;
                            }
                        }
                        if(handler) {
                            handler(NO);
                        }
                    }];
}
- (void) editRoom:(ReserveRoom* _Nonnull) room handler:(SuccessBlock _Nullable) handler {
    NSDateFormatter *formatter = [NSDateFormatter instanceWithUSLocale];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithDictionary:@{
                                                                                 @"beginTime" : [formatter stringFromDate:room.beginTime],
                                                                                 @"endTime" : [formatter stringFromDate:room.endTime],
                                                                                 @"usedTitle" : room.usedTitle,
                                                                                 @"usedDetail" : room.usedDetail,
                                                                                 @"timetable": room.timeTableData}];
    if(room.mailUser && room.mailPassword) {
        [param setObject:room.mailUser forKey:@"mailUser"];
        [param setObject:room.mailPassword forKey:@"mailPassword"];
    }
    if(room.attendees) {
        NSMutableArray<NSString*> *attendees = [[NSMutableArray alloc] init];
        for(ADAMember *member in room.attendees) {
            [attendees addObject:[member.userData valueForKey:@"email"]];
        }
        [param setObject:attendees forKey:@"attendees"];
    }
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:param options:NSJSONWritingPrettyPrinted error:nil];
    
    [ServiceCaller callJSON:[NSString stringWithFormat:@"%@room-reservation/edit", kSERVER]
                     method:@"POST"
                     header:nil
                  parameter: jsonData
                    handler:^(NSJSONSerialization *json) {
                        if(json){
                            if([[json valueForKey:@"result"] boolValue]) {
                                if(handler) {
                                    handler(YES);
                                }
                                return;
                            }
                        }
                        if(handler) {
                            handler(NO);
                        }
                    }];
}
- (void) cancelRoom:(ReserveRoom* _Nonnull) room handler:(SuccessBlock _Nullable) handler {
    NSDateFormatter *formatter = [NSDateFormatter instanceWithUSLocale];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithDictionary:@{
                                                                                 @"changeDetail" : room.changeDetail,
                                                                                 @"timetable": room.timeTableData}];
    if(room.mailUser && room.mailPassword) {
        [param setObject:room.mailUser forKey:@"mailUser"];
        [param setObject:room.mailPassword forKey:@"mailPassword"];
    }
    if(room.attendees) {
        NSMutableArray<NSString*> *attendees = [[NSMutableArray alloc] init];
        for(ADAMember *member in room.attendees) {
            [attendees addObject:[member.userData valueForKey:@"email"]];
        }
        [param setObject:attendees forKey:@"attendees"];
    }
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:param options:NSJSONWritingPrettyPrinted error:nil];
    
    [ServiceCaller callJSON:[NSString stringWithFormat:@"%@room-reservation/cancel", kSERVER]
                     method:@"POST"
                     header:nil
                  parameter: jsonData
                    handler:^(NSJSONSerialization *json) {
                        if(json){
                            if([[json valueForKey:@"result"] boolValue]) {
                                if(handler) {
                                    handler(YES);
                                }
                                return;
                            }
                        }
                        if(handler) {
                            handler(NO);
                        }
                    }];
}

- (void) confirmUsingRoom:(ReserveRoom* _Nonnull) room handler:(SuccessBlock _Nullable) handler {
    NSDateFormatter *formatter = [NSDateFormatter instanceWithUSLocale];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithDictionary:@{@"timetable": room.timeTableData}];
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:param options:NSJSONWritingPrettyPrinted error:nil];
    
    [ServiceCaller callJSON:[NSString stringWithFormat:@"%@room-reservation/confirm", kSERVER]
                     method:@"POST"
                     header:nil
                  parameter: jsonData
                    handler:^(NSJSONSerialization *json) {
                        if(json){
                            if([[json valueForKey:@"result"] boolValue]) {
                                if(handler) {
                                    handler(YES);
                                }
                                return;
                            }
                        }
                        if(handler) {
                            handler(NO);
                        }
                    }];
}

#pragma mark VOIP


- (void) sendVOIPCall:(id) targetMember handler:(SuccessBlock _Nullable) handler {
    NSString *userID = nil;
    NSString *userGroup = nil;
    NSDictionary<NSString*, id> *dict = nil;
    if([targetMember isKindOfClass:[ADAMember class]]) {
        ADAMember *member = (ADAMember*) targetMember;
        userID = member.userID;
        userGroup = member.group;
        dict = @{ @"name_th" : self.login.userNameTH,
                  @"name_en" : self.login.userNameEN,
                  @"user_id" : userID,
                  @"user_group": userGroup};
    }else{
        userID = [targetMember valueForKey:@"user_id"];
        userGroup = [targetMember valueForKey:@"user_group"];
        dict = @{ @"name_th" : self.login.userNameTH,
                  @"name_en" : self.login.userNameEN,
                  @"user_id" : userID,
                  @"user_group": userGroup};
    }
    
    
    
    NSJSONSerialization *json = [AppHelper jsonWithdictionary:dict];
    

    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:@{
                                                                 @"messageType": @"voip",
                                                                 @"message": [AppHelper stringWithJSON:json],
                                                                 @"users" : @[@{
                                                                                  @"user_id": userID,
                                                                                  @"user_group": userGroup
                                                                                  }]
                                                                 } options:NSJSONWritingPrettyPrinted error:nil];
    [ServiceCaller callJSON:[NSString stringWithFormat:@"%@pns/send-message",kSERVER]
                     method:@"POST"
                     header:@{@"enc":@"false"}
                  parameter:jsonData
                    handler:^(NSJSONSerialization *json) {
                        if(handler){
                            handler(json && [[json valueForKey:@"result"] boolValue]);
                        }
                    }];
}
- (void) registerVOIPToken:(NSData* _Nonnull) deviceToken handler:(SuccessBlock _Nullable) handler {
    
    if(deviceToken && self.login.group && self.login.usid){
        NSString *deviceTokenStr = [[[[deviceToken description]
                                      stringByReplacingOccurrencesOfString: @"<" withString: @""]
                                     stringByReplacingOccurrencesOfString: @">" withString: @""]
                                    stringByReplacingOccurrencesOfString: @" " withString: @""];
        NSString *UDID = [UIDevice currentDevice].identifierForVendor.UUIDString;
        [ServiceCaller callJSON:[NSString stringWithFormat:@"%@msg-member/register-voip/%@/%@/%@",
                                 kSERVER,
                                 self.login.group,
                                 self.login.usid,
                                 UDID
                                 ]
                         method:@"POST"
                         header:@{@"showMsg":@"N"}
                      parameter:@{
                                  @"device_key": deviceTokenStr
                                  }
                        handler:^(NSJSONSerialization *json) {
                            if(handler){
                                handler(json && [[json valueForKey:@"result"] boolValue]);
                            }
                        }];
    }
}



@end
