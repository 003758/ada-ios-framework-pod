//
//  ChatRoom.h
//  ADAFramework
//
//  Created by Choldarong-r on 4/12/2559 BE.
//  Copyright © 2559 G-ABLE ITS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ADAMember.h"

@interface ChatRoom : NSObject


typedef NS_ENUM(NSInteger, ChatRoomType) {
//typedef enum ChatRoomType : NSUInteger {
    ChatRoomTypeEvent       = 4,
    ChatRoomTypePending     = 3,
    ChatRoomTypeOfficial    = 2,
    ChatRoomTypePrivate     = 1,
    ChatRoomTypeGroup       = 0
    
};


@property (nonatomic, retain) NSString *jsonString;
@property (nonatomic, assign) NSInteger seq;
@property (nonatomic, assign) NSInteger eventID;
@property (nonatomic, retain) NSString *roomID;
@property (nonatomic, retain) NSString *roomName;
@property (nonatomic, retain) NSString *roomDecription;
@property (nonatomic, retain) NSString *imageID;
@property (nonatomic, retain) UIImage *image;
@property (nonatomic, assign) ChatRoomType roomType;
@property (nonatomic, retain) NSString *lastMessage;
@property (nonatomic, assign) NSInteger notificationCount;
@property (nonatomic, retain) NSSet<NSString*> *members;
@property (nonatomic, retain) NSSet<ADAMember*> *adaMembers;
@property (nonatomic, assign) BOOL notification;
@property (nonatomic, assign) BOOL joinRoom;
@property (nonatomic, assign) BOOL memberOnline;
@property (nonatomic, assign) BOOL roomOwner;
@property (nonatomic, assign) BOOL chatBot;
@property (nonatomic, assign) BOOL roomActive;
@property (nonatomic, assign) NSString* targetUserID;
@property (nonatomic, assign) NSString* targetUserGroup;

@property (nonatomic, assign) BOOL eventActive;
@property (nonatomic, assign) BOOL eventExpired;
@property (nonatomic, assign) BOOL eventOwner;
@property (nonatomic, retain) NSString* eventOwnerName;
@property (nonatomic, retain) NSString* eventImageID;
@property (nonatomic, retain) UIImage *eventImage;
@property (nonatomic, retain) NSDate* createDate;
@property (nonatomic, retain) NSDate* startDate;
@property (nonatomic, retain) NSDate* endDate;

@property (nonatomic, retain) UIImage *oImage;
@property (nonatomic, retain) UIImage *oEventImage;
@end
