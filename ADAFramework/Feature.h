//
//  Feature.h
//  ADAFramework
//
//  Created by Choldarong-r on 3/12/2559 BE.
//  Copyright © 2559 G-ABLE ITS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Feature : NSObject


typedef NS_ENUM(NSInteger, FeatureType) {
//typedef enum FeatureType : NSInteger {
    FeatureTypeUnknow              = 0,
    FeatureTypeContact             = 1,
    FeatureTypeNewsFeed            = 2,
    FeatureTypeChat                = 3,
    FeatureTypeInbox               = 4,
    FeatureTypePersonal            = 5,
    FeatureTypeShuttleBus          = 6,
    FeatureTypeWeatherForecast     = 7,
    FeatureTypeHelpDesk            = 8,
    FeatureTypeShareYourThought    = 9,
    FeatureTypeADACall             = 10,
    FeatureTypeCompanyProfile      = 11,
    FeatureTypeBOMs                = 12,
    FeatureTypeApps                = 13,
    FeatureTypeQuiz                = 14,
    FeatureTypeVote                = 15,
    FeatureTypeTaskAssignment      = 16,
    FeatureTypeCalendar            = 17,
    FeatureTypeWeb                 = 18,
    FeatureTypeAppPush             = 19,
    FeatureTypeChatAction          = 20,
    FeatureTypeEventRegister       = 21,
    FeatureTypeRoomReservation     = 22,
    FeatureTypeSetting             = 23,
    FeatureTypeQRScanner           = 24,
    FeatureTypeMyEvent             = 25,
    FeatureTypeGeneralInfo         = 99
};

+ (FeatureType) typeFromString:(NSString*) type;

@property (nonatomic, retain) NSString      *name;
@property (nonatomic, retain) NSString      *icon;
@property (nonatomic, assign) FeatureType   type;
@property (nonatomic, retain) NSString      *appLink;
@property (nonatomic, retain) NSString      *url;
@property (nonatomic, retain) NSString      *webID;
@property (nonatomic, retain) NSString      *appKey;
@property (nonatomic, retain) NSString      *proxyName;
@end
