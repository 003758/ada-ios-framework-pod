//
//  Quiz.h
//  ADAFramework
//
//  Created by Choldarong-r on 29/6/2560 BE.
//  Copyright © 2560 G-ABLE ITS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "QuizChoice.h"

@interface Quiz : NSObject

@property (nonatomic, assign) long questionID;
@property (nonatomic, assign) int seq;
@property (nonatomic, assign) BOOL mulitpleChoice;
@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *imageID;
@property (nonatomic, retain) NSSet *answer;
@property (nonatomic, retain) NSArray<QuizChoice*> *choices;

@end
