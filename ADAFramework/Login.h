//
//  Login.h
//  ADAFramework
//
//  Created by Choldarong-r on 4/12/2559 BE.
//  Copyright © 2559 G-ABLE ITS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Login : NSObject
@property (nonatomic, assign) NSInteger memberID;
@property (nonatomic, retain) NSString *usid;
@property (nonatomic, retain) NSString *group;
@property (nonatomic, retain) NSString *userName;
@property (nonatomic, retain) NSString *userNameTH;
@property (nonatomic, retain) NSString *userNameEN;
@property (nonatomic, retain) NSData *userSerializeData;
@property (nonatomic, retain) NSString *imageID;
@property (nonatomic, retain) NSString *unitName;
@property (nonatomic, assign) NSInteger siteID;
@property (nonatomic, retain) NSString *siteName;
@property (nonatomic, retain) NSString *siteDescription;
@property (nonatomic, retain) NSString *chatServerHost;
@property (nonatomic, retain) NSString *chatServerDomain;
@property (nonatomic, assign) NSInteger chatServerPort;
@property (nonatomic, retain) UIColor *bgColor;
@property (nonatomic, retain) UIColor *tintColor;
@property (nonatomic, retain) NSString *mapNearbyURL;

@property (nonatomic, retain) NSString *logoImageID;
@property (nonatomic, retain) NSString *topLogoImageID;

@end
