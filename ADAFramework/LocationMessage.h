//
//  LocationMessage.h
//  ADAFramework
//
//  Created by Choldarong-r on 1/3/2560 BE.
//  Copyright © 2560 G-ABLE ITS. All rights reserved.
//

#import <Foundation/Foundation.h>
@import CoreLocation;

@interface LocationMessage : NSObject

+ (LocationMessage* _Nonnull) instanceWithLocation:(CLLocation* _Nonnull) location placeName:(NSString* _Nullable) placeName address:(NSString* _Nullable) address;

@property (nonatomic, assign) double latitude;
@property (nonatomic, assign) double longitude;
@property (nonatomic, retain) NSString * _Nullable placeName;
@property (nonatomic, retain) NSString * _Nullable address;
@property (nonatomic, retain) NSString * _Nullable distance;
@end
