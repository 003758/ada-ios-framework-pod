//
//  LocationMessage.m
//  ADAFramework
//
//  Created by Choldarong-r on 1/3/2560 BE.
//  Copyright © 2560 G-ABLE ITS. All rights reserved.
//

#import "LocationMessage.h"

@implementation LocationMessage

+ (LocationMessage* _Nonnull) instanceWithLocation:(CLLocation*) location placeName:(NSString* _Nullable) placeName address:(NSString* _Nullable) address {
    LocationMessage *message = [[LocationMessage alloc] init];
    message.latitude = location.coordinate.latitude;
    message.longitude = location.coordinate.longitude;
    message.address = address;
    message.placeName = placeName;
    if(!placeName) {
        CLGeocoder *decoder = [[CLGeocoder alloc] init];
        [decoder reverseGeocodeLocation:location completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
            if(placemarks && placemarks.count > 0) {
                CLPlacemark *placemark = placemarks.firstObject;
                message.placeName = placemark.name;
                message.address = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@" "];
            }
        }];
    }
    return message;
}

@end
