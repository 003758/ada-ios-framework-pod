//
//  ADAMember.h
//  ADAFramework
//
//  Created by Choldarong-r on 19/12/2559 BE.
//  Copyright © 2559 G-ABLE ITS. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, ADAMemberType) {
    //typedef enum ChatMessageType : NSUInteger {
    ADAMemberTypePeople            = 1,
    ADAMemberTypeChatGroup         = 2
};

@interface ADAMember : NSObject
@property (nonatomic, assign) ADAMemberType memberType;
@property (nonatomic, retain) NSString *roomID;
@property (nonatomic, retain) NSString *roomName;
@property (nonatomic, retain) NSString *imageID;

@property (nonatomic, retain) NSString *userID;
@property (nonatomic, retain) NSString *group;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *contactInfo;
@property (nonatomic, retain) NSString *imageURL;
@property (nonatomic, retain) NSDictionary<NSString*, id> *userData;
- (void)setCurrentLanguage;
+ (ADAMember*) withDictionary:(NSDictionary<NSString*, id>* _Nonnull) member;
@end
