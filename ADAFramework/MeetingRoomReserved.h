//
//  MeetingRoomReserved.h
//  ADAFramework
//
//  Created by Choldarong-r on 7/8/2561 BE.
//  Copyright © 2561 G-ABLE Company Limited. All rights reserved.
//

#import <ADAFramework/ADAFramework.h>

@interface MeetingRoomReserved : MeetingRoom
@property (nonatomic, retain) NSArray<ReserveRoom*> *reservedRooms;
@end
