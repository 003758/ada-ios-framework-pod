//
//  EventRegister.h
//  ADAFramework
//
//  Created by Choldarong-r on 26/3/2561 BE.
//  Copyright © 2561 G-ABLE Company Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EventReviewTopic.h"
#import "EventRegisterMember.h"
#import <CoreLocation/CoreLocation.h>

@interface EventRegister : NSObject

@property (nonatomic, assign) NSInteger eventID;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *detail;
@property (nonatomic, retain) NSString *eventImageID;
@property (nonatomic, retain) NSDate *startDate;
@property (nonatomic, retain) NSDate *endDate;
@property (nonatomic, assign) NSInteger rating;
@property (nonatomic, retain) EventRegisterMember *member;
@property (nonatomic, assign) CLLocationCoordinate2D location;
@end
