//
//  LibSodiumHelper.h
//  ADA
//
//  Created by Choldarong-r on 11/5/2559 BE.
//  Copyright © 2559 G-ABLE ITS. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^ HandshakeHandler)(NSError* connectionError);

@interface LibSodiumHelper : NSObject

+ (void) resetHandshake;
+ (void) handshake:(NSMutableURLRequest *) req handler:(HandshakeHandler) handler;
+ (NSData*) encrypt:(NSData*) data error:(NSError **) error;
+ (NSData*) decrypt:(NSData*) data error:(NSError **) error;
+ (NSData*) decryptFromBase64:(NSData*) data error:(NSError **) error;

@end
