//
//  NSDateFormatter+Extend.m
//  OneMinTimeSheet
//
//  Created by Choldarong-r on 4/5/2557 BE.
//  Copyright (c) 2557 G-ABLE Co., Ltd. All rights reserved.
//

#import "NSDateFormatter+Extend.h"

@implementation NSDateFormatter (Extend)

+ (NSDateFormatter*) instanceWithUSLocale {
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setUSDateFormatter];
    return df;
}

- (void) setUSDateFormatter {
    //[self setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    [self setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en-US"]];
}

+ (NSDateFormatter*) instanceWithTHLocale {
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setTHDateFormatter];
    return df;
}

- (void) setTHDateFormatter {
    //[self setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    [self setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"th-TH"]];
}


- (NSString *) stringFromDefaultLocaleDate:(NSDate *)date {
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:self.dateFormat];
    return [df stringFromDate:date];
}


- (NSString *) stringFromUSLocaleStrng:(NSString *)string {
    
    NSDate *date = [self dateFromString:string];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:self.dateFormat];
    
    
    return [df stringFromDate:date];
}

@end
