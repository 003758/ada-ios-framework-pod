//
//  LibSodiumHelper.m
//  ADA
//
//  Created by Choldarong-r on 11/5/2559 BE.
//  Copyright © 2559 G-ABLE ITS. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "LibSodiumHelper.h"
#import "NSData+Base64.h"
#import "AppHelper.h"
@import NAChloride;

@implementation LibSodiumHelper
static NSURLSession* sharedSessionMainQueue = nil;
static NABoxKeypair* _deviceKeypair;
static NSInteger handShakeCount = 0;
static BOOL handshakeDone;
static BOOL stillHandShake = NO;
static NSMutableURLRequest *currentReq;
static NSMutableArray<HandshakeHandler> *handlers;

+ (void) resetHandshake {
    NSLog(@"resetHandshake");
    _deviceKeypair = nil;
    
    handShakeCount = 0;
    handshakeDone = NO;
    stillHandShake = NO;
    if (currentReq) {
        [self handshake:currentReq handler:nil];
    }
}

+ (void) handshake:(NSMutableURLRequest *) req handler:(HandshakeHandler) handler {
    currentReq = req;
    if(!handlers) {
        handlers = [[NSMutableArray alloc] init];
    }
    if(handshakeDone)  {
        if (handler) {
            handler(nil);
        }
        return;
    }
    if(stillHandShake){
        NSLog(@"stillHandShake %ld", (long)handlers.count);
        if(handler){
            [handlers addObject:handler];
        }
        return;
    }
    //dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0ul);
    //dispatch_async(queue, ^{
    
        if(!sharedSessionMainQueue){
            NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
            //sharedSessionMainQueue = [NSURLSession sessionWithConfiguration:sessionConfig delegate:nil delegateQueue:[NSOperationQueue mainQueue]];
            sharedSessionMainQueue = [NSURLSession sessionWithConfiguration:sessionConfig];
        }
        
        NSError *error = nil;
        _deviceKeypair = [NABoxKeypair generate:&error];
        NSData *publicKey = _deviceKeypair.publicKey;
        stillHandShake = YES;
        [req setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[publicKey length]] forHTTPHeaderField:@"Content-Length"];
        [req setValue:@"application/octet-stream" forHTTPHeaderField:@"Content-Type"];
        
        NSString *udid = [NSString stringWithFormat:@"%@", [UIDevice currentDevice].identifierForVendor.UUIDString];
        
        [req setValue:udid forHTTPHeaderField:@"udid"];
        [req setHTTPMethod:@"POST"];
        [req setHTTPBody: publicKey];
        
        NSURLSessionDataTask *task = [sharedSessionMainQueue dataTaskWithRequest:req completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            
            //NSLog(@"%@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
            if (!data || error) {
                if (handler) {
                    handler(error);
                }
                return ;
            }
            NSError *jsonError;
            NSJSONSerialization *json = [NSJSONSerialization JSONObjectWithData:data
                                                                        options:NSJSONReadingMutableLeaves
                                                                          error:&jsonError];
            if (jsonError) {
                NSLog(@"error : %@", jsonError.localizedDescription);
                if (handler) {
                    handler(jsonError);
                }
                return ;
            }
            
            
            NABox *box = [[NABox alloc] init];
            NSData *nonce = [NSData dataWithBase64EncodedString:[json valueForKey:@"$1"]];
            NSData *serverPublicKey = [NSData dataWithBase64EncodedString:[json valueForKey:@"$2"]];
            NSData *encKey = [NSData dataWithBase64EncodedString:[json valueForKey:@"$3"]];
            NSData *testMessage = [NSData dataWithBase64EncodedString:[json valueForKey:@"message"]];
            
            NABoxKeypair *key = [[NABoxKeypair alloc] initWithPublicKey:serverPublicKey secretKey:_deviceKeypair.secretKey error:nil];
            NSError *decryptError;
            NSData *realKey = [box decrypt:encKey nonce:nonce keypair:key error:&decryptError];
            if (decryptError) {
                NSLog(@"Decrypt fail1 : %@", decryptError);
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    //NSLog(@"try handshake again %ld", handShakeCount);
                    if(handShakeCount < 10) {
                        handShakeCount++;
                        [self handshake:req handler:handler];
                    }else{
                        if (handler) {
                            handler([NSError errorWithDomain:@"ADA" code:500 userInfo:nil]);
                        }
                    }
                });
                return;
            }
            
            /*NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
             [userDefault setObject:nonce  forKey:@"nonce"];
             [userDefault setObject:realKey forKey:@"secret"];
             dispatch_async(dispatch_get_main_queue(), ^{
             [userDefault synchronize];
             });
             */
            
            
            [AppHelper setUserData:nonce forKey:@"sodium.nonce"];
            [AppHelper setUserData:realKey forKey:@"sodium.realKey"];
            
            NSError *decError;
            NSData *testDecryptMessage = [self decrypt:testMessage error:&decError];
            
            if (testDecryptMessage && [@"SECURITY-DONE" isEqualToString:[[NSString alloc] initWithData:testDecryptMessage encoding:NSUTF8StringEncoding]]) {
                handShakeCount = 0;
                NSLog(@"Handshake Done");
                handshakeDone = YES;
                stillHandShake = NO;
                if (handler) {
                    handler(error);
                }
                
                for(HandshakeHandler h in handlers) {
                    h(error);
                }
                [handlers removeAllObjects];
            }else{
                if (handler) {
                    NSMutableDictionary* details = [NSMutableDictionary dictionary];
                    [details setValue:@"Handshake fail." forKey:NSLocalizedDescriptionKey];
                    // populate the error object with the details
                    NSError *error = [NSError errorWithDomain:@"Security" code:200 userInfo:details];
                    handler(error);
                }
            }
            
            
        }];
        
        [task resume];
    //});
}

+ (NSData*) encrypt:(NSData*) data error:(NSError **) error {
    NASecretBox *secretBox = [[NASecretBox alloc] init];
    //NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSData *nonce = [AppHelper getUserDataForKey:@"sodium.nonce"];//[userDefault dataForKey:@"nonce"];
    NSData *secret = [AppHelper getUserDataForKey:@"sodium.realKey"];//[userDefault dataForKey:@"secret"];
    NSData *encryptData = [secretBox encrypt:data nonce:nonce key:secret error:error];
    return encryptData;
}

+ (NSData*) decrypt:(NSData*) data error:(NSError **) error  {
    
    NASecretBox *secretBox = [[NASecretBox alloc] init];
    //NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSData *nonce = [AppHelper getUserDataForKey:@"sodium.nonce"];//[userDefault dataForKey:@"nonce"];
    NSData *secret = [AppHelper getUserDataForKey:@"sodium.realKey"];//[userDefault dataForKey:@"secret"];
    
    NSData *decryptData = [secretBox decrypt:data nonce:nonce key:secret error:error];

        
    return decryptData;
}

+ (NSData*) decryptFromBase64:(NSData*) data error:(NSError **) error   {
    
    if(!data){
        return nil;
    }
        
    NSString *base64 = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    return [self decrypt:[NSData dataWithBase64EncodedString:base64] error:error];
}

@end
