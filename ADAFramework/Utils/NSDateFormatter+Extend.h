//
//  NSDateFormatter+Extend.h
//  OneMinTimeSheet
//
//  Created by Choldarong-r on 4/5/2557 BE.
//  Copyright (c) 2557 G-ABLE Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDateFormatter (Extend)

+ (NSDateFormatter* _Nonnull) instanceWithUSLocale;
- (void) setUSDateFormatter;

+ (NSDateFormatter* _Nonnull) instanceWithTHLocale;
- (void) setTHDateFormatter;
- (NSString * _Nullable) stringFromDefaultLocaleDate:(NSDate * _Nonnull)date;
- (NSString * _Nullable) stringFromUSLocaleStrng:(NSString * _Nonnull)string;

@end
