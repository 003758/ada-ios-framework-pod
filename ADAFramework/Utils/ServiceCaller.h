//
//  ServiceCaller.h
//  GConnect
//
//  Created by Choldarong-r on 11/8/2558 BE.
//  Copyright (c) 2558 G-ABLE ITS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ServiceCaller : NSObject

typedef void (^JSONResponse)(NSJSONSerialization *json);
typedef void (^DataResponse)(NSData *data);
typedef void (^JSONResponseWithTarget)(id target, NSJSONSerialization *json);
typedef void (^DataResponseWithTarget)(id target, NSData *data);
#define HLCSRestMethodGet       @"GET"
#define HLCSRestMethodPost      @"POST"
#define HLCSRestMethodPut       @"PUT"
#define HLCSRestMethodDelete    @"DELETE"
typedef NSString*               HLCSRestMethod;


//+ (NSMutableDictionary<NSString*, id>*) getUserSessionForKey:(NSString*) key;
//+ (NSMutableDictionary<NSString*, id>*) getUserSession;
//+ (void) removeUserSessionWithKey:(NSString*) key;
//+ (void) setUserSession:(NSDictionary*) dict;

+ (void) reLogin:(JSONResponse) handler;
+ (void) signinWithUser:(NSString *)usid password:(NSString*) password group:(NSString*) group handler:(JSONResponse) hander;
+ (void) callJSON:(NSString *)url method:(NSString*) method header:(NSDictionary*) header parameter:(id) parameter handler:(JSONResponse) handler;
+ (void) callJSON:(NSString *)url method:(NSString*) method header:(NSDictionary*) header parameter:(id) parameter handler:(JSONResponseWithTarget) handler target:(id)target;
+ (void) callData:(NSString *)url method:(NSString*) method header:(NSDictionary*) header parameter:(id) parameter handler:(DataResponse) handler;
+ (void) callData:(NSString *)url method:(NSString*) method header:(NSDictionary*) header parameter:(id) parameter handler:(DataResponseWithTarget) handler target:(id)target;


+ (void) getJSONCache:(NSString *)url method:(NSString*) method header:(NSDictionary*) header parameter:(id) parameter handler:(JSONResponse) handler;
+ (void) getJSONCache:(NSString *)url method:(NSString*) method header:(NSDictionary*) header parameter:(id) parameter handler:(JSONResponseWithTarget) handler target:(id)target;
+ (void) getDataCache:(NSString *)url method:(NSString*) method header:(NSDictionary*) header parameter:(id) parameter handler:(DataResponse) handler;
+ (void) getDataCache:(NSString *)url method:(NSString*) method header:(NSDictionary*) header parameter:(id) parameter handler:(DataResponseWithTarget) handler target:(id)target;
+ (void)setUserAgent:(NSMutableURLRequest *)req;

@end
