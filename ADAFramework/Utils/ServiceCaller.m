//
//  ServiceCaller.m
//  GConnect
//
//  Created by Choldarong-r on 11/8/2558 BE.
//  Copyright (c) 2558 G-ABLE ITS. All rights reserved.
//

#import "ServiceCaller.h"
#import "URLConnection.h"
#import "AppHelper.h"
#import "NSData+Encryption.h"
#import "NSData+Base64.h"
#import "UserSession+CoreDataClass.h"
#import "CacheService+CoreDataClass.h"
#import "ManagedObjectContextFactory.h"
#import "LibSodiumHelper.h"
#include <sys/sysctl.h>
#import "ADAFramework.h"
#import <SystemConfiguration/CaptiveNetwork.h>

//#import "NSTimer+Blocks.h"


@implementation ServiceCaller
static NSInteger loginCount = 10;






static BOOL signingIn;
#pragma mark Call Webservice

+ (NSMutableDictionary<NSString*, id>*) getUserSessionForKey:(NSString*) key {
    UserSession* session = (UserSession*)[ManagedObjectContextFactory getObjectOrNewEntityForName:@"UserSession" withValue:key forFieldName:@"key"];
    NSMutableDictionary<NSString*, id> *dict = [NSMutableDictionary new];
    //NSLog(@"sessions : %@", sessions);
    if(session) {
        if(session.key) {
            if(session.data) {
                if(session.boolData) {
                    [dict setObject:[NSKeyedUnarchiver unarchiveObjectWithData:session.data] forKey:session.key];
                }else{
                    [dict setValue:session.data forKey:session.key];
                }
            }else if(session.stringData) {
                [dict setValue:session.stringData forKey:session.key];
            }else if(session.intData) {
                [dict setValue:[NSNumber numberWithInt:session.intData] forKey:session.key];
            }
        }
    }
    //NSLog(@"sessions : %@", dict);
    return dict;
}

+ (NSMutableDictionary<NSString*, id>*) getUserSession {
    NSArray<UserSession*> *sessions = [ManagedObjectContextFactory getAllObjectsFromEntity:@"UserSession" sortedBy:@{}];
    NSMutableDictionary<NSString*, id> *dict = [NSMutableDictionary new];
    //NSLog(@"sessions : %@", sessions);
    if(sessions) {
        for (UserSession *session in sessions) {
            if(session.key) {
                if(session.data) {
                    if(session.boolData) {
                        [dict setObject:[NSKeyedUnarchiver unarchiveObjectWithData:session.data] forKey:session.key];
                    }else{
                        [dict setValue:session.data forKey:session.key];
                    }
                }else if(session.stringData) {
                    [dict setValue:session.stringData forKey:session.key];
                }else if(session.intData) {
                    [dict setValue:[NSNumber numberWithInt:session.intData] forKey:session.key];
                }
            }
        }
    }
    //NSLog(@"sessions : %@", dict);
    return dict;
}

+ (void) setUserSession:(NSDictionary*) dict {
    
    for(NSString *key in  dict.allKeys){
        UserSession* session = (UserSession*)[ManagedObjectContextFactory getObjectOrNewEntityForName:@"UserSession" withValue:key forFieldName:@"key"];
        id value = [dict valueForKey:key];
        if(session) {
            session.key = key;
            if([value isKindOfClass:[NSData class]]) {
                session.data = value;
                session.boolData = NO;
            }else if([value isKindOfClass:[NSString class]]) {
                session.stringData = value;
            }else if([value isKindOfClass:[NSNumber class]]) {
                session.intData = [value intValue];
            }else if([value isKindOfClass:[NSMutableArray class]]) {
                NSData *data = [NSKeyedArchiver archivedDataWithRootObject:value];
                session.boolData = YES;
                session.data = data;
            }
            
        }
    }
    [ManagedObjectContextFactory saveContext];
}

+ (void) removeUserSessionWithKey:(NSString*) key {
    
    UserSession* session = (UserSession*)[ManagedObjectContextFactory getObjectOrNewEntityForName:@"UserSession" withValue:key forFieldName:@"key"];
    if(session) {
        NSLog(@"session : %@", session);
        [ManagedObjectContextFactory deleteObjectThenSave:session];
    }
    
}

+ (void) reLogin:(JSONResponse) handler {
    
    
    
    //NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSString *usid = [AppHelper getUserDataForKey:kUSER];
    NSString *passwd = [AppHelper getUserDataForKey:kPASSWORD];
    NSString *group = [AppHelper getUserDataForKey:kGROUP];
    NSString *UDID = [UIDevice currentDevice].identifierForVendor.UUIDString;
    NSData *passwordData = [[NSData dataWithBase64EncodedString:passwd] AES256DecryptWithKey:UDID];
    NSString *password = [[NSString alloc] initWithData:passwordData encoding:NSUTF8StringEncoding];
    [self signinWithUser:usid password:password group:group handler:handler];
}


+ (void) signinWithUser:(NSString *)usid password:(NSString*) password group:(NSString*) group handler:(JSONResponse) handler {
    
    if (!usid || !password || !group) {
        if (handler) {
            handler(nil);
        }
        return;
    }
 
    NSDictionary *data = @{
                           @"usid": usid,
                           @"password": password,
                           @"group": group
                           };
    
    
    NSString *url = [NSString stringWithFormat:@"%@user/signin", kSERVER];
    
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:data options:kNilOptions error:nil];
    NSError *error;
    NSData *encData = [LibSodiumHelper encrypt:jsonData error:&error];
    //NSLog(@"enc len %lu", (unsigned long)encData.length);
    if(error){
        NSLog(@"enc error %@", error.localizedDescription);
    }
    NSMutableURLRequest *req = [URLConnection requestFromURL:url
                                                      method:@"POST"
                                                 contentType:@"application/octet-stream"
                                                    data: encData];
    
    [req setValue:[UIDevice currentDevice].identifierForVendor.UUIDString forHTTPHeaderField:kSECURE_DEVICE];
    
    //NSUserDefaults *session = [NSUserDefaults standardUserDefaults];
    
    [req setValue:group forHTTPHeaderField:kADA_CLIENT];
    [req setValue:[AppHelper getUserDataForKey:kLanguage] forHTTPHeaderField:kLanguage];
    [req setValue:[[NSBundle bundleForClass:[ADA class]].infoDictionary valueForKey:@"CFBundleShortVersionString"]  forHTTPHeaderField:@"sdk-version"];
    NSString *version = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    
    [req setValue:version forHTTPHeaderField:@"app-version"];
    signingIn = YES;
    
    
    [[URLConnection getInstance] sendAsynchronousRequest:req
                                       completionHandler:^(NSURLResponse *response, NSData *encData, NSError *connectionError) {
                                           signingIn = NO;
                                           
                                           //[AppHelper logData:encData];
                                           
                                           NSError *decError;
                                           NSData *data = [LibSodiumHelper decryptFromBase64:encData error:&decError];
                                           
                                           if(decError){
                                               
                                               if (handler) {
                                                   handler(nil);
                                               }
      
                                               return;
                                           }
                                           
                                           loginCount = 10;
  
                                           NSJSONSerialization *jsonData = [AppHelper jsonWithData:data];

                                           if (![[jsonData valueForKey:@"result"] boolValue]) {
                                               if (handler) {
                                                   handler(nil);
                                                   return;
                                               }
                                           }

                                           [AppHelper setUserData:usid forKey:kUSER];
                                           [AppHelper setUserData:group forKey:kGROUP];
                                           [AppHelper setUserData:group forKey:kADA_CLIENT];
                                           
                                           
                                           //[userDefaults setBool:YES forKey:kLOGIN];
                                           
                                           NSData *passwordData = [password dataUsingEncoding:NSUTF8StringEncoding];
                                           NSString *UDID = [UIDevice currentDevice].identifierForVendor.UUIDString;
                                           NSData *encPassword = [passwordData AES256EncryptWithKey: UDID];
                                           NSData *encPass64 = [encPassword base64EncodedDataWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
                                           NSString *strEncPass = [[NSString alloc] initWithData:encPass64 encoding:NSUTF8StringEncoding];

                                           [AppHelper setUserData:strEncPass forKey:kPASSWORD];

                                           if (handler) {
                                               
                                               
                                               handler(jsonData);
                                           }
                                       }
     ];
}

+ (void) callJSON:(NSString *)url method:(NSString*) method header:(NSDictionary*) header parameter:(id) parameter handler:(JSONResponse) handler {
    [self callJSON:url method:method header:header parameter:parameter handler:^(id target, NSJSONSerialization *json) {
        if (handler) {
            handler(json);
        }
    } target:nil];
    
}


+ (void) callJSON:(NSString *)url method:(NSString*) method header:(NSDictionary*) header parameter:(id) parameter handler:(JSONResponseWithTarget) handler target:(id)target {
    NSMutableURLRequest *req = [URLConnection requestFromURL:url
                                                      method:method
                                                   parameter:parameter];
    
    
    if (header) {
        NSArray *keys = header.allKeys;
        for (NSString *strKey in keys) {
            [req setValue:[header valueForKey:strKey] forHTTPHeaderField:strKey];
        }
    }
    
    [req setValue:[UIDevice currentDevice].identifierForVendor.UUIDString forHTTPHeaderField:kSECURE_DEVICE];
    [req setValue:@"iOS" forHTTPHeaderField:@"device_os"];
    [req setValue:@"iOS" forHTTPHeaderField:@"device-os"];
    
    //NSUserDefaults *session = [NSUserDefaults standardUserDefaults];
    
//    NSLog(@"client %@", [AppHelper getUserDataForKey:kADA_CLIENT]);
    //NSData *encodedObject = [AppHelper getUserDataWithAttributeName:@"login"];
    NSString *group = [AppHelper getUserDataForKey:kADA_CLIENT];//[AppHelper getUserDataWithAttributeName:kADA_CLIENT];
    __block NSString *lang = [AppHelper getUserDataForKey:kLanguage];//[AppHelper getUserDataWithAttributeName:kLanguage];
    
    
    NSString *usid = [AppHelper getUserDataForKey:kUSER];
    NSString *passwd = [AppHelper getUserDataForKey:kPASSWORD];
    NSString *UDID = [UIDevice currentDevice].identifierForVendor.UUIDString;
    
    NSData *passwordData = [[NSData dataWithBase64EncodedString:passwd] AES256DecryptWithKey:UDID];
    NSString *password = [[NSString alloc] initWithData:passwordData encoding:NSUTF8StringEncoding];
 
    
    Login *object = [AppHelper getUserDataForKey:@"login"];
    if(object) {
        group = object.group;
        usid = object.usid;
        NSData *passwordData = [object.userSerializeData AES256DecryptWithKey:UDID];
        password = [[NSString alloc] initWithData:passwordData encoding:NSUTF8StringEncoding];
    }
    
//    NSLog(@"code : %@", group);
    
    [req setValue:group forHTTPHeaderField:kADA_CLIENT];
    [req setValue:lang forHTTPHeaderField:kLanguage];
    [req setValue:[[NSBundle bundleForClass:[ADA class]].infoDictionary valueForKey:@"CFBundleShortVersionString"]  forHTTPHeaderField:@"sdk-version"];
    NSString *version = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    [req setValue:version forHTTPHeaderField:@"app-version"];
    [self setUserAgent:req];
    
    if(loginCount < 0){
        loginCount = 10;
    }

    
    NSString *kCacheService = [NSString stringWithFormat:@"%@-%@-%@-%@-%@", url, method, parameter,
                               group,
                               lang];
    
    [[URLConnection getInstance] sendAsynchronousRequest:req completionHandler:^(NSURLResponse *response, NSData *encData, NSError *connectionError) {
        if(connectionError) {
            
            NSLog(@"Connection Error : %@", connectionError);
            NSLog(@"URL : %@", url);
            NSLog(@"Param : %@", parameter);
            if([connectionError.description rangeOfString:@"The request timed out."].location != NSNotFound) {
                NSLog(@"Stop loading");
                if (handler) {
                    handler(target, nil);
                }
                return;
            }
        }
        
        
        NSError *decError;
        NSData *data = nil;
        if(header && [header valueForKey:@"enc"] && [[header valueForKey:@"enc"] isEqualToString:@"false"]){
            data = encData;

        }else{
            data = [LibSodiumHelper decryptFromBase64:encData error:&decError];
        }
        
        if(decError) {
            NSLog(@"Decryption Error : %@", decError);
        }
 
        if(decError || !data){
            if (loginCount <= 1) {

                if (handler) {
                    handler(target, nil);
                }
                loginCount = 0;

                return;
            }
            NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@sodium-handshake", kSERVER_HOST]]];
            [req setValue:group forHTTPHeaderField:kADA_CLIENT];
            [req setValue:lang forHTTPHeaderField:kLanguage];
            
            [LibSodiumHelper handshake:req handler:^(NSError *connectionError) {
                if (!connectionError) {
  
                    if(signingIn){
                        __block NSTimer *timer = [NSTimer timerWithTimeInterval:0.5 repeats:YES block:^(NSTimer * _Nonnull timer){
                            if (!signingIn) {
                                [ServiceCaller signinWithUser:usid
                                                     password:password
                                                        group:group
                                                      handler:^(NSJSONSerialization *json) {
                                                          //AppDelegate *app = [UIApplication sharedApplication].delegate;
                                                          //NSLog(@"delay after login after 1 sec.");
                                                          dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                                                              ////NSLog(@"recall service.");
                                                              [ServiceCaller callJSON:url
                                                                               method:method
                                                                               header:header
                                                                            parameter:parameter
                                                                              handler:handler
                                                                               target:target
                                                               ];
                                                          });
                                                      }];
                                [timer invalidate];
                                
                            }
                        }];
                        NSRunLoop *runner = [NSRunLoop currentRunLoop];
                        [runner addTimer:timer forMode: NSDefaultRunLoopMode];
                    }else{
                        [ServiceCaller signinWithUser:usid
                                             password:password
                                                group:group
                                              handler:^(NSJSONSerialization *json) {
                                                  //AppDelegate *app = [UIApplication sharedApplication].delegate;
                                                  //NSLog(@"delay after login after 1 sec.");
                                                  dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                                                      //NSLog(@"recall service.");
                                                      [ServiceCaller callJSON:url
                                                                       method:method
                                                                       header:header
                                                                    parameter:parameter
                                                                      handler:handler
                                                                       target:target
                                                       ];
                                                  });
                                              }];
                    }
                }
            }];
            return;
        }
        
        
        
        if (connectionError) {
            
            
            if (handler) {
                handler(target, nil);
            }
            
            return;
        }
        
        
        
        if (handler) {
            //[AppHelper logData:data];
            
            NSJSONSerialization *json = [AppHelper jsonWithData:data];
            if (json && ![[json valueForKey:@"result"] boolValue] &&
                [[json valueForKey:@"code"] integerValue] == 100 &&
                ![@"true" isEqualToString:[header valueForKey:@"validate"]]) {
                
                if (loginCount <= 1) {
//                    NSLog(@"exit3");
                    
                    
                    
                    if (handler) {
                        handler(target, nil);
                    }
                    loginCount = 0;
                    return;
                }
                
                
                loginCount--;
                
                if(signingIn){
                    __block NSTimer *timer = [NSTimer timerWithTimeInterval:0.5 repeats:YES block:^(NSTimer * _Nonnull timer){
                        if (!signingIn) {
                            [ServiceCaller signinWithUser:usid
                                                 password:password
                                                    group:group
                                                  handler:^(NSJSONSerialization *json) {
                                                      //AppDelegate *app = [UIApplication sharedApplication].delegate;
//                                                      NSLog(@"delay after login after 1 sec.");
                                                      dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
  //                                                        NSLog(@"recall service.");
                                                          [ServiceCaller callJSON:url
                                                                           method:method
                                                                           header:header
                                                                        parameter:parameter
                                                                          handler:handler
                                                                           target:target
                                                           ];
                                                      });
                                                  }];
                            [timer invalidate];
                            
                        }
                    }];
                    NSRunLoop *runner = [NSRunLoop currentRunLoop];
                    [runner addTimer:timer forMode: NSDefaultRunLoopMode];
                }else{
                    [ServiceCaller signinWithUser:usid
                                         password:password
                                            group:group
                                          handler:^(NSJSONSerialization *json) {
                                              //AppDelegate *app = [UIApplication sharedApplication].delegate;
                                              //NSLog(@"delay after login after 1 sec.");
                                              dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                                                //  NSLog(@"recall service.");
                                                  [ServiceCaller callJSON:url
                                                                   method:method
                                                                   header:header
                                                                parameter:parameter
                                                                  handler:handler
                                                                   target:target
                                                   ];
                                              });
                                          }];
                    
                }
                
            }else{
                
                CacheService *cache = [ManagedObjectContextFactory
                                       getObjectFromEntity:@"CacheService"
                                       withValue:kCacheService
                                       fromFieldName:@"serviceKey"];
                if (!cache) {
                    cache = (CacheService*)[ManagedObjectContextFactory
                                            newEntityForName:@"CacheService"];
                    cache.serviceKey = kCacheService;
                }
                cache.serviceData = data;
                [ManagedObjectContextFactory saveContext];
                
                if (handler) {
                    handler(target, json);
                }
                
            }
        }
    }];
}


+ (void) callData:(NSString *)url method:(NSString*) method header:(NSDictionary*) header parameter:(id) parameter handler:(DataResponse) handler {
    [self callData:url method:method header:header parameter:parameter handler:^(id target, NSData *data) {
        if (handler) {
            handler(data);
        }
    } target:nil];
    
}

+ (void) callData:(NSString *)url method:(NSString*) method header:(NSDictionary*) header parameter:(id) parameter handler:(DataResponseWithTarget) handler  target:(id)target{
    NSMutableURLRequest *req = [URLConnection requestFromURL:url
                                                      method:method
                                                   parameter:parameter];
    if (header) {
        NSArray *keys = header.allKeys;
        for (NSString *strKey in keys) {
            [req setValue:[header valueForKey:strKey] forHTTPHeaderField:strKey];
        }
    }
    //AppDelegate *app = [UIApplication sharedApplication].delegate;
    //NSUserDefaults *session = [NSUserDefaults standardUserDefaults];
    
    //NSData *encodedObject = [AppHelper getUserDataWithAttributeName:@"login"];
    NSString *group = [AppHelper getUserDataForKey:kADA_CLIENT];
    NSString *lang = [AppHelper getUserDataForKey:kLanguage];
    Login *object = [AppHelper getUserDataForKey:@"login"];
    if(object) {
        group = object.group;
    }
    
    [req setValue:[UIDevice currentDevice].identifierForVendor.UUIDString forHTTPHeaderField:kSECURE_DEVICE];
    [req setValue:@"iOS" forHTTPHeaderField:@"device_os"];
    [req setValue:@"iOS" forHTTPHeaderField:@"device-os"];
    [req setValue:group forHTTPHeaderField:kADA_CLIENT];
    [req setValue:lang forHTTPHeaderField:kLanguage];
    [req setValue:[[NSBundle bundleForClass:[ADA class]].infoDictionary valueForKey:@"CFBundleShortVersionString"]  forHTTPHeaderField:@"sdk-version"];
    NSString *version = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    [req setValue:version forHTTPHeaderField:@"app-version"];
    [self setUserAgent:req];
    
    NSString *kCacheService = [NSString stringWithFormat:@"%@-%@-%@-%@-%@", url, method, parameter,
                               group,
                               lang];
    
    
    //NSLog(@"req %@", req);
    
    [[URLConnection getInstance] sendAsynchronousRequest:req completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        if (connectionError) {
            
            
            if (handler) {
                handler(target, nil);
            }
            return;
        }
        [AppHelper hideLoading];
        
        CacheService *cache = [ManagedObjectContextFactory
                               getObjectFromEntity:@"CacheService"
                               withValue:kCacheService
                               fromFieldName:@"serviceKey"];
        if (!cache) {
            cache = (CacheService*)[ManagedObjectContextFactory
                                    newEntityForName:@"CacheService"];
            cache.serviceKey = kCacheService;
        }
        cache.serviceData = data;
        [ManagedObjectContextFactory saveContext];
        
        if (handler) {
            handler(target, data);
        }else{
            if (handler) {
                handler(target, nil);
            }
        }
        
    }];
}

#pragma mark Cache


+ (void) getJSONCache:(NSString *)url method:(NSString*) method header:(NSDictionary*) header parameter:(id) parameter handler:(JSONResponse) handler {
    //NSUserDefaults *session = [NSUserDefaults standardUserDefaults];
    NSString *kCacheService = [NSString stringWithFormat:@"%@-%@-%@-%@-%@", url, method, parameter,
                               [AppHelper getUserDataForKey:kADA_CLIENT],
                               [AppHelper getUserDataForKey:kLanguage]];
    CacheService *cache = [ManagedObjectContextFactory
                           getObjectFromEntity:@"CacheService"
                           withValue:kCacheService
                           fromFieldName:@"serviceKey"];
    if (cache) {
        NSJSONSerialization *json = [AppHelper jsonWithData:cache.serviceData];
        if (handler) {
            handler(json);
        }
    }else{
        if (handler) {
            handler(nil);
        }
    }
}
+ (void) getJSONCache:(NSString *)url method:(NSString*) method header:(NSDictionary*) header parameter:(id) parameter handler:(JSONResponseWithTarget) handler target:(id)target {
    //NSString *kCacheService = [NSString stringWithFormat:@"%@-%@-%@", url, method, parameter];
    NSUserDefaults *session = [NSUserDefaults standardUserDefaults];
    
    NSData *encodedObject = [AppHelper getUserDataForKey:@"login"];
    NSString *group = [AppHelper getUserDataForKey:kADA_CLIENT];
    NSString *lang = [AppHelper getUserDataForKey:kLanguage];
    if(encodedObject) {
        Login *object = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
        group = object.group;
        
    }
    
    
    NSString *kCacheService = [NSString stringWithFormat:@"%@-%@-%@-%@-%@", url, method, parameter,
                               group,
                               lang];
    
//    NSLog(@"cache key %@", kCacheService);
    
    CacheService *cache = [ManagedObjectContextFactory
                           getObjectFromEntity:@"CacheService"
                           withValue:kCacheService
                           fromFieldName:@"serviceKey"];
    if (cache) {
        NSJSONSerialization *json = [AppHelper jsonWithData:cache.serviceData];
        if (handler) {
            handler(target, json);
        }
    }else{
        if (handler) {
            handler(target, nil);
        }
    }
    
}
+ (void) getDataCache:(NSString *)url method:(NSString*) method header:(NSDictionary*) header parameter:(id) parameter handler:(DataResponse) handler {
    //NSString *kCacheService = [NSString stringWithFormat:@"%@-%@-%@", url, method, parameter];
    NSUserDefaults *session = [NSUserDefaults standardUserDefaults];
    NSData *encodedObject = [AppHelper getUserDataForKey:@"login"];
    NSString *group = [AppHelper getUserDataForKey:kADA_CLIENT];
    //NSString *lang = [AppHelper getUserDataWithAttributeName:kLanguage];
    if(encodedObject) {
        Login *object = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
        group = object.group;
        
    }
    NSString *kCacheService = [NSString stringWithFormat:@"%@-%@-%@-%@-%@", url, method, parameter,
                               group,
                               [AppHelper getUserDataForKey:kLanguage]];
    CacheService *cache = [ManagedObjectContextFactory
                           getObjectFromEntity:@"CacheService"
                           withValue:kCacheService
                           fromFieldName:@"serviceKey"];
    if (cache) {
        if (handler) {
            handler(cache.serviceData);
        }
    }else{
        if (handler) {
            handler(nil);
        }
    }
    
}
+ (void) getDataCache:(NSString *)url method:(NSString*) method header:(NSDictionary*) header parameter:(id) parameter handler:(DataResponseWithTarget) handler target:(id)target {
    //NSString *kCacheService = [NSString stringWithFormat:@"%@-%@-%@", url, method, parameter];
    NSUserDefaults *session = [NSUserDefaults standardUserDefaults];
    NSData *encodedObject = [AppHelper getUserDataForKey:@"login"];
    NSString *group = [AppHelper getUserDataForKey:kADA_CLIENT];
    //NSString *lang = [AppHelper getUserDataWithAttributeName:kLanguage];
    if(encodedObject) {
        Login *object = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
        group = object.group;
        
    }
    NSString *kCacheService = [NSString stringWithFormat:@"%@-%@-%@-%@-%@", url, method, parameter,
                               group,
                               [AppHelper getUserDataForKey:kLanguage]];
    CacheService *cache = [ManagedObjectContextFactory
                           getObjectFromEntity:@"CacheService"
                           withValue:kCacheService
                           fromFieldName:@"serviceKey"];
    if (cache) {
        if (handler) {
            handler(target, cache.serviceData);
        }
    }else{
        if (handler) {
            handler(target, nil);
        }
    }
}

#pragma mark MISC
+ (void)setUserAgent:(NSMutableURLRequest *)req {
    UIDevice *device = [UIDevice currentDevice];
    NSString * appVersionString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    NSDictionary *agent = @{
                            @"device":[self platformType],
                            @"os": [@"iOS " stringByAppendingString:[device systemVersion]],
                            @"network": [self currentWifiSSID],
                            @"app_version": appVersionString
                            };
    
    //NSLog(@"agent %@", agent);
    NSData *agentData = [NSJSONSerialization dataWithJSONObject:agent
                                                        options:NSJSONWritingPrettyPrinted
                                                          error:nil];
    
    
    [req setValue:[[[NSString alloc] initWithData:agentData encoding:NSUTF8StringEncoding] stringByReplacingOccurrencesOfString:@"\n" withString:@""] forHTTPHeaderField:@"User-Agent"];
}








+ (NSString *) platformType
{
    
    size_t size;
    sysctlbyname("hw.machine", NULL, &size, NULL, 0);
    char *machine = malloc(size);
    sysctlbyname("hw.machine", machine, &size, NULL, 0);
    NSString *platform = [NSString stringWithCString:machine encoding:NSUTF8StringEncoding];
    
    
    free(machine);
    
    
    if ([platform isEqualToString:@"iPhone1,1"])    return @"iPhone 1G";
    if ([platform isEqualToString:@"iPhone1,2"])    return @"iPhone 3G";
    if ([platform isEqualToString:@"iPhone2,1"])    return @"iPhone 3GS";
    if ([platform isEqualToString:@"iPhone3,1"])    return @"iPhone 4";
    if ([platform isEqualToString:@"iPhone3,3"])    return @"Verizon iPhone 4";
    if ([platform isEqualToString:@"iPhone4,1"])    return @"iPhone 4S";
    if ([platform isEqualToString:@"iPhone5,1"])    return @"iPhone 5 (GSM)";
    if ([platform isEqualToString:@"iPhone5,2"])    return @"iPhone 5 (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone5,3"])    return @"iPhone 5c (GSM)";
    if ([platform isEqualToString:@"iPhone5,4"])    return @"iPhone 5c (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone6,1"])    return @"iPhone 5s (GSM)";
    if ([platform isEqualToString:@"iPhone6,2"])    return @"iPhone 5s (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone7,2"])    return @"iPhone 6";
    if ([platform isEqualToString:@"iPhone7,1"])    return @"iPhone 6 Plus";
    if ([platform isEqualToString:@"iPhone8,1"])    return @"iPhone 6s";
    if ([platform isEqualToString:@"iPhone8,2"])    return @"iPhone 6s Plus";
    if ([platform isEqualToString:@"iPhone9,1"])    return @"iPhone 7";
    if ([platform isEqualToString:@"iPhone9,2"])    return @"iPhone 7 Plus";
    if ([platform isEqualToString:@"iPhone10,1"])    return @"iPhone 8";
    if ([platform isEqualToString:@"iPhone10,2"])    return @"iPhone 8 Plus";
    if ([platform isEqualToString:@"iPhone10,3"])    return @"iPhone X";
    
    if ([platform isEqualToString:@"iPod1,1"])      return @"iPod Touch 1G";
    if ([platform isEqualToString:@"iPod2,1"])      return @"iPod Touch 2G";
    if ([platform isEqualToString:@"iPod3,1"])      return @"iPod Touch 3G";
    if ([platform isEqualToString:@"iPod4,1"])      return @"iPod Touch 4G";
    if ([platform isEqualToString:@"iPod5,1"])      return @"iPod Touch 5G";
    if ([platform isEqualToString:@"iPad1,1"])      return @"iPad";
    if ([platform isEqualToString:@"iPad2,1"])      return @"iPad 2 (WiFi)";
    if ([platform isEqualToString:@"iPad2,2"])      return @"iPad 2 (GSM)";
    if ([platform isEqualToString:@"iPad2,3"])      return @"iPad 2 (CDMA)";
    if ([platform isEqualToString:@"iPad2,4"])      return @"iPad 2 (WiFi)";
    if ([platform isEqualToString:@"iPad2,5"])      return @"iPad Mini (WiFi)";
    if ([platform isEqualToString:@"iPad2,6"])      return @"iPad Mini (GSM)";
    if ([platform isEqualToString:@"iPad2,7"])      return @"iPad Mini (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad3,1"])      return @"iPad 3 (WiFi)";
    if ([platform isEqualToString:@"iPad3,2"])      return @"iPad 3 (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad3,3"])      return @"iPad 3 (GSM)";
    if ([platform isEqualToString:@"iPad3,4"])      return @"iPad 4 (WiFi)";
    if ([platform isEqualToString:@"iPad3,5"])      return @"iPad 4 (GSM)";
    if ([platform isEqualToString:@"iPad3,6"])      return @"iPad 4 (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad4,1"])      return @"iPad Air (WiFi)";
    if ([platform isEqualToString:@"iPad4,2"])      return @"iPad Air (Cellular)";
    if ([platform isEqualToString:@"iPad4,3"])      return @"iPad Air";
    if ([platform isEqualToString:@"iPad4,4"])      return @"iPad Mini 2G (WiFi)";
    if ([platform isEqualToString:@"iPad4,5"])      return @"iPad Mini 2G (Cellular)";
    if ([platform isEqualToString:@"iPad4,6"])      return @"iPad Mini 2G";
    if ([platform isEqualToString:@"iPad4,7"])      return @"iPad Mini 3 (WiFi)";
    if ([platform isEqualToString:@"iPad4,8"])      return @"iPad Mini 3 (Cellular)";
    if ([platform isEqualToString:@"iPad4,9"])      return @"iPad Mini 3 (China)";
    if ([platform isEqualToString:@"iPad5,3"])      return @"iPad Air 2 (WiFi)";
    if ([platform isEqualToString:@"iPad5,4"])      return @"iPad Air 2 (Cellular)";
    if ([platform isEqualToString:@"AppleTV2,1"])   return @"Apple TV 2G";
    if ([platform isEqualToString:@"AppleTV3,1"])   return @"Apple TV 3";
    if ([platform isEqualToString:@"AppleTV3,2"])   return @"Apple TV 3 (2013)";
    if ([platform isEqualToString:@"i386"])         return @"Simulator";
    if ([platform isEqualToString:@"x86_64"])       return @"Simulator";

    return platform;
}

+ (NSString *)currentWifiSSID {
    // Does not work on the simulator.
    NSString *ssid = nil;
    NSArray *ifs = (__bridge_transfer id)CNCopySupportedInterfaces();
    for (NSString *ifnam in ifs) {
        NSDictionary *info = (__bridge_transfer id)CNCopyCurrentNetworkInfo((__bridge CFStringRef)ifnam);
        if (info[@"SSID"]) {
            ssid = info[@"SSID"];
        }
    }
    return ssid ? ssid : @"Cellular";
}

@end
