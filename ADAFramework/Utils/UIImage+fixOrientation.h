//
//  UIImage+fixOrientation.h
//  ADA
//
//  Created by Choldarong-r on 25/7/2559 BE.
//  Copyright © 2559 G-ABLE ITS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage(fixOrientation)

- (UIImage *)fixOrientation;

@end
