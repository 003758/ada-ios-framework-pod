//
//  NSData+Base64.h
//  OneMinTimeSheet
//
//  Created by Choldarong-r on 3/10/2557 BE.
//  Copyright (c) 2557 G-ABLE Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (Base64)


+ (NSData *) dataWithBase64EncodedString:(NSString *) string;

- (id) initWithBase64EncodedString:(NSString *) string;

- (NSString *) base64EncodingWithLineLength:(unsigned int) lineLength;

@end
