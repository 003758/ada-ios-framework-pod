//
//  URLConnection.m
//  OneMinTimeSheet
//
//  Created by Choldarong-r on 4/2/2557 BE.
//  Copyright (c) 2557 G-ABLE Co., Ltd. All rights reserved.
//
#import "URLConnection.h"



@implementation URLConnection {
    NSURLSessionDataTask *task;
    
}
int MAX_CONCURRENT = 10;
static int taskCount = 0;

- (id) init {
    _recvData = [[NSMutableData alloc] init];
    return self;
}

+ (NSDictionary *)tasks {
    static NSDictionary<NSString*, NSURLSessionDataTask*> *dict = nil;
    if (dict == nil) {
        dict = [[NSMutableDictionary alloc] init];
    }
    return dict;
}

- (void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    
    [_recvData appendData:data];
}

- (void) connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    
    _currentResponse = response;
}

- (void) connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    
    _connectionError = error;
    
    
    
    
    
    if (_currentHandler) {
        _currentHandler(_currentResponse, nil, _connectionError);
    }
    if (_currentHandlerWithSenderData) {
        _currentHandlerWithSenderData(_currentResponse, nil, _senderData,_connectionError);
    }
    [connection cancel];
    
    
}



- (void) connectionDidFinishLoading:(NSURLConnection *)connection {
    
    
    if (_currentHandler) {
        _currentHandler(_currentResponse, _recvData, _connectionError);
    }
    if (_currentHandlerWithSenderData) {
        _currentHandlerWithSenderData(_currentResponse, _recvData, _senderData,_connectionError);
    }
}


- (NSCachedURLResponse *)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse *)cachedResponse {
    return nil;
}

/*
 - (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
 return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
 }
 
 - (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
 if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]) {
 //        if ([trustedHosts containsObject:challenge.protectionSpace.host])
 
 OSStatus                err;
 NSURLProtectionSpace *  protectionSpace;
 SecTrustRef             trust;
 SecTrustResultType      trustResult;
 BOOL                    trusted;
 
 protectionSpace = [challenge protectionSpace];
 assert(protectionSpace != nil);
 
 trust = [protectionSpace serverTrust];
 assert(trust != NULL);
 err = SecTrustEvaluate(trust, &trustResult);
 trusted = (err == noErr) && ((trustResult == kSecTrustResultProceed) || (trustResult == kSecTrustResultUnspecified));
 
 // If that fails, apply our certificates as anchors and see if that helps.
 //
 // It's perfectly acceptable to apply all of our certificates to the SecTrust
 // object, and let the SecTrust object sort out the mess.  Of course, this assumes
 // that the user trusts all certificates equally in all situations, which is implicit
 // in our user interface; you could provide a more sophisticated user interface
 // to allow the user to trust certain certificates for certain sites and so on).
 //        NSLog(@"%@", [NSURLCredentialStorage sharedCredentialStorage].allCredentials);
 if ( ! trusted ) {
 err = SecTrustSetAnchorCertificates(trust, (CFArrayRef) [NSURLCredentialStorage sharedCredentialStorage].allCredentials);
 if (err == noErr) {
 err = SecTrustEvaluate(trust, &trustResult);
 }
 trusted = (err == noErr) && ((trustResult == kSecTrustResultProceed) || (trustResult == kSecTrustResultUnspecified));
 }
 trusted = YES;
 
 if(trusted)
 [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
 }
 
 [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
 }
 */


/*
 -  (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
 
 }
 - (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
 
 }
 
 - (BOOL)connectionShouldUseCredentialStorage:(NSURLConnection *)connection {
 
 }*/

- (void) dealloc {
    
    /*connectionError = nil;
     currentResponse = nil;
     con = nil;
     recvData = nil;*/
}


- (NSString*) sendAsynchronousRequest:(NSURLRequest*) request completionHandler:(ConnectionCompletion) handler {
    
    if (!request || taskCount == MAX_CONCURRENT) {
        return nil;
    }
    
    //
    
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    //[sessionConfig setTimeoutIntervalForResource:15];
    _sharedSessionMainQueue = [NSURLSession sessionWithConfiguration:sessionConfig delegate:nil delegateQueue:[NSOperationQueue mainQueue]];
    //_sharedSessionMainQueue = [NSURLSession sessionWithConfiguration:sessionConfig];
    
    taskCount ++;
    task = [_sharedSessionMainQueue dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        dispatch_async(dispatch_get_main_queue(), ^{
        if (handler) {
            handler(response, data, error);
        }
        });
        taskCount--;
    }];
    
    [task resume];
    NSString *uuid = [[NSUUID UUID] UUIDString];
    [URLConnection.tasks setValue:task forKey:uuid];
    
    return uuid;
}

- (NSString *) sendAsynchronousRequest:(NSURLRequest*) request completionHandler:(ConnectionCompletionWithSenderData) handler senderData:(id) senderData {
    
    if (!request || taskCount == MAX_CONCURRENT) {
        return nil;
    }
    
    
    /*NSURLSession * session = [NSURLSession sessionWithConfiguration:sessionConfig
     delegate:nil
     delegateQueue:[NSOperationQueue mainQueue]];
     */
    
    NSURLSessionConfiguration *sessionConfig =
    [NSURLSessionConfiguration defaultSessionConfiguration];
    _sharedSessionMainQueue = [NSURLSession sessionWithConfiguration:sessionConfig delegate:nil delegateQueue:[NSOperationQueue mainQueue]];
    //_sharedSessionMainQueue = [NSURLSession sessionWithConfiguration:sessionConfig];
    taskCount++;
    task = [_sharedSessionMainQueue dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        dispatch_async(dispatch_get_main_queue(), ^{
        if (handler) {
            handler(response, data, senderData, error);
        }
        });
        taskCount--;
    }];
    [task resume];
    
    NSString *uuid = [[NSUUID UUID] UUIDString];
    [URLConnection.tasks setValue:task forKey:uuid];
    
    return uuid;
}

- (void) cancel {
    if (task) {
        [task cancel];
        task = nil;
    }
}

+ (NSMutableURLRequest*) requestFromURL:(NSString*) strURL method:(NSString*) method parameter:(id) parameters {
    
    
    if ([[method uppercaseString] isEqualToString:@"POST"]) {
        return [self requestFromURL:strURL method:method contentType:@"application/x-www-form-urlencoded" parameter:parameters];
    }else{
        return [self requestFromURL:strURL method:method contentType:nil parameter:parameters];
    }
}

+ (NSMutableURLRequest*) requestFromURL:(NSString*) strURL method:(NSString*) method contentType:(NSString*) contentType parameter:(id) parameters {
    if (!strURL) {
        return nil;
    }
    NSMutableString *strData = [[NSMutableString alloc] init];
    
    if(parameters){
        if ([parameters isKindOfClass:[NSDictionary class]]) {
            NSArray *keys = [[parameters keyEnumerator] allObjects];
            for (NSString *key in keys) {
                [strData appendString:key];
                [strData appendString:@"="];
                
                id value = [parameters valueForKey:key];
                if ([value isKindOfClass:[NSString class]]) {
                    [strData appendString:[value stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]]];
                }else{
                    [strData appendString:[[NSString stringWithFormat:@"%@", value] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]]] ;
                }
                
                [strData appendString:@"&"];
            }
            if (strData.length > 0) {
                [strData deleteCharactersInRange:NSMakeRange(strData.length-1, 1)];
            }
        }else if ([parameters isKindOfClass:[NSData class]]) {
            [strData appendString:[[NSString alloc] initWithData:parameters encoding:NSUTF8StringEncoding]];
        }else{
            [strData appendString:parameters];
        }
        
        
    }
    NSURL *url = nil;
    if ([[method uppercaseString] isEqualToString:@"POST"]) {
        url = [NSURL URLWithString:strURL];
    }else{
        if (strData.length == 0) {
            url = [NSURL URLWithString:strURL];
        }else{
            url = [NSURL URLWithString:[NSString stringWithFormat:@"%@?%@", strURL, strData]];
        }
    }
    NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:url
                                                       cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                   timeoutInterval:20];
    
    if ([[method uppercaseString] isEqualToString:@"POST"]) {
        NSData *data = [strData dataUsingEncoding:NSUTF8StringEncoding];
        [req setValue:contentType forHTTPHeaderField:@"Content-Type"];
        [req setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[data length]] forHTTPHeaderField:@"Content-Length"];
        [req setHTTPMethod:@"POST"];
        [req setHTTPBody: data];
    }else{
        [req setHTTPMethod:[method uppercaseString]];
        if(contentType){
            [req setValue:contentType forHTTPHeaderField:@"Content-Type"];
        }
    }
    
    
    return req;
}


+ (NSMutableURLRequest*) requestFromURL:(NSString*) strURL method:(NSString*) method contentType:(NSString*) contentType jsonData:(NSDictionary *) data {
    if (!strURL) {
        return nil;
    }
    //    NSMutableString *strData = [[NSMutableString alloc] init];
    NSData *jsonData = nil;
    if(data){
        jsonData = [NSJSONSerialization dataWithJSONObject:data options:kNilOptions error:nil];
    }
    NSURL *url = nil;
    if ([[method uppercaseString] isEqualToString:@"POST"]) {
        url = [NSURL URLWithString:strURL];
    }else{
        url = [NSURL URLWithString:strURL];
        
    }
    NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:url
                                                       cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                   timeoutInterval:20];
    
    if ([[method uppercaseString] isEqualToString:@"POST"]) {
        [req setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [req setValue:contentType forHTTPHeaderField:@"Content-Type"];
        [req setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[jsonData length]] forHTTPHeaderField:@"Content-Length"];
        [req setHTTPMethod:@"POST"];
        [req setHTTPBody: jsonData];
    }else{
        [req setHTTPMethod:[method uppercaseString]];
        [req setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [req setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[jsonData length]] forHTTPHeaderField:@"Content-Length"];
        [req setHTTPBody: jsonData];
        if(contentType){
            [req setValue:contentType forHTTPHeaderField:@"Content-Type"];
        }
        
    }
    [req setValue:kAuthorization forHTTPHeaderField:@"Authorization"];
    
    return req;
}

+ (NSMutableURLRequest*) requestFromURL:(NSString*) strURL method:(NSString*) method contentType:(NSString*) contentType data:(NSData *) data {
    if (!strURL) {
        return nil;
    }
    //    NSMutableString *strData = [[NSMutableString alloc] init];
    
    NSURL *url = nil;
    if ([[method uppercaseString] isEqualToString:@"POST"]) {
        url = [NSURL URLWithString:strURL];
    }else{
        url = [NSURL URLWithString:strURL];
        
    }
    NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:url
                                                       cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                   timeoutInterval:20];
    
    if ([[method uppercaseString] isEqualToString:@"POST"]) {
        //[req setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        if(contentType){
            [req setValue:contentType forHTTPHeaderField:@"Content-Type"];
        }
        [req setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[data length]] forHTTPHeaderField:@"Content-Length"];
        [req setHTTPMethod:@"POST"];
        [req setHTTPBody: data];
        
    }else{
        [req setHTTPMethod:[method uppercaseString]];
        [req setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [req setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[data length]] forHTTPHeaderField:@"Content-Length"];
        [req setHTTPBody: data];
        if(contentType){
            [req setValue:contentType forHTTPHeaderField:@"Content-Type"];
        }
        
    }
    [req setValue:kAuthorization forHTTPHeaderField:@"Authorization"];
    
    return req;
}


+ (id) getInstance {
    return [[URLConnection alloc] init];
}



@end

/*
 
 
 //
 //  URLConnection.m
 //  OneMinTimeSheet
 //
 //  Created by Choldarong-r on 4/2/2557 BE.
 //  Copyright (c) 2557 G-ABLE Co., Ltd. All rights reserved.
 //
 
 #import "URLConnection.h"
 
 @implementation URLConnection
 
 - (id) init {
 _recvData = [[NSMutableData alloc] init];
 return self;
 }
 
 - (void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
 
 [_recvData appendData:data];
 }
 
 - (void) connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
 
 _currentResponse = response;
 }
 
 - (void) connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
 
 _connectionError = error;
 
 
 
 
 
 if (_currentHandler) {
 _currentHandler(_currentResponse, nil, _connectionError);
 }
 if (_currentHandlerWithSenderData) {
 _currentHandlerWithSenderData(_currentResponse, nil, _senderData,_connectionError);
 }
 [connection cancel];
 
 
 }
 
 
 
 - (void) connectionDidFinishLoading:(NSURLConnection *)connection {
 
 
 if (_currentHandler) {
 _currentHandler(_currentResponse, _recvData, _connectionError);
 }
 if (_currentHandlerWithSenderData) {
 _currentHandlerWithSenderData(_currentResponse, _recvData, _senderData,_connectionError);
 }
 }
 
 
 - (NSCachedURLResponse *)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse *)cachedResponse {
 return nil;
 }
 
 - (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
 return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
 }
 
 - (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
 if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]) {
 //        if ([trustedHosts containsObject:challenge.protectionSpace.host])
 
 OSStatus                err;
 NSURLProtectionSpace *  protectionSpace;
 SecTrustRef             trust;
 SecTrustResultType      trustResult;
 BOOL                    trusted;
 
 protectionSpace = [challenge protectionSpace];
 assert(protectionSpace != nil);
 
 trust = [protectionSpace serverTrust];
 assert(trust != NULL);
 err = SecTrustEvaluate(trust, &trustResult);
 trusted = (err == noErr) && ((trustResult == kSecTrustResultProceed) || (trustResult == kSecTrustResultUnspecified));
 
 // If that fails, apply our certificates as anchors and see if that helps.
 //
 // It's perfectly acceptable to apply all of our certificates to the SecTrust
 // object, and let the SecTrust object sort out the mess.  Of course, this assumes
 // that the user trusts all certificates equally in all situations, which is implicit
 // in our user interface; you could provide a more sophisticated user interface
 // to allow the user to trust certain certificates for certain sites and so on).
 NSLog(@"%@", [NSURLCredentialStorage sharedCredentialStorage].allCredentials);
 if ( ! trusted ) {
 err = SecTrustSetAnchorCertificates(trust, (CFArrayRef) [NSURLCredentialStorage sharedCredentialStorage].allCredentials);
 if (err == noErr) {
 err = SecTrustEvaluate(trust, &trustResult);
 }
 trusted = (err == noErr) && ((trustResult == kSecTrustResultProceed) || (trustResult == kSecTrustResultUnspecified));
 }
 trusted = YES;
 
 if(trusted)
 [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
 }
 
 [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
 }
 
 
 
 
 -  (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
 
 }
 - (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
 
 }
 
 - (BOOL)connectionShouldUseCredentialStorage:(NSURLConnection *)connection {
 
 
 
 - (void) dealloc {
 
 
 }
 
 
 - (void) sendAsynchronousRequest:(NSURLRequest*) request completionHandler:(ConnectionCompletion) handler {
 
 if (!request) {
 return;
 }
 
 
 _currentHandler = handler;
 _connectionError = nil;
 _currentResponse = nil;
 if(_recvData){
 [_recvData setData:[NSData dataWithBytes:NULL length:0]];
 }else{
 _recvData = [[NSMutableData alloc] init];
 }
 
 
 
 _con = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:NO];
 
 //_con = [NSURLConnection connectionWithRequest:request delegate:self];
 [_con setDelegateQueue:[NSOperationQueue mainQueue]];
 //[_con scheduleInRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
 [_con start];
 }
 
 - (void) sendAsynchronousRequest:(NSURLRequest*) request completionHandler:(ConnectionCompletionWithSenderData) handler senderData:(id) data {
 
 if (!request) {
 return;
 }
 
 
 _currentHandlerWithSenderData = handler;
 _connectionError = nil;
 _currentResponse = nil;
 _senderData = data;
 if(_recvData){
 [_recvData setData:[NSData dataWithBytes:NULL length:0]];
 }else{
 _recvData = [[NSMutableData alloc] init];
 }
 
 
 
 _con = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:NO];
 
 //_con = [NSURLConnection connectionWithRequest:request delegate:self];
 [_con setDelegateQueue:[NSOperationQueue mainQueue]];
 //[_con scheduleInRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
 [_con start];
 }
 
 - (void) cancel {
 if (_con) {
 [_con cancel];
 _con = nil;
 }
 }
 
 + (NSMutableURLRequest*) requestFromURL:(NSString*) strURL method:(NSString*) method parameter:(NSDictionary *) parameters {
 
 
 if ([[method uppercaseString] isEqualToString:@"POST"]) {
 return [self requestFromURL:strURL method:method contentType:@"application/x-www-form-urlencoded" parameter:parameters];
 }else{
 return [self requestFromURL:strURL method:method contentType:nil parameter:parameters];
 }
 }
 
 + (NSMutableURLRequest*) requestFromURL:(NSString*) strURL method:(NSString*) method contentType:(NSString*) contentType parameter:(NSDictionary *) parameters {
 if (!strURL) {
 return nil;
 }
 NSMutableString *strData = [[NSMutableString alloc] init];
 if(parameters){
 NSArray *keys = [[parameters keyEnumerator] allObjects];
 for (NSString *key in keys) {
 [strData appendString:key];
 [strData appendString:@"="];
 
 id value = [parameters valueForKey:key];
 if ([value isKindOfClass:[NSString class]]) {
 [strData appendString:[[value stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] stringByReplacingOccurrencesOfString:@"&" withString:@"%26"]] ;
 }else{
 [strData appendString:[[[NSString stringWithFormat:@"%@", value] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] stringByReplacingOccurrencesOfString:@"&" withString:@"%26"]] ;
 }
 
 [strData appendString:@"&"];
 }
 if (strData.length > 0) {
 [strData deleteCharactersInRange:NSMakeRange(strData.length-1, 1)];
 }
 }
 NSURL *url = nil;
 if ([[method uppercaseString] isEqualToString:@"POST"]) {
 url = [NSURL URLWithString:strURL];
 }else{
 if (strData.length == 0) {
 url = [NSURL URLWithString:strURL];
 }else{
 url = [NSURL URLWithString:[NSString stringWithFormat:@"%@?%@", strURL, strData]];
 }
 }
 NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:url
 cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
 timeoutInterval:20];
 
 if ([[method uppercaseString] isEqualToString:@"POST"]) {
 NSData *data = [strData dataUsingEncoding:NSUTF8StringEncoding];
 [req setValue:contentType forHTTPHeaderField:@"Content-Type"];
 [req setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[data length]] forHTTPHeaderField:@"Content-Length"];
 [req setHTTPMethod:@"POST"];
 [req setHTTPBody: data];
 }else{
 [req setHTTPMethod:[method uppercaseString]];
 if(contentType){
 [req setValue:contentType forHTTPHeaderField:@"Content-Type"];
 }
 }
 
 
 NSLog(@"%@", url);
 
 return req;
 }
 
 
 + (NSMutableURLRequest*) requestFromURL:(NSString*) strURL method:(NSString*) method contentType:(NSString*) contentType jsonData:(NSDictionary *) data {
 if (!strURL) {
 return nil;
 }
 //    NSMutableString *strData = [[NSMutableString alloc] init];
 NSData *jsonData = nil;
 if(data){
 jsonData = [NSJSONSerialization dataWithJSONObject:data options:kNilOptions error:nil];
 }
 NSURL *url = nil;
 if ([[method uppercaseString] isEqualToString:@"POST"]) {
 url = [NSURL URLWithString:strURL];
 }else{
 url = [NSURL URLWithString:strURL];
 
 }
 NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:url
 cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
 timeoutInterval:20];
 
 if ([[method uppercaseString] isEqualToString:@"POST"]) {
 [req setValue:@"application/json" forHTTPHeaderField:@"Accept"];
 [req setValue:contentType forHTTPHeaderField:@"Content-Type"];
 [req setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[jsonData length]] forHTTPHeaderField:@"Content-Length"];
 [req setHTTPMethod:@"POST"];
 [req setHTTPBody: jsonData];
 }else{
 [req setHTTPMethod:[method uppercaseString]];
 [req setValue:@"application/json" forHTTPHeaderField:@"Accept"];
 [req setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[jsonData length]] forHTTPHeaderField:@"Content-Length"];
 [req setHTTPBody: jsonData];
 if(contentType){
 [req setValue:contentType forHTTPHeaderField:@"Content-Type"];
 }
 
 }
 [req setValue:kAuthorization forHTTPHeaderField:@"Authorization"];
 
 return req;
 }
 
 
 + (id) getInstance {
 return [[URLConnection alloc] init];
 }
 
 
 
 @end
 */
