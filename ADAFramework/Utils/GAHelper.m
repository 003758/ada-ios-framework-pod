//
//  GAHelper.m
//  BOMS
//
//  Created by Choldarong-r on 24/3/2560 BE.
//  Copyright © 2560 G-ABLE ITS. All rights reserved.
//

#import "GAHelper.h"

#import <GoogleAnalytics/GAI.h>
#import <GoogleAnalytics/GAIFields.h>
#import <GoogleAnalytics/GAIDictionaryBuilder.h>


@implementation GAHelper


id<GAITracker> tracker;
+ (void) trackID:(NSString*) trackID screen:(NSString*) screenName framework:(ADA*) ada {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (!tracker) {
            tracker = [[GAI sharedInstance] trackerWithTrackingId:trackID];
        }
        
        
        
        [tracker set:kGAIScreenName value:screenName];
        [tracker set:kGAIClientId value:[UIDevice currentDevice].identifierForVendor.UUIDString];
        if(ada.login) {
            [tracker set:kGAIUserId value:ada.login.usid];
            [tracker set:kGAICampaignName value:ada.login.group];
        }
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    });
    
     
    
    
    
    /*
    [FIRAnalytics logEventWithName:kFIREventSelectContent
                        parameters:@{
                                     kFIRParameterItemID:[NSString stringWithFormat:@"id-%@", name],
                                     kFIRParameterItemName:name,
                                     kFIRParameterContentType:@"Screen"
                                     }];*/
}

@end
