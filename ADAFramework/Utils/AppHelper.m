//
//  AppHelper.m
//  VirtualStore
//
//  Created by Choldarong-r on 7/9/2557 BE.
//  Copyright (c) 2557 G-ABLE ITS. All rights reserved.
//
#import <CommonCrypto/CommonDigest.h>
#import "AppHelper.h"
#import "UIImage+Extend.h"
#import "URLConnection.h"
#import <AudioToolbox/AudioToolbox.h>
#import <AudioToolbox/AudioServices.h>
#import <QuartzCore/QuartzCore.h>
//#import "NSTimer+Blocks.h"
@import NSTimer_Blocks;
#import "ServiceCaller.h"
#import "MuteChecker.h"
#import "UITextView+Blocks.h"
#import "UIResponder+FirstResponder.h"
#import "CoreDataUtils.h"
#import "AppData+CoreDataClass.h"


@interface UIBlockButton : UIButton {
    SuccessCompletion _actionBlock;
}

-(void) handleControlEvent:(UIControlEvents)event
                 withBlock:(SuccessCompletion) action;
@end
@implementation UIBlockButton

-(void) handleControlEvent:(UIControlEvents)event withBlock:(SuccessCompletion) action{
    _actionBlock = action;
    [self addTarget:self action:@selector(callActionBlock:) forControlEvents:event];
}

-(void) callActionBlock:(id)sender{
    if(_actionBlock){
        _actionBlock(YES);
    }
}
@end




@implementation AppHelper



//static int loadingCnt;
static int loadingTag = 999902;
static MuteChecker *checker;
static NSTimer *timer;
static BOOL showingKeyboard;
static NSMutableDictionary<NSString*, NSString*> *observerMap;
#define FONT_NAME                           @"SukhumvitSet-Text"
#define FONT_BOLD_NAME                      @"SukhumvitSet-Bold"
#define FONT_AWESOME                        @"FontAwesome"




+ (UIImage *) imageWithView:(UIView *)view
{
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.opaque, 0.0);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return img;
}

+ (UIImage *) getImageWithTintedColor:(UIImage *)image withTint:(UIColor *)color withIntensity:(float)alpha {
    CGSize size = image.size;
    
    UIGraphicsBeginImageContextWithOptions(size, FALSE, 2);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    [image drawAtPoint:CGPointZero blendMode:kCGBlendModeNormal alpha:1.0];
    
    CGContextSetFillColorWithColor(context, color.CGColor);
    CGContextSetBlendMode(context, kCGBlendModeOverlay);
    CGContextSetAlpha(context, alpha);
    
    CGContextFillRect(UIGraphicsGetCurrentContext(), CGRectMake(CGPointZero.x, CGPointZero.y, image.size.width, image.size.height));
    
    UIImage * tintedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return tintedImage;
}


+ (void) roundCornerButton:(UIButton*) button borderColor:(UIColor*) color {
    button.layer.cornerRadius = 10;//half of the width
    if(color){
        button.layer.borderColor = color.CGColor;
    }
    button.layer.borderWidth =2.0f;
}


//light blue gradient background
+ (CAGradientLayer*) lightBlueGradient {
    
    UIColor *colorOne = [UIColor colorWithRed:(0/255.0) green:(156/255.0) blue:(255/255.0) alpha:1.0];
    UIColor *colorTwo = [UIColor colorWithRed:(0/255.0)  green:(110/255.0)  blue:(187/255.0)  alpha:1.0];
    
    NSArray *colors = [NSArray arrayWithObjects:(id)colorOne.CGColor, colorTwo.CGColor, nil];
    NSNumber *stopOne = [NSNumber numberWithFloat:0.0];
    NSNumber *stopTwo = [NSNumber numberWithFloat:1.0];
    
    NSArray *locations = [NSArray arrayWithObjects:stopOne, stopTwo, nil];
    
    CAGradientLayer *headerLayer = [CAGradientLayer layer];
    headerLayer.colors = colors;
    headerLayer.locations = locations;
    
    return headerLayer;
}

+ (NSString*) stringWithJSON:(NSJSONSerialization*) json {
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:json
                                                       options:kNilOptions
                                                         error: nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData
                                                 encoding:NSUTF8StringEncoding];
    return jsonString;

}

+ (NSJSONSerialization*) jsonWithdictionary:(NSDictionary*) dict {
    NSData *data = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:nil];
    
    return [self jsonWithData:data];
}

+ (NSJSONSerialization*) jsonWithData:(NSData*) data {
    @try {
        
        //[self logData:data];
        NSError *error;
        
        
        NSJSONSerialization *json = [NSJSONSerialization JSONObjectWithData:data
                                                                    options:NSJSONReadingMutableContainers
                                                                      error:&error];
        if (error) {
            NSString *string = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            NSArray *arrString = [string componentsSeparatedByString:@"}"];
            string = @"";
            unsigned long cnt = arrString.count;
            for (NSString *part in arrString) {
                cnt--;
                if(part.length == 0 && cnt == 0){
                    break;
                }
                string = [[string stringByAppendingString:part] stringByAppendingString:@"}"];
                
            }
            NSData *d = [string dataUsingEncoding:NSUTF8StringEncoding];
            json = [NSJSONSerialization JSONObjectWithData:d
                                                   options:NSJSONReadingMutableContainers
                                                     error:&error];
            if (error) {
                
                NSLog(@"json parser %@", error);
                NSLog(@"with string : %@", string);
                //[self logData:data];
                return nil;
            }
        }
        
        
        return json;
    }
    @catch (NSException *exception) {
        return nil;
    }
    
}

+ (NSArray*) jsonArrayWithData:(NSData*) data {
    @try {
        
        //[self logResponse:data];
        NSError *error;
        NSString *string = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        string = [string substringToIndex:[string rangeOfString:@"]" options:NSBackwardsSearch].location + 1];
        NSData *d = [string dataUsingEncoding:NSUTF8StringEncoding];
        NSArray *json = [NSJSONSerialization JSONObjectWithData:d
                                                        options:NSJSONReadingMutableContainers
                                                          error:&error];
        
        if (error) {
            NSLog(@"json parser error %@", error);
            NSLog(@"with string : %@", string);
        }
        return json;
        
    }
    @catch (NSException *exception) {
        return nil;
    }
    
}

+ (BOOL) isEmpty:(NSString* _Nullable)string {
    return !string || string == nil;
}

+ (id) nullToBlank:(id)string {
    return string == [NSNull null] ? @"" : string;
}

+ (void)rotateSpinningView:(UIView *) view
{
    UIView *tableContainer = view;
    UILabel *spiningView = [tableContainer viewWithTag:98];
    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        [spiningView setTransform:CGAffineTransformRotate(spiningView.transform, M_PI_2)];
    } completion:^(BOOL finished) {
        if (finished && spiningView && [spiningView.text isEqualToString:@""]) {
            [self rotateSpinningView:view];
        }
    }];
}

+ (UIViewController*) currentContainerViewController {
    UIViewController *rootView = [UIApplication sharedApplication].delegate.window.rootViewController;
    UIViewController *view = nil;
    
    if (rootView.presentedViewController) { //presented as modal
        
        UIViewController *modalView =  rootView.presentedViewController;
        if ([modalView isKindOfClass:[UINavigationController class]]) {
            NSArray *views = ((UINavigationController*)modalView).viewControllers;
            view = views.lastObject;
        }else if(modalView.navigationController){
            view = modalView.navigationController.viewControllers.lastObject;
        }else{
            view = modalView;
        }
    }else{
        if ([rootView isKindOfClass:[UINavigationController class]]) {
            NSArray *views = ((UINavigationController*)rootView).viewControllers;
            view = views.lastObject;
        }else if(rootView.navigationController){
            view = rootView.navigationController.viewControllers.lastObject;
        }else{
            view = rootView;
        }
    }
    
    if ([view isKindOfClass:[UIViewController class]]) {
        if (((UIViewController*) view).navigationController) {
            view = ((UIViewController*) view).navigationController.viewControllers.lastObject;
        }else{
            view =((UIViewController*) view);
        }
    }
    
    //NSLog(@"inview %@", view);
    if(view.presentedViewController){
        return view.presentedViewController;
    }
    
    return view;
}

+ (UIView*) currentContainerView {
    UIViewController *rootView = [UIApplication sharedApplication].delegate.window.rootViewController;
    UIView *view = nil;
    
    if (rootView.presentedViewController) { //presented as modal
        
        UIViewController *modalView =  rootView.presentedViewController;
        if ([modalView isKindOfClass:[UINavigationController class]]) {
            NSArray *views = ((UINavigationController*)modalView).viewControllers;
            view = [views.lastObject view];
        }else if(modalView.navigationController){
            view = modalView.navigationController.viewControllers.lastObject.view;
        }else{
            view = modalView.view;
        }
    }else{
        if ([rootView isKindOfClass:[UINavigationController class]]) {
            NSArray *views = ((UINavigationController*)rootView).viewControllers;
            view = views.lastObject;
        }else if(rootView.navigationController){
            view = rootView.navigationController.viewControllers.lastObject.view;
        }else{
            view = rootView.view;
        }
    }
    
    if ([view isKindOfClass:[UIViewController class]]) {
        if (((UIViewController*) view).navigationController) {
            view = ((UIViewController*) view).navigationController.viewControllers.lastObject.view;
        }else{
            view =((UIViewController*) view).view;
        }
    }
    
    //NSLog(@"inview %@", view);
    
    return view;
}


+ (void) showLoading:(UIView*) view {
    
    [self showLoadingWithMessage:[AppHelper getLabelForThai:@"กำลังดึงข้อมูล..." eng:@"Loading..."]  inView:view];
    
    
}


+ (void) showLoadingWithMessage:(NSString*) message inView:(UIView*) view{
    [self showLoadingWithMessage:message withButton:nil handler:nil inView:view];
}


+ (void) showLoadingWithMessage:(NSString*) message icon:(NSString *) icon inView:(UIView*) view {
    [self showLoadingWithMessage:message icon:icon withButton1:nil handler1:nil withButton2:nil handler2:nil inView:view];
}


+ (void) showLoadingWithMessage:(NSString*) message
                           icon:(NSString*) txtIcon
                    withButton1:(NSString*) title1 handler1:(SuccessCompletion) handler1
                    withButton2:(NSString*) title2 handler2:(SuccessCompletion) handler2
                         inView:(UIView*) view {
    
    UIViewController *viewController = (UIViewController*)view.nextResponder;
    BOOL fromTableView = [viewController isKindOfClass:[UITableViewController class]];
    CGFloat top = fromTableView ? 0 : ((UIViewController*)view.nextResponder).navigationController &&
    !((UIViewController*)view.nextResponder).navigationController.isNavigationBarHidden
    && ((UIViewController*)view.nextResponder).navigationController.navigationBar.translucent
    ? 64 : 20;
    
    
    
    
    
    
    //[self hideLoading:view finished:^(BOOL success) {
    
    
    CGSize screenSize = [UIScreen mainScreen].bounds.size;
    UIView *msgView = [view viewWithTag:loadingTag];
    if (msgView) {
        msgView.frame = CGRectMake(0, top, screenSize.width, 0);
    }else{
        msgView = [[UIView alloc] initWithFrame:CGRectMake(0, top, screenSize.width, 0)];
    }
    
    
    UILabel *tmp = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, msgView.frame.size.width - 60, 35)];
    tmp.numberOfLines = 0;
    tmp.font = [UIFont fontWithName:FONT_BOLD_NAME size:15];
    [tmp setMinimumScaleFactor:8.0/15.0];
    tmp.text = message;
    CGSize textSize = [tmp sizeThatFits:CGSizeMake(tmp.frame.size.width, CGFLOAT_MAX)];
    
    
    NSArray *subViews = msgView.subviews;
    for (UIView *subView in subViews) {
        [subView removeFromSuperview];
    }
    
    msgView.alpha = 0;
    msgView.layer.zPosition = MAXFLOAT;
    msgView.tag = loadingTag;
    msgView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.85];
    UIView *tableContainer = view;
    [tableContainer addSubview:msgView];
    
    UILabel *icon = [[UILabel alloc] initWithFrame:CGRectMake(15, 15, 25, 25)];
    icon.text = txtIcon;
    icon.textColor = [txtIcon isEqualToString:@""] ? [UIColor yellowColor] : [UIColor whiteColor];
    icon.textAlignment = NSTextAlignmentCenter;
    icon.font = [UIFont fontWithName:FONT_AWESOME size:15];
    icon.tag = 98;
    icon.transform = CGAffineTransformMakeRotation(0);
    [icon sizeToFit];
    [msgView addSubview:icon];
    
    
    if([txtIcon isEqualToString:@""]){//spinner icon
        [self rotateSpinningView:view];
    }
    
    
    NSMutableParagraphStyle *style =  [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    style.alignment = NSTextAlignmentJustified;
    /*style.firstLineHeadIndent = 25.0f;
     style.headIndent = 25.0f;
     style.tailIndent = 0.0f;*/
    
    NSAttributedString *attrText = [[NSAttributedString alloc] initWithString:message
                                                                   attributes:@{ NSParagraphStyleAttributeName : style}];
    
    UILabel *txt = [[UILabel alloc] initWithFrame:CGRectMake(45, 10, msgView.frame.size.width - 60, textSize.height)];
    txt.attributedText = attrText;
    txt.textAlignment = NSTextAlignmentCenter;
    txt.tag = 99;
    txt.font = [UIFont fontWithName:FONT_BOLD_NAME size:15];
    [txt setMinimumScaleFactor:8.0/15.0];
    txt.numberOfLines = 0;
    //[txt setAdjustsFontSizeToFitWidth:YES];
    txt.textColor = [UIColor whiteColor];
    
    //txt.center = CGPointMake(msgView.center.x + 11.5, 15);
    //icon.center = CGPointMake(txt.frame.origin.x - 14.5, 15);
    [msgView addSubview:txt];
    
    
    CGFloat size = 44.0f;
    if(!title1){
        txt.center = CGPointMake(txt.center.x, size/2);
        icon.center = CGPointMake(icon.center.x, size/2);
    }
    msgView.frame = CGRectMake(0, top, screenSize.width,  MAX(size, textSize.height + 10 + (title1 ? 55 : 0)));
    
    if (title1) {
        UIBlockButton *btnAction = [[UIBlockButton alloc] initWithFrame:CGRectMake(0, msgView.frame.size.height - 45, 150, 35)];
        [btnAction setTitle:title1 forState:UIControlStateNormal];
        CGPoint btnCenter = btnAction.center;
        btnAction.center = CGPointMake(title2 ? msgView.center.x - 80 : msgView.center.x, btnCenter.y);
        btnAction.layer.cornerRadius = 4;
        btnAction.titleLabel.font = [UIFont fontWithName:FONT_BOLD_NAME size:15];
        btnAction.layer.borderColor = [UIColor whiteColor].CGColor;
        btnAction.layer.borderWidth = 1;
        btnAction.backgroundColor = [UIColor clearColor];
        btnAction.tag = 101;
        [btnAction handleControlEvent:UIControlEventTouchUpInside withBlock:^(BOOL success) {
//            NSLog(@"btn1 tapped");
            [self hideLoading:view];
            if (handler1) {
                handler1(YES);
            }
        }];
        [msgView addSubview:btnAction];
        
        
    }
    
    
    if (title2) {
        UIBlockButton *btnAction = [[UIBlockButton alloc] initWithFrame:CGRectMake(0, msgView.frame.size.height - 45, 150, 35)];
        [btnAction setTitle:title2 forState:UIControlStateNormal];
        CGPoint btnCenter = btnAction.center;
        btnAction.center = CGPointMake(title1 ? msgView.center.x + 80 : msgView.center.x, btnCenter.y);
        btnAction.layer.cornerRadius = 4;
        btnAction.titleLabel.font = [UIFont fontWithName:FONT_BOLD_NAME size:15];
        btnAction.layer.borderColor = [UIColor whiteColor].CGColor;
        btnAction.layer.borderWidth = 1;
        btnAction.backgroundColor = [UIColor clearColor];
        btnAction.tag = 102;
        [btnAction handleControlEvent:UIControlEventTouchUpInside withBlock:^(BOOL success) {
            [self hideLoading:view];
            if (handler2) {
                handler2(YES);
            }
        }];
        [msgView addSubview:btnAction];
        
        
    }
    
    
    [UIView animateWithDuration:0.3 animations:^{
        
        //self.tableView.contentInset = UIEdgeInsetsMake(149, 0, 0, 0);
        
        msgView.alpha = 1;
    } completion:^(BOOL finished) {
        if (timer) {
            return ;
        }
        
        if (fromTableView) {
            UITableViewController *tableViewController = (UITableViewController*) viewController;
            UITableView *tableView = tableViewController.tableView;
            CGFloat navHeight = (tableViewController.navigationController &&
                                 !tableViewController.navigationController.isNavigationBarHidden &&
                                 tableViewController.navigationController.navigationBar.translucent
                                 ?
                                 tableViewController.navigationController.navigationBar.frame.size.height  : 0) +
            ([[UIApplication sharedApplication] statusBarFrame].size.height);
            timer = [NSTimer timerWithTimeInterval:0.5 repeats:YES block:^(NSTimer * _Nonnull timer) {
                //ispatch_async(dispatch_get_main_queue(), ^{
                if (![view viewWithTag:loadingTag]) {
                    [timer invalidate];
                    //NSLog(@"invalidate timer");
                    
                }
                CGRect msgFrame = msgView.frame;
                [UIView animateWithDuration:0.3 animations:^{
                    
                    msgView.frame = CGRectMake(0, navHeight + tableView.contentOffset.y,
                                               msgFrame.size.width,
                                               msgFrame.size.height);
                    
                    //msgView.frame = CGRectMake(0, navHeight + tableView.contentOffset.y, screenSize.width,
                    //                           [msgView viewWithTag:101] || [msgView viewWithTag:102] ? 85 : size);
                }];
                //});
            }];
            NSRunLoop *runner = [NSRunLoop currentRunLoop];
            [runner addTimer:timer forMode: NSDefaultRunLoopMode];
        }else{
            
            
            timer = [NSTimer timerWithTimeInterval:0.5 repeats:YES block:^(NSTimer * _Nonnull timer) {
                CGFloat top = fromTableView ? 0 : ((UIViewController*)view.nextResponder).navigationController &&
                !((UIViewController*)view.nextResponder).navigationController.isNavigationBarHidden
                && ((UIViewController*)view.nextResponder).navigationController.navigationBar.translucent
                ? 64 : 20;
                
                //dispatch_async(dispatch_get_main_queue(), ^{
                if (![view viewWithTag:loadingTag]) {
                    [timer invalidate];
                    //NSLog(@"invalidate timer");
                    
                }
                CGRect msgFrame = msgView.frame;
                [UIView animateWithDuration:0.3 animations:^{
                    
                    msgView.frame = CGRectMake(0, top,
                                               msgFrame.size.width,
                                               msgFrame.size.height);
                    //msgView.frame = CGRectMake(0, top, screenSize.width,
                    //                           [msgView viewWithTag:101] || [msgView viewWithTag:102] ? 85 : size);
                }];
                //});
            }];
            NSRunLoop *runner = [NSRunLoop currentRunLoop];
            [runner addTimer:timer forMode: NSDefaultRunLoopMode];
        }
        
    }];
    
    //}];
    
    
}


+ (void) showLoadingWithMessage:(NSString*) message withButton:(NSString*) title handler:(SuccessCompletion) handler inView:(UIView*) view{
    
    UIViewController *viewController = (UIViewController*)view.nextResponder;
    BOOL fromTableView = [viewController isKindOfClass:[UITableViewController class]];
    CGFloat top = fromTableView ? 0 : ((UIViewController*)view.nextResponder).navigationController &&
    !((UIViewController*)view.nextResponder).navigationController.isNavigationBarHidden
    && ((UIViewController*)view.nextResponder).navigationController.navigationBar.translucent
    ? 64 : ((UIViewController*)view.nextResponder).navigationController.navigationBar.translucent ? 20 : 0;
    
    
    
    //[self hideLoading:view finished:^(BOOL success) {
    NSString *txtIcon = @"";
    CGSize screenSize = [UIScreen mainScreen].bounds.size;
    UIView *msgView = [view viewWithTag:loadingTag];
    if (msgView) {
        msgView.frame = CGRectMake(0, top, screenSize.width, 0);
    }else{
        msgView = [[UIView alloc] initWithFrame:CGRectMake(0, top, screenSize.width, 0)];
    }
    
    NSArray *subViews = msgView.subviews;
    for (UIView *subView in subViews) {
        [subView removeFromSuperview];
    }
    
    msgView.alpha = 0;
    msgView.layer.zPosition = MAXFLOAT;
    msgView.tag = loadingTag;
    msgView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.85];
    UIView *tableContainer = view;
    [tableContainer addSubview:msgView];
    
    UILabel *icon = [[UILabel alloc] initWithFrame:CGRectMake(15, 15, 25, 35)];
    icon.text = txtIcon;
    icon.textColor = [txtIcon isEqualToString:@""] ? [UIColor yellowColor] : [UIColor whiteColor];
    icon.textAlignment = NSTextAlignmentCenter;
    icon.font = [UIFont fontWithName:FONT_AWESOME size:15];
    icon.tag = 98;
    icon.transform = CGAffineTransformMakeRotation(0);
    [msgView addSubview:icon];
    
    if([txtIcon isEqualToString:@""]){//spinner icon
        [self rotateSpinningView:view];
    }
    
    
    NSMutableParagraphStyle *style =  [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    style.alignment = NSTextAlignmentJustified;
    /*style.firstLineHeadIndent = 25.0f;
     style.headIndent = 25.0f;
     style.tailIndent = 0.0f;*/
    
    NSAttributedString *attrText = [[NSAttributedString alloc] initWithString:message
                                                                   attributes:@{ NSParagraphStyleAttributeName : style}];
    
    UILabel *txt = [[UILabel alloc] initWithFrame:CGRectMake(45, 15, msgView.frame.size.width - 60, 35)];
    txt.attributedText = attrText;
    txt.textAlignment = NSTextAlignmentCenter;
    txt.tag = 99;
    txt.minimumScaleFactor = .5f;
    txt.adjustsFontSizeToFitWidth = YES;
    txt.textColor = [UIColor whiteColor];
    txt.font = [UIFont fontWithName:FONT_BOLD_NAME size:15];
    [txt sizeToFit];
    
    [txt sizeToFit];
    
    CGRect txtRect = txt.frame;
    txtRect.size.width = MIN(screenSize.width - 55, txtRect.size.width);
    [txt setFrame:txtRect];
    [txt adjustsFontSizeToFitWidth];
    txt.center = CGPointMake(msgView.center.x + 11.5, 15);
    
    icon.center = CGPointMake(txt.frame.origin.x - 14.5, 15);
    
    
    
    [msgView addSubview:txt];
    
    if (title) {
        UIBlockButton *btnAction = [[UIBlockButton alloc] initWithFrame:CGRectMake(0, 0, 150, 35)];
        [btnAction setTitle:title forState:UIControlStateNormal];
        btnAction.center = CGPointMake(msgView.center.x, 55);
        btnAction.layer.cornerRadius = 4;
        btnAction.titleLabel.font = [UIFont fontWithName:FONT_BOLD_NAME size:15];
        btnAction.layer.borderColor = [UIColor whiteColor].CGColor;
        btnAction.layer.borderWidth = 1;
        btnAction.backgroundColor = [UIColor clearColor];
        btnAction.tag = 101;
        [btnAction handleControlEvent:UIControlEventTouchUpInside withBlock:^(BOOL success) {
            [self hideLoading:view];
            if (handler) {
                handler(YES);
            }
        }];
        [msgView addSubview:btnAction];
        
        /*UIButton *btnClose = [[UIButton alloc] initWithFrame:CGRectMake(msgView.frame.size.width - 45, txt.frame.origin.y, 35, 35)];
         [btnClose setTitle:@"" forState:UIControlStateNormal];
         btnClose.titleLabel.font = [UIFont fontWithName:FONT_AWESOME size:15];
         btnClose.backgroundColor = [UIColor clearColor];
         [btnClose addTarget:self
         action:@selector(hideConnectionWarning:)
         forControlEvents:UIControlEventTouchUpInside];
         [msgView addSubview:btnClose];*/
    }
    
    
    CGFloat size = 44.0f;
    if(!title){
        txt.center = CGPointMake(txt.center.x, size/2);
        icon.center = CGPointMake(icon.center.x, size/2);
    }
    msgView.frame = CGRectMake(0, top, screenSize.width,  title ? 85 : size);
    [UIView animateWithDuration:0.3 animations:^{
        
        //self.tableView.contentInset = UIEdgeInsetsMake(149, 0, 0, 0);
        
        msgView.alpha = 1;
    } completion:^(BOOL finished) {
        if (timer) {
            return ;
        }
        if (fromTableView) {
            UITableViewController *tableViewController = (UITableViewController*) viewController;
            UITableView *tableView = tableViewController.tableView;
            CGFloat navHeight = (tableViewController.navigationController &&
                                 !tableViewController.navigationController.isNavigationBarHidden
                                 && tableViewController.navigationController.navigationBar.translucent
                                 ? tableViewController.navigationController.navigationBar.frame.size.height  : 0) +
            ([[UIApplication sharedApplication] statusBarFrame].size.height);
            timer = [NSTimer timerWithTimeInterval:0.5 block:^{
                if (![view viewWithTag:loadingTag]) {
                    [timer invalidate];
                    //NSLog(@"invalidate timer");
                    
                }
                [UIView animateWithDuration:0.3 animations:^{
                    
                    msgView.frame = CGRectMake(0, navHeight + tableView.contentOffset.y, screenSize.width,
                                               [msgView viewWithTag:101] || [msgView viewWithTag:102] ? 85 : size);
                }];
            } repeats:YES];
            
            NSRunLoop *runner = [NSRunLoop currentRunLoop];
            [runner addTimer:timer forMode: NSDefaultRunLoopMode];
            
        }else{
            
            
            timer = [NSTimer timerWithTimeInterval:0.5 block:^{
                CGFloat top = fromTableView ? 0 : ((UIViewController*)view.nextResponder).navigationController &&
                !((UIViewController*)view.nextResponder).navigationController.isNavigationBarHidden
                && ((UIViewController*)view.nextResponder).navigationController.navigationBar.translucent
                ? 64 : ((UIViewController*)view.nextResponder).navigationController.navigationBar.translucent ? 20 : 0;
                
                //dispatch_async(dispatch_get_main_queue(), ^{
                if (![view viewWithTag:loadingTag]) {
                    [timer invalidate];
                    //NSLog(@"invalidate timer");
                    
                }
                [UIView animateWithDuration:0.3 animations:^{
                    
                    msgView.frame = CGRectMake(0, top, screenSize.width,
                                               [msgView viewWithTag:101] || [msgView viewWithTag:102] ? 85 : size);
                    
                }];
                //});
            } repeats:YES];
            NSRunLoop *runner = [NSRunLoop currentRunLoop];
            [runner addTimer:timer forMode: NSDefaultRunLoopMode];
        }
        
    }];
    
    //}];
    
    
    
    //progress.color = [UIColor colorWithHexString:COLOR_TINT];
    //progress.labelColor = [UIColor whiteColor];
    
}

+ (void) changeLoadingMessage:(NSString*) message icon:(NSString*) txtIcon {
    UIView *view = [self currentContainerView];
    UIView *msgView = [view viewWithTag:loadingTag];
    if (msgView) {
        CGFloat size= msgView.frame.size.height;
        if (message) {
            CGSize screenSize = [UIScreen mainScreen].bounds.size;
            UILabel *txt = [msgView viewWithTag:99];
            txt.text = message;
            CGRect txtRect = txt.frame;
            txtRect.size.width = MIN(screenSize.width - 55, txtRect.size.width);
            [txt setFrame:txtRect];
            [txt adjustsFontSizeToFitWidth];
            txt.center = CGPointMake(msgView.center.x + 11.5, 15);
            txt.center = CGPointMake(txt.center.x, size/2);
        }
        
        if (txtIcon) {
            UILabel *icon = [msgView viewWithTag:98];
            icon.text = txtIcon;
            icon.transform = CGAffineTransformMakeRotation(0);
            icon.center = CGPointMake(icon.center.x, size/2);
            if([txtIcon isEqualToString:@""]){//spinner icon
                [self rotateSpinningView:view];
            }
            
        }
        
        
    }
}


+ (void) showLoadingWithMessage:(NSString*) message {
    
    UIView *view = [self currentContainerView];
    /*MBProgressHUD *progress = [MBProgressHUD HUDForView:view];
     
     loadingCnt++;
     if (!progress) {
     progress = [MBProgressHUD showHUDAddedTo:view animated:YES];
     progress.labelFont = [UIFont fontWithName:@"CenturyGothic" size:15];
     progress.labelText = message;
     }else{
     progress.labelText = message;
     [progress show:YES];
     }*/
    //progress.color = [UIColor colorWithHexString:COLOR_TINT];
    //progress.labelColor = [UIColor whiteColor];
    [self showLoadingWithMessage:message inView:view];
}

+ (void) showLoadingWithMessage:(NSString*) message withIcon:(NSString*) icon {
    
    UIView *view = [self currentContainerView];
    
    [self showLoadingWithMessage:message icon:icon inView:view];
}

+ (void) hideLoading:(UIView*) view finished:(SuccessCompletion) handler {
    BOOL fromTableView = [view.nextResponder isMemberOfClass:[UITableViewController class]];
    
    if (fromTableView) {
        UITableViewController *controller = (UITableViewController*)view.nextResponder;
        controller.tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    }
    
    UIView *tableContainer = view;
    UIView *msgView = [tableContainer viewWithTag:loadingTag];
    if (msgView) {
        //CGSize screenSize = [UIScreen mainScreen].bounds.size;
        [UIView animateWithDuration:0.3 animations:^{
            msgView.alpha = 0;
        } completion:^(BOOL finished) {
            [msgView removeFromSuperview];
            if (handler) {
                handler(finished);
            }
        }];
        
    }else{
        if (handler) {
            handler(YES);
        }
    }
}

+ (void) hideLoading:(UIView*) view  {
    /*if (--loadingCnt <= 0) {
     [MBProgressHUD hideHUDForView:view animated:YES];
     loadingCnt = 0;
     }*/
    
    
    [self hideLoading:view finished:nil];
    
}

+ (void) hideLoading  {
    
    
    [self hideLoading:[self currentContainerView] finished:nil];
    
}

+ (void) hideLoadingAndChangeMessage:(NSString *) message {
    
    //    UIView *view = [UIApplication sharedApplication].keyWindow.subviews.firstObject;
    UIView *view = [self currentContainerView];
    /*
     if (--loadingCnt <= 0) {
     [MBProgressHUD hideAllHUDsForView:view animated:YES];
     loadingCnt = 0;
     }else{
     MBProgressHUD *progress = [MBProgressHUD HUDForView:view];
     progress.labelText = message;
     }
     NSLog(@"loadingCnt = %i", loadingCnt);
     */
    [self hideLoading:view];
    
}

+ (NSString *) md5:(NSString *)str {
    const char *cStr = [str UTF8String];
    unsigned char result[16];
    CC_MD5( cStr, (unsigned int)strlen(cStr), result );
    return [NSString stringWithFormat:
            @"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
            result[0], result[1], result[2], result[3],
            result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11],
            result[12], result[13], result[14], result[15]
            ];
}

+ (void) logData: (NSData*) data {
    NSLog(@"Log: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
}


+ (void) showError:(NSError*) error completion:(SuccessCompletion) handler {
    
    UIView *view = [self currentContainerView];
    [self showLoadingWithMessage:error.localizedDescription
                            icon:@""
                     withButton1:[AppHelper getLabelForThai:@"ตกลง" eng:@"OK"]
                        handler1:handler
                     withButton2:nil
                        handler2:nil
                          inView:view];
    
    /*
     [[[UIAlertView alloc] initWithTitle:@"Error"
     message:error.localizedDescription
     cancelButtonItem:[RIButtonItem itemWithLabel:[AppHelper getLabelForThai:@"ตกลง" eng:@"OK"] action:^{
     if (handler) {
     handler(YES);
     }
     }]
     otherButtonItems:nil, nil] show];
     */
    
}

+ (void) showErrorMessage:(NSString*) message completion:(SuccessCompletion) handler {
    
    UIView *view = [self currentContainerView];
    [self showLoadingWithMessage:message
                            icon:@""
                     withButton1:[AppHelper getLabelForThai:@"ตกลง" eng:@"OK"]
                        handler1:handler
                     withButton2:nil
                        handler2:nil
                          inView:view];
    /*
     [[[UIAlertView alloc] initWithTitle:@"Error"
     message:message
     cancelButtonItem:[RIButtonItem itemWithLabel:[AppHelper getLabelForThai:@"ตกลง" eng:@"OK"] action:^{
     if (handler) {
     handler(YES);
     }
     }]
     otherButtonItems:nil, nil] show];*/
}

+ (void) setPastelBackgroundInView:(UIView *) view verticalStrip:(BOOL) verticalStrip {
    NSArray *colors = @[@"#ffe6e6", @"#fcf8c1", @"#defcce", @"#e9dbf9", @"#ceeef9"];
    //NSArray *colors = @[@"#1c1950", @"#f0ff00", @"#eeeeee", @"#F47F20", @"#404041"];
    CGFloat screenHeight = view.frame.size.height;
    CGFloat screenWidth = view.frame.size.width;
    CGFloat factor = 0;
    for (int i = 0; i < colors.count; i++) {
        UIView *stripView = nil;
        if (verticalStrip) {
            CGFloat width = screenWidth / colors.count;
            
            //width = (i == 2 ? screenWidth - ((width - 50)*4) : width - 50);
            
            stripView = [[UIView alloc] initWithFrame:CGRectMake(factor, 0, width, screenHeight+ 100)];
            factor += width;
        }else{
            CGFloat height = screenHeight / colors.count;
            
            stripView = [[UIView alloc] initWithFrame:CGRectMake(0, factor, screenWidth, height + 100)];
            factor += height;
        }
        stripView.tag = 1199;
        [stripView setBackgroundColor:[UIColor colorWithHexString:[colors objectAtIndex:i]]];
        [view addSubview:stripView];
    }
    
    for (UIView *subView in view.subviews) {
        if (subView.tag != 1199) {
            [view bringSubviewToFront:subView];
        }
    }
}

+ (void) alertVibrate {
    AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
}

+ (void) alertSound {
    
    if (!checker) {
        checker = [[MuteChecker alloc] initWithCompletionBlk:^(NSTimeInterval lapse, BOOL muted) {
            
            //            NSLog(@"mute %d", muted);
            if (muted) {
                AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
            }else{
                //Ref http://iphonedevwiki.net/index.php/AudioServices
                AudioServicesPlaySystemSound(1307);
            }
        }];
    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [checker check];
    });
    
}

+ (void) incomingCallAlert {
    
    AudioServicesPlayAlertSound(1154);
    
}

+ (void) hideBackBarButtonText:(UIViewController *)viewController {
    
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 29, 29)];
    lbl.text = @"";
    lbl.font = [UIFont fontWithName:@"FontAwesome" size:40];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithImage:[UIImage imageWithText:@""
                                                                 inSize:CGSizeMake(15, 29)
                                                               withFont:[UIFont fontWithName:@"FontAwesome" size:40]]
                                   style: UIBarButtonItemStyleDone
                                   target: viewController.navigationController action: @selector(popViewControllerAnimated:)];
    
    
    viewController.navigationItem.leftBarButtonItem = backButton;
}


+ (void) checkVersionFromURL:(NSString *) serviceURL completion:(AppVersionCompletion) completion {
    
    
    
    
    
    URLConnection *con = [[URLConnection alloc] init];
    
    NSMutableURLRequest *req = [URLConnection requestFromURL:serviceURL method:@"GET" parameter:nil];
    [req setValue:[AppHelper getUserDataForKey:kADA_CLIENT] forHTTPHeaderField:kADA_CLIENT];
    [con sendAsynchronousRequest: req completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        if(connectionError){
            if (completion) {
                completion(NO, nil, nil, nil, nil);
            }
            /*[[[UIAlertView alloc] initWithTitle:@"Error"
             message:connectionError.description
             delegate:nil
             cancelButtonTitle:[AppHelper getLabelForThai:@"ตกลง" eng:@"OK"]
             otherButtonTitles:nil, nil] show];
             */
            return ;
        }
        
        NSJSONSerialization *json = [AppHelper jsonWithData:data];
        
        if ([json valueForKey:@"ios"]) {
            NSString *version = [[json valueForKey:@"ios"] valueForKey:@"version"];
            NSString *url = [[json valueForKey:@"ios"] valueForKey:@"app_path"];
            
            
            
            NSString * appVersionString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
            
            
            int lastVer = 0;
            int lastMajor = 0;
            int lastMinor = 0;
            
            int currVer = 0;
            int currMajor = 0;
            int currMinor = 0;
            
            NSString *lastestVersion = [NSString stringWithFormat:@"%@", version];
            NSLog(@"version = %@", lastestVersion);
            NSArray *arrVers = [lastestVersion componentsSeparatedByString:@"."];
            if ([arrVers count] > 2) {
                lastVer = [[arrVers objectAtIndex:0] intValue];
                lastMajor = [[arrVers objectAtIndex:1] intValue];
                lastMinor = [[arrVers objectAtIndex:2] intValue];
            }else if([arrVers count] > 1){
                lastVer = [[arrVers objectAtIndex:0] intValue];
                lastMajor = [[arrVers objectAtIndex:1] intValue];
            }else{
                lastVer = [lastestVersion intValue];
            }
            
            NSString *currentVersion = appVersionString;
            
            NSArray *arrCurrVers = [currentVersion componentsSeparatedByString:@"."];
            if ([arrCurrVers count] > 2) {
                currVer = [[arrCurrVers objectAtIndex:0] intValue];
                currMajor = [[arrCurrVers objectAtIndex:1] intValue];
                currMinor = [[arrCurrVers objectAtIndex:2] intValue];
            }else if([arrCurrVers count] > 1){
                currVer = [[arrCurrVers objectAtIndex:0] intValue];
                currMajor = [[arrCurrVers objectAtIndex:1] intValue];
            }else{
                currVer = [currentVersion intValue];
            }
            
            BOOL foundUpdate = NO;
            if (lastVer > currVer) {
                foundUpdate = YES;
            }else if(lastMajor > currMajor){
                foundUpdate = YES;
            }else if(lastMinor > currMinor){
                foundUpdate = YES;
            }
            
            
            if (completion) {
                completion(foundUpdate, version, appVersionString, url, json);
            }
            
            
        }
    }];
}

+ (void) showAlert:(NSString *) message handler:(void (^ __nullable)(UIAlertAction *action))handler {
    UIViewController *viewController = [self currentContainerViewController];
    
    UIAlertController *alertView = [UIAlertController alertControllerWithTitle:viewController.navigationItem.title
                                                                       message:message
                                                                preferredStyle:UIAlertControllerStyleAlert];
    [alertView addAction:[UIAlertAction actionWithTitle:[self getLabelForThai:@"ตกลง" eng:@"OK"]
                                                  style:UIAlertActionStyleCancel
                                                handler:handler]];
    if (viewController.presentedViewController) {
        [viewController.presentedViewController presentViewController:alertView animated:YES completion:nil];
    }else{
        [viewController presentViewController:alertView animated:YES completion:nil];
    }
}


+ (NSString*) getLanguage {
    //NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    //NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    //return [userDefaults valueForKey:kLanguage];
    return [AppHelper getUserDataForKey:kLanguage];
}

+ (BOOL) isThaiLanguage {
    NSString *lang = [self getLanguage];
    if (!lang) {
        BOOL thaiLang = [[NSLocale currentLocale].localeIdentifier rangeOfString:@"th_TH"].location != NSNotFound;
        return thaiLang;
    }
    return [@"TH" isEqualToString:lang];
}



+ (NSString*) getLabelForThai:(NSString*) labelThai eng:(NSString*) labelEng {
    return [self isThaiLanguage] ? labelThai : labelEng;
}

+ (void)calculatePosition:(CGRect)keyboardFrameBeginRect origin:(CGPoint)origin viewController:(UIViewController*) viewController baseView:(UIView*) baseView {
    CGFloat height = keyboardFrameBeginRect.size.height;
    CGFloat viewHeight = viewController.view.frame.size.height;
    CGFloat bY = baseView.center.y;
    CGFloat vY = origin.y;
    
    
    
    
    if (bY > vY) {// baseView below center
        //NSLog(@"baseView below center");
        CGFloat baseMargin = (viewHeight - (baseView.frame.origin.y + baseView.frame.size.height + 8));
        CGFloat y = vY - (height - baseMargin);
        //y = y - (bY - vY);
        CGPoint location = CGPointMake(origin.x, y);
        viewController.view.center = location;
    }else{
        //NSLog(@"baseView above center");
        CGFloat baseMargin = (viewHeight - (baseView.frame.origin.y + baseView.frame.size.height + 8));
        CGFloat y = vY - (height - baseMargin);
        //CGFloat y = vY - height;
        if(y < origin.y){
            CGPoint location = CGPointMake(origin.x, y);
            viewController.view.center = location;
        }
    }
}

+ (UIView*) getFirstResponderView:(UIView*) view {
    
    
    
    return [UIResponder currentFirstResponder];
    
}

+ (void) observeKeyboard:(UIViewController* _Nonnull __strong) viewController  {
    
    if(!observerMap) {
        observerMap = [[NSMutableDictionary alloc] init];
    }
    
    
    [viewController.view endEditing:YES];
    
    
    
    showingKeyboard = NO;
    [observerMap setObject:@"N" forKey:[viewController description]];
    __block CGPoint origin = viewController.view.center;
    __block CGRect keyboardFrameBeginRect;
    __block float duration;
 
    
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter] removeObserver:viewController
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:viewController
                                                    name:UIDeviceOrientationDidChangeNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:UIDeviceOrientationDidChangeNotification
                                                      object:[UIDevice currentDevice]
                                                       queue:nil
                                                  usingBlock:^(NSNotification *notification) {
                                                      
                                                      //NSLog(@"y : %f", viewController.view.frame.origin.y);
                                                      if([[observerMap valueForKey:[viewController description]] isEqualToString:@"Y"]){
                                                          return;
                                                      }
                                                      
                                                      origin = viewController.view.center;
                                                      if(!showingKeyboard || viewController.view.frame.origin.y < 0){
                                                          return;
                                                      }
                                                      
                                                      CGFloat height = keyboardFrameBeginRect.size.height;
                                                      dispatch_async(dispatch_get_main_queue(), ^{
                                                          [UIView animateWithDuration:duration animations:^{
                                                              
                                                              
                                                              
                                                              //UIView *baseView = [self getFirstResponderView:viewController.view];
                                                              
                                                              //NSLog(@"baseView : %@", baseView);
                                                              //if(baseView)
                                                              {
                                                                  /*[self calculatePosition:keyboardFrameBeginRect
                                                                   origin:origin
                                                                   viewController:viewController
                                                                   baseView:baseView];
                                                                   */
                                                                  
                                                              }//else
                                                              
                                                              {
                                                                  CGPoint location = CGPointMake(origin.x, origin.y - height);
                                                                  viewController.view.center = location;
                                                              }
                                                              
                                                              
                                                          }];
                                                      });

                                                  }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:UIKeyboardWillShowNotification
                                                      object:nil
                                                       queue:nil
                                                  usingBlock:^(NSNotification *notification) {
                                      
                                                      if([[observerMap valueForKey:[viewController description]] isEqualToString:@"Y"]){
                                                          return;
                                                      }
                                                      
                                                      NSDictionary* keyboardInfo = [notification userInfo];
                                                      
                                                      NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
                                                      keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
                                                      duration = [[keyboardInfo valueForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue] + 0.2;
                                                      CGFloat height = keyboardFrameBeginRect.size.height;
                                                      
                                                      
                                                      UIView *responderView  = [self getFirstResponderView:viewController.view];
                                                      
                                                      CGRect frame = responderView.frame;
                                                      
                                                      
                                                      
                                                      
                                                      CGFloat positionY = frame.origin.y + frame.size.height;
                                                      
                                                      
                                                      
                                                      UITableViewCell *cell = (UITableViewCell*)[self getClosestView:responderView of:[UITableViewCell class]];
                                                      
                                                      if(cell){
                                                          positionY = 0;
                                                          UITableView *tableView = (UITableView*)[self getClosestView:responderView of:[UITableView class]];
                                                          NSIndexPath *indexPath = [tableView indexPathForCell:cell];
                                                          CGRect rect = [tableView rectForRowAtIndexPath:indexPath];
                                                          
                                                          CGFloat tabHeight = 0;
                                                          if(viewController.tabBarController){
                                                              tabHeight = viewController.tabBarController.tabBar.frame.size.height;
                                                          }
                                                          
                                                          /*
                                                          if((rect.origin.y - (keyboardFrameBeginRect.size.height - tabHeight)) < 8){
                                                              return;
                                                          }*/
                                                          positionY = tableView.frame.origin.y + rect.origin.y;
                                                          [tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:NO];
                                                          
                                                      }

                                                      
                                                      CGFloat keyBoardPosition = viewController.view.frame.size.height - height;
                                                      NSLog(@"positionY %f", positionY);
                                                      NSLog(@"keyBoardPosition %f", keyBoardPosition);
                                                      if(positionY < (keyBoardPosition - 32)) {
                                                          return;
                                                      }
                                                      
                                                      dispatch_async(dispatch_get_main_queue(), ^{
                                                          [UIView animateWithDuration:duration animations:^{
                                                              
                                                              [observerMap setObject:@"Y" forKey:[viewController description]];
                                                              showingKeyboard = YES;
                                                              //UIView *baseView = [self getFirstResponderView:viewController.view];
                                                              
                                                              //NSLog(@"baseView : %@", baseView);
                                                              //if(baseView)
                                                              {
                                                                  /*[self calculatePosition:keyboardFrameBeginRect
                                                                                   origin:origin
                                                                           viewController:viewController
                                                                                 baseView:baseView];
                                                                  */
                                                                  
                                                              }//else
                                                              
                                                              {
                                                                  CGPoint location = CGPointMake(origin.x, origin.y - height);
                                                                  viewController.view.center = location;
                                                              }
                                                              
                                                              
                                                          }];
                                                      });
                                                      
                                                  }];
    
    
    [[NSNotificationCenter defaultCenter] removeObserver:viewController
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserverForName:UIKeyboardWillHideNotification
                                                      object:nil
                                                       queue:nil
                                                  usingBlock:^(NSNotification *notification) {
                                                      NSDictionary* keyboardInfo = [notification userInfo];
                                                      float duration = [[keyboardInfo valueForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
                                                      showingKeyboard = NO;
                                                      [observerMap setObject:@"N" forKey:[viewController description]];
                                                      dispatch_async(dispatch_get_main_queue(), ^{
                                                          [UIView animateWithDuration:duration animations:^{
                                                              
                                                              viewController.view.center = origin;
                                                          }];
                                                          
                                                      });
                                                      
                                                      
                                                      
                                                  }];
    
}

+ (void) observeKeyboard:(UIViewController* _Nonnull) viewController inTableView:(UITableView *)tableView {
    if(!observerMap) {
        observerMap = [[NSMutableDictionary alloc] init];
    }
    
    
    [viewController.view endEditing:YES];
    showingKeyboard = NO;
    [observerMap setObject:@"N" forKey:[viewController description]];
    
    CGPoint origin = viewController.view.center;
    
    [[NSNotificationCenter defaultCenter] removeObserver:viewController
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] addObserverForName:UIKeyboardWillShowNotification
                                                      object:nil
                                                       queue:nil
                                                  usingBlock:^(NSNotification *notification) {
                                            
                                                      if([[observerMap valueForKey:[viewController description]] isEqualToString:@"Y"]){
                                                          return;
                                                      }
                                                      
                                                      NSDictionary* keyboardInfo = [notification userInfo];
                                                      NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
                                                      CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
                                                      
                                                      UIView *textView = [self getFirstResponderView:viewController.view];
                                                      
                                                      
                                                      UITableViewCell *cell = (UITableViewCell*)[self getClosestView:textView of:[UITableViewCell class]];
                                                      CGFloat y = 0;
                                                      NSIndexPath *currentIndexPath = nil;
                                                      if(cell){
                                                          NSIndexPath *indexPath = [tableView indexPathForCell:cell];
                                                          CGRect rect = [tableView rectForRowAtIndexPath:indexPath];
                                                          
                                                          CGFloat tabHeight = 0;
                                                          if(viewController.tabBarController){
                                                              tabHeight = viewController.tabBarController.tabBar.frame.size.height;
                                                          }
                                                          
                                                          
                                                          if((rect.origin.y - (keyboardFrameBeginRect.size.height - tabHeight)) < 30){
                                                              return;
                                                          }
                                                          y = rect.origin.y;
                                                          
                                                          currentIndexPath = [tableView indexPathForCell:cell];
                                                      }
                                                      showingKeyboard = YES;
                                                      [observerMap setObject:@"Y" forKey:[viewController description]];
                                                      //move frame up for ensure visible textbox
                                                      [UIView animateWithDuration:0.3f
                                                                       animations:^{
                                                                           CGFloat tabHeight = 0;
                                                                           if(viewController.tabBarController){
                                                                               tabHeight = viewController.tabBarController.tabBar.frame.size.height;
                                                                           }
                                                                           
                                                                           CGFloat height = keyboardFrameBeginRect.size.height - tabHeight;
                                                                           CGPoint location = CGPointMake(origin.x, origin.y - (height));
                                                                           viewController.view.center = location;
                                                                       }
                                                                       completion:^(BOOL finished){
                                                                           if (currentIndexPath) {
                                                                               [tableView scrollToRowAtIndexPath:currentIndexPath
                                                                                                 atScrollPosition:UITableViewScrollPositionBottom
                                                                                                         animated:YES];
                                                                           }else{
                                                                               
                                                                               [tableView scrollToRowAtIndexPath:tableView.indexPathForSelectedRow
                                                                                                 atScrollPosition:UITableViewScrollPositionBottom
                                                                                                         animated:YES];
                                                                           }
                                                                       }];
                                                      
                                                  }];
    
    
    
    
    [[NSNotificationCenter defaultCenter] removeObserver:viewController
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserverForName:UIKeyboardWillHideNotification
                                                      object:nil
                                                       queue:nil
                                                  usingBlock:^(NSNotification *notification) {
                                                      NSDictionary* keyboardInfo = [notification userInfo];
                                                      float duration = [[keyboardInfo valueForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
                                                      showingKeyboard = NO;
                                                      [observerMap setObject:@"N" forKey:[viewController description]];
                                                      dispatch_async(dispatch_get_main_queue(), ^{
                                                          [UIView animateWithDuration:duration animations:^{
                                                              
                                                              viewController.view.center = origin;
                                                          }];
                                                          
                                                      });
                                                      
                                                      
                                                      
                                                  }];
}

+ (UIView*) getClosestView:(UIView*) view of:(Class) aClass {
    
    if(view){
        if([view isKindOfClass:aClass]){
            return view;
        }else{
            return [self getClosestView:view.superview of:aClass];
        }
    }
    
    return nil;
}

+ (void) observeKeyboard:(UIViewController* _Nonnull) viewController baseView:(UIView* _Nonnull) baseView {
    if(!observerMap) {
        observerMap = [[NSMutableDictionary alloc] init];
    }
    
    [viewController.view endEditing:YES];
    showingKeyboard = NO;
    [observerMap setObject:@"N" forKey:[viewController description]];
    
    CGPoint origin = viewController.view.center;
    
    [[NSNotificationCenter defaultCenter] removeObserver:viewController
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] addObserverForName:UIKeyboardWillShowNotification
                                                      object:nil
                                                       queue:nil
                                                  usingBlock:^(NSNotification *notification) {
                                                      
                                                      
                                                      
                                                      if([[observerMap valueForKey:[viewController description]] isEqualToString:@"Y"]){
                                                          return;
                                                      }
                                                      
                                                      NSDictionary* keyboardInfo = [notification userInfo];
                                                      
                                                      NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
                                                      CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
                                                      float duration = [[keyboardInfo valueForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
                                                      showingKeyboard = YES;
                                                      [observerMap setObject:@"Y" forKey:[viewController description]];
                                                      dispatch_async(dispatch_get_main_queue(), ^{
                                                          [UIView animateWithDuration:duration animations:^{
                                                              
                                                              [self calculatePosition:keyboardFrameBeginRect
                                                                               origin:origin
                                                                       viewController:viewController
                                                                             baseView:baseView];
                                                          }];
                                                      });
                                                      
                                                  }];
    
    /*
     [[NSNotificationCenter defaultCenter] removeObserver:viewController
     name:UIKeyboardWillChangeFrameNotification
     object:nil];
     
     [[NSNotificationCenter defaultCenter] addObserverForName:UIKeyboardWillChangeFrameNotification
     object:nil
     queue:nil
     usingBlock:^(NSNotification *notification) {
     
     NSDictionary* keyboardInfo = [notification userInfo];
     
     NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
     CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
     
     dispatch_async(dispatch_get_main_queue(), ^{
     [UIView animateWithDuration:0.3f animations:^{
     [self calculatePosition:keyboardFrameBeginRect
     origin:origin
     viewController:viewController
     baseView:baseView];
     }];
     });
     }];
     
     */
    
    [[NSNotificationCenter defaultCenter] removeObserver:viewController
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserverForName:UIKeyboardWillHideNotification
                                                      object:nil
                                                       queue:nil
                                                  usingBlock:^(NSNotification *notification) {
                                                      NSDictionary* keyboardInfo = [notification userInfo];
                                                      float duration = [[keyboardInfo valueForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
                                                      showingKeyboard = NO;
                                                      [observerMap setObject:@"N" forKey:[viewController description]];
                                                      dispatch_async(dispatch_get_main_queue(), ^{
                                                          [UIView animateWithDuration:duration animations:^{
                                                              
                                                              viewController.view.center = origin;
                                                          }];
                                                          
                                                      });
                                                      
                                                      
                                                      
                                                  }];
    
}


+ (UIImage*) getImageIcon:(NSString*) icon withSize:(CGSize) size {
    UIFont *font = [UIFont fontWithName:@"FontAwesome" size:size.width-8];
    return [UIImage imageWithText:icon inSize:size withFont:font];
}

+ (UIImage*) getImageIcon:(NSString*) icon withSize:(CGSize) size color:(UIColor*) color {
    UIFont *font = [UIFont fontWithName:@"FontAwesome" size:size.width-8];
    return [UIImage imageWithText:icon inSize:size color:color withFont:font];
}

+ (UIImage* _Nullable) getImageIcon:(NSString* _Nonnull) icon withSize:(CGSize) size fontSize:(CGFloat) fontSize color:(UIColor* _Nullable) color {
    UIFont *font = [UIFont fontWithName:@"FontAwesome" size:fontSize];
    return [UIImage imageWithText:icon inSize:size color:color withFont:font];
}


static NSMutableDictionary<NSString*, id> *persistData;
static CoreDataUtils *userDataDB;
static AppData *userData;


+ (BOOL) setUserData: (id) data forKey:(NSString* _Nonnull) key {
    
    if(!userDataDB) {
        userDataDB = [[CoreDataUtils alloc] initDBName:@"USERDATA"];
    }
    
    
    if(!persistData) {
        persistData = [[NSMutableDictionary alloc] init];
        [persistData setValue:data forKey:key];
    }
    
    [persistData setValue:data forKey:key];
    if(!userData) {
        userData = [userDataDB getObjectOrNewEntityForName:@"AppData" withValue:@"user-data" forFieldName:@"key"];
        userData.key =  @"user-data";
    }
    NSData *infoData = [NSKeyedArchiver archivedDataWithRootObject:persistData];
    userData.data =  infoData;
    
    
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    NSURL *tmpDirURL = [NSURL fileURLWithPath:NSTemporaryDirectory() isDirectory:YES];
    NSString *imgFolder = [NSString stringWithFormat:@"%@ada-user-data", tmpDirURL];
    imgFolder = [imgFolder stringByReplacingOccurrencesOfString:@"file://" withString:@""];
    if (![fileMgr fileExistsAtPath:imgFolder]) {
        [fileMgr createDirectoryAtPath:imgFolder withIntermediateDirectories:YES attributes:nil error:nil];
    }
    NSString *strPath = [NSString stringWithFormat:@"%@/%@", imgFolder, key];
    [[NSKeyedArchiver archivedDataWithRootObject:data] writeToFile:strPath atomically:YES];
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:data] forKey:key];
    
    
    [self syncUserData];
    
    
    return YES;
}

+ (id) getUserDataForKey:(NSString* _Nonnull) key  {
    
    
    
    if(!userDataDB) {
        userDataDB = [[CoreDataUtils alloc] initDBName:@"USERDATA"];
    }
    
    if(!persistData) {
        if(!userData) {
            userData = [userDataDB getObjectOrNewEntityForName:@"AppData" withValue:@"user-data" forFieldName:@"key"];
            userData.key =  @"user-data";
        }
        if(userData) {
            NSData *data = userData.data;
            if(data) {
                persistData = [NSKeyedUnarchiver unarchiveObjectWithData:data];
            }
        }
    }
    if(persistData) {
        return [persistData valueForKey:key];
    }else{
        
        NSFileManager *fileMgr = [NSFileManager defaultManager];
        NSURL *tmpDirURL = [NSURL fileURLWithPath:NSTemporaryDirectory() isDirectory:YES];
        NSString *imgFolder = [NSString stringWithFormat:@"%@ada-user-data", tmpDirURL];
        imgFolder = [imgFolder stringByReplacingOccurrencesOfString:@"file://" withString:@""];
        NSString *strPath = [NSString stringWithFormat:@"%@/%@", imgFolder, key];
        if ([fileMgr fileExistsAtPath:strPath]){
            NSData* data = [fileMgr contentsAtPath:strPath];
            if(data && data.length > 0) {
                id userData = [NSKeyedUnarchiver unarchiveObjectWithData:data];
                if(userData != nil) {
                    return userData;
                }
            }
        }
        
        NSData* data = [[NSUserDefaults standardUserDefaults] dataForKey:key];
        id userData = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        if(userData != nil) {
            return userData;
        }
        
        return nil;
    }
    
}

+ (BOOL) removeUserDataForKey:(NSString* _Nonnull) key  {
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    NSURL *tmpDirURL = [NSURL fileURLWithPath:NSTemporaryDirectory() isDirectory:YES];
    NSString *imgFolder = [NSString stringWithFormat:@"%@ada-user-data", tmpDirURL];
    imgFolder = [imgFolder stringByReplacingOccurrencesOfString:@"file://" withString:@""];
    NSString *strPath = [NSString stringWithFormat:@"%@/%@", imgFolder, key];
    NSError *error;
    if([fileMgr fileExistsAtPath:strPath]) {
        [fileMgr removeItemAtPath:strPath error: &error];
        if(error) {
            NSLog(@"Error : %@", error);
        }
    }
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:key];
    
    if(!userDataDB) {
        userDataDB = [[CoreDataUtils alloc] initDBName:@"USERDATA"];
    }
    
    
    if(!userData) {
        userData = [userDataDB getObjectOrNewEntityForName:@"AppData" withValue:@"user-data" forFieldName:@"key"];
        userData.key =  @"user-data";
    }
    if(!persistData) {
        if(userData) {
            NSData *data = userData.data;
            if(data) {
                persistData = [NSKeyedUnarchiver unarchiveObjectWithData:data];
            }
        }
    }
    if(persistData) {
        [persistData removeObjectForKey:key];
        NSData *infoData = [NSKeyedArchiver archivedDataWithRootObject:persistData];
        userData.data =  infoData;

        
        [self syncUserData];
        
        
        return YES;
    }else{
        return NO;
    }
    
    
    
}

+ (void) syncUserData {
    if(userDataDB) {
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            [userDataDB saveContext];
            NSLog(@"User Data Updated.");
        });
    }
}


CGFloat DegreesToRadians1(CGFloat degrees)
{
    return degrees * M_PI / 180;
};

CGFloat RadiansToDegrees1(CGFloat radians)
{
    return radians * 180 / M_PI;
};

+ (NSNumber*)distFromLat:(float) lat1 FromLng:(float) lng1 ToLat:(float) lat2 ToLng:(float) lng2 {
    double earthRadius = 3958.75;
    double dLat = DegreesToRadians1(lat2-lat1);
    double dLng = DegreesToRadians1(lng2-lng1);
    double a = sin(dLat/2) * sin(dLat/2) + cos(DegreesToRadians1(lat1)) * cos(DegreesToRadians1(lat2)) * sin(dLng/2) * sin(dLng/2);
    double c = 2 * atan2(sqrt(a), sqrt(1-a));
    double dist = earthRadius * c;
    
    int meterConversion = 1609;
    
    return [NSNumber numberWithFloat:(float) (dist * meterConversion)];
}

@end
