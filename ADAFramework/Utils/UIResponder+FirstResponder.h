//
//  UIResponder+FirstResponder.h
//  ADAFramework
//
//  Created by Choldarong-r on 1/8/2560 BE.
//  Copyright © 2560 G-ABLE ITS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIResponder (FirstResponder)

+(id)currentFirstResponder;

@end
