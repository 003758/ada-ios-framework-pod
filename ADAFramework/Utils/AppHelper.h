//
//  AppHelper.h
//  VirtualStore
//
//  Created by Choldarong-r on 7/9/2557 BE.
//  Copyright (c) 2557 G-ABLE ITS. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import <Foundation/Foundation.h>
#import "UIColor+HexString.h"
#import "ADAMember.h"


@interface AppHelper : NSObject

#define kLOGIN                              @"ada-login-status"
#define kUSER                               @"ada-user"
#define kPASSWORD                           @"ada-password"
#define kGROUP                              @"ada-group"
#define kADA_CLIENT                         @"ada-client"
#define kLanguage                           @"ada-language"
#define kSECURE_DEVICE                      @"secure-device"
#define kSERVER_NAME                        @"https://ada.g-able.com/"
//#define kSERVER_NAME                       @"https://mobileservice.g-able.com/"
#define kSERVER_HOST                        kSERVER_NAME @"ada-cloud/api/"
#define kSERVER                             kSERVER_HOST @"rest/"
#define kCFG_SERVER                         kSERVER_NAME @"app-config/"
typedef void (^ SuccessCompletion)(BOOL success);
typedef void (^ KeyboardRectBlock)(CGRect rect);
typedef void (^ AppVersionCompletion)(BOOL needUpgrade, NSString  * _Nullable lastestVersion, NSString * _Nullable currentVersion, NSString * _Nullable updateURL, NSJSONSerialization*  _Nullable json);


+ (UIImage * _Nonnull) imageWithView:(UIView * _Nonnull)view;
+ (UIImage * _Nonnull) getImageWithTintedColor:(UIImage * _Nonnull)image withTint:(UIColor * _Nonnull)color withIntensity:(float)alpha;

+ (NSString* _Nullable) stringWithJSON:(NSJSONSerialization* _Nonnull) json;
+ (NSJSONSerialization* _Nullable) jsonWithdictionary:(NSDictionary* _Nonnull) dict;
+ (NSJSONSerialization* _Nullable) jsonWithData:(NSData* _Nonnull) data;
+ (NSArray* _Nullable) jsonArrayWithData:(NSData* _Nonnull) data;
+ (void) roundCornerButton:(UIButton*_Nullable) button borderColor:(UIColor*_Nullable) color;

+ (CAGradientLayer*_Nullable) lightBlueGradient;

+ (BOOL) isEmpty:(NSString* _Nullable)string;
+ (id _Nullable) nullToBlank:(id _Nullable)string;
+ (void) logData: (NSData* _Nullable) data;
+ (UIView* _Nullable) currentContainerView;
+ (UIViewController* _Nullable) currentContainerViewController;
+ (void) showError:(NSError* _Nullable) error completion:(SuccessCompletion _Nullable) handler;
+ (void) showErrorMessage:(NSString* _Nullable) message completion:(SuccessCompletion _Nullable) handler;
+ (void) showLoading:(UIView* _Nullable) view;
+ (void) showLoadingWithMessage:(NSString* _Nullable) message inView:(UIView* _Nonnull) view;
+ (void) showLoadingWithMessage:(NSString* _Nullable) message icon:(NSString * _Nullable) icon inView:(UIView* _Nullable) view;
+ (void) showLoadingWithMessage:(NSString* _Nullable) message withButton:(NSString* _Nullable) title handler:(SuccessCompletion _Nullable) handler inView:(UIView* _Nonnull) view;
+ (void) showLoadingWithMessage:(NSString* _Nullable) message
                           icon:(NSString* _Nullable) icon
                    withButton1:(NSString* _Nullable) title1 handler1:(SuccessCompletion _Nullable) handler1
                    withButton2:(NSString* _Nullable) title2 handler2:(SuccessCompletion _Nullable) handler2
                         inView:(UIView* _Nonnull) view;
+ (void) showLoadingWithMessage:(NSString* _Nullable) message;
+ (void) showLoadingWithMessage:(NSString* _Nullable) message withIcon:(NSString* _Nullable) icon;
+ (void) changeLoadingMessage:(NSString* _Nullable) message icon:(NSString* _Nullable) icon;
+ (void) hideLoading;
+ (void) hideLoading:(UIView* _Nonnull) view;
+ (void) hideLoading:(UIView* _Nonnull) view finished:(SuccessCompletion _Nullable) handler;
+ (void) hideLoadingAndChangeMessage:(NSString * _Nullable) message;
+ (NSString *_Nonnull) md5:(NSString *_Nonnull)str;

+ (void) setPastelBackgroundInView:(UIView * _Nonnull) view verticalStrip:(BOOL) verticalStrip;

+ (void) alertVibrate;
+ (void) alertSound;
+ (void) incomingCallAlert;
+ (void) hideBackBarButtonText:(UIViewController* _Nonnull) viewController;
+ (void) checkVersionFromURL:(NSString * _Nonnull) serviceURL completion:(AppVersionCompletion _Nullable) completion;

+ (UIImage* _Nullable) getImageIcon:(NSString* _Nonnull) icon withSize:(CGSize) size;
+ (UIImage* _Nullable) getImageIcon:(NSString* _Nonnull) icon withSize:(CGSize) size color:(UIColor* _Nullable) color;
+ (UIImage* _Nullable) getImageIcon:(NSString* _Nonnull) icon withSize:(CGSize) size fontSize:(CGFloat) fontSize color:(UIColor* _Nullable) color;

+ (NSString* _Nonnull) getLanguage;
+ (BOOL) isThaiLanguage;
+ (NSString* _Nonnull) getLabelForThai:(NSString* _Nonnull) labelThai eng:(NSString* _Nonnull) labelEng;
+ (void) showAlert:(NSString * _Nullable) message handler:(void (^ __nullable)(UIAlertAction * __nullable action))handler;
+ (UIView* _Nullable) getClosestView:(UIView* _Nullable) view of:(Class _Nonnull) aClass;
+ (void) observeKeyboard:(UIViewController* _Nonnull) viewController;
+ (void) observeKeyboard:(UIViewController* _Nonnull) viewController baseView:(UIView* _Nonnull) baseView;
+ (void) observeKeyboard:(UIViewController* _Nonnull) viewController inTableView:(UITableView *_Nonnull)tableView;
+ (BOOL) setUserData: (id _Nonnull) data forKey:(NSString* _Nonnull) attributeName;
+ (id __nullable) getUserDataForKey:(NSString* _Nonnull) attributeName;
+ (BOOL) removeUserDataForKey:(NSString* _Nonnull) attributeName;
+ (void) syncUserData;

+ (NSNumber* _Nonnull)distFromLat:(float) lat1 FromLng:(float) lng1 ToLat:(float) lat2 ToLng:(float) lng2;
@end
