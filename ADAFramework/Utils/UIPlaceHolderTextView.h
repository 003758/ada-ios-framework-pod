//
//  UIPlaceHolderTextView.h
//  GConnect
//
//  Created by Choldarong-r on 25/8/2558 BE.
//  Copyright (c) 2558 G-ABLE ITS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIPlaceHolderTextView : UITextView

@property (nonatomic, retain) NSString *placeholder;
@property (nonatomic, retain) UIColor *placeholderColor;
@property (nonatomic, retain) UILabel *placeHolderLabel;
@property (nonatomic, assign) CGFloat textIndent;
@property (nonatomic, assign) BOOL makeCursorSmaller;

-(void)textChanged:(NSNotification*)notification;

@end
