//
//  AppDataCoreData.h
//  ADAFramework
//
//  Created by Choldarong-r on 9/12/2559 BE.
//  Copyright © 2559 G-ABLE ITS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface AppDataCoreData : NSObject

@property (nonatomic, retain) NSString *dbName;
- (instancetype)initDBName:(NSString*) dbName;

//- (void)initDB;

// get context instance
//- (NSManagedObjectContext *)getManagedObjectContext;

// manage transactions
- (void)saveContext;
- (void)reset;
- (void)rollback;
- (void)releaseObjectContext;

// insert
- (id)insertNewObjectForEntityForName:(NSString *)entityName;

// delete
- (void)deleteObject:(NSManagedObject *)object;
- (void)deleteObjectThenSave:(NSManagedObject *)object;

// read
- (id)getObjectFromEntity:(NSString *)entityName
                withValue:(NSString *)value
            fromFieldName:(NSString *)name;

- (NSArray *)getObjectFromEntity:(NSString *)entityName
                      withFormat:(NSString *)predicateFormat
                   argumentArray:(NSArray *)arguments
                      sortFields:(NSDictionary *)orders;

- (NSArray *)getObjectFromEntity:(NSString *)entityName
                      withFormat:(NSString *)predicateFormat
                   argumentArray:(NSArray *)arguments
                      sortFields:(NSDictionary *)orders
                    fetchLimitTo:(int)limit;

- (NSArray *)getAllObjectsFromEntity:(NSString *)entityName
                            sortedBy:(NSDictionary *)keys;

// new object
- (id)newEntityForName:(NSString *)entityName;
// read if exist, insert (but not save yet) if doesn't.
- (id)getObjectOrNewEntityForName:(NSString *)entityName withValue:(NSString *)value forFieldName:(NSString *)name;
@end
