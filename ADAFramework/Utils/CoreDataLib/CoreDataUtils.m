//
//  CoreDataUtils.m
//  ADAFramework
//
//  Created by Choldarong-r on 9/12/2559 BE.
//  Copyright © 2559 G-ABLE ITS. All rights reserved.
//

#import "CoreDataUtils.h"
#import "ADA.h"
@implementation CoreDataUtils {

    #define DB_NAME @"ADA"
    NSString* dbName;
    NSManagedObjectContext *managedObjectContext_;
    NSManagedObjectModel *managedObjectModel_;
    NSPersistentStoreCoordinator *persistentStoreCoordinator_;
}
- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
        self.dbName = DB_NAME;
    }
    
    return self;
}

- (instancetype)initDBName:(NSString*) dbName {
    self = [super init];
    if (self) {
        // Initialization code here.
        [self releaseObjectContext];
        if(dbName){
            self.dbName = dbName;
        }
    }
    
    return self;
}

- (void)initDB
{
    
    
    NSDictionary *options = @{
                              NSMigratePersistentStoresAutomaticallyOption : @YES,
                              NSInferMappingModelAutomaticallyOption : @YES
                              };
    
    if([@"USERDATA" isEqualToString:self.dbName]) {
        NSURL *storeURL = [[[[NSFileManager defaultManager] URLsForDirectory:NSLibraryDirectory inDomains:NSUserDomainMask] lastObject]URLByAppendingPathComponent:[NSString stringWithFormat:@"%@.sqlite", self.dbName]];
        //NSLog(@"%@", storeURL);
        NSURL *modelURL = [[NSBundle bundleForClass:[ADA class]] URLForResource:self.dbName withExtension:@"momd"];//[[NSBundle mainBundle] URLForResource:DB_NAME withExtension:@"momd"];
        //NSLog(@"modelURL: %@", modelURL);
        
        managedObjectModel_ = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
        
        NSError *error = nil;
        persistentStoreCoordinator_ = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:managedObjectModel_];
        if (![persistentStoreCoordinator_ addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error]) {
            NSLog(@"Unresolved error %@", error);
            abort();
        }
    }else{
        NSURL *storeURL = [[[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject]URLByAppendingPathComponent:[NSString stringWithFormat:@"%@.sqlite", self.dbName]];
        //NSLog(@"%@", storeURL);
        NSURL *modelURL = [[NSBundle mainBundle] URLForResource:self.dbName withExtension:@"momd"];
        
        managedObjectModel_ = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
        
        NSError *error = nil;
        persistentStoreCoordinator_ = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:managedObjectModel_];
        if (![persistentStoreCoordinator_ addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error]) {
            NSLog(@"Unresolved error %@", error);
            abort();
        }
    }
    
    if (persistentStoreCoordinator_ != nil) {
        //managedObjectContext_ = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        managedObjectContext_ = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        
        [managedObjectContext_ setPersistentStoreCoordinator:persistentStoreCoordinator_];
    }
    
}

#pragma mark - get context instance

- (NSManagedObjectContext *)getManagedObjectContext
{
    if (managedObjectContext_) {
        return managedObjectContext_;
    }
    [self initDB];
    return managedObjectContext_;
}

#pragma mark - manage transactions

- (void)saveContext
{
    
    managedObjectContext_ = [self getManagedObjectContext];
    if([managedObjectContext_ hasChanges]) {
        [managedObjectContext_ performBlock:^{
            NSError *error = nil;
            [self->managedObjectContext_ save:&error];
            if(error) {
                NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
                abort();
            }
        }];
    }
    
    /*if ([managedObjectContext_ hasChanges] && ![managedObjectContext_ save:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }*/
}

- (void)reset
{
    [[self getManagedObjectContext] reset];
}

- (void)rollback
{
    [[self getManagedObjectContext] rollback];
}

- (void)releaseObjectContext
{
    managedObjectContext_ = nil;
    managedObjectModel_ = nil;
    persistentStoreCoordinator_ = nil;
}

#pragma mark - insert

- (id)insertNewObjectForEntityForName:(NSString *)entityName
{
    managedObjectContext_ = [self getManagedObjectContext];
    return [NSEntityDescription insertNewObjectForEntityForName:entityName
                                         inManagedObjectContext:managedObjectContext_];
}

#pragma mark - delete

- (void)deleteObject:(NSManagedObject *)object
{
    [[self getManagedObjectContext] deleteObject:object];
}

- (void)deleteObjectThenSave:(NSManagedObject *)object
{
    [self deleteObject:object];
    [self saveContext];
}

#pragma mark - read

- (NSArray *)getObjectFromEntity:(NSString *)entityName
                      withFormat:(NSString *)predicateFormat
                   argumentArray:(NSArray *)arguments
                      sortFields:(NSDictionary *)orders
{
    return [self getObjectFromEntity:entityName withFormat:predicateFormat argumentArray:arguments sortFields:orders fetchLimitTo:0];
}

- (id)getObjectFromEntity:(NSString *)entityName withValue:(NSString *)value fromFieldName:(NSString *)name
{
    NSString *whereFormat = [name stringByAppendingString:@" = %@"];
    NSArray *resultArray = [self getObjectFromEntity:entityName
                                          withFormat:whereFormat
                                       argumentArray:[NSArray arrayWithObject:value]
                                          sortFields:[NSDictionary dictionaryWithObjectsAndKeys:@"YES", name, nil]
                                        fetchLimitTo:1];
    
    if (resultArray) {
        if ([resultArray count] > 0) {
            return [resultArray objectAtIndex:0];
        }
    }
    return nil;
}

- (NSArray *)getObjectFromEntity:(NSString *)entityName
                      withFormat:(NSString *)predicateFormat
                   argumentArray:(NSArray *)arguments
                      sortFields:(NSDictionary *)orders
                    fetchLimitTo:(int)limit
{
    // sample:
    // predicateFormat = @"firstName = %@ and lastname = %@";
    //  (เหมือน where ใน SQL มี like ได้)
    
    managedObjectContext_ = [self getManagedObjectContext];
    //NSLog(@"managedObjectContext_ %@", managedObjectContext_);
    //NSLog(@"persistentStoreCoordinator_ %@", persistentStoreCoordinator_);
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:managedObjectContext_];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entity];
    
    if (limit > 0) {
        [request setFetchLimit:limit];
    }
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFormat argumentArray:arguments];
    //NSLog(@"%@", predicate);
    
    [request setPredicate:predicate];
    
    NSMutableArray *sortDescriptors = [[NSMutableArray alloc] init];
    
    for (NSString *keyName in [orders allKeys])
    {
        BOOL asc = [[orders valueForKey:keyName] isEqualToString:@"YES"];
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:keyName ascending:asc];
        [sortDescriptors addObject:sortDescriptor];
    }
    
    [request setSortDescriptors:sortDescriptors];
    
    NSArray *resultArray = [managedObjectContext_ executeFetchRequest:request error:nil];
    
    if (resultArray && ([resultArray count] > 0))
        return resultArray;
    else
        return nil;
}

- (NSArray *)getAllObjectsFromEntity:(NSString *)entityName sortedBy:(NSDictionary *)keys;
{
    managedObjectContext_ = [self getManagedObjectContext];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:managedObjectContext_];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entity];
    
    // WARNING!!! initWithKey's value is case sensitive !!!
    
    NSMutableArray *sortDescriptors = [[NSMutableArray alloc] init];
    
    if (keys)
    {
        for (NSString *keyName in [keys allKeys])
        {
            BOOL asc = [[keys valueForKey:keyName] isEqualToString:@"YES"];
            NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:keyName ascending:asc];
            [sortDescriptors addObject:sortDescriptor];
        }
        
        [request setSortDescriptors:sortDescriptors];
    }
    
    NSError *error;
    NSArray *fetchResults = [managedObjectContext_ executeFetchRequest:request error:&error];
    
    if (!fetchResults) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
    }
    
    return fetchResults;
}

#pragma mark - Read or create a New one

- (id)newEntityForName:(NSString *)entityName
{
    return [self insertNewObjectForEntityForName:entityName];
}

- (id)getObjectOrNewEntityForName:(NSString *)entityName withValue:(NSString *)value forFieldName:(NSString *)name
{
    id object = [self getObjectFromEntity:entityName withValue:value fromFieldName:name];
    if (!object) {
        object = [self insertNewObjectForEntityForName:entityName];
    }
    return object;
}
@end
