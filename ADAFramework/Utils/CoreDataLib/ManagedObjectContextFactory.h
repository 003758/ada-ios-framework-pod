//
//  ManagedObjectContextFactory.h
//  iOneBill
//
//  Created by Olarn U. on 5/2/56 BE.
//  Copyright (c) 2556 Olarn U. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface ManagedObjectContextFactory : NSObject

+ (void)initDB;

// get context instance
+ (NSManagedObjectContext *)getManagedObjectContext;

// manage transactions
+ (void)saveContext;
+ (void)reset;
+ (void)rollback;
+ (void)releaseObjectContext;

// insert
+ (id)insertNewObjectForEntityForName:(NSString *)entityName;

// delete
+ (void)deleteObject:(NSManagedObject *)object;
+ (void)deleteObjectThenSave:(NSManagedObject *)object;

// read
+ (id)getObjectFromEntity:(NSString *)entityName
                withValue:(NSString *)value
            fromFieldName:(NSString *)name;

+ (NSArray *)getObjectFromEntity:(NSString *)entityName
                      withFormat:(NSString *)predicateFormat
                   argumentArray:(NSArray *)arguments
                      sortFields:(NSDictionary *)orders;

+ (NSArray *)getObjectFromEntity:(NSString *)entityName
                      withFormat:(NSString *)predicateFormat
                   argumentArray:(NSArray *)arguments
                      sortFields:(NSDictionary *)orders
                    fetchLimitTo:(int)limit;

+ (NSArray *)getAllObjectsFromEntity:(NSString *)entityName
                            sortedBy:(NSDictionary *)keys;

// new object
+ (id)newEntityForName:(NSString *)entityName;
// read if exist, insert (but not save yet) if doesn't.
+ (id)getObjectOrNewEntityForName:(NSString *)entityName withValue:(NSString *)value forFieldName:(NSString *)name;


@end
