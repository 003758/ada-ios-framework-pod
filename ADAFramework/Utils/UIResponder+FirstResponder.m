//
//  UIResponder+FirstResponder.m
//  ADAFramework
//
//  Created by Choldarong-r on 1/8/2560 BE.
//  Copyright © 2560 G-ABLE ITS. All rights reserved.
//
#import "UIResponder+FirstResponder.h"
static __weak id currentFirstResponder;
@implementation UIResponder (FirstResponder)
+(id)currentFirstResponder {
    currentFirstResponder = nil;
    [[UIApplication sharedApplication] sendAction:@selector(findFirstResponder:) to:nil from:nil forEvent:nil];
    return currentFirstResponder;
}
-(void)findFirstResponder:(id)sender {
    currentFirstResponder = self;
}
@end
