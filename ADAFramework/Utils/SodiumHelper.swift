//
//  ViewController.swift
//  TestSodium
//
//  Created by Choldarong-r on 22/9/2560 BE.
//  Copyright © 2560 G-ABLE Company Limite. All rights reserved.
//
/*
import UIKit
import Sodium

@objc public class SodiumHelper: NSObject {
    
    

    @objc public func getKeyPair() -> [Data] {
        let sodium = Sodium()!
        let keyPair = sodium.box.keyPair()!
        return [keyPair.publicKey, keyPair.secretKey]

    }
    
    @objc public func secretBoxEncrypt(_ message: Data, nonce: Data, secretKey: Data) -> Data? {
        
        
        
        
        var authenticatedCipherText = Data(count: message.count + crypto_secretbox_macbytes())
        
        
        let result = authenticatedCipherText.withUnsafeMutableBytes { authenticatedCipherTextPtr in
            return message.withUnsafeBytes { messagePtr in
                return nonce.withUnsafeBytes { noncePtr in
                    return secretKey.withUnsafeBytes { secretKeyPtr in
                        return crypto_secretbox_easy(
                            authenticatedCipherTextPtr,
                            messagePtr,
                            UInt64(message.count),
                            noncePtr,
                            secretKeyPtr)
                    }
                }
            }
        }
        
        if result != 0 {
            return nil
        }
        
        return  authenticatedCipherText
        
        
    }
    
    @objc public func secretBoxDecrypt(_ authenticatedCipherText: Data, nonce: Data, secretKey: Data) -> Data? {
        
        if authenticatedCipherText.count - crypto_secretbox_macbytes() < 0 {
            return nil
        }
        
        var message = Data(count: authenticatedCipherText.count - crypto_secretbox_macbytes())
        
        let result = message.withUnsafeMutableBytes { messagePtr in
            return authenticatedCipherText.withUnsafeBytes { authenticatedCipherTextPtr in
                return nonce.withUnsafeBytes { noncePtr in
                    return secretKey.withUnsafeBytes { secretKeyPtr in
                        return crypto_secretbox_open_easy(
                            messagePtr,
                            authenticatedCipherTextPtr,
                            UInt64(authenticatedCipherText.count),
                            noncePtr,
                            secretKeyPtr)
                    }
                }
            }
        }
        
        if result != 0 {
            return nil
        }
        
        return message
        
        
    }
    
    @objc public func boxEncrypt(_ data: Data, publicKey: Data, privateKey: Data) -> Data {
        let sodium = Sodium()!
        let encryptedMessage: Data =
            sodium.box.seal(message: data,
                            recipientPublicKey: publicKey,
                            senderSecretKey: privateKey)!
        return encryptedMessage
    }
    
    @objc public func boxDecrypt(_ authenticatedCipherText: Data, nonce: Data, publicKey: Data, privateKey: Data) -> Data? {
        if authenticatedCipherText.count - crypto_secretbox_macbytes() < 0 {
            return nil
        }
        
        var message = Data(count: authenticatedCipherText.count - crypto_secretbox_macbytes())
        let result = message.withUnsafeMutableBytes { messagePtr in
            return authenticatedCipherText.withUnsafeBytes { authenticatedCipherTextPtr in
                return nonce.withUnsafeBytes { noncePtr in
                    return publicKey.withUnsafeBytes { senderPublicKeyPtr in
                        return privateKey.withUnsafeBytes { recipientSecretKeyPtr in
                            return crypto_box_open_easy(
                                messagePtr,
                                authenticatedCipherTextPtr,
                                CUnsignedLongLong(authenticatedCipherText.count),
                                noncePtr,
                                senderPublicKeyPtr,
                                recipientSecretKeyPtr)
                        }
                    }
                }
            }
        }
        
        
        
        
        if result != 0 {
            return nil
        }
        
        return message
        
    }

}

*/
