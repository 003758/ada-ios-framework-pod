//
//  NSMutableArray.h
//  iSmart
//
//  Created by Choldarong-r on 4/9/2558 BE.
//  Copyright (c) 2558 G-ABLE ITS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableArray (Shuffle)
- (void)shuffle;
@end
