//
//  UIColor+HexString.h
//  VirtualStore
//
//  Created by Choldarong-r on 7/8/2557 BE.
//  Copyright (c) 2557 G-ABLE ITS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (HexString)

+ (UIColor *) colorWithHexString: (NSString *) hexString;
+ (CGFloat) colorComponentFrom: (NSString *) string start: (NSUInteger) start length: (NSUInteger) length;

@end
