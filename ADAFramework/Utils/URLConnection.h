//
//  URLConnection.h
//  OneMinTimeSheet
//
//  Created by Choldarong-r on 4/2/2557 BE.
//  Copyright (c) 2557 G-ABLE Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface URLConnection : NSObject <NSURLConnectionDataDelegate>

typedef void (^ ConnectionCompletion)(NSURLResponse* response, NSData* data, NSError* connectionError);
typedef void (^ ConnectionCompletionWithSenderData)(NSURLResponse* response, NSData* data, id senderData, NSError* connectionError);

#define kAuthorization              @"Basic bW9iaWxlZGlyZWN0OjB1Z3ZnW2Jd"

@property (nonatomic, retain) NSError *connectionError;
@property (nonatomic, retain) NSURLResponse *currentResponse;
@property (nonatomic, copy) ConnectionCompletion currentHandler;
@property (nonatomic, copy) ConnectionCompletionWithSenderData currentHandlerWithSenderData;
@property (nonatomic, strong) NSURLConnection *con;
@property (nonatomic, strong) NSURLSession *sharedSessionMainQueue;
@property (nonatomic, retain) NSMutableData *recvData;
@property (nonatomic, assign) BOOL sync;
@property (nonatomic, retain) id senderData;
+ (NSDictionary<NSString*, NSURLSessionDataTask*> *) tasks;





//- (NSData *) sendSynchronousRequest:(NSURLRequest *)request returningResponse:(NSURLResponse **)response error:(NSError **)error;
- (NSString *) sendAsynchronousRequest:(NSURLRequest*) request completionHandler:(ConnectionCompletion) handler;
- (NSString *) sendAsynchronousRequest:(NSURLRequest*) request completionHandler:(ConnectionCompletionWithSenderData) handler senderData:(id) data;
- (void) cancel;
+ (NSMutableURLRequest*) requestFromURL:(NSString*) strURL method:(NSString*) method parameter:(NSDictionary *) parameters;
+ (NSMutableURLRequest*) requestFromURL:(NSString*) strURL method:(NSString*) method contentType:(NSString*) contentType parameter:(NSDictionary *) parameters;
+ (NSMutableURLRequest*) requestFromURL:(NSString*) strURL method:(NSString*) method contentType:(NSString*) contentType jsonData:(NSDictionary *) data;
+ (NSMutableURLRequest*) requestFromURL:(NSString*) strURL method:(NSString*) method contentType:(NSString*) contentType data:(NSData *) data;
+ (id) getInstance;
@end
