//
//  UIImage+Extend.h
//  OneMinTimeSheet
//
//  Created by Choldarong-r on 3/19/2557 BE.
//  Copyright (c) 2557 G-ABLE Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Extend)

+ (UIImage *)imageWithColor:(UIColor *)color;
+ (UIImage *)imageWithBorderFromImage:(UIImage*)source;
- (UIImage*) scaleToSize:(CGSize)size;
- (NSString*) base64String;
- (NSString *)contentType;
- (NSString *)extension;
- (UIImage*) rotate : (UIImageOrientation) orientation;
- (UIImage *)crop:(CGRect)rect;
+ (UIImage *)grabImageFromView: (UIView *) viewToGrab;
- (UIImage*)imageByScalingAndCroppingForSize:(CGSize)targetSize;
+ (UIImage*) imageWithText:(NSString*) text
              inSize:(CGSize)  imageSize
            withFont:(UIFont*) font;
+(UIImage*) imageWithText:(NSString*) text
                   inSize:(CGSize)  imageSize
                    color:(UIColor*) color
                 withFont:(UIFont*) font;
+(UIImage*) imageWithText:(NSString*) text
                   inSize:(CGSize)  imageSize
                    color:(UIColor*) color
          backgroundColor:(UIColor*) bgColor
                 withFont:(UIFont*) font;
- (UIImage *) getImageWithTint:(UIColor *)color withIntensity:(float)alpha;
+ (UIImage *)imageByCroppingImage:(UIImage *)image toSize:(CGSize)size;
+ (UIImage *)mergeImage:(UIImage*)srcImage withImage:(UIImage *)image;

@end
