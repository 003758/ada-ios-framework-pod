//
//  GAHelper.h
//  BOMS
//
//  Created by Choldarong-r on 24/3/2560 BE.
//  Copyright © 2560 G-ABLE ITS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ADA.h"
@interface GAHelper : NSObject

+ (void) trackID:(NSString* _Nonnull) trackID screen:(NSString* _Nonnull) screenName framework:(ADA* _Nonnull) ada;

@end
