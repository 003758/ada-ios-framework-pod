//
//  UIColor+Extend.h
//  iSmart
//
//  Created by Choldarong-r on 5/10/2558 BE.
//  Copyright © 2558 G-ABLE ITS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor(Extend)


- (UIColor *)lighterColor;

- (UIColor *)darkerColor;

@end
