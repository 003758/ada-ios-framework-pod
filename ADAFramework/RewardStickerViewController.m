//
//  RewardStickerViewController.m
//  ADAFramework
//
//  Created by Choldarong-r on 21/12/2559 BE.
//  Copyright © 2559 G-ABLE ITS. All rights reserved.
//

#import "RewardStickerViewController.h"
#import "ServiceCaller.h"
#import "URLConnection.h"
#import "AppHelper.h"
#import "UIPlaceHolderTextView.h"

@import AFNetworking;

@interface RewardStickerViewController () <UICollectionViewDataSource, UICollectionViewDelegate>

@end

@implementation RewardStickerViewController{
    UIScrollView *stickerScroll1;
    UIScrollView *stickerScroll2;
    UIScrollView *stickerCateScroll;
    UIView *parentView;
    UIButton *switchKeyBtn;
    UIPageControl *pageControl;
    NSMutableDictionary *stickersDict;
    NSMutableDictionary *imageViewDict;
    NSMutableDictionary *stickerImageDict;
    NSMutableArray *collectionViews;
    NSFileManager *fileMgr;
    NSURL *tmpDirURL;
    CGRect currentFrame;
    NSIndexPath *selectedIndexPath;
    NSMutableArray *rewardList;
    NSMutableArray *rewardGroup;
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"RewardStickerViewController");
    self.automaticallyAdjustsScrollViewInsets = NO;
    fileMgr = [NSFileManager defaultManager];
    tmpDirURL = [NSURL fileURLWithPath:NSTemporaryDirectory() isDirectory:YES];
    self.view.backgroundColor = [UIColor whiteColor];
    CGRect stickerFrame = self.view.frame;
    CGRect scrollRect = stickerFrame;
    scrollRect.size.width = stickerFrame.size.width;
    stickerImageDict = [[NSMutableDictionary alloc] init];
    stickersDict = [[NSMutableDictionary alloc] init];
    collectionViews = [[NSMutableArray alloc] init];
    imageViewDict = [[NSMutableDictionary alloc] init];
    
    
    rewardList = [[NSMutableArray alloc] init];
    rewardGroup = [[NSMutableArray alloc] init];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(rotate)
                                                 name:UIDeviceOrientationDidChangeNotification
                                               object:[UIDevice currentDevice]];
    
    
    
    UIBarButtonItem *btnSend = [[UIBarButtonItem alloc] initWithTitle:[AppHelper getLabelForThai:@"ส่ง" eng:@"Send"] style:UIBarButtonItemStyleDone target:self action:@selector(didSelectSticker:)];
    self.navigationItem.rightBarButtonItem = btnSend;
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    layout.minimumLineSpacing = 2;
    layout.minimumInteritemSpacing = 2;
    [layout setHeaderReferenceSize:CGSizeMake(self.view.frame.size.width, 46)];
    CGFloat topMargin = self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height;
    UICollectionView *collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, topMargin, self.view.frame.size.width, (self.view.frame.size.height/1.5)-50) collectionViewLayout:layout];
    collectionView.tag = 1111;
    collectionView.delegate = self;
    collectionView.dataSource = self;
    //collectionView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    collectionView.backgroundColor = [UIColor clearColor];
    [collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"stickerCell"];
    [collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"headerView"];
    
    [collectionViews addObject:collectionView];
    collectionView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin);
    
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(8, collectionView.frame.origin.y + collectionView.frame.size.height + 4, self.view.frame.size.width-8, 25)];
    lblTitle.text = [AppHelper getLabelForThai:@"ความคิดเห็น" eng:@"Comment"];
    //lblTitle.font = _boldFont;
    lblTitle.tag = 77;
    [self.view addSubview:lblTitle];
    
    
    UIPlaceHolderTextView *txtReason = [[UIPlaceHolderTextView alloc] initWithFrame:CGRectMake(4, lblTitle.frame.origin.y + lblTitle.frame.size.height + 4, self.view.frame.size.width-8, (self.view.frame.size.height - (lblTitle.frame.origin.y + lblTitle.frame.size.height+8)))];
    //txtReason.font = _textFont;
    txtReason.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin);
    txtReason.backgroundColor = [UIColor lightTextColor];
    txtReason.tag = 88;
    txtReason.layer.borderColor =  [UIColor lightGrayColor].CGColor;
    txtReason.layer.borderWidth = 1;
    txtReason.layer.cornerRadius = 4;
    txtReason.clipsToBounds = YES;
    txtReason.placeholder = [AppHelper getLabelForThai:@"เขียนข้อความที่นี่" eng: @"Write your comment here."];
    [self.view addSubview:collectionView];
    [self.view addSubview:txtReason];
    //[AppHelper observeKeyboard:self baseView:txtReason];
}

- (void) rotate {
    UITextView *txt = (UITextView*)[self.view viewWithTag:88];
    [txt resignFirstResponder];
    //[AppHelper observeKeyboard:self baseView:txt];
    [UIView animateWithDuration:0.3 animations:^{
        UICollectionView *collectionView = (UICollectionView*)[self.view viewWithTag:1111];
        CGFloat topMargin = self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height;
        collectionView.frame = CGRectMake(0, topMargin, self.view.frame.size.width, (self.view.frame.size.height/1.5)-50);
        UILabel *lblTitle = (UILabel*)[self.view viewWithTag:77];
        lblTitle.frame = CGRectMake(8, collectionView.frame.origin.y + collectionView.frame.size.height + 4, self.view.frame.size.width-8, 25);
        UITextView *txt = (UITextView*)[self.view viewWithTag:88];
        txt.frame = CGRectMake(4, lblTitle.frame.origin.y + lblTitle.frame.size.height + 4, self.view.frame.size.width-8, (self.view.frame.size.height - (lblTitle.frame.origin.y + lblTitle.frame.size.height+8)));
    }];
    
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear: animated];
    [self loadStickerSet];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) dismissViewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView {
    UITextView *txt = (UITextView*)[self.view viewWithTag:88];
    [txt resignFirstResponder];
}

- (void) loadStickerSet {
    
    [ServiceCaller callJSON:[NSString stringWithFormat:@"%@msg-reward-point/get-sticker-by-group/%@",
                             kSERVER, _adaConfig.login.group]
                     method:@"GET"
                     header:nil
                  parameter:nil
                    handler:^(NSJSONSerialization *json) {
                        [self->rewardList removeAllObjects];
                        [self->rewardGroup removeAllObjects];
                        if ([[json valueForKey:@"result"] boolValue]) {
                            NSArray *stickers = [json valueForKey:@"data"];
                            
                            for (NSJSONSerialization *row in stickers) {
                                
                                if (![self->rewardGroup containsObject:[row valueForKey:@"name"]]) {
                                    [self->rewardGroup addObject:[row valueForKey:@"name"]];
                                    [self->rewardList addObject:[[NSMutableArray alloc] init]];
                                }
                                
                                
                                [self->rewardList.lastObject addObject:@{
                                                                   @"type": [row valueForKey:@"image"],
                                                                   @"icon" : [row valueForKey:@"image"],
                                                                   @"title" : [row valueForKey:@"title"],
                                                                   @"detail" : [row valueForKey:@"detail"],
                                                                   @"full_detail" : [row valueForKey:@"full_detail"] ? [row valueForKey:@"full_detail"] : @""
                                                                   }];
                            }
                        }
                        UICollectionView *collectionView = (UICollectionView*)[self.view viewWithTag:1111];
                        [collectionView  reloadData];
                    }];
     
        
        
    
    
    
}

- (void)didSelectSticker:(id) sender {
    if (!selectedIndexPath) {
        
        if (self.delegate) {
            [self.delegate didSelectSticker:nil reason:nil];
        }
    }else{
        if (self.delegate) {
            NSDictionary *item = [[rewardList objectAtIndex:selectedIndexPath.section] objectAtIndex:selectedIndexPath.row];
            NSString *code = [item valueForKey:@"icon"];
            UITextView *txt = (UITextView*)[self.view viewWithTag:88];
            [self.delegate didSelectSticker:code reason:txt.text];
        }
    }
    
    [self dismissViewController];
    
}

#pragma mark - UICollectionView

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return rewardList.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [[rewardList objectAtIndex:section] count];
}


- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    
    if (kind == UICollectionElementKindSectionHeader) {
        UICollectionReusableView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"headerView" forIndexPath:indexPath];
        
        if (headerView==nil) {
            headerView=[[UICollectionReusableView alloc] initWithFrame:CGRectMake(0, 0, collectionView.frame.size.width, 46)];
        }
        
        
        NSString *title = [rewardGroup objectAtIndex:indexPath.section];
        if (![headerView viewWithTag:999]) {
            UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(8, 8, collectionView.frame.size.width-16, 30)];
            lblTitle.tag = 999;
            lblTitle.font = [lblTitle.font fontWithSize:12];
            [headerView addSubview:lblTitle];
        }
        UILabel *lblTitle = [headerView viewWithTag:999];
        lblTitle.text = title;
        reusableview = headerView;
    }
    
    /*if (kind == UICollectionElementKindSectionFooter) {
     UICollectionReusableView *footerview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"FooterView" forIndexPath:indexPath];
     
     reusableview = footerview;
     }
     */
    return reusableview;
}


// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    //NSArray *images = [stickersDict objectForKey:@(indexPath.section)];
    
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"stickerCell" forIndexPath:indexPath];
    
    
    if (selectedIndexPath && selectedIndexPath.row == indexPath.row && selectedIndexPath.section == indexPath.section) {
        cell.contentView.layer.borderWidth = 2;
        cell.contentView.layer.borderColor = _adaConfig.login.bgColor.CGColor;
        cell.contentView.backgroundColor = [_adaConfig.login.bgColor colorWithAlphaComponent:0.2];
    }else{
        cell.contentView.layer.borderWidth = 1;
        cell.contentView.layer.borderColor = [UIColor lightGrayColor].CGColor;
        cell.contentView.backgroundColor = [UIColor whiteColor];
    }
    cell.contentView.layer.cornerRadius = 3;
    cell.contentView.clipsToBounds = YES;
    
    
    if (![cell viewWithTag:1122]) {
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(25, 4, 100, 100)];
        imgView.tag = 1122;
        [cell addSubview:imgView];
        imgView.translatesAutoresizingMaskIntoConstraints = NO;
        [imgView.widthAnchor constraintEqualToConstant:100].active = YES;
        [imgView.heightAnchor constraintEqualToConstant:100].active = YES;
        [imgView.centerXAnchor constraintEqualToAnchor:cell.centerXAnchor].active = YES;
        [imgView.topAnchor constraintEqualToAnchor:cell.topAnchor constant:4].active = YES;
        if (![cell viewWithTag:1123]) {
            UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(8, 104, 142, 26)];
            lblTitle.tag = 1123;
            lblTitle.numberOfLines = 2;
            lblTitle.textAlignment = NSTextAlignmentCenter;
            [cell addSubview:lblTitle];
            lblTitle.translatesAutoresizingMaskIntoConstraints = NO;
            [lblTitle.leadingAnchor constraintEqualToAnchor:cell.leadingAnchor constant:4].active = YES;
            [lblTitle.trailingAnchor constraintEqualToAnchor:cell.trailingAnchor constant:-4].active = YES;
            [lblTitle.heightAnchor constraintGreaterThanOrEqualToConstant:26].active = YES;
            [lblTitle.topAnchor constraintEqualToAnchor:imgView.bottomAnchor constant:0].active = YES;
            
            lblTitle.font = [lblTitle.font fontWithSize:12];if (![cell viewWithTag:1124]) {
                UITextView *lblDesc = [[UITextView alloc] initWithFrame:CGRectMake(4, 130, 142, 50)];
                lblDesc.backgroundColor = [UIColor clearColor];
                lblDesc.tag = 1124;
                lblDesc.editable = NO;
                lblDesc.scrollEnabled = NO;
                lblDesc.selectable = NO;
                
                lblDesc.textContainerInset = UIEdgeInsetsZero;
                lblDesc.textContainer.lineFragmentPadding = 0;
                lblDesc.userInteractionEnabled = NO;
                lblDesc.canCancelContentTouches = YES;
                lblDesc.delaysContentTouches = YES;
                
                [cell addSubview:lblDesc];
                lblDesc.translatesAutoresizingMaskIntoConstraints = NO;
                [lblDesc.leadingAnchor constraintEqualToAnchor:cell.leadingAnchor constant:4].active = YES;
                [lblDesc.trailingAnchor constraintEqualToAnchor:cell.trailingAnchor constant:-4].active = YES;
                [lblDesc.topAnchor constraintEqualToAnchor:lblTitle.bottomAnchor constant:0].active = YES;
                [lblDesc.bottomAnchor constraintEqualToAnchor:cell.bottomAnchor constant:-8].active = YES;
                
                
                lblDesc.font = [lblDesc.font fontWithSize:12];
            }
        }
    }
    
    
    
    
    
    NSDictionary *item = [[rewardList objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    
    NSString *imageID = [item valueForKey:@"icon"];
    
    UIImageView *imgView = (UIImageView*)[cell viewWithTag:1122];
    UILabel *lblTitle = (UILabel*)[cell viewWithTag:1123];
    UILabel *lblDesc = (UILabel*)[cell viewWithTag:1124];
    
    
    lblTitle.text = [item valueForKey:@"title"];
    lblDesc.text = [item valueForKey:@"detail"];
    lblDesc.font = lblTitle.font;
    
    if ([stickerImageDict valueForKey:imageID]) {
        imgView.image = [stickerImageDict valueForKey:imageID];
        return cell;
    }
    
    
    
    NSString *imgFolder = [NSString stringWithFormat:@"%@chat/sticker-reward/%@", tmpDirURL, imageID];
    imgFolder = [imgFolder stringByReplacingOccurrencesOfString:@"file://" withString:@""];
    if (![fileMgr fileExistsAtPath:imgFolder]) {
        if([fileMgr createDirectoryAtPath:imgFolder withIntermediateDirectories:YES attributes:nil error:nil]){
            //NSLog(@"Dir created");
        }
    }
    
    NSString *strPath = [NSString stringWithFormat:@"%@/%@", imgFolder, imageID];
    
    if ([fileMgr fileExistsAtPath:strPath]){
        NSData *imgData = [fileMgr contentsAtPath:strPath];
        UIImage *image = [UIImage imageWithData:imgData];
        imgView.image = image;
        [stickerImageDict setObject:image forKey:imageID];
        
    }else{
        
        
        NSMutableURLRequest *req = [URLConnection requestFromURL:[NSString stringWithFormat:@"%@file", kSERVER_HOST]
                                    
                                                          method:@"GET"
                                                       parameter:@{
                                                                   @"id": imageID,
                                                                   @"mediaType": @"sticker"                                                                   }];
        [req setValue:[AppHelper getUserDataForKey:kADA_CLIENT] forHTTPHeaderField:kADA_CLIENT];
        [imageViewDict setObject:imgView forKey:req.URL];
        [imgView setImageWithURLRequest:req
                       placeholderImage:[UIImage imageNamed:@"emo-76"]
                                success:^(NSURLRequest * req, NSHTTPURLResponse * res, UIImage * image) {
                                    if (image) {
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            UIImageView *imgView = [self->imageViewDict objectForKey:req.URL];
                                            imgView.image = image;
                                            [self->stickerImageDict setObject:image forKey:imageID];
                                            NSData *imgData = UIImagePNGRepresentation(image);
                                            if([imgData writeToFile:strPath atomically:YES]){
                                                //NSLog(@"write success");
                                            }
                                        });
                                    }
                                } failure:^(NSURLRequest *req, NSHTTPURLResponse * res, NSError * error) {
                                }];
    }
    
    
    
    
    //imgView.image = [UIImage imageNamed:[images objectAtIndex:indexPath.row]];
    
    
    return cell;
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    //NSArray *images = [stickersDict objectForKey:@(indexPath.section)];
    //NSJSONSerialization *json = [stickersDict objectForKey:@(indexPath.section)];
    //NSJSONSerialization *item = [[json valueForKey:@"detail"] objectAtIndex:indexPath.row];
    
    selectedIndexPath = indexPath;
    UITextView *txt = (UITextView*)[self.view viewWithTag:88];
    [txt resignFirstResponder];
    [collectionView reloadData];
    
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    UIEdgeInsets insets=UIEdgeInsetsMake(2, 2, 2, 2);
    return UIEdgeInsetsZero;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((collectionView.bounds.size.width - 16) / 2, 200);
}


@end
