//
//  NewsFeedDetail.h
//  ADAFramework
//
//  Created by Choldarong-r on 7/12/2559 BE.
//  Copyright © 2559 G-ABLE ITS. All rights reserved.
//

#import "NewsFeed.h"

@interface NewsFeedDetail : NewsFeed

@property (nonatomic, retain) NSArray   *imageURLs;
@property (nonatomic, retain) NSData    *imageData;

@end
