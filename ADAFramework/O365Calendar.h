//
//  O365Calendar.h
//  ADAFramework
//
//  Created by Choldarong-r on 11/4/2560 BE.
//  Copyright © 2560 G-ABLE ITS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "O365Attendee.h"

@interface O365Calendar : NSObject
@property (nonatomic, retain) NSString *subject;
@property (nonatomic, retain) NSString *body;
@property (nonatomic, retain) NSString *bodyPreview;
@property (nonatomic, retain) NSDate *startDate;
@property (nonatomic, retain) NSDate *endDate;
@property (nonatomic, retain) NSString *location;
@property (nonatomic, retain) NSString *organizerName;
@property (nonatomic, retain) NSString *organizerAddress;
@property (nonatomic, retain) NSDictionary<NSString*, id> *organizerUserData;
@property (nonatomic, retain) NSArray<O365Attendee*> *attendees;
@end
