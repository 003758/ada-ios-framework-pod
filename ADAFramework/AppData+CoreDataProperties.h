//
//  AppData+CoreDataProperties.h
//  ADAFramework
//
//  Created by Choldarong-r on 24/10/2561 BE.
//  Copyright © 2561 G-ABLE Company Limited. All rights reserved.
//
//

#import "AppData+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface AppData (CoreDataProperties)

+ (NSFetchRequest<AppData *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *key;
@property (nullable, nonatomic, retain) NSData *data;

@end

NS_ASSUME_NONNULL_END
