//
//  ChatViewController.h
//  ADAFramework
//
//  Created by Choldarong-r on 5/12/2559 BE.
//  Copyright © 2559 G-ABLE ITS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ADA.h"
#import "ChatRoom.h"

@interface ADAChatViewController : UIViewController

@property (nonatomic, getter=isStickerLoaded) BOOL stickerLoaded;
@property (nonatomic, getter=isOnline) BOOL online;
@property (nonatomic, retain) ChatRoom * _Nonnull room;
//- (ChatRoom *)room;
//- (void)setRoom:(ChatRoom *)room;
 //- (void) reloadHistory:(SuccessBlock) handler;
- (void) loadPreviousHistory:(ChatMessage* _Nullable) message handler:(SuccessBlock _Nullable) handler;
//Chat Function
/*
- (void) enterRoom:(ChatRoom*) room
 connectionHandler:(ChatConnectionBlock) connectionHandler
      usersHandler:(ChatUsersBlock) usersHandler
updateMessageHandler:(SuccessBlock) updateMessageHandler
    messageHandler:(ChatMessageBlock) messageHandler
             queue:(dispatch_queue_t) queue;
*/
 
- (void) chatServerConnectionState:(BOOL) connected;
- (void) onlineChatUsers:(NSArray<NSString*>* _Nonnull) users;
- (void) updateMessageState;
- (void) receivedMessage:(ChatMessage* _Nonnull) message scrollToBottom:(BOOL) scrollToBottom;

- (void) searchMessageWithKeyword:(NSString* _Nonnull) keyword handler:(ChatMessageBlock _Nullable) handler;

- (void) sendPlainChatMessage:(ChatMessage* _Nonnull) message;
- (void) sendImage:(ImageMessage* _Nonnull) message progress:(ProgressBlock _Nullable) progress;
- (void) sendVideo:(VideoMessage* _Nonnull) message progress:(ProgressBlock _Nullable) progress;
- (void) sendFile:(FileMessage* _Nonnull) message progress:(ProgressBlock _Nullable) progress;
- (void) sendSticker:(StickerMessage* _Nonnull) message;
- (void) sendLocation:(LocationMessage* _Nonnull) message;
- (void) sendVote:(VoteMessage* _Nonnull) message;
- (void) markingReadStatus:(NSArray<ChatMessage*>* _Nonnull) messages;

//for view message
- (void) loadImage:(ChatMessage* _Nonnull) message withPlaceholder:(UIImage* _Nullable) placeholder in:(UIImageView* _Nonnull) imageView;
- (void) loadVideo:(ChatMessage* _Nonnull) message withPlaceholder:(UIImage* _Nullable) placeholder  in:(UIImageView* _Nonnull) imageView;

- (void) loadStickerSet:(SuccessBlock _Nullable) handler;
- (void) loadSticker:(UIImageView* _Nonnull) imageView withCode:(NSString*  _Nonnull) stickerID handler:(SuccessBlock _Nullable) handler;
- (void) loadVideoPreview:(UIImageView* _Nonnull) imageView withVideoID:(NSString* _Nonnull) videoID handler:(SuccessBlock _Nullable) handler;
- (void) loadVideoPreview:(UIImageView* _Nonnull) imageView withVideoID:(NSString* _Nonnull) videoID placeholder:(UIImage* _Nullable) placeholder handler:(SuccessBlock _Nullable) handler;
- (void) playVideo:(ChatMessage* _Nonnull) message;
- (void) showImage:(ChatMessage* _Nonnull) message;
- (void) showFile:(ChatMessage* _Nonnull) message progress:(ProgressBlock _Nullable) progress;
- (void) getAllVideo:(ChatMessageBlock _Nullable) handler;
- (void) getAllImages:(ChatMessageBlock _Nullable) handler;
- (void) getAllFiles:(ChatMessageBlock _Nullable) handler;
- (void) getRoomMembers:(ADAMembersBlock _Nullable) handler;
- (NSString* _Nullable) getImageURL:(ChatMessage* _Nullable) message;

//for send
- (void) openImagePicker:(ChatImageBlock _Nullable) handler;
- (void) openCamera:(ChatImageBlock _Nullable) handler;
- (void) openVideoPicker:(ChatVideoBlock _Nullable) handler;
- (void) openVideo:(ChatVideoBlock _Nullable) handler;
- (void) openFilePicker:(ChatFileBlock _Nullable) handler;
- (void) openSticker:(ChatStickerBlock _Nullable) handler rewardSticker:(BOOL) rewardSticker;
- (void) canSendRewardSticker:(SuccessBlock _Nullable) handler;
- (void) dontLeaveConnection;
//member
- (void) leaveGroup:(SuccessBlock _Nullable) handler;
- (void) addMember:(NSArray* _Nonnull) members handler:(SuccessBlock _Nullable) handler;



@end
