//
//  ShuttleBusTimeTable.h
//  ADAFramework
//
//  Created by Choldarong-r on 22/6/2560 BE.
//  Copyright © 2560 G-ABLE ITS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ShuttleBusTimeTable : NSObject

@property (nonatomic, retain) NSString *time;
@property (nonatomic, retain) NSString *remark;
@end
