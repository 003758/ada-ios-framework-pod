//
//  Login.m
//  ADAFramework
//
//  Created by Choldarong-r on 4/12/2559 BE.
//  Copyright © 2559 G-ABLE ITS. All rights reserved.
//

#import "Login.h"

@implementation Login

/*
 @property (nonatomic, assign) NSInteger memberID;
 @property (nonatomic, retain) NSString *usid;
 @property (nonatomic, retain) NSString *group;
 @property (nonatomic, retain) NSString *userName;
 @property (nonatomic, retain) NSString *imageID;
 @property (nonatomic, retain) NSString *unitName;
 @property (nonatomic, assign) NSInteger siteID;
 @property (nonatomic, retain) NSString *siteName;
 @property (nonatomic, retain) NSString *siteDescription;
 @property (nonatomic, retain) NSString *chatServerHost;
 @property (nonatomic, retain) NSString *chatServerDomain;
 @property (nonatomic, assign) NSInteger chatServerPort;
 @property (nonatomic, retain) UIColor *bgColor;
 @property (nonatomic, retain) UIColor *tintColor;
 */

- (void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    [encoder encodeObject:[NSNumber numberWithInteger:self.memberID] forKey:@"memberID"];
    [encoder encodeObject:self.usid forKey:@"usid"];
    [encoder encodeObject:self.group forKey:@"group"];
    [encoder encodeObject:self.userName forKey:@"userName"];
    [encoder encodeObject:self.userNameTH forKey:@"userNameTH"];
    [encoder encodeObject:self.userNameEN forKey:@"userNameEN"];
    [encoder encodeObject:self.imageID forKey:@"imageID"];
    [encoder encodeObject:self.unitName forKey:@"unitName"];
    [encoder encodeObject:[NSNumber numberWithInteger:self.siteID] forKey:@"siteID"];
    [encoder encodeObject:self.siteName forKey:@"siteName"];
    [encoder encodeObject:self.siteDescription forKey:@"siteDescription"];
    [encoder encodeObject:self.chatServerDomain forKey:@"chatServerDomain"];
    [encoder encodeObject:self.chatServerHost forKey:@"chatServerHost"];
    [encoder encodeObject:[NSNumber numberWithInteger:self.chatServerPort] forKey:@"chatServerPort"];
    [encoder encodeObject:self.bgColor forKey:@"bgColor"];
    [encoder encodeObject:self.tintColor forKey:@"tintColor"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        self.memberID = [[decoder decodeObjectForKey:@"memberID"] integerValue];
        self.usid = [decoder decodeObjectForKey:@"usid"];
        self.group = [decoder decodeObjectForKey:@"group"];
        self.userName = [decoder decodeObjectForKey:@"userName"];
        self.userNameTH = [decoder decodeObjectForKey:@"userNameTH"];
        self.userNameEN = [decoder decodeObjectForKey:@"userNameEN"];
        self.imageID = [decoder decodeObjectForKey:@"imageID"];
        self.unitName = [decoder decodeObjectForKey:@"unitName"];
        self.siteID = [[decoder decodeObjectForKey:@"siteID"] integerValue];
        self.siteName = [decoder decodeObjectForKey:@"siteName"];
        self.siteDescription = [decoder decodeObjectForKey:@"siteDescription"];
        self.chatServerDomain = [decoder decodeObjectForKey:@"chatServerDomain"];
        self.chatServerHost = [decoder decodeObjectForKey:@"chatServerHost"];
        self.chatServerPort = [[decoder decodeObjectForKey:@"chatServerPort"] integerValue];
        self.bgColor = [decoder decodeObjectForKey:@"bgColor"];
        self.tintColor = [decoder decodeObjectForKey:@"tintColor"];
    }
    return self;
}


@end
