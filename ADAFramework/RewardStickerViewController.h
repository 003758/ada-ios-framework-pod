//
//  RewardStickerViewController.h
//  ADAFramework
//
//  Created by Choldarong-r on 21/12/2559 BE.
//  Copyright © 2559 G-ABLE ITS. All rights reserved.
//
#import "ADA.h"
#import <UIKit/UIKit.h>
@protocol RewardStickerViewDelegate <NSObject>
- (void) didSelectSticker:(NSString* _Nullable) code reason:( NSString* _Nullable ) reason;
@end

@interface RewardStickerViewController : UIViewController
@property (nonatomic, retain) ADA*  _Nonnull adaConfig;
@property (nonatomic, weak) id<RewardStickerViewDelegate> _Nullable delegate;
@end
