//
//  ShuttleBusLocation.h
//  ADAFramework
//
//  Created by Choldarong-r on 27/2/2562 BE.
//  Copyright © 2562 G-ABLE Company Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
@import CoreLocation;

NS_ASSUME_NONNULL_BEGIN

@interface ShuttleBusLocation : NSObject

@property (nonatomic, assign) int routeID;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, assign) int direction;
@property (nonatomic, assign) int engine;
@property (nonatomic, assign) int speed;
@property (nonatomic, retain) NSDate *date;

@end

NS_ASSUME_NONNULL_END
