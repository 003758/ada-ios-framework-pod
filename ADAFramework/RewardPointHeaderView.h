//
//  RewardPointHeaderView.h
//  ADA
//
//  Created by Choldarong-r on 12/1/2559 BE.
//  Copyright © 2559 G-ABLE ITS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RewardPointHeaderView : UICollectionReusableView
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@end
