//
//  ADAAppDelegate.m
//  ADAFramework
//
//  Created by Choldarong-r on 17/7/2560 BE.
//  Copyright © 2560 G-ABLE ITS. All rights reserved.
//

#import "ADAAppDelegate.h"
#import "LibSodiumHelper.h"
@implementation ADAAppDelegate


- (instancetype)init {
    
    self.ada = [[ADA alloc] init];
    return [super init];
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    //[LibSodiumHelper resetHandshake];
    //[[UIApplication sharedApplication] setApplicationIconBadgeNumber:[self.ada getNotifications].count];
    /*[[UNUserNotificationCenter currentNotificationCenter] getDeliveredNotificationsWithCompletionHandler:^(NSArray<UNNotification *> * _Nonnull notifications) {
        for(UNNotification *notification in notifications) {
            [[NSNotificationCenter defaultCenter] postNotificationName:kADARecivedRemoteBGMessage
                                                                object:nil
                                                              userInfo:notification.request.content.userInfo];
        }
    }];*/
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:[self.ada getNotifications].count];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    
    [AppHelper syncUserData];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    
    [AppHelper syncUserData];
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    [self.ada registerDeviceToken:deviceToken handler:^(BOOL success) {
        if(success){
            [[NSNotificationCenter defaultCenter] postNotificationName:kADARecivedDeviceToken
                                                                object:nil
                                                              userInfo:@{@"deviceToken" : deviceToken}];
        }
    }];
    
    [UNUserNotificationCenter currentNotificationCenter].delegate = self;
}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kADARecivedRemoteMessage
                                                        object:nil
                                                      userInfo:userInfo];
    
    
    if(completionHandler){
        completionHandler(UIBackgroundFetchResultNewData);
    }
}


- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)(void))completionHandler {
    
    //NSLog(@"%@", response.notification.request.content.userInfo);
    
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kADARecivedRemoteBGMessage
                                                        object:nil
                                                      userInfo:response.notification.request.content.userInfo];
    if(completionHandler){
        completionHandler();
    }
}







- (void) userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {

    /*
    if(notification.request.content.userInfo) {
        NSDictionary *userInfo = notification.request.content.userInfo;
        if([userInfo valueForKey:@"aps"] && [[userInfo valueForKey:@"aps"] valueForKey:@"content-available"] && [[[userInfo valueForKey:@"aps"] valueForKey:@"content-available"] integerValue] == 1) {
            return;
        }
    }*/
    //NSLog(@"willPresentNotification");
    //NSLog(@"%x@", notification.request.content.userInfo);
    [[NSNotificationCenter defaultCenter] postNotificationName:kADARecivedRemoteMessage
                                                        object:nil
                                                      userInfo:notification.request.content.userInfo];
}

- (BOOL)application:(UIApplication *)application shouldSaveApplicationState:(NSCoder *)coder {
    return YES;
}

- (BOOL)application:(UIApplication *)application shouldRestoreApplicationState:(NSCoder *)coder {
    return YES;
}



@end
