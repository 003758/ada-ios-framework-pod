//
//  ImageMessage.m
//  ADAFramework
//
//  Created by Choldarong-r on 13/12/2559 BE.
//  Copyright © 2559 G-ABLE ITS. All rights reserved.
//

#import "ImageMessage.h"
#import "UIImage+Extend.h"
@import MobileCoreServices;

@implementation ImageMessage

+ (NSArray<NSString*>*) mediaTypes {
    return @[(NSString*)kUTTypeImage];
}

+ (ImageMessage *)instanceWithImage:(UIImage *)oImage {
    
    
    
    
    CGSize imgSize = oImage.size;
    //NSLog(@"oImage or %ld", (long)oImage.imageOrientation);
    if (oImage.imageOrientation != UIImageOrientationUp && oImage.imageOrientation != UIImageOrientationDown) {
        //imgSize = CGSizeMake(imgSize.height, imgSize.width);
    }
    
    CGFloat ratio = imgSize.width/imgSize.height;
    
    int width = MIN(1024, MAX(1024, imgSize.width));//self.view.frame.size.width * 3;
    int height = width * ratio;
    
    UIImage *image = [oImage scaleToSize:CGSizeMake(width, height)];
    //NSLog(@"image or %ld", (long)image.imageOrientation);
    ImageMessage *message = [ImageMessage new];
    message.image = image;
    message.width = width;
    message.height = height;
    message.sizeRatio = ratio;
    
    return message;
}

@end
