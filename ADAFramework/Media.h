//
//  Media.h
//  ADAFramework
//
//  Created by Choldarong-r on 25/7/2560 BE.
//  Copyright © 2560 G-ABLE ITS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Feature.h"

typedef NS_ENUM(NSInteger, MediaContentType) {
//typedef enum MediaContentType : NSUInteger {
    MediaContentTypePDF            = 1,
    MediaContentTypeVideo         = 2,
    MediaContentTypeWeb         = 3,
    MediaContentTypePicture         = 4
    
};

@interface Media : NSObject


@property (nonatomic, retain) Feature *feature;
@property (nonatomic, retain) NSString *mediaID;
@property (nonatomic, retain) NSString *icon;
@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *detail;
@property (nonatomic, retain) NSURL    *url;
@property (nonatomic, assign) MediaContentType mediaType;
@end

