//
//  EventReviewTopic.h
//  ADAFramework
//
//  Created by Choldarong-r on 29/3/2561 BE.
//  Copyright © 2561 G-ABLE Company Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EventReviewTopic : NSObject

typedef NS_ENUM(NSInteger, EventReviewTopicType) {

    EventReviewTopicTypeRating              = 0,
    EventReviewTopicTypeComment             = 1
};



@property (nonatomic, assign) NSInteger topicID;
@property (nonatomic, retain) NSString *topic;
@property (nonatomic, assign) EventReviewTopicType topicType;
@property (nonatomic, assign) NSInteger rating;
@property (nonatomic, retain) NSString *comment;
@end
