//
//  ShuttleBusStation.h
//  ADAFramework
//
//  Created by Choldarong-r on 28/2/2562 BE.
//  Copyright © 2562 G-ABLE Company Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
@import CoreLocation;

NS_ASSUME_NONNULL_BEGIN

@interface ShuttleBusStation : NSObject
@property (nonatomic, assign) int stationID;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@end

NS_ASSUME_NONNULL_END
