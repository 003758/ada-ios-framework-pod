//
//  VideoMessage.h
//  ADAFramework
//
//  Created by Choldarong-r on 13/12/2559 BE.
//  Copyright © 2559 G-ABLE ITS. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface VideoMessage : NSObject
typedef void (^ ChatVideoBlock)(VideoMessage* _Nullable message);
+ (void) instanceWithURL:(NSURL * _Nonnull)videoURL handler:(ChatVideoBlock _Nullable) handler;
+ (NSArray<NSString*>* _Nonnull) mediaTypes;

@property (nonatomic, retain) NSString * _Nullable videoID;
@property (nonatomic, retain) NSString * _Nullable contentType;
@property (nonatomic, retain) NSString * _Nullable filePath;
@property (nonatomic, retain) NSString * _Nullable fileName;
@property (nonatomic, assign) NSInteger angle;
@end
