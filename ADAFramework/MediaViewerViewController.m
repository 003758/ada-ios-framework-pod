//
//  MediaViewerViewController.m
//  ADAFramework
//
//  Created by Choldarong-r on 26/7/2560 BE.
//  Copyright © 2560 G-ABLE ITS. All rights reserved.
//

#import "MediaViewerViewController.h"
@import AFNetworking;
@import AVFoundation;
@import AVKit;
@import QuickLook;
#import "ADAAppDelegate.h"
#import "ServiceCaller.h"
#import <CoreMedia/CoreMedia.h>
#import <CoreVideo/CoreVideo.h>
#import "AVFoundation/AVFoundation.h"
#import <MediaPlayer/MediaPlayer.h>
#import <SafariServices/SafariServices.h>
#define IS_IOS11orHIGHER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 11.0)


@interface MediaViewerViewController  () <AVPlayerViewControllerDelegate,
QLPreviewControllerDelegate, QLPreviewControllerDataSource,
UINavigationControllerDelegate, SFSafariViewControllerDelegate>
@property (nonatomic, retain) AVPlayer* player;
@property (nonatomic , retain) ADA* ada;
@property (nonatomic , retain) NSMutableDictionary<NSString*, NSString*>* fileMap;
@end

@implementation MediaViewerViewController {
    NSString *strFilePath;
    UIColor *statusBarColor;
    ProgressBlock progressHandler;
}

- (instancetype)initWith:(Media*) media {
    _media = media;
    return [super init];
}

- (instancetype)initWithArray:(NSArray<Media*>*) mediaList {
    _mediaList = mediaList;
    return [super init];
}



- (void)viewDidLoad {
    [super viewDidLoad];
    _ada = ((ADAAppDelegate*)[UIApplication sharedApplication].delegate).ada;
    _fileMap = [[NSMutableDictionary alloc] init];
    
    

    /*UIView* statusBar = [[UIApplication sharedApplication] valueForKey:@"statusBar"];
    if(statusBar){
        statusBarColor = statusBar.backgroundColor;
    }
    self.view.backgroundColor = [UIColor clearColor];*/
    UIView *view =  [[UIView alloc] initWithFrame:self.view.bounds];
    view.backgroundColor = UIColor.groupTableViewBackgroundColor;
    view.tag = 111;
    [self.view addSubview:view];
    view.translatesAutoresizingMaskIntoConstraints = NO;
    [view.leadingAnchor constraintEqualToAnchor:self.view.leadingAnchor].active = YES;
    [view.trailingAnchor constraintEqualToAnchor:self.view.trailingAnchor].active = YES;
    [view.topAnchor constraintEqualToAnchor:self.view.topAnchor].active = YES;
    [view.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor].active = YES;
    
    
    UIActivityIndicatorView *loading = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [view addSubview:loading];
    loading.transform = CGAffineTransformMakeScale(1.5, 1.5);
    [loading setCenter:self.view.center];
    loading.translatesAutoresizingMaskIntoConstraints = NO;
    [loading.centerXAnchor constraintEqualToAnchor:view.centerXAnchor].active = YES;
    [loading.centerYAnchor constraintEqualToAnchor:view.centerYAnchor].active = YES;
    [loading startAnimating];
    // Do any additional setup after loading the view.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear: animated];
    
    [self initView];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear: animated];
    UIView* statusBar = [[UIApplication sharedApplication] valueForKey:@"statusBar"];
    if(statusBar){
        statusBar.backgroundColor = statusBarColor;
    }
    [[self.view viewWithTag:111] removeFromSuperview];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)addDoneButton:(MediaContentType) type {
    if IS_IOS11orHIGHER {
        NSLog(@"Add close button");
        UIButton * btnClose = [[UIButton alloc] initWithFrame:CGRectMake(8, 22, 80, 28)];
        if(type == MediaContentTypePDF) {
            CGSize size = [UIScreen mainScreen].bounds.size;
            btnClose.frame = CGRectMake(size.width - 88, 44, 80, 28);
        }
        
        [btnClose setTitle:@"Done" forState:UIControlStateNormal];
        btnClose.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.6];
        [btnClose setTintColor:UIColor.whiteColor];
        [btnClose setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
        btnClose.titleLabel.font  = [btnClose.titleLabel.font fontWithSize:13];
        btnClose.layer.cornerRadius = 4;
        btnClose.clipsToBounds = YES;
        [btnClose addTarget:self action:@selector(dismissViewController) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btnClose];
    }
}

- (void)initView {
    
    if(_mediaList && _fileMap.count == _mediaList.count) {
        return;
    }
    
    
    if(_media) {
        if(_media.mediaType == MediaContentTypeVideo) {
            UIView* statusBar = [[UIApplication sharedApplication] valueForKey:@"statusBar"];
            if(statusBar){
                statusBar.backgroundColor = [UIColor clearColor];
            }
            
            if(progressHandler){
                progressHandler(0, 0);
            }
            
            NSMutableDictionary * headers = [NSMutableDictionary dictionary];
            //NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [headers setObject:[AppHelper getUserDataForKey:kADA_CLIENT] forKey:kADA_CLIENT];
            NSURL *url = nil;
            if(_media.mediaID){
                NSArray<NSString*> *m = [_media.mediaID componentsSeparatedByString:@":"];
                
                NSString *strURL = [NSString stringWithFormat:@"%@file?id=%@&mediaType=%@",
                                    kSERVER_HOST,
                                    m.firstObject,
                                    m.lastObject
                                    ];
                //NSLog(@"opening : %@", strURL);
                url = [NSURL URLWithString:strURL];
            }else{
                url = _media.url;
            }
            AVURLAsset * asset = [AVURLAsset URLAssetWithURL:url options:@{@"AVURLAssetHTTPHeaderFieldsKey" : headers}];
            AVPlayerItem * item = [AVPlayerItem playerItemWithAsset:asset];
            self.player = [[AVPlayer alloc] initWithPlayerItem: item];
            
            
            
            AVPlayerViewController *preview = [[AVPlayerViewController alloc] init];
            preview.player = _player;
            [preview.view setFrame:self.view.frame];
            
            
            [[self.view viewWithTag:111] removeFromSuperview];
            preview.showsPlaybackControls = YES;
            
            [self addChildViewController:preview];//*view controller containment
            
            preview.view.frame = self.view.frame;//CGRectMake(0, 0,w, h);
            
            UIView *view = preview.view;
            [self.view addSubview:view];
            
            view.translatesAutoresizingMaskIntoConstraints = NO;
            [view.leadingAnchor constraintEqualToAnchor:self.view.leadingAnchor].active = YES;
            [view.trailingAnchor constraintEqualToAnchor:self.view.trailingAnchor].active = YES;
            [view.topAnchor constraintEqualToAnchor:self.view.topAnchor].active = YES;
            [view.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor].active = YES;
            
            
            [preview didMoveToParentViewController:self];
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(dismissViewController)
                                                         name:AVPlayerItemDidPlayToEndTimeNotification
                                                       object:_player];
            
        }else if(_media.mediaType == MediaContentTypePDF || _media.mediaType == MediaContentTypePicture) {
            
            
            if(_media.mediaID){
                NSString *fileName = _media.title ?  _media.title : _media.mediaID;
                fileName = [fileName stringByReplacingOccurrencesOfString:@":" withString:@"-"];
                if(_media.mediaType == MediaContentTypePDF){
                    if([[fileName lowercaseString] rangeOfString:@".pdf"].location == NSNotFound){
                        fileName = [fileName stringByAppendingString:@".pdf"];
                    }
                }else{
                    if([[fileName lowercaseString] rangeOfString:@".jpg"].location == NSNotFound){
                        fileName = [fileName stringByAppendingString:@".jpg"];
                    }
                }
                [_ada download:_media.mediaID type:nil saveName:fileName cache:true progress:^(long progress, long total) {
                    if (self->progressHandler) {
                        self->progressHandler(progress, total);
                    }
                } handler:^(NSData *data, NSString *filePath, NSError *error) {
                    NSLog(@"%@", filePath);
                    if(error){
                        NSLog(@"Error: %@", error);
                    }
                    self->strFilePath = filePath;
                    [[self.view viewWithTag:111] removeFromSuperview];
                    self.dataSource = self;
                    self.delegate = self;
                    
                }];
            }else{
                //NSLog(@"url : %@", _media.url);
                [_ada downloadFromURL:_media.url cache:true progress:^(long progress, long total) {
                    if (self->progressHandler) {
                        self->progressHandler(progress, total);
                    }
                } handler:^(NSData *data, NSString *filePath, NSError *error) {
                    self->strFilePath = filePath;
                    [[self.view viewWithTag:111] removeFromSuperview];
                    self.dataSource = self;
                    self.delegate = self;
                    
                }];
            }
        }else if(_media.mediaType == MediaContentTypeWeb) {
            if(progressHandler){
                progressHandler(0, 0);
            }
            
            
            
            if(_media.feature.proxyName && ![_media.feature.proxyName isEqualToString:@""]){
                
                [ServiceCaller callJSON:[NSString stringWithFormat:@"%@proxy/call-by-name?serviceName=%@",
                                         kSERVER, _media.feature.proxyName]
                                 method:@"POST"
                                 header:@{@"enc": @"false"}
                              parameter:@{@"username": _ada.login.usid,
                                          @"web_id": _media.feature.webID,
                                          @"key": _media.feature.appKey}
                                handler:^(NSJSONSerialization *json) {
                                    
                                    
                                    NSString *url = self.media.feature.url;
                                    if(json && [[json valueForKey:@"result"] boolValue]){
                                        NSDictionary *appToken = [json valueForKey:@"data"];
                                        if(![[appToken valueForKey:@"response_code"] boolValue]){
                                            [AppHelper showAlert:[appToken valueForKey:@"response_detail"] handler:nil];
                                            return;
                                        }
                                        NSArray *keys = appToken.allKeys;
                                        for(NSString *key in keys){
                                            NSString *KEY = [NSString stringWithFormat:@"{%@}", key];
                                            url = [url stringByReplacingOccurrencesOfString:KEY withString:[appToken valueForKey:key]];
                                        }
                                        NSLog(@"url =  %@", url);
                                        SFSafariViewController *safariVC = [[SFSafariViewController alloc]initWithURL: [NSURL URLWithString:url]
                                                                                              entersReaderIfAvailable:NO];
                                        
                                        
                                        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 10.0) {
                                            //safariVC.preferredBarTintColor = _ada.login.bgColor;
                                            safariVC.preferredControlTintColor = self.ada.login.bgColor;
                                        } else {
                                            safariVC.view.tintColor = self.ada.login.bgColor;
                                        }
                                        
                                        safariVC.delegate = self;
                                        [self addChildViewController:safariVC];//*view controller containment
                          
                                        safariVC.view.frame = self.view.frame;//CGRectMake(0, 0,w, h);
                                        UIView *webView = safariVC.view;
                                        [self.view addSubview:webView];
                                        [[self.view viewWithTag:111] removeFromSuperview];
                                        webView.translatesAutoresizingMaskIntoConstraints = NO;
                                        [webView.leadingAnchor constraintEqualToAnchor:self.view.leadingAnchor].active = YES;
                                        [webView.trailingAnchor constraintEqualToAnchor:self.view.trailingAnchor].active = YES;
                                        [webView.topAnchor constraintEqualToAnchor:self.view.topAnchor].active = YES;
                                        [webView.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor].active = YES;
                                        
                                        [safariVC didMoveToParentViewController:self];
                                    }else{
                                        [AppHelper showAlert:[json valueForKey:@"message"] handler:nil];
                                    }
                                }];
                return;
            
            }
            
            
            
            [[self.view viewWithTag:111] removeFromSuperview];
            SFSafariViewController *safariVC = [[SFSafariViewController alloc]initWithURL: _media.url
                                                                 entersReaderIfAvailable:NO];
            
            
            if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 10.0) {
                //safariVC.preferredBarTintColor = _ada.login.bgColor;
                safariVC.preferredControlTintColor = _ada.login.bgColor;
            } else {
                safariVC.view.tintColor = _ada.login.bgColor;
            }
            
            safariVC.delegate = self;
            [self addChildViewController:safariVC];//*view controller containment
     
            safariVC.view.frame = self.view.frame;//CGRectMake(0, 0,w, h);
            
            UIView *webView = safariVC.view;
            [self.view addSubview:webView];
            
            webView.translatesAutoresizingMaskIntoConstraints = NO;
            [webView.leadingAnchor constraintEqualToAnchor:self.view.leadingAnchor].active = YES;
            [webView.trailingAnchor constraintEqualToAnchor:self.view.trailingAnchor].active = YES;
            [webView.topAnchor constraintEqualToAnchor:self.view.topAnchor].active = YES;
            [webView.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor].active = YES;
            
            
            [safariVC didMoveToParentViewController:self];
     
        }
    }else if(_mediaList) {
        for(Media *m in _mediaList) {
            if(m.mediaType == MediaContentTypePDF || m.mediaType == MediaContentTypePicture) {
                if(m.mediaID) {
                    NSString *fileName = m.title ?  m.title : m.mediaID;
                    fileName = [fileName stringByReplacingOccurrencesOfString:@":" withString:@"-"];
                    if(m.mediaType == MediaContentTypePDF){
                        if([[fileName lowercaseString] rangeOfString:@".pdf"].location == NSNotFound){
                            fileName = [fileName stringByAppendingString:@".pdf"];
                        }
                    }else{
                        if([[fileName lowercaseString] rangeOfString:@".jpg"].location == NSNotFound){
                            fileName = [fileName stringByAppendingString:@".jpg"];
                        }
                    }
                    [_ada download:m.mediaID type:nil saveName:fileName cache:true progress:^(long progress, long total) {
                        if (self->progressHandler) {
                            self->progressHandler(progress, total);
                        }
                    } handler:^(id ref, NSData *data, NSString *filePath, NSError *error) {
                        
                        if(error){
                            NSLog(@"Error: %@", error);
                        }
                        self->strFilePath = filePath;
                        
                        
                        NSString *key = [NSString stringWithFormat:@"%@", ref];
                        [self.fileMap setValue:filePath forKey:key];
                        if(self.fileMap.count == self.mediaList.count) {
                            [[self.view viewWithTag:111] removeFromSuperview];
                            self.dataSource = self;
                            self.delegate = self;
                            [self reloadData];
                            self.currentPreviewItemIndex = self.previewItemIndex;
                        }
                    } ref: m.mediaID];
                }else{
                    //NSLog(@"url : %@", _media.url);
                    [_ada downloadFromURL:m.url cache:true progress:^(long progress, long total) {
                        if (self->progressHandler) {
                            self->progressHandler(progress, total);
                        }
                    } handler:^(id ref, NSData *data, NSString *filePath, NSError *error) {
                        self->strFilePath = filePath;
                        
                        
                        NSString *key = [NSString stringWithFormat:@"%@", ref];
                        key = [key stringByReplacingOccurrencesOfString:@"." withString:@""];
                        [self.fileMap setValue:filePath forKey:key];
                        
                        if(self.fileMap.count == self.mediaList.count) {
                            [[self.view viewWithTag:111] removeFromSuperview];
                            self.dataSource = self;
                            self.delegate = self;
                            [self reloadData];
                            self.currentPreviewItemIndex = self.previewItemIndex;
                        }
                    } ref:m.url.absoluteString];
                }
            }
        }
    }
}

- (void) setProgressHandler:(ProgressBlock) handler {
    progressHandler = handler;
}

- (void) dismissViewController {
    [self dismissViewControllerAnimated:true completion:nil];
}

- (NSInteger)numberOfPreviewItemsInPreviewController:(QLPreviewController *)controller {
    return _mediaList ? _fileMap.count : 1;
}


- (id <QLPreviewItem>)previewController:(QLPreviewController *)controller previewItemAtIndex:(NSInteger)index {
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    if(_mediaList) {
        Media *m = [_mediaList objectAtIndex:index];
        if(m.mediaID) {
            NSString *strPath = [_fileMap valueForKey:m.mediaID];
            if(strPath) {
                if([fileMgr fileExistsAtPath:strPath]){
                    //NSLog(@"has file");
                }else{
                    [self dismissViewController];
                }
                return [NSURL fileURLWithPath:strPath];
            }
        }else{
            NSString *key = m.url.absoluteString;
            key = [key stringByReplacingOccurrencesOfString:@"." withString:@""];
            NSString *strPath = [_fileMap valueForKey:key];
            if(strPath) {
                //NSLog(@"QL Path %@", strPath);
                if([fileMgr fileExistsAtPath:strPath]){
                    //NSLog(@"has file");
                }else{
                    [self dismissViewController];
                }
                return [NSURL fileURLWithPath:strPath];
            }
        }
    }else{
        //NSURL *tmpDirURL = [NSURL fileURLWithPath:NSTemporaryDirectory() isDirectory:YES];
        NSString *strPath = strFilePath;
        //NSLog(@"QL Path %@", strPath);
        if([fileMgr fileExistsAtPath:strPath]){
            //NSLog(@"has file");
        }else{
            [self dismissViewController];
        }
        return [NSURL fileURLWithPath:strPath];
    }
    return nil;
}



@end
