//
//  InboxMessage.h
//  ADAFramework
//
//  Created by Choldarong-r on 18/7/2560 BE.
//  Copyright © 2560 G-ABLE ITS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Media.h"

@interface InboxMessage : NSObject<NSCopying>
@property (nonatomic, assign) long messageID;
@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *message;
@property (nonatomic, retain) NSString *author;
@property (nonatomic, retain) NSDate *date;
/*@property (nonatomic, retain) NSString *imageID;
@property (nonatomic, retain) NSString *pdfFileID;
@property (nonatomic, retain) NSString *pdfFileName;*/
@property (nonatomic, retain) Media *image;
@property (nonatomic, retain) Media *pdf;
@property (nonatomic, assign) BOOL read;
@end
