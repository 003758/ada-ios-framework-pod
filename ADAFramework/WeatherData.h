//
//  WeatherData.h
//  ADAFramework
//
//  Created by Choldarong-r on 24/9/2560 BE.
//  Copyright © 2560 G-ABLE ITS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WeatherData : NSObject

@property (nonatomic, retain) NSString *weatherDescription;
@property (nonatomic, retain) NSString *iconID;
@property (nonatomic, retain) NSString *icon;
@property (nonatomic, retain) NSString *weather;
@property (nonatomic, retain) NSDate *weatherDate;
@property (nonatomic, retain) NSDate *updateDate;

@property (nonatomic, assign) float windSpped;
@property (nonatomic, assign) float windDegree;
@property (nonatomic, assign) float clouds;
@property (nonatomic, assign) float temp;
@property (nonatomic, assign) float tempMin;
@property (nonatomic, assign) float tempMax;

@property (nonatomic, retain) NSArray<WeatherData*> *forecast;
@end
