//
//  VoteAnswerMember.h
//  ADAFramework
//
//  Created by Choldarong-r on 29/8/2560 BE.
//  Copyright © 2560 G-ABLE ITS. All rights reserved.
//

#import "ADAMember.h"
#import "VoteChoice.h"

@interface VoteAnswerMember : ADAMember


@property (nonatomic, retain) NSArray<VoteChoice*>* answers;

@end
