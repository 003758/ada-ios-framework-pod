//
//  ContactColumn.h
//  ADAFramework
//
//  Created by Choldarong-r on 6/12/2559 BE.
//  Copyright © 2559 G-ABLE ITS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ContactColumn : NSObject

@property (nonatomic, retain) NSString *labelTH;
@property (nonatomic, retain) NSString *labelEN;
@property (nonatomic, retain) NSString *labelDB;
@property (nonatomic, retain) NSString *attributeTH;
@property (nonatomic, retain) NSString *attributeEN;
@property (nonatomic, assign) NSInteger sequence;
@property (nonatomic, assign) BOOL editable;
@property (nonatomic, assign) BOOL preContactDisplay;
@property (nonatomic, assign) BOOL contactDisplay;
@property (nonatomic, retain) NSString *templateValue;
@property (nonatomic, assign) NSInteger sortSequence;
@property (nonatomic, assign) BOOL sortAccending;
@end
