//
//  QuizChoice.h
//  ADAFramework
//
//  Created by Choldarong-r on 29/6/2560 BE.
//  Copyright © 2560 G-ABLE ITS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QuizChoice : NSObject
@property (nonatomic, assign) long choiceID;
@property (nonatomic, assign) long seq;
@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *imageID;
@property (nonatomic, assign) BOOL correctAnswer;
@property (nonatomic, assign) long totalPerson;
@property (nonatomic, assign) double percent;
@end
