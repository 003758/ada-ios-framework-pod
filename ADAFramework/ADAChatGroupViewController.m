//
//  ADAChatGroupViewController.m
//  ADAFramework
//
//  Created by Choldarong-r on 19/12/2559 BE.
//  Copyright © 2559 G-ABLE ITS. All rights reserved.
//
#import "ADAChatGroupViewController.h"
#import "ADAAppDelegate.h"
#import "AppHelper.h"
#import "ServiceCaller.h"
#import "ChatRoom.h"
#import "ChatRoomGroup.h"
#import "NSDateFormatter+Extend.h"
@import XMPPFramework;

@interface ADAChatGroupViewController () <XMPPStreamDelegate>

@end

#ifdef DEBUG
#   define NSLog(...) NSLog(__VA_ARGS__)
#else
#   define NSLog(...) (void)0
#endif


@implementation ADAChatGroupViewController{
    NSString *kGroupEvent;
    NSString *kGroupHidden;
    NSString *kGroupPending;
    NSString *kGroupOnlineActive;
    NSString *kGroupActive;
    NSString *kGroupDept;
    NSMutableArray *chatGroups;
    NSMutableArray *usersOnline;
    ADA *ada;
    XMPPStream* chatStream;
    BOOL connected;
    BOOL canAddGroup;
    NSMutableDictionary<NSString*, NSNumber*> *notiCountMap;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    ada = ((ADAAppDelegate*)[UIApplication sharedApplication].delegate).ada;
    chatGroups = [[NSMutableArray alloc] init];
    usersOnline = [[NSMutableArray alloc] init];
    notiCountMap = [[NSMutableDictionary alloc] init];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    /*
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidBecomeActiveNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(viewWillAppear:)
                                                name:UIApplicationDidBecomeActiveNotification
                                              object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationWillResignActiveNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(closeXMPPConnection)
                                                name:UIApplicationWillResignActiveNotification
                                              object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationWillTerminateNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(closeXMPPConnection)
                                                name:UIApplicationWillTerminateNotification
                                              object:nil];
    
    
    if(chatGroups && chatGroups.count > 0){
        //[self connectXMPPConnection];
    }*/
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear: animated];
    [self fetchChatGroup];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear: animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidBecomeActiveNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationWillResignActiveNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationWillTerminateNotification
                                                  object:nil];
    
    
    
    [self closeXMPPConnection];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL) isServerConnected {
    return connected;
}

- (void)beforeDisplayChatGroup {
    
}

- (void) fetchChatGroup {
    
    //NSLog(@"fetchChatGroup");
    if ([AppHelper isThaiLanguage]) {
        kGroupEvent         = @"ห้องสนทนาหลัก";
        kGroupHidden        = @"ห้องสนทนาที่ถูกซ่อน";
        kGroupPending       = @"เชิญเข้าห้องสนทนากลุ่ม";
        
        kGroupOnlineActive  = @"ห้องสนทนาออนไลน์";
        //kGroupActive        = @"ห้องสนทนาออฟไลน์";
        kGroupActive        = @"ห้องสนทนา";
        kGroupDept          = @"ห้องสนทนาหน่วยงาน";
    }else{
        kGroupEvent         = @"Official Groups";
        kGroupHidden        = @"Hidden Chats";
        kGroupPending       = @"Group Invitations";
        
        kGroupOnlineActive  = @"Online Chats";
        //kGroupActive        = @"Offline Chats";
        kGroupActive        = @"Chat Rooms";
        kGroupDept          = @"Unit Groups";
    }
    
    
    if (![ada isLoggedOn]) {

        return;
    }
    
    [notiCountMap removeAllObjects];
    //NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    
    NSString *kNotificationKey = [NSString stringWithFormat:@"%@-%@@notifications", ada.login.usid, ada.login.group];
    
    NSArray *notifications = (NSArray*)[AppHelper getUserDataForKey:kNotificationKey];//[userDefaults valueForKey:kNotificationKey];
    //NSLog(@"notifications %@", notifications);
    if(notifications){
        NSMutableArray<NSData*> *arr = [NSMutableArray arrayWithArray:notifications];
        
        for(NSData *data in arr) {
            ADANotification *notification = [NSKeyedUnarchiver unarchiveObjectWithData:data];
            if(notification.json && (notification.featureType == FeatureTypeChat || notification.featureType == FeatureTypeChatAction)) {
                
                NSString *roomID = [NSString stringWithFormat:@"%@", [notification.json valueForKey:kRoomID]];
                if([notiCountMap valueForKey:roomID]){
                    NSNumber *count = [notiCountMap valueForKey:roomID];
                    count = [NSNumber numberWithInteger:count.integerValue + 1];
                    [notiCountMap setValue:count forKey:roomID];
                }else{
                    [notiCountMap setValue:[NSNumber numberWithInteger:1] forKey:roomID];
                }
            }
        }
        
    }
    
    
    Login *login = ada.login;
    NSString *usid = login.usid;
    NSString *group = login.group;
    
    [chatGroups removeAllObjects];
    //[self connectXMPPConnection];
    [self beforeDisplayChatGroup];
    [ServiceCaller getJSONCache:[NSString stringWithFormat:@"%@msg-groups/get-related-rooms/%@/%@",
                             kSERVER, group, usid]
                     method:@"GET"
                     header:nil
                  parameter:@{@"status":@"A"}
                    handler:^(NSJSONSerialization *json) {
                        
                        if(json){
                            NSMutableArray<ChatRoomGroup*> *roomsList = [[NSMutableArray<ChatRoomGroup*> alloc] init];
                            
                            [self doProcessChatGroup:roomsList fromJSON:json];
                            
                            [self displayChatGroup:roomsList];
                            [ServiceCaller getJSONCache:[NSString stringWithFormat:@"%@msg-groups/get-event-room",
                                                     kSERVER]
                                             method:@"GET"
                                             header:nil
                                          parameter:@{}
                                            handler:^(NSJSONSerialization *json) {
                                                if(json){
                                                    NSMutableArray<ChatRoom*> *eventList = [[NSMutableArray<ChatRoom*> alloc] init];
                                                    [self doProcessEventGroup:eventList fromJSON:json];
                                                    
                                                    [self displayChatGroup:roomsList];
                                                    [self displayEvent:eventList];
                                                }else{
                                                    [self displayChatGroup:roomsList];
                                                }
                                            }];
                            
                        }
                        [self beforeDisplayChatGroup];
                        [ServiceCaller callJSON:[NSString stringWithFormat:@"%@msg-groups/get-related-rooms/%@/%@",
                                                 kSERVER, group, usid]
                                         method:@"GET"
                                         header:nil
                                      parameter:@{@"status":@"A"}
                                        handler:^(NSJSONSerialization *json) {
                                            if(json){
                                                NSMutableArray<ChatRoomGroup*> *roomsList = [[NSMutableArray<ChatRoomGroup*> alloc] init];
                                                
                                                [self doProcessChatGroup:roomsList fromJSON:json];
                                                
                                                [self displayChatGroup:roomsList];
                                                [ServiceCaller callJSON:[NSString stringWithFormat:@"%@msg-groups/get-event-room",
                                                                         kSERVER]
                                                                 method:@"GET"
                                                                 header:nil
                                                              parameter:@{}
                                                                handler:^(NSJSONSerialization *json) {
                                                                    if(json){
                                                                        NSMutableArray<ChatRoom*> *eventList = [[NSMutableArray<ChatRoom*> alloc] init];
                                                                        [self doProcessEventGroup:eventList fromJSON:json];
                                                                        
                                                                        
                                                                        [self displayChatGroup:roomsList];
                                                                        [self displayEvent:eventList];
                                                                    }else{
                                                                        [self displayChatGroup:roomsList];
                                                                    }
                                                                }];
                                            }
                                        }];
                    }];
}

- (BOOL) canAddNewGroup {
    return canAddGroup;
}

- (void) doProcessChatGroup:(NSMutableArray*) roomsList fromJSON:(NSJSONSerialization*) json {
    if([[json valueForKey:@"result"] boolValue]){
        [chatGroups removeAllObjects];
        NSArray *rooms = [json valueForKey:@"data"];
        
        NSMutableArray *groupList = [[NSMutableArray alloc] init];
        NSMutableArray *displayList = [[NSMutableArray alloc] init];
        NSInteger seq = 1;
        
        NSJSONSerialization *site = [json valueForKey:@"site"];
        if([[site valueForKey:@"chat_user_grp_enabled"] boolValue]){
            canAddGroup = YES;
        }else{
            canAddGroup = NO;
        }
        
        NSDateFormatter *parser = [NSDateFormatter instanceWithUSLocale];
        parser.dateFormat = @"yyyy-MM-dd HH:mm:ss.0";
        
        for (NSJSONSerialization *room in rooms) {
            BOOL online = NO;
            
            NSString *member = [room valueForKey:@"members"];
            
            NSMutableSet *memberSet = [[NSMutableSet alloc] init];
            NSArray *members = [member componentsSeparatedByString:@","];
            for (NSString *m in members) {
                [memberSet addObject:[[m stringByAppendingString:@"@"] stringByAppendingString:ada.login.chatServerDomain]];
            }
            
            
            for (NSString *onlineUser in usersOnline) {
                if([memberSet containsObject:onlineUser]){
                    online = YES;
                    break;
                }
            }
            
            ChatRoom *r = [ChatRoom new];
            
            if([notiCountMap valueForKey:[NSString stringWithFormat:@"%@", [room valueForKey:@"id"]]]){
                r.notificationCount = [notiCountMap valueForKey:[NSString stringWithFormat:@"%@", [room valueForKey:@"id"]]].integerValue;
            }
            r.memberOnline = online;
            r.seq = seq++;
            r.roomID = [NSString stringWithFormat:@"%@", [room valueForKey:@"id"]];
            r.roomName = [room valueForKey:@"group_name"];
            r.roomDecription = [room valueForKey:@"group_desc"];
            r.targetUserID = [room valueForKey:@"usid"];
            r.targetUserGroup = [room valueForKey:@"user_group"];
            r.lastMessage = [room valueForKey:@"last_message"];
            r.members = memberSet;
            if([room valueForKey:@"group_picture"] && ![[room valueForKey:@"group_picture"] isEqualToString:@""]){
                r.imageID = [NSString stringWithFormat:@"%@:profilePicture", [room valueForKey:@"group_picture"]];
            }
            r.notification = [[room valueForKey:@"notify"] boolValue];
            r.joinRoom = [[room valueForKey:@"join_status"] boolValue];
            r.roomType = [[room valueForKey:@"group_type"] integerValue];
            r.roomOwner = [[room valueForKey:@"create_by"] isEqualToString:ada.login.usid];
            if(r.roomType == ChatRoomTypePrivate) {
                ADAMember *member = [ADAMember new];
                
                member.userID = r.targetUserID;
                member.group = r.targetUserGroup;
                NSString *name = [ada getContactNickname:member];
                if(name) {
                    r.roomName = name;
                }
            }
            
            if([room valueForKey:@"event"]) {
                NSJSONSerialization *roomJSON = [room valueForKey:@"event"];
                //NSLog(@"%@", roomJSON);
                r.eventOwner = [[roomJSON valueForKey:@"create_by"] isEqualToString:ada.login.usid];
                r.eventID = [[roomJSON valueForKey:@"id"] integerValue];
                if(r.eventOwner) {
                    r.roomName = [NSString stringWithFormat:@"%@@%@", [roomJSON valueForKey:@"event_name"], [room valueForKey:@"member_name"]];
                }else{
                    r.roomName = [roomJSON valueForKey:@"event_name"];
                }
                r.roomDecription = [roomJSON valueForKey:@"event_detail"];
                r.chatBot = [[roomJSON valueForKey:@"bot"] boolValue];
                r.startDate = [parser dateFromString:[roomJSON valueForKey:@"start_date"]];
                r.endDate = [parser dateFromString:[roomJSON valueForKey:@"end_date"]];
                r.eventOwnerName = [roomJSON valueForKey:@"name"];
                r.eventActive = [[roomJSON valueForKey:@"event_status"] isEqualToString:@"A"];
                r.eventExpired = [[roomJSON valueForKey:@"event_status"] isEqualToString:@"E"];
                if([roomJSON valueForKey:@"event_image"] && ![[roomJSON valueForKey:@"event_image"] isEqualToString:@""]){
                    r.eventImageID = [NSString stringWithFormat:@"%@:chatEvent", [roomJSON valueForKey:@"event_image"]];
                }
                if([roomJSON valueForKey:@"profile_image"] && ![[roomJSON valueForKey:@"profile_image"] isEqualToString:@""]){
                    r.imageID = [NSString stringWithFormat:@"%@:chatEvent", [roomJSON valueForKey:@"profile_image"]];
                }
                
                
            }
            
            
            if(online){//[[json valueForKey:@"online_members"] integerValue] > 0 || online){
                [room setValue:[NSNumber numberWithInt:1] forKey:@"online_members"];
            }else{
                [room setValue:[NSNumber numberWithInt:0] forKey:@"online_members"];
            }
            if ([[room valueForKey:@"join_status"] integerValue] == 0) {
                if(![groupList containsObject:kGroupPending]){
                    [groupList addObject:kGroupPending];
                    [displayList addObject:[[NSMutableArray alloc] init]];
                }
                
                [[displayList objectAtIndex:[groupList indexOfObject:kGroupPending]] addObject:r];
            }else{
                if([[room valueForKey:@"group_type"] integerValue] == 2){
                    if(![groupList containsObject:kGroupDept]){
                        [groupList addObject:kGroupDept];
                        [displayList addObject:[[NSMutableArray alloc] init]];
                    }
                    [[displayList objectAtIndex:[groupList indexOfObject:kGroupDept]] addObject:r];
                }else{
                    
                    if(online){//[[room valueForKey:@"online_members"] integerValue] > 0 || online){
                        if(![groupList containsObject:kGroupOnlineActive]){
                            [groupList addObject:kGroupOnlineActive];
                            [displayList addObject:[[NSMutableArray alloc] init]];
                        }
                        [[displayList objectAtIndex:[groupList indexOfObject:kGroupOnlineActive]] addObject:r];
                    }else{
                        if(![groupList containsObject:kGroupActive]){
                            [groupList addObject:kGroupActive];
                            [displayList addObject:[[NSMutableArray alloc] init]];
                        }
                        
                        [[displayList objectAtIndex:[groupList indexOfObject:kGroupActive]] addObject:r];
                    }
                }
            }
        }
        
        NSMutableArray *arr1 =  [groupList containsObject:kGroupDept] ? [displayList objectAtIndex:[groupList indexOfObject:kGroupDept]] : nil;
        
        NSMutableArray *arr2 =  [groupList containsObject:kGroupPending] ? [displayList objectAtIndex:[groupList indexOfObject:kGroupPending]] : nil;
        
        NSMutableArray *arr3 =  [groupList containsObject:kGroupOnlineActive] ? [displayList objectAtIndex:[groupList indexOfObject:kGroupOnlineActive]] : nil;
        
        NSMutableArray *arr4 =  [groupList containsObject:kGroupActive] ? [displayList objectAtIndex:[groupList indexOfObject:kGroupActive]] : nil;
        
        if(arr2){
            
            ChatRoomGroup *g = [ChatRoomGroup new];
            g.groupTitle = kGroupPending;
            g.roomType = ChatRoomTypePending;
            g.rooms = arr2;
            g.seq = 1;
            [roomsList addObject:g];
        }
        if(arr1){
            ChatRoomGroup *g = [ChatRoomGroup new];
            g.groupTitle = kGroupDept;
            g.roomType = ChatRoomTypeOfficial;
            g.rooms = arr1;
            g.seq = 2;
            [roomsList addObject:g];
        }
        
        if(arr3){
            
            ChatRoomGroup *g = [ChatRoomGroup new];
            g.groupTitle = kGroupOnlineActive;
            g.roomType = ChatRoomTypeGroup;
            g.rooms = arr3;
            g.seq = 3;
            [roomsList addObject:g];
        }
        if(arr4){
            
            ChatRoomGroup *g = [ChatRoomGroup new];
            g.groupTitle = kGroupActive;
            g.roomType = ChatRoomTypeGroup;
            g.rooms = arr4;
            g.seq = 4;
            [roomsList addObject:g];
        }
        [chatGroups addObjectsFromArray:roomsList];
        
        
    }
}

- (void) doProcessEventGroup:(NSMutableArray*) roomsList fromJSON:(NSJSONSerialization*) json {
    if([[json valueForKey:@"result"] boolValue]){
        //[chatGroups removeAllObjects];
        NSArray *rooms = [json valueForKey:@"data"];
        
        NSInteger seq = 1;
        NSDateFormatter *parser = [NSDateFormatter instanceWithUSLocale];
        parser.dateFormat = @"yyyy-MM-dd HH:mm:ss.0";
        for(NSJSONSerialization *room in rooms){
            ChatRoom *r = [ChatRoom new];
            r.roomType = ChatRoomTypeEvent;
            r.seq = seq++;
            r.eventID = [[room valueForKey:@"id"] integerValue];
            r.roomName = [room valueForKey:@"event_name"];
            r.roomDecription = [room valueForKey:@"event_detail"];
            r.chatBot = [[room valueForKey:@"bot"] boolValue];
            r.lastMessage = nil;
            r.startDate = [parser dateFromString:[room valueForKey:@"start_date"]];
            r.endDate = [parser dateFromString:[room valueForKey:@"end_date"]];
            r.eventOwnerName = [room valueForKey:@"name"];
            
            if([room valueForKey:@"event_image"]){
                r.eventImageID = [NSString stringWithFormat:@"%@:chatEvent", [room valueForKey:@"event_image"]];
            }
            if([room valueForKey:@"profile_image"]){
                r.imageID = [NSString stringWithFormat:@"%@:chatEvent", [room valueForKey:@"profile_image"]];
            }
            
            r.roomOwner = [[room valueForKey:@"create_by"] isEqualToString:ada.login.usid];
            
            [roomsList addObject:r];
            
        }
    }
}

#pragma mark - XMPPStream

- (void)connectXMPPConnection {
    
    
 

    //self.XMPP_HOST = @"localhost";
    //NSLog(@"Connecting...%@:%ld", self->ada.login.chatServerHost, (long)self->ada.login.chatServerPort);
    chatStream = [[XMPPStream alloc] init];
    [chatStream setHostName:self->ada.login.chatServerHost];
    [chatStream setHostPort:self->ada.login.chatServerPort];
    [chatStream addDelegate:self delegateQueue:dispatch_get_main_queue()];
    
    NSString *userID = [[NSString stringWithFormat:@"%@-%@", self->ada.login.usid, self->ada.login.group] lowercaseString];
    XMPPJID *myJID = [XMPPJID jidWithString:[[userID stringByAppendingString:@"@"] stringByAppendingString:self->ada.login.chatServerDomain] resource:[UIDevice currentDevice].identifierForVendor.UUIDString];
    
    [chatStream setMyJID:myJID];
    //NSLog(@"userID %@", chatStream.myJID.full);
    NSError *error = nil;
    
    if (![chatStream connectWithTimeout:5 error:&error]){
        NSLog(@"Cannot connect %@", error);
        
        return;
    }
    
}

- (void) closeXMPPConnection {
    
    
    //NSLog(@"close socket");
    if(chatStream){
        [chatStream disconnect];
        chatStream = nil;
        
    }
    
}

- (void)xmppStreamDidDisconnect:(XMPPStream *)sender withError:(NSError *)error {
    NSLog(@"connot connect %@", error);
    
   
    chatStream = nil;
    [self chatServerConnectionState:NO];
    connected = NO;
}



- (void)xmppStreamWillConnect:(XMPPStream *)sender {
    //NSLog(@"will connect");
}

- (void)xmppStream:(XMPPStream *)sender didReceiveError:(DDXMLElement *)error {
    if (error) {
        NSLog(@"error %@", error);
    }
    [self chatServerConnectionState:NO];
}



- (void)xmppStream:(XMPPStream *)sender socketDidConnect:(GCDAsyncSocket *)socket {
    
    //NSLog(@"connect socket");
}

- (void) xmppStream:(XMPPStream *)sender didNotAuthenticate:(DDXMLElement *)error {
    NSLog(@"didNotAuthenticate : %@", error);
    [self chatServerConnectionState:NO];
}

- (void)xmppStreamDidStartNegotiation:(XMPPStream *)sender {
    
}

- (void) xmppStreamDidConnect:(XMPPStream *)sender {
    
    //NSLog(@"Connected");
    NSString *password = [[NSString stringWithFormat:@"%@-%@", self->ada.login.usid, self->ada.login.group] lowercaseString];
    //dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    NSError *error = nil;
    [chatStream authenticateWithPassword:password error:&error];
    //xmppAutoTime = [[XMPPAutoTime alloc] initWithDispatchQueue:dispatch_get_main_queue()];
    //[xmppAutoTime activate:chatStream];
    if (error) {
        NSLog(@"error %@", error);
    } 
}


- (void) xmppStreamDidAuthenticate:(XMPPStream *)sender {
    //NSLog(@"Authen.");
    // Initialize XMPPPresence variable
    XMPPPresence *presence = [XMPPPresence presenceWithType:@"available"];
    [sender sendElement:presence];
    connected = YES;
}


- (void)xmppStream:(XMPPStream *)sender didReceivePresence:(XMPPPresence *)presence {
    NSString *user = presence.from.full;
    NSString *type = presence.type;
    user = [user substringToIndex:[user rangeOfString:@"/"].location];
    //NSLog(@"user %@, type: %@", user, type);
    NSString *currentUser = [[NSString stringWithFormat:@"%@-%@", self->ada.login.usid, self->ada.login.group] lowercaseString];
    if ([user rangeOfString:currentUser].location == NSNotFound) {
        if ([@"unavailable" isEqualToString:type]) {
            [usersOnline removeObject:user];
        }else{
            [usersOnline addObject:user];
        }
        NSMutableArray *onlineRooms = [[NSMutableArray alloc] init];
        
        
        
        for(ChatRoomGroup *group in chatGroups){
            for(ChatRoom *room in group.rooms){
                room.memberOnline = NO;
                for (NSString *onlineUser in usersOnline) {
                    if([room.members containsObject:onlineUser]){
                        room.memberOnline = YES;
                        if(room.roomType != ChatRoomTypeOfficial && room.joinRoom){
                            [onlineRooms addObject:room];
                        }
                        break;
                    }
                }
                
            }
            NSMutableArray *rooms = (NSMutableArray*)group.rooms;
            [rooms removeObjectsInArray:onlineRooms];
        }
        
        if(onlineRooms.count > 0){
            NSArray *onlineGroupList = [chatGroups filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"groupTitle == %@", kGroupOnlineActive]];
            if(onlineGroupList.count == 0){
                ChatRoomGroup *g = [ChatRoomGroup new];
                g.groupTitle = kGroupOnlineActive;
                g.rooms = onlineRooms;
                g.seq = 3;
                [chatGroups insertObject:g atIndex:1];
            }else{
                ChatRoomGroup *g = onlineGroupList.firstObject;
                g.rooms = onlineRooms;
            }
            
        }else{
            NSArray *onlineGroup = [chatGroups filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"groupTitle == %@", kGroupOnlineActive]];
            if (onlineGroup && onlineGroup.count > 0) {
                ChatRoomGroup *g = onlineGroup.firstObject;
                for (ChatRoom *room in g.rooms) {
                    NSArray *offlineGroup = [chatGroups filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"groupTitle == %@", kGroupActive]];
                    if(offlineGroup && offlineGroup.count > 0){
                        ChatRoomGroup *offGroup = offlineGroup.firstObject;
                        [((NSMutableArray*)offGroup.rooms) addObject:room];
                    }
                }
                [chatGroups removeObject:g];
            }
        }
        for(ChatRoomGroup *group in chatGroups){
            group.rooms =  [NSMutableArray arrayWithArray:[group.rooms sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"seq" ascending:YES]]]];
        }
        
        [chatGroups sortUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"seq" ascending:YES]]];
        
        [self displayChatGroup:chatGroups];
        
    }

}

- (BOOL)xmppStream:(XMPPStream *)sender didReceiveIQ:(XMPPIQ *)iq{
    
    return NO;
}

- (void)displayChatGroup:(NSArray *)groups {
    
}

- (void)displayEvent:(NSArray<ChatRoom *> *)events {
    
}

- (void)chatServerConnectionState:(BOOL)connected {
    
}



@end
