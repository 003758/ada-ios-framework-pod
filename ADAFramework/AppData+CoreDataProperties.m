//
//  AppData+CoreDataProperties.m
//  ADAFramework
//
//  Created by Choldarong-r on 24/10/2561 BE.
//  Copyright © 2561 G-ABLE Company Limited. All rights reserved.
//
//

#import "AppData+CoreDataProperties.h"

@implementation AppData (CoreDataProperties)

+ (NSFetchRequest<AppData *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"AppData"];
}

@dynamic key;
@dynamic data;

@end
