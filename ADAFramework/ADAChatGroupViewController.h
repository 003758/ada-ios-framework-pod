//
//  ADAChatGroupViewController.h
//  ADAFramework
//
//  Created by Choldarong-r on 19/12/2559 BE.
//  Copyright © 2559 G-ABLE ITS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ADA.h"
#import "ChatRoomGroup.h"

@interface ADAChatGroupViewController : UIViewController




- (void) fetchChatGroup;
- (void) beforeDisplayChatGroup;
- (void) displayChatGroup:(NSArray<ChatRoomGroup*>*) groups;
- (void) displayEvent:(NSArray<ChatRoom*>*) events;
- (void) chatServerConnectionState:(BOOL) connected;
- (BOOL) isServerConnected;
- (BOOL) canAddNewGroup;

//- (void) connectChatServerWithHandler:(ChatConnectionBlock) connectionHandler roomHandler:(ArrayBlock) updatedRoomHandler;
@end
