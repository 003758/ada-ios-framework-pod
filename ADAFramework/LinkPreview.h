//
//  LinkPreview.h
//  ADAFramework
//
//  Created by Choldarong-r on 24/4/2561 BE.
//  Copyright © 2561 G-ABLE Company Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LinkPreview : NSObject

@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *linkDescription;
@property (nonatomic, retain) NSString *linkURL;
@property (nonatomic, retain) NSString *imageURL;
@property (nonatomic, assign) float imageWidth;
@property (nonatomic, assign) float imageHeight;

@end
