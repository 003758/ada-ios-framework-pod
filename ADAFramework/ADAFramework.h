//
//  ADAFramework.h
//  ADAFramework
//
//  Created by Choldarong-r on 2/12/2559 BE.
//  Copyright © 2559 G-ABLE ITS. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "ADA.h"
#import "ADAAppDelegate.h"
#import "GAHelper.h"
#import "AppHelper.h"
#import "Feature.h"
#import "FeatureGroup.h"
#import "ADAMember.h"
#import "ChatRoom.h"
#import "ChatRoomGroup.h"
#import "ContactPerson.h"
#import "ContactColumn.h"
#import "ChatMessage.h"
#import "O365User.h"
#import "O365Calendar.h"
#import "O365Attendee.h"
#import "ADAChatViewController.h"
#import "ADAChatGroupViewController.h"
#import "ChatTapGestureRecognizer.h"
#import "NewsFeedCategory.h"
#import "NewsFeed.h"
#import "NewsFeedDetail.h"
#import "FlashNews.h"
#import "RSSFeed.h"
#import "RSSFeedChannel.h"
#import "UIColor+HexString.h"
#import "UIImage+Extend.h"
#import "NSDateFormatter+Extend.h"
#import "UIImage+ImageEffects.h"
#import "CoreDataUtils.h"
#import "Personal.h"
#import "ShuttleBusTimeTable.h"
#import "ShuttleBusRoute.h"
#import "ShuttleBusLocation.h"
#import "ShuttleBusStation.h"
#import "QuizSet.h"
#import "Quiz.h"
#import "QuizChoice.h"
#import "Vote.h"
#import "VoteChoice.h"
#import "VoteAnswerMember.h"
#import "ShareYourThoughtCategory.h"
#import "ShareYourThought.h"
#import "ADANotification.h"
#import "InboxMessage.h"
#import "CompanyProfile.h"
#import "Media.h"
#import "MediaViewerViewController.h"
#import "ADAClient.h"
#import "WeatherData.h"
#import "UIResponder+FirstResponder.h"
#import "Version.h"
#import "EventRegister.h"
#import "EventReviewTopic.h"
#import "ReserveRoom.h"
#import "MeetingRoom.h"
#import "MeetingRoomReserved.h"
#import "Building.h"


//! Project version number for ADAFramework.
FOUNDATION_EXPORT double ADAFrameworkVersionNumber;

//! Project version string for ADAFramework.
FOUNDATION_EXPORT const unsigned char ADAFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ADAFramework/PublicHeader.h>


