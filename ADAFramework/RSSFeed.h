//
//  RSSFeed.h
//  ADAFramework
//
//  Created by Choldarong-r on 6/12/2559 BE.
//  Copyright © 2559 G-ABLE ITS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RSSFeed : NSObject

typedef NS_ENUM(NSInteger, RSSFeedType) {
//typedef enum RSSFeedType : NSUInteger {
    RSSFeedTypeWeb             = 1,
    RSSFeedTypeTwitter         = 5
};

@property (nonatomic, retain) NSString *ID;
@property (nonatomic, retain) NSString *url;
@property (nonatomic, retain) NSString *title;
@property (nonatomic, assign) RSSFeedType type;
@end
