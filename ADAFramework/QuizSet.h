//
//  QuizSet.h
//  ADAFramework
//
//  Created by Choldarong-r on 29/6/2560 BE.
//  Copyright © 2560 G-ABLE ITS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Quiz.h"
#import "ADAMember.h"


@interface QuizSet : NSObject

typedef NS_OPTIONS(NSInteger, QuizState) {
    QuizStateSubmited = 1 << 3,
    QuizStateNotAnswered = 1 << 2,
    QuizStateExpired = 1 << 1,
    QuizStateActive = 1 << 0
};

@property (nonatomic, assign) long setID;
@property (nonatomic, assign) BOOL active;
@property (nonatomic, retain) NSString* quizName;
@property (nonatomic, retain) NSString* quizDescription;
@property (nonatomic, retain) NSString* imageID;
@property (nonatomic, retain) NSArray<Quiz*> *quizList;
@property (nonatomic, retain) NSDate *startDate;
@property (nonatomic, retain) NSDate *endDate;
@property (nonatomic, assign) NSInteger answeredMembers;
@property (nonatomic, assign) BOOL answered;
@property (nonatomic, assign) BOOL expired;
@property (nonatomic, assign) BOOL poll;
@property (nonatomic, retain) ADAMember *creator;
@property (nonatomic, retain) NSDate *createDate;
@end
