//
//  StickerViewController.h
//  ADAFramework
//
//  Created by Choldarong-r on 17/12/2559 BE.
//  Copyright © 2559 G-ABLE ITS. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol StickerViewDelegate <NSObject>
- (void) didSelectSticker:(id) sender withCode:(NSString*) code;
@end


@interface StickerViewController : UIViewController

- (void) dismissViewController;
@property (nonatomic, retain) NSArray* stickerSet;
@property (nonatomic, weak) id<StickerViewDelegate> delegate;

@end
