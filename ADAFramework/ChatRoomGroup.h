//
//  ChatRoomGroup.h
//  ADAFramework
//
//  Created by Choldarong-r on 12/12/2559 BE.
//  Copyright © 2559 G-ABLE ITS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ChatRoom.h"

@interface ChatRoomGroup : NSObject

@property (nonatomic, assign) NSInteger seq;
@property (nonatomic, retain) NSString *groupTitle;
@property (nonatomic, retain) NSArray<ChatRoom*> *rooms;
@property (nonatomic, assign) ChatRoomType roomType;

@end
