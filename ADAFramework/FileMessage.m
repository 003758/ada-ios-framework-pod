//
//  FileMessage.m
//  ADAFramework
//
//  Created by Choldarong-r on 13/12/2559 BE.
//  Copyright © 2559 G-ABLE ITS. All rights reserved.
//

#import "FileMessage.h"
@import MobileCoreServices;

@implementation FileMessage

+ (FileMessage*) instanceWithURL:(NSURL*) url {
    NSLog(@"%@", url);
    NSString *exportPath = [[[NSString stringWithFormat:@"%@", url] stringByRemovingPercentEncoding]  stringByReplacingOccurrencesOfString:@"file://" withString:@""];
    
    exportPath = [exportPath stringByReplacingOccurrencesOfString:@" " withString:@"_"];
    
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    NSURL *tmpDirURL = [NSURL fileURLWithPath:NSTemporaryDirectory() isDirectory:YES];
    
    NSString *imgFolder = [NSString stringWithFormat:@"%@chat/files/document", tmpDirURL];
    imgFolder = [imgFolder stringByReplacingOccurrencesOfString:@"file://" withString:@""];
    if (![fileMgr fileExistsAtPath:imgFolder]) {
        if([fileMgr createDirectoryAtPath:imgFolder withIntermediateDirectories:YES attributes:nil error:nil]){
            //NSLog(@"Dir created");
        }
    }
    
    NSError *error;
    if ([[NSFileManager defaultManager] fileExistsAtPath:exportPath])
    {
        //removing file
        if (![[NSFileManager defaultManager] removeItemAtPath:exportPath error:&error])
        {
            NSLog(@"Could not remove old files. Error:%@",error);
        }
    }
    
    
    if (![[NSFileManager defaultManager] copyItemAtURL:url toURL:[NSURL fileURLWithPath:exportPath] error:&error]) {
        NSLog(@"%@", error);
    }
    
    
    
    
    NSString *filePath = [exportPath stringByReplacingOccurrencesOfString:@"file://" withString:@""];
    NSArray *fileNames = [filePath componentsSeparatedByString:@"/"];
    NSString *fileName = fileNames.lastObject;
    FileMessage *message = [FileMessage new];
    message.filePath = filePath;
    message.name = fileName;
    return message;

}

+ (NSArray<NSString *> *)allowDocumentTypes {
    return @[(NSString*)kUTTypePDF,
             (NSString*)kUTTypeRTFD,
             @"org.openxmlformats.wordprocessingml.document",
             //(NSString*)kUTTypeData,
             (NSString*)kUTTypePresentation,
             (NSString*)kUTTypeSpreadsheet,
             (NSString*)kUTTypePlainText,
             (NSString*)kUTTypeText];
    
}
@end
