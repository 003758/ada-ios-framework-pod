//
//  AppData+CoreDataClass.h
//  ADAFramework
//
//  Created by Choldarong-r on 24/10/2561 BE.
//  Copyright © 2561 G-ABLE Company Limited. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface AppData : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "AppData+CoreDataProperties.h"
