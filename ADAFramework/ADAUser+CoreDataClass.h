//
//  ADAUser+CoreDataClass.h
//  ADAFramework
//
//  Created by Choldarong-r on 21/12/2559 BE.
//  Copyright © 2559 G-ABLE ITS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class MessageLog;
@class CacheService;
NS_ASSUME_NONNULL_BEGIN

@interface ADAUser : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "ADAUser+CoreDataProperties.h"
