//
//  Personal.h
//  ADAFramework
//
//  Created by Choldarong-r on 19/6/2560 BE.
//  Copyright © 2560 G-ABLE ITS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Personal : NSObject

@property (nonatomic, retain) NSString      *backgroundImageID;
@property (nonatomic, retain) NSString *recentTabImageID;
@property (nonatomic, retain) NSString *peopleTabImageID;
@property (nonatomic, retain) NSString *serviceTabImageID;
@property (nonatomic, retain) NSString *inboxTabImageID;
@property (nonatomic, retain) NSString      *personalImageURL;
@property (nonatomic, retain) NSString      *personalInfo;
@property (nonatomic, retain) NSJSONSerialization  *personalData;
@property (nonatomic, retain) NSArray       *columns;
@end
